package net.sf.ljup.media.renderer.dummy;

import java.util.logging.Logger;

import org.cybergarage.upnp.device.InvalidDescriptionException;

import com.cidero.upnp.ConnectionManager;

/**
 * Prismiq ConnectionManager class
 * 
 * This class doesn't really do anything - all the actions use the default
 * handler methods in the ConnectionManger superclass. This is due to the fact
 * that all the supported actions are just get's of state variables - there
 * aren't any that change any state info
 */
public class DummyConnectionManager extends ConnectionManager {
	private static Logger logger = Logger
			.getLogger("net.sf.ljup.media.renderer.dummy.DummyConnectionManager");

	private DummyMediaRenderer mediaRenderer;

	/**
	 * Creates a new <code>ConnectionManager</code> instance.
	 * 
	 */
	public DummyConnectionManager(DummyMediaRenderer mediaRenderer)
			throws InvalidDescriptionException {
		super(mediaRenderer);

		logger.fine("Entered DummyConnectionManager constructor");

		this.mediaRenderer = mediaRenderer;

		logger.fine("Leaving DummyConnectionManager constructor");
	}

	/**
	 * Initialize state variables. Note that required state variables have been
	 * given 'reasonable' default values in the base class version of this
	 * routine
	 */
	public void initializeStateVariables() {
		super.initializeStateVariables();

		// Audiotron acts as a sink for audio data only
		setStateVariable("SourceProtocolInfo", "");
		setStateVariable(
				"SinkProtocolInfo",
				"http-get:*:audio/mpeg:*,http-get:*:audio/mpegurl:*,http-get:*:audio/x-mpegurl:*,http-get:*:audio/x-scpls:*,");
		// TODO: Add WMA mime type to above if PRISMIQ supports it (check)
	}

}
