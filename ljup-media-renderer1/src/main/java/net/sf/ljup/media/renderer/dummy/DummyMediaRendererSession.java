package net.sf.ljup.media.renderer.dummy;

import com.cidero.bridge.AbstractMediaRendererSession;
import com.cidero.bridge.MediaRendererException;
import com.cidero.renderer.IMediaRenderer;
import com.cidero.upnp.UPnPException;

public class DummyMediaRendererSession extends AbstractMediaRendererSession {

	protected DummyMediaRendererSession(IMediaRenderer rendererBridge,
			String resourceURL, String metaData, String userAgent,
			int syncWaitMillisec) throws UPnPException {
		super(rendererBridge, resourceURL, metaData, userAgent,
				syncWaitMillisec);
	}

	@Override
	protected void play(IMediaRenderer renderer) throws MediaRendererException {
		// TODO Auto-generated method stub

	}

}
