package net.sf.ljup.media.renderer.dummy;

import java.util.logging.Logger;

import org.cybergarage.upnp.device.InvalidDescriptionException;

import com.cidero.bridge.MediaRendererException;
import com.cidero.renderer.AbstractMediaRenderer;
import com.cidero.upnp.AVTransport;
import com.cidero.upnp.ConnectionManager;
import com.cidero.upnp.RenderingControl;
import com.cidero.util.NetUtil;

public class DummyMediaRenderer extends AbstractMediaRenderer {
	
	public static final int DEFAULT_TIMEOUT = 5000;

	private static final Logger logger = Logger
			.getLogger("net.sf.ljup.media.renderer.dummy.DummyMediaRenderer");

	private final static String DESCRIPTION_FILE_NAME = "net/sf/ljup/media/renderer/dummy/DummyMediaRenderer.xml";

	private final static String FRIENDLY_NAME = "DummyMediaRenderer";

	private static int instanceCount = 0;

	// Dummy versions of UPnP services
	private DummyRenderingControl renderingControl;
	private DummyAVTransport avTransport;
	private DummyConnectionManager connectionManager;

	private DummyStateModel stateModel;

	private int unitId;

	public DummyMediaRenderer() throws InvalidDescriptionException {
		super(DESCRIPTION_FILE_NAME, FRIENDLY_NAME);
		// getProperties();

		//
		// Override UDN from description.xml to make it unique for friendlyName,
		// hostName combinations. Cybergarage API requires a call to setUUID,
		// followed by a call to updateUDN, to reset the UDN.
		//
		setUUID("ljup-" + getFriendlyName() + "-" + NetUtil.getLocalHostName());
		updateUDN(); // This sets the UDN to 'uuid:<UUID>'

		//
		// Status model is observed by the AVTransport and RenderingControl
		// service instances
		//
		stateModel = new DummyStateModel();

		// Setup Prismiq-specific versions of UPnP services
		// Note: these constructors throw a InvalidDescriptionException if
		// the device description doesn't have a matching service
		renderingControl = new DummyRenderingControl(this);
		avTransport = new DummyAVTransport(this);
		connectionManager = new DummyConnectionManager(this);

		unitId = instanceCount++;
	}

	@Override
	public void avTransportPause() throws MediaRendererException {
		logger.info("avTransportPause");
	}

	@Override
	public void avTransportPlay(String speed) throws MediaRendererException {
		logger.info("avTransportPlay");
	}

	@Override
	public void avTransportSetTransportURI(String uri)
			throws MediaRendererException {
		logger.info("avTransportSetTransportURI");
	}

	@Override
	public void avTransportStop() throws MediaRendererException {
		logger.info("avTransportStop");
	}

	@Override
	public AVTransport getAVTransport() {
		logger.info("getAVTransport");
		return avTransport;
	}

	@Override
	public ConnectionManager getConnectionManager() {
		logger.info("getConnectionManager");
		return connectionManager;
	}

	@Override
	public RenderingControl getRenderingControl() {
		logger.info("getRenderingControl");
		return renderingControl;
	}
	
	public DummyStateModel getStateModel() {
		return stateModel;
	}

	String send(String cmd, int timeoutMillisec) {
		logger.info("send '" + cmd + "', " + timeoutMillisec);
		return "";
	}

	String send(String cmd) {
		logger.info("send '" + cmd + "'");
		return "";
	}

}
