package net.sf.ljup.media.renderer;

import net.sf.ljup.media.renderer.dummy.DummyMediaRenderer;

import com.cidero.bridge.prismiq.PrismiqMediaRenderer;
import com.cidero.renderer.IMediaRenderer;

public class MediaRendererTest {

	private static final String INET_ADDR_DEVICE = "192.168.0.2";

	private static final String NAME = "TEST_MEDIA_RENDERER";

	public static void main(String[] args) throws Exception {
		/*
		final IMediaRenderer dut = new PrismiqMediaRenderer(INET_ADDR_DEVICE,
				NAME);
				*/
		final IMediaRenderer dut = new DummyMediaRenderer();
		dut.start();

		//
		// Make sure stop routine is invoked for all devices at shutdown
		// It is beneficial to send out UPnP bye-bye messages to all control
		// points if possible
		//
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				System.out.println("Java runtime shutting down");
				dut.stop();
			}
		});

	}

}
