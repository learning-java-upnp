package net.sf.ljup.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;


public class Exec {

  private static final String DEFAULT_CS = "UTF-8";

  public static final class Result {

    private final int exitValue;

    private final String out;

    private final String err;

    private Result(int exitValue, String out, String err) {
      this.exitValue = exitValue;
      this.out = out;
      this.err = err;
    }

    public int getExitValue() {
      return exitValue;
    }

    public String getOut() {
      return out;
    }

    public String getErr() {
      return err;
    }

    public String toString() {
      final StringBuffer result = new StringBuffer();
      result.append("ExecResult<");
      result.append(exitValue);
      result.append(" out=\n'");
      result.append(out);
      result.append("'\nerr=\n'");
      result.append(err);
      result.append("'\n>");
      return result.toString();
    }

  }

  private static final class ReaderThread extends Thread {

    private final InputStream in;

    private final String charset;

    private String result;

    private Throwable throwable;

    private ReaderThread(InputStream in, String charset) {
      this.in = in;
      this.charset = charset;
    }

    public void run() {
      try {
        result = inputStream2String(in, charset);
      }
      catch (IOException e) {
        throwable = e;
      }
    }

    public String getResultAsString()
      throws IOException
    {
      if (throwable != null) {
        throw (IOException) throwable;
      }
      return result;
    }

    private String inputStream2String(InputStream in, String charset) throws IOException {
      final Reader reader = new InputStreamReader(in, charset);
      return reader2String(reader);
    }

    private String reader2String(Reader reader) throws IOException {
      final StringWriter writer = new StringWriter();
      copy(reader, writer);
      return writer.toString();
    }

    private static void copy(Reader in, Writer out) throws IOException {
      if (!in.markSupported()) {
        in = new BufferedReader(in);
      }
      try {
        final char[] buffer = new char[8 * 1024];
        int len;
        do {
          len = in.read(buffer);
          if (len > 0) {
            out.write(buffer, 0, len);
          }
          else if (len == 0) {
            Thread.yield();
          }
        }
        while (len >= 0);
      }
      finally {
        try {
          in.close();
        }
        finally {
          out.close();
        }
      }
    }

  }

  private static final class WriterThread extends Thread {

    private final OutputStream out;

    private final String toWrite;

    private final String charset;

    private Throwable throwable;

    private WriterThread(OutputStream out, String toWrite, String charset) {
      this.out = out;
      this.toWrite = toWrite;
      this.charset = charset;
    }

    public void run() {
      try {
        final Writer writer = new OutputStreamWriter(out, charset);
        writer.write(toWrite);
      }
      catch (IOException e) {
        throwable = e;
      }
      finally {
        try {
          out.close();
        }
        catch (IOException e) {
          throwable = e;
        }
      }
    }

    public Throwable getThrowable()
    {
      return throwable;
    }

  }

  private final String[] cmdarray;

  private final String[] envp;

  private final File dir;

  public Exec(String[] cmdarray, String[] envp, File dir) {
    this.cmdarray = cmdarray;
    this.envp = envp;
    this.dir = dir;
  }

  public Exec(String[] cmdarray, String[] envp) {
    this(cmdarray, envp, new File(System.getProperty("user.dir")));
  }

  public Result exec(String input)
    throws IOException, InterruptedException
  {
    return exec(input, DEFAULT_CS);
  }

  public Result exec(String input, String charset)
    throws IOException, InterruptedException
  {
    final Process process = Runtime.getRuntime().exec(cmdarray, envp, dir);
    final WriterThread in = new WriterThread(process.getOutputStream(), input, charset);
    final ReaderThread out = new ReaderThread(process.getInputStream(), charset);
    final ReaderThread err = new ReaderThread(process.getErrorStream(), charset);
    in.start();
    out.start();
    err.start();
    final Result result = new Result(process.waitFor(),
        out.getResultAsString(),
        err.getResultAsString());
    return result;
  }

  public Result exec()
    throws IOException, InterruptedException
  {
    return execWithCs(DEFAULT_CS);
  }

  public Result execWithCs(String charset)
    throws IOException, InterruptedException
  {
    final Process process = Runtime.getRuntime().exec(cmdarray, envp, dir);
    final ReaderThread out = new ReaderThread(process.getInputStream(), charset);
    final ReaderThread err = new ReaderThread(process.getErrorStream(), charset);
    out.start();
    err.start();
    final Result result = new Result(process.waitFor(),
        out.getResultAsString(),
        err.getResultAsString());
    return result;
  }

}
