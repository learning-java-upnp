package com.ha.common.windows;

/**
 * This class is used to detect is standby(sleep) was requested on the PC it is running on
 * and to disallow it optionally
 */

public class StandByDetector {
    static {
        System.loadLibrary("StandByDetector");
    }
    private StandByRequestListener listener;
    
    public StandByDetector(StandByRequestListener listener) {
        this.listener = listener;
        init();
    }
    
    public void fireStandByRequested() {
        listener.standByRequested();
    }
    
    private native boolean init();
  /*
     allowStandBy==false
     means that no standby is allowed while this app is running
   */
    public native void setAllowStandby(boolean allowStandby);
    public native void destroy();
    
    //for testing...
    public static void main(String args[]) {
        StandByDetector sd=new StandByDetector(new StandByRequestListener() {
            public void standByRequested() {
                System.out.println("standby requested");
            }
        });
        sd.setAllowStandby(false);
        javax.swing.JFrame f=new javax.swing.JFrame();
        f.getContentPane().add(new javax.swing.JLabel("close to end test"));
        f.setSize(300,100);
        f.setVisible(true);
        f.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
    }
}