package com.ha.common.windows;
/**
 * called when a Stand By Request is made on the PC
 */
public interface StandByRequestListener {
    public void standByRequested();
}