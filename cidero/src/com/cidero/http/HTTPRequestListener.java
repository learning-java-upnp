/******************************************************************
*
*	CyberHTTP for Java
*
*	Copyright (C) Satoshi Konno 2002
*
*	File: HTTPRequestListener.java
*
*	Revision;
*
*	12/13/02
*		- first revision.
*	
******************************************************************/

package com.cidero.http;

public interface HTTPRequestListener
{
	public boolean httpRequestReceived( HTTPRequest httpReq );
}
