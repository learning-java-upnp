/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: SyncProxy.java,v $
 *
 */
package com.cidero.proxy;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Logger;

import com.cidero.http.*;
import com.cidero.util.AppPreferences;
import com.cidero.util.MrUtil;

import com.cidero.upnp.UPnPException;

/**
 * HTTP synchronous server proxy top-level application class. Gathers the
 * application preferences and invokes lower-level HTTPProxyServer class
 * to do the actual work.
 */
public class SyncProxy
{
  private static Logger logger = Logger.getLogger("com.cidero.proxy");

  static AppPreferences pref;

  int proxyPort;
  int syncWaitMillisec;

  public SyncProxy()
  {
    loadPreferences();

    logger.fine("HTTPProxyServer: Opening server on port: " + proxyPort +
                " syncWaitMillisec: " + syncWaitMillisec );

    try 
    {
      // Start set of server threads (one per network interface)
      HTTPProxyServer proxyServer = 
        new HTTPProxyServer( proxyPort, syncWaitMillisec );

      proxyServer.start();
    }
    catch( IOException e )
    {
      logger.warning("Failure starting synchronous proxy server: " + e);
    }
  }
  
  public void loadPreferences()
  {
    pref = new AppPreferences(".cidero");
    pref.load("SyncProxy", "SyncProxy" );   // appName, className

    // Default server port is 18081
    proxyPort = pref.getInt( "proxyPort", 18081 );

    // Default syncWaitMillisec is 2 sec
    syncWaitMillisec = pref.getInt( "syncWaitMillisec", 2000 );
  }
  

  public static void main( String args[] )
  {
    SyncProxy  syncProxy;

    try
    {
      syncProxy = new SyncProxy();
    }
    catch( Exception e )
    {
      logger.severe("General exception " + e );
      e.printStackTrace();
      System.exit(-1);
    }
  }
}
