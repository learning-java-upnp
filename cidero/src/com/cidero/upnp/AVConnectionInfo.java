/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: AVConnectionInfo.java,v $
 *
 */
package com.cidero.upnp;

/**
 *  Class for use with AVTransport service.  Container for information 
 *  returned by the AVTransport GetCurrentConnectionInfo action
 *
 *  TODO: implementation not yet complete! (only needed IDs)
 */
public class AVConnectionInfo
{
  int    connectionID;
  int    avTransportID;   
  int    renderingControlID;    // UPnP terms this 'RcsID'
  String protocolInfo;          // MIME-type
  String peerConnectionManager;
  int    peerConnectionID;
  String direction;             // 'Input' or 'Output'
  String status;                // 'OK'  (others?)
   
  public AVConnectionInfo( int connectionID, int avTransportID,
                           int renderingControlID )
  {
    this.connectionID = connectionID;
    this.avTransportID = avTransportID;
    this.renderingControlID = renderingControlID;
  }

  public void setConnectionID( int id ) {
    connectionID = id;
  }
  public int getConnectionID() {
    return connectionID;
  }

  public void setAVTransportID( int id ) {
    avTransportID = id;
  }
  public int getAVTransportID() {
    return avTransportID;
  }

  public void setRenderingControlID( int id ) {
    renderingControlID = id;
  }
  public int getRenderingControlID() {
    return renderingControlID;
  }

  public void setProtocolInfo( String protocolInfo ) {
    this.protocolInfo = protocolInfo;
  }
  public String getProtocolInfo() {
    return protocolInfo;
  }
  
}
