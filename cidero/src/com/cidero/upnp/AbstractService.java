/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: AbstractService.java,v $
 *
 */
package com.cidero.upnp;

import com.sun.org.apache.xerces.internal.parsers.*;

import java.lang.reflect.*;
import java.util.logging.Logger;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import org.cybergarage.upnp.*;
import org.cybergarage.upnp.control.ActionListener;
import org.cybergarage.upnp.control.QueryListener;
import org.cybergarage.upnp.device.InvalidDescriptionException;
import org.cybergarage.upnp.event.EventListener;

import com.cidero.util.XMLUtil;

/**
 *  Base class for service-specific 'helper' classes that extend the 
 *  functionality of the CyberLink package's Service class. 
 *
 *  From the device perspective, this class provides an automatic dispatcher
 *  for incoming UPnP actions to methods with the same name as the
 *  action.  So if, for example, your 'MyDeviceAVTransport' service 
 *  defines the method 'actionPlay', the method will be automatically
 *  invoked as long as MyDeviceAVTransport is derived from this class.
 *
 *  From the control point perspective, this class provides an automatic
 *  dispatcher for incoming events (notification of changes to a device's
 *  state variables). The local copy of the state variable is automatically
 *  updated, and if there is a method of the name 'event<StateVarName>' in
 *  the derived class, it is invoked (in that order)
 *
 */
public abstract class AbstractService
                implements ActionListener,
                           EventListener,
                           QueryListener,
                           ContentHandler, IService
{
  private static Logger logger = Logger.getLogger("com.cidero.upnp");

  /**
   * Underlying Cybergarage UPnP Device,Service objects
   */
  IDevice  device = null;
  Service service = null;

  private long seq;   // event seq number

  /**
   * Some SOAP messages have non-escaped '&' characters, which fault
   * out the Apache parser. If detected in past SOAP packets (flag true),
   * fix'em up prior to first XML parse
   */
  boolean escapeNonEntityAmpersands = false;   // Assume XML ok
  
  /**
   * Constructor. Setup all the action/query listeners for this service,
   * using the CLink device service with the same name as this service
   *
   * @param      CLink device object
   *
   * @exception  Throws InvalidDescriptionException if device has no 
   *             matching service
   */
  public AbstractService( IDevice device )
    throws InvalidDescriptionException
  {
    // Get underlying Cybergarage service object for this service and 
    // install actionControlReceived() 'dispatcher' method below for 
    // all actions

    this.device = device;
    
    ServiceList serviceList = device.getServiceList();
    if (serviceList == null)
    {
      logger.severe("Null service list in service constructor");
      throw new InvalidDescriptionException("Null device service list");
    }
    
    logger.fine("This service type = " + getServiceType());

    int n;
    for (n = 0; n < serviceList.size(); n++)
    {
      Service service = serviceList.getService(n);
      logger.fine("ServiceType = " + service.getServiceType() );
      logger.fine("ServiceId = " + service.getServiceID());

      if( service.isService(getServiceType()) )
      {
        logger.fine("Matching service found ");

        this.service = service;
          
        service.setQueryListener( this );
        service.addEventListener( this );

        ActionList actionList = service.getActionList();
        
        for (int j = 0; j < actionList.size(); j++)
        {
          Action action = actionList.getAction( j );
          action.setActionListener( this );
        }
        break;
      }
      else
      {
        logger.fine("Not a matching service ");
      }
    }

    if (n == serviceList.size())
    {
      throw new InvalidDescriptionException("Device doesn't support service '" + getServiceType() );
    }

    // If this service is being instantiated on the *device* side (as
    // opposed to the control point side), initialize state variables
    if( device.getInstancePerspective() == UPnP.DEVICE )
      initializeStateVariables();
    
  }

  public AbstractService()
  {
  }
  

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#getServiceType()
 */
  abstract public String getServiceType();

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#initializeStateVariables()
 */
abstract public void initializeStateVariables();

  /*
  public void setService( Service service ) 
  {
    this.service = service;
  }
  */
  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#getDevice()
 */
public IDevice getDevice()
  {
    return device;
  }
  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#getService()
 */
public Service getService()
  {
    return service;
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#getAction(java.lang.String)
 */
public Action getAction( String actionName )
  {
    if( service == null )
      return null;
    
    return service.getAction( actionName );
  }
  

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#setStateVariable(java.lang.String, java.lang.String)
 */
public void setStateVariable( String varName, String value )
  {
    StateVariable stateVar = service.getStateVariable( varName );
    if( stateVar == null )
    {
      logger.warning("state variable '" + varName + "' not found" );
      return;
    }
    stateVar.setValue( value );
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#getStateVariable(java.lang.String)
 */
public StateVariable getStateVariable( String varName )
  {
    return service.getStateVariable( varName );
  }
  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#getStateVariableValue(java.lang.String)
 */
public String getStateVariableValue( String varName )
  {
    return service.getStateVariable( varName ).getValue();
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#actionControlReceived(org.cybergarage.upnp.Action)
 */
  public boolean actionControlReceived(Action action)
  {
    logger.finer("actionControlReceived: " +action.getName());

    //
    // Convention is for this service to have methods with same name
    // as UPNP action. Look up method by name and invoke it
    //
    Class c = this.getClass();
    Class[] parameterTypes = new Class[] {Action.class};
    Method actionMethod;
    Object[] arguments = new Object[] { action };

    try
    {
      //
      // Action methods are named using the string "action" followed
      // by the UPNP Action name, e.g. "actionPlay". This is to
      // clearly differentiate which methods in derived classes are 
      // action callbacks
      //
      actionMethod = c.getMethod( "action" + action.getName(),
                                  parameterTypes );

      Boolean result = (Boolean) actionMethod.invoke( this, arguments );

      return result.booleanValue();
    }
    catch( NoSuchMethodException e )
    {
      logger.warning("Unsupported action! [" + action.getName() + "]" + e );
    }
    catch( IllegalAccessException e )
    {
      logger.warning("Access denied for method" + e );
    }
    catch( InvocationTargetException e )
    {
      logger.warning("Invocation exception" );
      logger.warning( "Cause: " + e.getCause().toString() );
      e.getCause().printStackTrace();
    }

    return false;

  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#queryControlReceived(org.cybergarage.upnp.StateVariable)
 */
  public boolean queryControlReceived( StateVariable stateVar ) 
  {
    String varName = stateVar.getName();
    //logger.fine("queryControlReceived, var = " + varName );

    StateVariable serviceStateVar = service.getStateVariable( varName );
    if( serviceStateVar == null )
      return false;

    stateVar.setValue( serviceStateVar.getValue() );

    return true;
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#eventNotifyReceived(java.lang.String, long, java.lang.String, java.lang.String)
 */
  public void eventNotifyReceived( String uuid, long seq, String name,
                                   String value )
  {
    /*
    logger.info("----------- AbstractService Event Received----------------");
    logger.info("event notify : uuid = " + uuid + ", seq = " + 
                seq + ", name = " + name + ", value =" + value); 
    */

    this.seq = seq;
    
    StateVariable serviceStateVar = service.getStateVariable( name );
    if( serviceStateVar == null )
    {
      IDevice dev = service.getRootDevice();
      
      logger.warning("Event with name '" + name + 
                     "' has no associated state variable in device service's XML description \n" +
                     "Device: " + dev.getFriendlyName() +
                     " ServiceType: " + getServiceType() );
      
      StringBuffer buf = new StringBuffer();
      ServiceStateTable stateTable = service.getServiceStateTable();
      for( int n = 0 ; n < stateTable.size() ; n++ )
      {
        StateVariable knownVar = stateTable.getStateVariable(n);
        buf.append( knownVar.getName() + " " );
      }
      logger.warning("Known variables:\n" + buf.toString() );
      
      // Patch by manually adding the state variable to the service here
      // so the next event for the variable won't generate a warning.
      // This is a hack - the *real* fix is for the device to add the 
      // variable to its service description XML
      logger.warning("Patch - Manually adding state variable: " + 
                     name );
      service.addStateVariable( name, "string" );
      return;
    }
    // Set the local instance of the corresponding state variable. Since
    // we're on the control point side use 'setValueNoEvent' since we're
    // an event *receiver*, not an event sender
    serviceStateVar.setValueNoEvent( value );
    
    //
    // Convention is for this service to have methods with same name
    // as UPNP event. Look up method by name and invoke it
    //
    Class c = this.getClass();
    Class[] parameterTypes = new Class[] {String.class};
    Method eventMethod;
    Object[] arguments = new Object[] { value };

    try
    {
      eventMethod = c.getMethod( "event" + name,
                                  parameterTypes );

      eventMethod.invoke( this, arguments );
    }
    catch( NoSuchMethodException e )
    {
      logger.finest( "No event method for stateVar '" + name + "'" );
    }
    catch( IllegalAccessException e )
    {
      logger.warning( "event processing exception: " + e );
    }
    catch( InvocationTargetException e )
    {
      logger.warning( "event processing exception: " + e);
      logger.warning( "Cause: " + e.getCause().toString() );
      e.getCause().printStackTrace();
    }

    return;
  }

  /** 
   *
   *  For the AVTransport and RendingControl services, a two-tiered 
   *  event scheme using the 'LastChange' state variable exists that
   *  requires an extra level of XML parsing/dispatching (yech - this
   *  was unnecessary, IMHO).   
   *  
   *  This dispatcher code deals with it, and makes it appear to the
   *  application that all the state variables passed via the LastChange
   *  mechanism are 'normal' evented state variables. 
   *
   *  Sample of LastChange value XML syntax:
   *
   *  <Event>
   *    <InstanceID val="0">
   *      <Volume val="76" />
   *      <Mute val="0" />
   *    </InstanceID>
   *  </Event>
   *
   *  Special cases:
   *
   *  RenderingControl has a single 'Volume' state variable, but the 
   *  the SetVolume/GetVolume actions have a channel argument ('Master', 
   *  "LF", "RF", "LR", "RR"....)  This results in the following 'LastChange'
   *  syntax:
   *
   *  <Event>
   *    <InstanceID val="0">
   *      <Volume val="76" channel="LF" />
   *    </InstanceID>
   *  </Event>
   * 
   *  This case breaks the one state variable per state item rule, it 
   *  seems. In other words, the service has only a single 'Volume' 
   *  state variable, but there are really a number of volume states,
   *  one for each channel.  This makes it hard to use the service 
   *  state variable as the underlying data model for device state, 
   *  which would be the cleanest way to implement things...
   */ 

  XMLReader parser = null;

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#eventLastChange(java.lang.String)
 */
public void eventLastChange( String value )
  {
    logger.fine("--------- eventLastChange --------------");
    logger.fine("parsing XML with value = " + value); 

    if( parser == null )
    {
      parser = new SAXParser();
      parser.setContentHandler( this );
    }
    
    // If there were XML errors due to unescaped Entity refs in the past
    // apply patch up front to avoid parse/patch/reparse overhead. This
    // also prevents repeated dire-sounding error messages.
    if( escapeNonEntityAmpersands )
      value = XMLUtil.escapeNonEntityAmpersands( value );

    byte[] xmlByteArray = value.getBytes();
    int length = xmlByteArray.length;

    // trim off any trailing bytes after closing '>'  (some UPnP 
    // implementations leave some extraneous non-printable characters at
    // end of XML packet, and SAX parser throws exception)
    for( int n = xmlByteArray.length-1 ; n > 0 ; n-- )
    {
      if( xmlByteArray[n] == '>' )
        break;
      length--;
    }

    ByteArrayInputStream xmlStream =
      new ByteArrayInputStream( xmlByteArray, 0, length );
    
    try
    {
      InputSource inputSource = new InputSource( xmlStream );
      inputSource.setEncoding( "UTF-8" );
      parser.parse( inputSource );
    }
    catch( IOException e )
    {
      logger.warning("Error parsing XML" + e );
    }
    catch( SAXException e )
    {
      if( escapeNonEntityAmpersands )  
      {
        // Already tried patch and it didn't work
        logger.warning("Error parsing XML" + e );
        return;
      }
      
      // Fix for some renderers that leave '&' unescaped
      logger.warning("Error parsing XML - trying to patch XML: " + e );
      value = XMLUtil.escapeNonEntityAmpersands( value );
      xmlByteArray = value.getBytes();
      length = xmlByteArray.length;
      for( int n = xmlByteArray.length-1 ; n > 0 ; n-- )
      {
        if( xmlByteArray[n] == '>' )
          break;
        length--;
      }
      xmlStream = new ByteArrayInputStream( xmlByteArray, 0, length );

      try
      {
        InputSource inputSource = new InputSource( xmlStream );
        inputSource.setEncoding( "UTF-8" );
        parser.parse( inputSource );

        // Fix worked - latch success so next time we can be smart and 
        // apply patch prior to first XML parse
        escapeNonEntityAmpersands = true;  
        logger.warning("XML entity reference patch suceeded - will automatically apply to all future 'lastChange' event packets");
      }
      catch( IOException e2 )
      {
        logger.warning("Error parsing XML" + e2 );
        return;
      }
      catch( SAXException e2 )
      {
        logger.warning("Unfixable error parsing XML: " + e2 );
        return;
      }
    }

    //
    //  Invoke routine at end of LastChange data. Higher level apps should
    //  override this if they want to fire off a model-changed notify event
    // 
    eventLastChangeEnd();

  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#eventLastChangeEnd()
 */
  public void eventLastChangeEnd()
  {
    ;
  }


  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#setDocumentLocator(org.xml.sax.Locator)
 */

  public void setDocumentLocator( Locator locator )
  {
    //logger.finest("setDocumentLocator:" + "\n" );
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#startDocument()
 */
public void startDocument() throws SAXException
  {
    //logger.finest("startDocument:" + "\n" );
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#endDocument()
 */
public void endDocument() throws SAXException
  {
    //logger.finest("endDocument:" + "\n" );
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#processingInstruction(java.lang.String, java.lang.String)
 */
public void processingInstruction( String target, String data )
    throws SAXException
  {
    //logger.finest("processingInstruction: target: " + target +
    //                       " data: " + data );
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#startPrefixMapping(java.lang.String, java.lang.String)
 */
public void startPrefixMapping( String prefix, String uri )
    throws SAXException
  {
    //logger.finest("startPrefixMapping: prefix: " + prefix +
    //                  " uri: " + uri );
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#endPrefixMapping(java.lang.String)
 */
public void endPrefixMapping( String prefix )
    throws SAXException
  {
    //logger.finest("endPrefixMapping: prefix: " + prefix );
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
 */
public void startElement( String namespaceURI, String localName,
                            String rawName, Attributes atts )
    throws SAXException
  {
    //    logger.finest("startElement: " +
    //                " localName: " + localName +
    //                " rawName: " + rawName );

    String value = null;
    
    for( int n = 0 ; n < atts.getLength() ; n++ )
    {
      //      logger.fine("  Attr: " + atts.getLocalName(n) +
      //                         " = " + atts.getValue(n) );
      if( atts.getLocalName(n).equals("val") )
        value = atts.getValue(n);
    }

    // skip root 'Event' &  'InstanceID' node
    if( localName.equals("Event") || localName.equals("InstanceID") )
      return;

    // Some implementations don't specify 'val' attribute if it's empty
    // Use null string in this case
    if( value == null )
    {
      //      logger.info("No 'val' attribute for elem " + rawName +
      //                  " using empty string");
      value = "";
    }
    
    eventNotifyReceived( "LastChangeUUID", seq, localName, value );
    
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#endElement(java.lang.String, java.lang.String, java.lang.String)
 */
public void endElement( String namespaceURI, String localName,
                          String rawName )
    throws SAXException
  {
    //    System.out.println("endElement: " +
    //                       " localName: " + localName +
    //                       " rawName: " + rawName  );
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#characters(char[], int, int)
 */
public void characters( char[] ch, int start, int end )
    throws SAXException
  {
    //    String s = new String( ch, start, end );
    //    System.out.println( "characters: " + s );
  }

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#ignorableWhitespace(char[], int, int)
 */
public void ignorableWhitespace( char[] ch, int start, int end )
    throws SAXException
  {
    //    String s = new String( ch, start, end );
    //    System.out.println( "whitespace: [" + s + "]" );
  }
  
  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#skippedEntity(java.lang.String)
 */
public void skippedEntity( String name )
    throws SAXException
  {
    //    System.out.println("skippedEntity: name: " + name );
  }
  

  /* (non-Javadoc)
 * @see com.cidero.upnp.IService#errorToString(int)
 */
abstract public String errorToString(int code);


}
