/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: CDSPlaylistItem.java,v $
 *
 */

package com.cidero.upnp;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.cybergarage.xml.XML;

/**
 *
 *  Derived class of CDSItem used to represent a playable set of resources
 *  The <res> element of the object is a reference to a playlist file 
 *  authored outside the ContentDirectory (e.g. an M3U playlist file URL)
 *  
 *
 *  Notes:
 *
 *  This should not be used in new applications - use CDSPlaylistContainer
 *  instead. It is included here to support browsing of older, non-NMPR 
 *  CDS implementations
 *
 */

public class CDSPlaylistItem extends CDSItem
{
  static String upnpClass = "object.item.playlistItem";

  // UPnP namespace fields
  String  artist;                          // Not required
  String  genre;                           // Not required
  String  longDescription;                 // Not required
  String  storageMedium;                   // Not required

  // Dublin Core namespace fields
  String  description;                     // Not required
  String  date;                            // Not required 
  String  language;                        // Not required

  /**
   *
   */
  public CDSPlaylistItem()
  {
  }

  /**
   * This contructor is useful when creating playlist items from M3U files
   * with EXTINF metadata 
   */
  public CDSPlaylistItem( String artist, String title,
                          String durationSecs, String resource )
  {
    setId( "-1" );
    setParentId( "-1" );

    this.artist = artist;

    // creator is required property, so make it something reasonable
    if( artist != null )
      setCreator( artist );
    else
      setCreator( "" );

    this.title = title;
    
    CDSResource cdsResource = new CDSResource();
    cdsResource.setName( resource );
    cdsResource.setProtocolInfo( "http-get:*:audio/mpeg:*" );
    cdsResource.setDurationSecs( Integer.parseInt(durationSecs) );
    //trackResource.setSize( 5000000 );  // Set to default of 5 Mb

    addResource( cdsResource );
  }

  public CDSPlaylistItem( Node node )
  {
    super( node );

    NodeList children = node.getChildNodes();

    for( int n = 0 ; n < children.getLength() ; n++ )
    {
      String nodeName = children.item(n).getNodeName();
      //      System.out.println( "Node is: " + nodeName );
      
      if (nodeName.equals("upnp:artist"))
        artist = CDS.getSingleTextNodeValue(children.item(n));
      else if( nodeName.equals("upnp:genre") )
        genre = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("upnp:longDescription") )
        longDescription = CDS.getSingleTextNodeValue( children.item(n) );
      else if (nodeName.equals("upnp:storageMedium"))
        storageMedium = CDS.getSingleTextNodeValue(children.item(n));

      else if (nodeName.equals("dc:date")) 
        date = CDS.getSingleTextNodeValue(children.item(n));
      else if( nodeName.equals("dc:description") )
        description = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("dc:language") )
        language = CDS.getSingleTextNodeValue( children.item(n) );
    }
  }

  /**
   * Convert this playlist item object to a playlist container object
   *
   * @return 
   */
  public CDSPlaylistContainer convertToPlaylistContainer()
  {
    CDSPlaylistContainer container = new CDSPlaylistContainer();
    
    // Copy fields from CDSObject base class
    container.setId( getId() );
    container.setParentId( getParentId() );
    container.setTitle( getTitle() );
    container.setCreator( getCreator() );

    container.setRestricted( getRestricted() );
    container.setWriteStatus( getWriteStatus() );

    // Private (non-UPnP) library field
    container.setNumericalId( getNumericalId() );
    
    int resourceCount = getResourceCount();
    for( int n = 0 ; n < resourceCount ; n++ )
      container.addResource( getResource(n) );

    container.setArtist( artist );
    container.setGenre( genre );
    container.setStorageMedium( storageMedium );
    container.setLongDescription( longDescription );
    container.setDescription( description );
    container.setDate( date );
    container.setLanguage( language );

    return container;
  }

  public void setArtist(String artist) { this.artist = artist; }
  public String getArtist() { return artist; }

  public void setGenre( String genre ) { this.genre = genre; }
  public String getGenre() { return genre; }

  public void setStorageMedium(String storageMedium) {
    this.storageMedium = storageMedium;
  }
  public String getStorageMedium() { return storageMedium; }

  public void setLongDescription( String description ) {
    this.longDescription = description;
  }
  public String getLongDescription() { return longDescription; }

  public void setDescription( String description ) {
    this.description = description;
  }
  public String getDescription() { return description; }

  public void setDate(String date) { this.date = date; }
  public String getDate() { return date; }

  public void setLanguage( String language ) { this.language = language; }
  public String getLanguage() { return language; }


  /**
   *  Get object class
   *
   *  @return  UPNP class string
   */
  public String getUPNPClass() { return upnpClass; }

  public String attributesToXML( CDSFilter filter )
  {
    // No extra attributes for this class (just elements)
    return super.attributesToXML( filter );  
  }

  public String elementsToXML( CDSFilter filter )
  {
    // Need to build element string starting with elements in base classes
    StringBuffer buf = new StringBuffer();
    
    buf.append( super.elementsToXML( filter ) );

    if( (artist != null) && filter.propertyEnabled("upnp:artist") )
    {
      buf.append("  <upnp:artist>");
      buf.append( XML.escapeXMLChars(artist) );
      buf.append("</upnp:artist>\n");
    }
    if( (genre != null) && filter.propertyEnabled( "upnp:genre" ) )
    {
      buf.append("  <upnp:genre>");
      buf.append( XML.escapeXMLChars(genre)  );
      buf.append("</upnp:genre>\n");
    }
    
    if( (longDescription != null) &&
        filter.propertyEnabled( "upnp:longDescription" ) )
    {
      buf.append( "  <upnp:longDescription>");
      buf.append( XML.escapeXMLChars(longDescription) );
      buf.append("</upnp:longDescription>\n");
    }
    if( (storageMedium != null) &&
        filter.propertyEnabled( "upnp:storageMedium" ) )
    {
      buf.append( "  <upnp:storageMedium>" + storageMedium +
                  "</upnp:storageMedium>\n" );
    }

    if( (description != null) && filter.propertyEnabled( "dc:description" ) )
      buf.append( "  <dc:description>" + description + "</dc:description>\n" );
    if( (date != null) && filter.propertyEnabled("dc:date") )
      buf.append("  <dc:date>" + date + "</dc:date>\n");
    if( (language != null) && filter.propertyEnabled( "dc:language" ) )
      buf.append( "  <dc:language>" + language + "</dc:language>\n" );

    return buf.toString();
  }
}
