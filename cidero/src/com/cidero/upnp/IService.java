package com.cidero.upnp;

import org.cybergarage.upnp.Action;
import org.cybergarage.upnp.IDevice;
import org.cybergarage.upnp.Service;
import org.cybergarage.upnp.StateVariable;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public interface IService {

	/**
	 * Get name of this service from derived class. Example:
	 *     "urn:schemas-upnp-org:service:AVTransport:1";
	 */
	String getServiceType();

	void initializeStateVariables();

	/*
	  public void setService( Service service ) 
	  {
	    this.service = service;
	  }
	 */
	IDevice getDevice();

	Service getService();

	Action getAction(String actionName);

	void setStateVariable(String varName, String value);

	StateVariable getStateVariable(String varName);

	String getStateVariableValue(String varName);

	/**
	 * Process an incoming action and invoke a class method of the derived
	 * class with the same name as the action.  For example, for the 
	 * MediaRenderer action 'Play', a method called 'actionPlay(action)'
	 * would be searched for and invoked if it existed.
	 *
	 * @param action  UPnP action object
	 *
	 * @return  true if method existed and completed successfully, otherwise
	 *          false
	 */
	boolean actionControlReceived(Action action);

	/**
	 * Process an incoming state variable query. Cybergarage CLink API
	 * assumes application may be using it's own data model, so CLink
	 * doesn't automatically return the current value of the state
	 * variable as it exists in the CLink Service object. Instead,
	 * an empty version of the state variable (value set to empty
	 * string) is passed to this routine. If the variable is a 
	 * known state variable, the value should be set and the 
	 * routine should return 'true'. At that point, CLink takes
	 * over and automatically sends the Query response.
	 * If the variable is unknown, this routine should return false.
	 *
	 * Default implementation below just returns the value of
	 * the state variable associated with the service
	 * class with the same name as the action.  This is appropriate
	 * if the application is using the service's state variables
	 * as the 'master state model'
	 *
	 * @param stateVar    State variable to query, with value set
	 *                    to empty string (""). Upon return,
	 *                    the value should be set to the current
	 *                    value
	 *
	 * @return  true if variable is a recognized one for this 
	 *          service, otherwise false
	 *
	 * Notes: Use of the query mechanism (querying one variable at a time)
	 * is generally discouraged in favor of using available actions that 
	 * return logically grouped sets of state variables, in combination with
	 * eventing. Getting one variable at a time is too prone to race 
	 * conditions between logically grouped variables.
	 */
	boolean queryControlReceived(StateVariable stateVar);

	/**
	 * Process an incoming event and invoke a class method of the derived
	 * class with the same name as the event.  For example, for an event
	 * with name "TransportState", the method 'eventTransportState'
	 * would be searched for and invoked if it existed.
	 *
	 * @param uuid
	 * @param seq
	 * @param name
	 * @param value
	 *
	 */
	void eventNotifyReceived(String uuid, long seq, String name, String value);

	void eventLastChange(String value);

	/**
	 *  Default at end of LastChange is no-op
	 */
	void eventLastChangeEnd();

	/** 
	 *  Parser interface methods
	 */

	void setDocumentLocator(Locator locator);

	void startDocument() throws SAXException;

	void endDocument() throws SAXException;

	void processingInstruction(String target, String data) throws SAXException;

	void startPrefixMapping(String prefix, String uri) throws SAXException;

	void endPrefixMapping(String prefix) throws SAXException;

	void startElement(String namespaceURI, String localName, String rawName,
			Attributes atts) throws SAXException;

	void endElement(String namespaceURI, String localName, String rawName)
			throws SAXException;

	void characters(char[] ch, int start, int end) throws SAXException;

	void ignorableWhitespace(char[] ch, int start, int end) throws SAXException;

	void skippedEntity(String name) throws SAXException;

	String errorToString(int code);

}