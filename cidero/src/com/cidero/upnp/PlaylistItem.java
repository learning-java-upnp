/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: PlaylistItem.java,v $
 *
 */
package com.cidero.upnp;


/**
 *  Lightweight playlist item class for use with simple file-based playlists
 *  (M3U,PLS).
 */
public class PlaylistItem
{
  String artist;
  String title;
  String durationSecs;
  String resource;

  public PlaylistItem()
  {
  }

  public PlaylistItem( String title, String resource )
  {
    this( null, title, null, resource );
  }

  public PlaylistItem( String artist, String title, String durationSecs,
                       String resource )
  {
    this.artist = artist;
    this.title = title;
    this.durationSecs = durationSecs;
    this.resource = resource;
  }

  public void setArtist( String artist ) 
  {
    this.artist = artist;
  }
  public String getArtist() 
  {
    return artist;
  }

  public void setTitle( String title ) 
  {
    this.title = title;
  }
  public String getTitle() 
  {
    return title;
  }

  public void setDurationSecs( String durationSecs ) 
  {
    this.durationSecs = durationSecs;
  }
  public String getDurationSecs() 
  {
    return durationSecs;
  }

  public void setResource( String resource ) 
  {
    this.resource = resource;
  }
  public String getResource() 
  {
    return resource;
  }
  

}
  


