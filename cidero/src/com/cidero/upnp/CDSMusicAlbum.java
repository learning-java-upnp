/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: CDSMusicAlbum.java,v $
 *
 */

package com.cidero.upnp;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.cybergarage.xml.XML;

/**
 *
 *  UPnP music album class.
 *  
 */
public class CDSMusicAlbum extends CDSAlbum
{
  String  artist;
  String  genre;
  String  producer;
  String  albumArtURI;
  String  toc;

  public CDSMusicAlbum()
  {
  }

  public CDSMusicAlbum( Node node )
  {
    super( node );

    NodeList children = node.getChildNodes();

    for( int n = 0 ; n < children.getLength() ; n++ )
    {
      String nodeName = children.item(n).getNodeName();
      //      System.out.println( "Node is: " + nodeName );
      
      if( nodeName.equals("upnp:artist") )
      {
        artist = CDS.getSingleTextNodeValue( children.item(n) );

        /* TODO - remove this debugging stuff when done
        try
        {
          byte[] cb = artist.getBytes("UTF-8");
          System.out.println(" $$$$$$$$$$$$ Artist = " + artist + 
                             "len = " + cb.length  );
          for( int m = 0 ; m < cb.length ; m++ )
            System.out.print( (int)cb[m] + " " );
          System.out.println("");
        }
        catch( Exception e )
        {
        }
        */
      }
      else if( nodeName.equals("upnp:genre") )
        genre = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("upnp:producer") )
        producer = CDS.getSingleTextNodeValue( children.item(n) );

    }

  }

  public void setArtist( String artist ) { this.artist = artist; }
  public String getArtist() { return artist; }

  public void setGenre( String genre ) { this.genre = genre; }
  public String getGenre() { return genre; }

  public void setProducer( String producer ) {
    this.producer = producer;
  }
  public String getProducer() { return producer; }

  public void setAlbumArtURI( String albumArtURI ) {
    this.albumArtURI = albumArtURI;
  }
  public String getAlbumArtURI() { return albumArtURI; }

  public void setTOC( String toc ) { this.toc = toc; }
  public String getTOC() { return toc; }


  static String upnpClass = "object.container.album.musicAlbum";

  /**
   *  Get object class
   *
   *  @return  UPNP class string
   */
  public String getUPNPClass() { return upnpClass; }

  public String attributesToXML( CDSFilter filter )
  {
    // No extra attributes for this class (just elements)
    return super.attributesToXML( filter );
  }

  public String elementsToXML( CDSFilter filter )
  {
    // Need to build element string starting with elements in base classes
    String elementXML = super.elementsToXML( filter );

    StringBuffer buf = new StringBuffer();
    
    if( artist != null && filter.propertyEnabled( "upnp:artist" ) )
    {
      buf.append( "  <upnp:artist>");
      buf.append( XML.escapeXMLChars(artist) );
      buf.append( "</upnp:artist>\n" );
    }
    if( genre != null && filter.propertyEnabled( "upnp:genre" ) )
    {
      buf.append( "  <upnp:genre>");
      buf.append( XML.escapeXMLChars(genre) );
      buf.append( "</upnp:genre>\n" );
    }
    if( producer != null && filter.propertyEnabled( "upnp:producer" ) )
    {
      buf.append( "  <upnp:producer>");
      buf.append( XML.escapeXMLChars(producer) );
      buf.append( "</upnp:producer>\n" );
    }

    elementXML += buf.toString();

    return elementXML;
  }


}
