/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ContentDirectory.java,v $
 *
 */

package com.cidero.upnp;

import java.util.logging.Logger;

import org.cybergarage.upnp.*;
import org.cybergarage.upnp.device.InvalidDescriptionException;

import com.cidero.upnp.AbstractService;

/**
 *  ContentDirectory service abstract base class. Provides default
 *  implementations for some actions where possible, but mostly just
 *  defines dumb methods that just return 'false' (action not supported).
 *
 *  Notes:
 *
 *  Method header documentation in this class is intended to be the 
 *  'master' copy.  Derived classes should just include
 *  implemention-specific details in their method headers
 *
 *  THIS SERVICE IS NOT YET REALLY IMPLEMENTED - TODO
 *
 */
public class ContentDirectory extends AbstractService
{
  private static Logger logger = Logger.getLogger("com.cidero.upnp");

  /**
   *  AV Content Directory service official UPnP status codes
   *  (Service-specific codes in the range of 700-799). See Cybergarage
   *  UPnPStatus class for generic UPnP codes
   */
	public static final int NO_SUCH_OBJECT = 701;
	public static final int INVALID_CURRENT_TAG = 702;
	public static final int INVALID_NEW_TAG = 703;
	public static final int REQUIRED_TAG = 704;
	public static final int READ_ONLY_TAG = 705;
	public static final int PARAMETER_MISMATCH = 706;
	public static final int INVALID_SEARCH_CRITERIA = 708;
	public static final int INVALID_SORT_CRITERIA = 709;
	public static final int NO_SUCH_CONTAINER = 710;
	public static final int RESTRICTED_OBJECT = 711;
	public static final int BAD_METADATA = 712;
	public static final int RESTRICTED_PARENT_OBJECT = 713;
	public static final int NO_SUCH_SOURCE_RESOURCE = 714;
	public static final int SOURCE_RESOURCE_ACCESS_DENIED = 715;
	public static final int TRANSFER_BUSY = 716;
	public static final int NO_SUCH_FILE_TRANSFER = 717;
	public static final int NO_SUCH_DEST_RESOURCE = 718;
	public static final int DEST_RESOURCE_ACCESS_DENIED = 719;
	public static final int CANNOT_PROCESS_REQUEST = 720;


  /**
   * Constructor
   */
  public ContentDirectory( IDevice device )
    throws InvalidDescriptionException
  {
    super( device );  
    
    logger.fine("Entered ContentDirectory base class constructor");

    logger.fine("Leaving ContentDirectory constructor");
  }
	
  /**
   * No argument constructor. Useful when one wants to instantiate an
   * object to provide access to errorToString() method (among others)
   * TODO: There may be better way to deal with this...(OJN)
   */
  public ContentDirectory()
  {
  }

  public String getServiceType()
  {
    return "urn:schemas-upnp-org:service:ContentDirectory:1";
  }

  /**
   *  Initialize all required state variables to reasonable values 
   *  Optional device-specific state variables are initialized in the 
   *  derived class instances
   *
   *  Note: ContentDirectory service doesn't have many state variables -
   *  it's really an action-based service
   */
  public void initializeStateVariables()
  {
    logger.fine("Entered - initializing state variables for base class");

    setStateVariable("SearchCapabilities", "" );
    setStateVariable("SortCapabilities", "" );
    setStateVariable("SystemUpdateID", "0" );
    setStateVariable("ContainerUpdateIDs", "" );
  }

  /**
   * Get current connection info
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: Yes
   *
   */
  public boolean actionSearch( Action action )
  {
    return false;
    
    /*
    String objId = action.getArgumentValue( "ObjectID" );
    String searchCriteria = action.getArgumentValue( "SearchCriteria" );
    String filter = action.getArgumentValue( "Filter" );
    String sortCriteria = action.getArgumentValue( "SortCriteria" );
    if( sortCriteria == null )
      sortCriteria = "";
            
    logger.fine( "actionSearch: objId = " + objId );
    logger.fine( "searchCriteria = " + searchCriteria );
    logger.fine( "filter = " + filter );
    logger.fine( "sortCrit = " + sortCriteria );

    // Search not really yet supported
    action.setArgumentValue( "Result", "" );
    action.setArgumentValue( "NumberReturned", "0" );
    action.setArgumentValue( "TotalMatches", "0" );
    action.setArgumentValue( "UpdateID", "0" );
    return true;
		*/
  }
	
  /**
   * Destroy a CDS object
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   *
   */
  public boolean actionDestroyObject( Action action )
  {
    logger.fine( "DestroyObject" );
    //String objectId = action.getArgumentValue( "ObjectID" );
    return false;
  }
	
  /**
   * Update a CDS object
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   *
   */
  public boolean actionUpdateObject( Action action ) 
  {
    logger.fine( "UpdateObject" );

    /*
    String objectId = action.getArgumentValue( "ObjectID" );
    String currentTagValue = action.getArgumentValue( "CurrentTagValue" );
    String newTagValue = action.getArgumentValue( "NewTagValue" );
    */
    return false;
  }
	
  /**
   * Export a CDS resource
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   *
   */
  public boolean actionExportResource( Action action )
  {
    logger.fine( "ExportResource" );
    
    /*
    String sourceID = action.getArgumentValue( "SourceID" );
    String destinationURI = action.getArgumentValue( "DestinationURI" );
    String transferID = action.getArgumentValue( "TransferID" );
    */

    return false;
  }
	
  /**
   * Get transfer progress
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   */
  public boolean actionGetTransferProgress( Action action )
  {
    logger.fine( "GetTransferProgress" );
    
    /*
    String transferID = action.getArgumentValue( "TransferID" );
    action.setArgumentValue("TransferStatus", "" );
    action.setArgumentValue("TransferLength", "" );
    action.setArgumentValue("TransferTotal", "" );
    */

    return false;
  }
	
  /**
   * Get search capabilities
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   */
  public boolean actionGetSearchCapabilities( Action action )
  {
    logger.fine( "GetSearchCapabilities()" );
		
    //action.setArgumentValue("SearchCaps", "" );

    return true;
  }
	
  /**
   * Get current value of SystemUpdateID state variable
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   */
  public boolean actionGetSystemUpdateID( Action action )
  {
    logger.fine( "GetSystemUpdateID()" );
		
    action.setArgumentValue( "Id", "100" );
    return true;
  }
	
  /**
   * Create a new object in the CDS heirarchy
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   */
  public boolean actionCreateObject( Action action )
  {
    logger.fine( "CreateObject" );
    
    /*
    String containerID = action.getArgumentValue( "ContainerID" );
    String elements = action.getArgumentValue( "Elements" );
    action.setArgumentValue( "ObjectID", "100" );
    action.setArgumentValue( "Result", "" );
    */

    return false;
  }
  
  /**
   * Import a resource into the CDS heirarchy
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   */
  public boolean actionImportResource( Action action )
  {
    logger.fine( "CD:ImportResource" );
    
    /*
    String sourceURI = action.getArgumentValue( "SourceURI" );
    String destinationURI = action.getArgumentValue( "DestinationURI" );
    action.setArgumentValue( "TransferID", "100" );
    */

    return false;
  }
	
  /**
   * Create a reference to a CDS object (like a symbolic link)
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   */
  public boolean actionCreateReference( Action action )
  {
    logger.fine("CD:CreateReference");
    
    /*
    String containerID = action.getArgumentValue( "ContainerID" );
    String objectID = action.getArgumentValue( "ObjectID" );
    action.setArgumentValue( "NewID", "100" );
    */

    return false;
  }
	
  /**
   * Delete a CDS resource
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   */
  public boolean actionDeleteResource( Action action )
  {
    logger.fine("CD:DeleteResource" );

    // String resourceURI = action.getArgumentValue( "ResourceURI" );

    return false;
  }

  /**
   * Get the value of the sort capabilities state variable
   *
   * @param  action  UPnP action object
   *
   * NMPR Compliance Notes:
   *   Required Action: No
   */
  public boolean actionGetSortCapabilities( Action action )
  {
    logger.fine("GetSortCapabilities()");
    //action.setArgumentValue( "SortCaps", "" );
    return false;
  }



  public String errorToString(int code)
  {
    return codeToString( code );
  }
  

  public static final String codeToString(int code)
  {
    switch (code) 
    {
      case NO_SUCH_OBJECT: return "No such object";
      case INVALID_CURRENT_TAG: return "Invalid current tag";
      case INVALID_NEW_TAG: return "Invalid new tag";
      case REQUIRED_TAG: return "Required tag";
      case READ_ONLY_TAG: return "Read-only tag";
      case PARAMETER_MISMATCH: return "Parameter mismatch";
      case INVALID_SEARCH_CRITERIA: return "Invalid search criteria";
      case INVALID_SORT_CRITERIA: return "Invalid sort criteria";
      case NO_SUCH_CONTAINER: return "No such container";
      case RESTRICTED_OBJECT: return "Restricted object";
      case BAD_METADATA: return "Bad metadata";
      case RESTRICTED_PARENT_OBJECT: return "Restricted parent object";
      case NO_SUCH_SOURCE_RESOURCE: return "No such source resource";
      case SOURCE_RESOURCE_ACCESS_DENIED: return "Source resource access denied";
      case TRANSFER_BUSY: return "Transfer busy";
      case NO_SUCH_FILE_TRANSFER: return "No such file transfer";
      case NO_SUCH_DEST_RESOURCE: return "No such destination resource";
      case DEST_RESOURCE_ACCESS_DENIED: return "Destination resource access denied";
      case CANNOT_PROCESS_REQUEST: return "Cannot process request";
    }
    
    return UPnPStatus.code2String(code);
  }


  
}
