/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: CDSItem.java,v $
 *
 */

package com.cidero.upnp;

import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 *
 *  Derived class of CDSObject used to represent 'atomic' content
 *  objects, i.e., objects that don't contain any other objects.
 *  
 */
public class CDSItem extends CDSObject
{
  String  refId;       // Reference id (to support symbolic link-like behavior)

  static String upnpClass = "object.item";
  static String xmlElementName = "item";

  public CDSItem()
  {
    refId = "-1";
  }

  public CDSItem( Node node )
  {
    super( node );
    refId = "-1";

    NamedNodeMap attributes = node.getAttributes();

    String tmpString;
    
    try
    {
      Node tmpNode = attributes.getNamedItem("refId");
      if( tmpNode != null )
        refId = tmpNode.getNodeValue();
      
    }
    catch( DOMException e )
    {
      System.out.println( "Error accessing node value: " + e.getMessage() );
    }

  }

  /**
   * Convert this playlist item object to a playlist container object
   *
   * @return 
   */
  public CDSPlaylistContainer convertToPlaylistContainer()
  {
    CDSPlaylistContainer container = new CDSPlaylistContainer();
    
    // Copy fields from CDSObject base class
    container.setId( getId() );
    container.setParentId( getParentId() );
    container.setTitle( getTitle() );
    container.setCreator( getCreator() );

    container.setRestricted( getRestricted() );
    container.setWriteStatus( getWriteStatus() );

    // Private (non-UPnP) library field
    container.setNumericalId( getNumericalId() );
    
    int resourceCount = getResourceCount();
    for( int n = 0 ; n < resourceCount ; n++ )
      container.addResource( getResource(n) );

    return container;
  }

  /* Set/Get methods */

  /**
   *  Set reference id.
   *
   *  The presence of a reference id in an item is an indication that this
   *  item is a reference to another item (like a symbolic link).
   *
   *  @param  refId    Reference id string. Set to "-1" for non-reference items
   */
  public void setRefId( String refId )
  {
    this.refId = refId;
  }

  /**
   *  Get reference id
   *
   *  @return  Reference id string 
   */
  public String getRefId() { return refId; }

  /**
   *  Check if item is a reference to another item
   *
   *  @return  'true' if item is a reference
   */
  public boolean isReference()
  {
    if( refId.equals("-1") )
      return false;
    else
      return true;
  }

  /**
   * Convenience function to get the track duration - retrieves the duration
   * of the first resource that has the duration property set
   *
   * @return  Returns the playing duration in [H+]:MM:SS[.F+] format.
   */
  public String getDuration()
  {
    String duration = null;

    for( int n = 0 ; n < getResourceCount() ; n++ )
    {
      CDSResource resource = getResource( n );
      if ( (duration = resource.getDuration()) != null )
        return duration;
    }
    return duration;
  }

  public String getDurationNoMillisec()
  {
    String duration = null;

    for( int n = 0 ; n < getResourceCount() ; n++ )
    {
      CDSResource resource = getResource( n );
      if ( (duration = resource.getDurationNoMillisec()) != null )
        return duration;
    }
    return duration;
  }


  /**
   *  Get object class
   *
   *  @return  UPNP class string
   */
  public String getUPNPClass() { return upnpClass; }
  public String getObjectXMLElementName() { return xmlElementName; }

  public String attributesToXML( CDSFilter filter )
  {
    if( refId.equals("-1") || !filter.propertyEnabled( "@refId" ) )
      return super.attributesToXML( filter );

    String attributeXML = super.attributesToXML( filter ) + " refId=\"" + refId + "\"";

    return attributeXML;
  }

  public String elementsToXML( CDSFilter filter )
  {
    // item adds no new XML elements to base class
    return super.elementsToXML( filter );
  }

  public boolean isContainer() { return false; }

}
