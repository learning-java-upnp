/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: CDSPhoto.java,v $
 *
 */

package com.cidero.upnp;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 *  Derived class of CDSItem used to represent a photo.
 *  A 'photo' is an instance of an image that should be interpreted
 *  as a photo, as opposed to, for example, an icon.
 *  
 */
public class CDSPhoto extends CDSImageItem
{
  static String upnpClass = "object.item.imageItem.photo";

  // UPNP namespace fields

  // Is this a link back to parent album? Spec doesn't say. I don't 
  // like it since the album concept is fading for photos 
  String  album;    

  public CDSPhoto()
  {
  }

  public CDSPhoto( Node node )
  {
    super( node );

    NodeList children = node.getChildNodes();

    for( int n = 0 ; n < children.getLength() ; n++ )
    {
      String nodeName = children.item(n).getNodeName();
      //      System.out.println( "Node is: " + nodeName );
      
      if( nodeName.equals("upnp:album") )
        album = CDS.getSingleTextNodeValue( children.item(n) );

    }

  }

  public void setAlbum( String album ) {
    this.album = album;
  }
  public String getAlbum() { return album; }


  /**
   *  Get object class
   *
   *  @return  UPNP class string
   */
  public String getUPNPClass() { return upnpClass; }

  public String attributesToXML( CDSFilter filter )
  {
    // No extra attributes for this class (just elements)
    return super.attributesToXML( filter );  
  }

  public String elementsToXML( CDSFilter filter )
  {
    // Need to build element string starting with elements in base classes
    String elementXML = super.elementsToXML( filter );

    // Not supporting album element yet - not convinced its a good idea

    //elementXML += "  <upnp:rating>" + genre + "</upnp:rating>\n";

    return elementXML;
  }
  

}
