/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: CDSVideoItem.java,v $
 *
 */

package com.cidero.upnp;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.cybergarage.xml.XML;

/**
 *
 *  Derived class of CDSItem used to represent generic video
 *  content. Serves as a base class for more specific classes, such as
 *  'movie'
 *  
 */
public class CDSVideoItem extends CDSItem
{
  static String upnpClass = "object.item.videoItem";

  // UPNP namespace fields
  String  genre;
  String  longDescription;
  String  producer;
  String  rating;
  String  actor;
  String  director;

  // Dublin core namespace
  String  description;
  String  publisher;
  String  language;
  String  relation;


  public CDSVideoItem()
  {
  }

  public CDSVideoItem( Node node )
  {
    super( node );

    NodeList children = node.getChildNodes();

    for( int n = 0 ; n < children.getLength() ; n++ )
    {
      String nodeName = children.item(n).getNodeName();

      //System.out.println( "Node is: " + nodeName );
      
      if( nodeName.equals("dc:description") )
        description = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("upnp:actor") )
        actor = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("upnp:rating") )
        rating = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("upnp:director") )
        director = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("upnp:genre") )
        genre = CDS.getSingleTextNodeValue( children.item(n) );

    }

  }

  public void setGenre( String genre ) { this.genre = genre; }
  public String getGenre() { return genre; }

  public void setLongDescription( String description ) {
    this.longDescription = description;
  }
  public String getLongDescription() { return longDescription; }

  public void setProducer( String producer ) {
    this.producer = producer;
  }
  public String getProducer() { return producer; }

  public void setRating( String rating ) {
    this.rating = rating;
  }
  public String getRating() { return rating; }

  public void setActor( String actor ) {
    this.actor = actor;
  }
  public String getActor() { return actor; }

  public void setDirector( String director ) {
    this.director = director;
  }
  public String getDirector() { return director; }


  public void setDescription( String description ) {
    this.description = description;
  }
  public String getDescription() { return description; }

  public void setPublisher( String publisher ) {
    this.publisher = publisher;
  }
  public String getPublisher() { return publisher; }

  public void setLanguage( String language ) {
    this.language = language;
  }
  public String getLanguage() { return language; }

  public void setRelation( String relation ) {
    this.relation = relation;
  }
  public String getRelation() { return relation; }


  /**
   *  Get object class
   *
   *  @return  UPNP class string
   */
  public String getUPNPClass() { return upnpClass; }

  public String attributesToXML( CDSFilter filter )
  {
    // No extra attributes for this class (just elements)
    return super.attributesToXML( filter );  
  }

  public String elementsToXML( CDSFilter filter )
  {
    // Need to build element string starting with elements in base classes
    StringBuffer buf = new StringBuffer();

    buf.append( super.elementsToXML( filter ) );

    // only support subset of elements for now

    if( (genre != null) && filter.propertyEnabled( "upnp:genre" ) )
    {
      buf.append( "  <upnp:genre>");
      buf.append( XML.escapeXMLChars(genre) );
      buf.append("</upnp:genre>\n");
    }

    if( (rating != null) && filter.propertyEnabled( "upnp:rating" ) )
    {
      buf.append("  <upnp:rating>");
      buf.append( XML.escapeXMLChars(rating) );
      buf.append("</upnp:rating>\n");
    }
    
    if( (actor != null) && filter.propertyEnabled( "upnp:actor" ) )
    {
      buf.append( "  <upnp:actor>" );
      buf.append( XML.escapeXMLChars(actor) );
      buf.append( "</upnp:actor>\n" );
    }
    
    if( (director != null) && filter.propertyEnabled( "upnp:director" ) )
    {
      buf.append( "  <upnp:director>" );
      buf.append( XML.escapeXMLChars( director ) );
      buf.append( "</upnp:director>\n" );
    }
    
    if( (description != null) && filter.propertyEnabled( "dc:description" ) )
    {
      buf.append( "  <dc:description>" );
      buf.append( XML.escapeXMLChars(description) );
      buf.append( "</dc:description>\n" );
    }
    if( (relation != null) && filter.propertyEnabled( "dc:relation" ) )
    {
      buf.append( "  <dc:relation>" );
      buf.append( XML.escapeXMLChars(relation) );
      buf.append( "</dc:relation>\n" );
    }
    
    return buf.toString();
  }

}
