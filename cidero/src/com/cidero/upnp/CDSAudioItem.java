/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: CDSAudioItem.java,v $
 *
 */

package com.cidero.upnp;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.cybergarage.xml.XML;


/**
 *
 *  Derived class of CDSItem used to represent generic audio
 *  content. Serves as a base class for more specific classes, such as
 *  'musicTrack' and 'audioBroadcast'
 *  
 *  TODO: Use Eclipse to add javadoc stuff for methods
 */

public class CDSAudioItem extends CDSItem
{
  static String upnpClass = "object.item.audioItem";

  // Dublin Core namespace elements
  String  description = null;
  String  publisher;
  String  language;
  String  relation;
  String  rights;

  // UPnP namespace elements
  String  longDescription;
  String  genre;


  public CDSAudioItem()
  {
  }

  public CDSAudioItem( Node node )
  {
    super( node );

    NodeList children = node.getChildNodes();

    for( int n = 0 ; n < children.getLength() ; n++ )
    {
      String nodeName = children.item(n).getNodeName();
      //      System.out.println( "Node is: " + nodeName );
      
      if( nodeName.equals("dc:description") )
        description = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("upnp:longDescription") )
        longDescription = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("upnp:genre") )
        genre = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("dc:publisher") )
        publisher = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("dc:language") )
        language = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("dc:relation") )
        relation = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("dc:rights") )
        rights = CDS.getSingleTextNodeValue( children.item(n) );
    }

  }

  public void setGenre( String genre ) { this.genre = genre; }
  public String getGenre() { return genre; }

  public void setDescription( String description ) {
    this.description = description;
  }
  public String getDescription() { return description; }

  public void setLongDescription( String description ) {
    this.longDescription = description;
  }
  public String getLongDescription() { return longDescription; }

  public void setPublisher( String publisher ) {
    this.publisher = publisher;
  }
  public String getPublisher() { return publisher; }

  public void setLanguage( String language ) {
    this.language = language;
  }
  public String getLanguage() { return language; }

  public void setRelation( String relation ) {
    this.relation = relation;
  }
  public String getRelation() { return relation; }

  public void setRights( String rights ) {
    this.rights = rights;
  }
  public String getRights() { return rights; }

  /**
   *  Get object class
   *
   *  @return  UPNP class string
   */
  public String getUPNPClass() { return upnpClass; }

  public String attributesToXML( CDSFilter filter )
  {
    // No extra attributes for this class (just elements)
    return super.attributesToXML( filter );  
  }

  public String elementsToXML( CDSFilter filter )
  {
    // Need to build element string starting with elements in base classes
    StringBuffer buf = new StringBuffer();
    
    buf.append( super.elementsToXML( filter ) );

    // TODO: Add support for ALL elements...missing some here

    //
    // Dublin core namespace elements
    //
    if( (description != null) && filter.propertyEnabled( "dc:description" ) )
    {
      buf.append( "  <dc:description>");
      buf.append( XML.escapeXMLChars(description) );
      buf.append("</dc:description>\n");
    }
    if( (publisher != null) && filter.propertyEnabled( "dc:publisher" ) )
    {
      buf.append( "  <dc:publisher>" );
      buf.append( XML.escapeXMLChars(publisher) );
      buf.append( "</dc:publisher>\n" );
    }
    if( (language != null) && filter.propertyEnabled( "dc:language" ) )
    {
      buf.append( "  <dc:language>" );
      buf.append( XML.escapeXMLChars(language) );
      buf.append( "</dc:language>\n" );
    }
    if( (relation != null) && filter.propertyEnabled( "dc:relation" ) )
    {
      buf.append( "  <dc:relation>" );
      buf.append( XML.escapeXMLChars(relation) );
      buf.append( "</dc:relation>\n" );
    }
    if( (rights != null) && filter.propertyEnabled( "dc:rights" ) )
    {
      buf.append( "  <dc:rights>" );
      buf.append( XML.escapeXMLChars(rights) );
      buf.append( "</dc:rights>\n" );
    }
    
    //
    // UPnP namespace elements
    //
    if( (genre != null) && filter.propertyEnabled( "upnp:genre" ) )
    {
      buf.append( "  <upnp:genre>");
      buf.append( XML.escapeXMLChars(genre) );
      buf.append("</upnp:genre>\n");
    }
    if( (longDescription != null) && 
        filter.propertyEnabled( "upnp:longDescription" ) )
    {
      buf.append("  <upnp:longDescription>");
      buf.append( XML.escapeXMLChars(longDescription) );
      buf.append("</upnp:longDescription>\n");
    }

    return buf.toString();
  }
  

}
