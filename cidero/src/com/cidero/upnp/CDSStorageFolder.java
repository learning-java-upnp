/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: CDSStorageFolder.java,v $
 *
 */

package com.cidero.upnp;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 *  Derived class of CDSContainer used to hold other objects.
 *  The most common examples of a 'StorageFolder' instance is a 
 *  directory on a disk drive.
 *  
 *  TODO - Figure out why this container doesn't have 'storageFree' 
 *  property - (it isn't derived from 'storageVolume' container class,
 *  which has that property)
 *
 */
public class CDSStorageFolder extends CDSContainer
{
  static String upnpClass = "object.container.storageFolder";
  long storageUsed;

  public CDSStorageFolder()
  {
    storageUsed = -1;
  }

  public CDSStorageFolder( Node node )
  {
    super( node );

    NodeList children = node.getChildNodes();

    for( int n = 0 ; n < children.getLength() ; n++ )
    {
      String nodeName = children.item(n).getNodeName();
      
      if( nodeName.equals("upnp:storageUsed") )
        storageUsed = Long.parseLong( 
               CDS.getSingleTextNodeValue( children.item(n) ) );

    }

  }

  /**
   *  Set storage used
   *
   *  @param  storageUsed    Storage used, in bytes
   */
  public void setStorageUsed( long storageUsed )
  {
    this.storageUsed = storageUsed;
  }

  /**
   *  Get storage used
   *
   *  @return  Storage used, in bytes
   */
  public long getStorageUsed() { return storageUsed; }

  
  public String getUPNPClass() { return upnpClass; }

  public String attributesToXML( CDSFilter filter )
  {
    return super.attributesToXML( filter );
  }

  public String elementsToXML( CDSFilter filter )
  {
    // Need to build element string starting with elements in base classes
    String elementXML = super.elementsToXML( filter );
    
    // Add class elements
    elementXML += "  <upnp:storageUsed>" + storageUsed +
                    "</upnp:storageUsed>\n";

    return elementXML;
  }

}
