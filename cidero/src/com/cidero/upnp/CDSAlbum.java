/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: CDSAlbum.java,v $
 *
 */

package com.cidero.upnp;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.cybergarage.xml.XML;

/**
 *
 *  Derived class of CDSObject used to represent 'albums'.
 *  Subclasses are musicAlbum, photoAlbum
 *  
 */
public class CDSAlbum extends CDSContainer
{
  String  storageMedium;
  String  description;
  String  longDescription;
  String  publisher;
  String  contributor;
  String  date;

  public CDSAlbum()
  {
  }

  public CDSAlbum( Node node )
  {
    super( node );

    NodeList children = node.getChildNodes();

    for( int n = 0 ; n < children.getLength() ; n++ )
    {
      String nodeName = children.item(n).getNodeName();
      
      if( nodeName.equals("dc:description") )
        description = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("dc:date") )
        date = CDS.getSingleTextNodeValue( children.item(n) );
      else if( nodeName.equals("upnp:storageMedium") )
        storageMedium = CDS.getSingleTextNodeValue( children.item(n) );

    }

  }

  public void setStorageMedium( String storageMedium )
  { this.storageMedium = storageMedium; }
  public String getStorageMedium() { return storageMedium; }
  
  public void setDescription( String description ) {
    this.description = description;
  }
  public String getDescription() { return description; }

  public void setLongDescription( String description ) {
    this.longDescription = description;
  }
  public String getLongDescription() { return longDescription; }

  public void setPublisher( String publisher ) {
    this.publisher = publisher;
  }
  public String getPublisher() { return publisher; }

  public void setContributor( String contributor ) {
    this.contributor = contributor;
  }
  public String getContributor() { return contributor; }

  public void setDate( String date ) {
    this.date = date;
  }
  public String getDate() { return date; }


  static String upnpClass = "object.container.album";

  /**
   *  Get object class
   *
   *  @return  UPNP class string
   */
  public String getUPNPClass() { return upnpClass; }

  public String attributesToXML( CDSFilter filter )
  {
    // No extra attributes for this class (just elements)
    return super.attributesToXML( filter );  
  }

  public String elementsToXML( CDSFilter filter )
  {
    // Need to build element string starting with elements in base classes
    StringBuffer buf = new StringBuffer();

    buf.append( super.elementsToXML( filter ) );

    buf.append("  <dc:description>" + XML.escapeXMLChars(description) +
               "</dc:description>\n");
    if( date != null )
      buf.append("  <dc:date>" + date + "</dc:date>\n");
    if( storageMedium != null )
      buf.append("  <upnp:storageMedium>" + storageMedium + "</upnp:storageMedium>\n");

    return buf.toString();
  }

}
