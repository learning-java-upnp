/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: CDSMusicTrack.java,v $
 *
 */

package com.cidero.upnp;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.cybergarage.xml.XML;

/**
 * Derived class of CDSAudioItem used to represent a music track
 * 
 */
public class CDSMusicTrack extends CDSAudioItem
{
  static String upnpClass = "object.item.audioItem.musicTrack";
  String artist;
  String album;
  int originalTrackNum;
  String playlist;
  String storageMedium;
  String contributor;
  String date;
  //String playingTime;

  public CDSMusicTrack() {
  }

  public CDSMusicTrack(Node node)
  {
    super(node);

    NodeList children = node.getChildNodes();
        
    for (int n = 0; n < children.getLength(); n++)
    {
      String nodeName = children.item(n).getNodeName();
      //System.out.println( "Node is: " + nodeName );

      if (nodeName.equals("upnp:artist"))
        artist = CDS.getSingleTextNodeValue(children.item(n));
      else if (nodeName.equals("upnp:album"))
        album = CDS.getSingleTextNodeValue(children.item(n));
      else if (nodeName.equals("upnp:originalTrackNumber"))
      {
        String trackNum = CDS.getSingleTextNodeValue(children.item(n));
        originalTrackNum = Integer.parseInt(trackNum);
      }
      else if (nodeName.equals("upnp:storageMedium"))
        storageMedium = CDS.getSingleTextNodeValue(children.item(n));
      else if (nodeName.equals("dc:date")) 
        date = CDS.getSingleTextNodeValue(children.item(n));

      /*
      else if (nodeName.equals("res"))
      {
        CDSResource resource = new CDSResource(children.item(n));
        // System.out.println("Duration = " +resource.getDuration());
        setPlayingTime(resource.getDuration());
      }
      */       
    }
  }

  public void setArtist(String artist) {
    this.artist = artist;
  }

  public String getArtist() {
    return artist;
  }

  public void setAlbum(String album) {
    this.album = album;
  }

  public String getAlbum() {
    return album;
  }

  public void setOriginalTrackNum(int trackNum) {
    this.originalTrackNum = trackNum;
  }

  public int getOriginalTrackNum() {
    return originalTrackNum;
  }

  public void setPlaylist(String playlist) {
    this.playlist = playlist;
  }

  public String getPlaylist() {
    return playlist;
  }

  public void setStorageMedium(String storageMedium) {
    this.storageMedium = storageMedium;
  }

  public String getStorageMedium() {
    return storageMedium;
  }

  public void setContributor(String contributor) {
    this.contributor = contributor;
  }

  public String getContributor() {
    return contributor;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getDate() {
    return date;
  }

  /**
   * Get object class
   * 
   * @return UPNP class string
   */
  public String getUPNPClass() {
    return upnpClass;
  }


  public String attributesToXML( CDSFilter filter ) {
    // No extra attributes for this class (just elements)
    return super.attributesToXML( filter );
  }

  public String elementsToXML( CDSFilter filter )
  {
    StringBuffer buf = new StringBuffer();

    // Need to build element string starting with elements in base classes
    buf.append( super.elementsToXML( filter ) );

    // Output optional attributes if filter has them enabled
    if( (artist != null) && filter.propertyEnabled("upnp:artist") )
    {
      buf.append("  <upnp:artist>");
      buf.append( XML.escapeXMLChars(artist) );
      buf.append("</upnp:artist>\n");
    }
    
    if( (album != null) && filter.propertyEnabled("upnp:album") )
    {
      buf.append("  <upnp:album>");
      buf.append( XML.escapeXMLChars(album) );
      buf.append("</upnp:album>\n");
    }
    
    if( filter.propertyEnabled("upnp:originalTrackNumber") )
    {
      buf.append("  <upnp:originalTrackNumber>" + originalTrackNum +
                 "</upnp:originalTrackNumber>\n");
    }
    
    if( (date != null) && filter.propertyEnabled("dc:date") )
      buf.append("  <dc:date>" + date + "</dc:date>\n");

    return buf.toString();
  }

  public static void main(String[] args)
  {
    CDSMusicTrack obj = new CDSMusicTrack();

    obj.setId("10");
    obj.setParentId("1");
    obj.setTitle("Mysterious Ways");
    obj.setCreator("U2");
    obj.setRestricted(false);
    obj.setWriteStatus("WRITABLE");

    CDSResource resource = new CDSResource();

    resource.setName("http://10.0.0.1/U2_Mysterious_Ways.mp3");
    resource.setProtocolInfo("http-get:*:audio/mpeg:*");
    resource.setSize(5000000);
    resource.setDuration("300");

    obj.addResource(resource);

    obj.setArtist("U2");
    obj.setAlbum("Actung Baby");
    obj.setOriginalTrackNum(8);
    obj.setGenre("Rock");
    //obj.setDescription("Cool-sounding bass guitar");
    obj.setDate("2003-04-20");

    CDSFilter filter = new CDSFilter("@id,@parentID,dc:title,dc:description,res,res@protocolInfo,container@childCount");

    System.out.println( obj.toXML( filter ) );

    //filter = new CDSFilter("*");
    filter = new CDSFilter("");

    System.out.println( obj.toXML( filter ) );

  }


}
