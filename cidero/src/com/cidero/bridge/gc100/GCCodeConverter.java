/*
 *  Copyright (C) 2003 MediaRush, Inc.
 *
 *  This software is copyrighted to MediaRush, Inc. It cannot
 *  be distributed or modified without the express written 
 *  approval of MediaRush, Inc.
 * 
 *  postmaster@mediarush.com
 * 
 *  File: ConvertPronto.java
 *
 */

package com.cidero.bridge.gc100;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * <code>ConvertPronto</code> here.
 *
 * Convert pronto remote codes to GC100 format
 *
 * Pronto format:
 *
 *  [Preamble]  [BurstPairSequence1] [BurstPairSequence2] 
 *
 *  [Preamble]  4 words
 *
 *    Word   Value
 *  ------------------------------------------------------------------------ 
 *      0    0x0000    Always 0, indicating that the IR pattern is learned
 *
 *      1    freq      The frequency of the IR carrier in terms of the Pronto
 *                     internal clock. The following formula where N 
 *                     represents the decimal value of this hex number
 *                     will give you the frequency of the carrier in Hz: 
 *                     Frequency = 1000000/(N * .241246)
 *
 *                     A Sony remote will usually have a value for N of 103
 *                     (this shows as 67 Hex). Doing the arithmetic we have
 *                     Freq=1000000/(103*. 241246)= 40,244 or approximately
 *                     40,000 cycles per second (well within a tolerance of
 *                     40,000 +/- 10%)
 *
 *      2    count     The number of Burst Pairs in Burst Pair Sequence #1.
 *                     Each Burst pair consists of two 4 digit Hex numbers
 *                     representing the On and Off time of that burst 
 *                     (single binary Bit).
 * 
 *      3    count     The number of Burst Pairs in Burst Pair Sequence #2. 
 *
 *    Burst Pair Sequence #1 starts at word 5 if it is present and is 
 *    immediately followed by the digits of Burst Pair Sequence #2 if 
 *    it is present (word 4>0000). If Sequence #1 is missing (word 3=0000),
 *    then Burst Sequence Number 2 starts at word 5.
 *
 *    A Burst Pair Sequence usually looks as follows: 
 *
 *    The Lead In Burst pair can be thought of as the hello or wake up 
 *    burst. It tells the receiver to start listening (or rather looking)
 *    very closely as what is coming. It is usually of different timing
 *    duration than the Burst Pairs in the data part. Technically it is 
 *    also used to set the receivers AGC level, a factor related to how
 *    much the receiver will amplify the IR light it sees.
 *
 *    The Lead Out burst pair marks the end of the message and usually 
 *    has a long OFF time period to guarantee that two IR messages can$B!G(Bt
 *    be sent too close together. It may actually be incorporated as part
 *    of the last data bit if the ON period is what carries the information
 *    (that is, the off time is constant in the data portion and the On 
 *    time varies between two values). Once again, Sony does exactly that.
 *
 *    Note that the pronto codes are all in hex, while the GC100 uses 
 *    decimal
 *
 *
 */
public class GCCodeConverter
{
  public GCCodeConverter()
  {
  }
  
  public String convertProntoCode( String prontoIRCode )
  {
    String[] prontoTok = prontoIRCode.split(" ");
    
    System.out.println("Converting pronto code: " + prontoIRCode );
    System.out.println("Found " + prontoTok.length + " tokens" );

    return "";
    
  }

  /**
   *  Send an IR command to the specified GC100 port address
   *
   *  @param  irPortAddr    2:1, 2:2, or 2:3 for vanilla GC-100-6
   *  @param  cmdId         16-bit id used to tag responses
   *  @param  freq          Frequency of carrier (Hz)
   *  @param  count         Repeat count for command
   *  @param  offset        If count is > 1, this number indicates the offset
   *                        within the timing pattern to start repeating the
   *                        IR command. Offset will always be odd since a 
   *                        timing pattern begins with an <on> state
   *
   *  @param  onOffPattern  On/Off pattern. The pattern values are measured
   *                        in periods of the carrier frequency (1-65536)
   *
   *                        There must be an equal number of on/off states.
   *                        Every on/off state must meet an 80uS minimum 
   *                        time requirement for the GC100 to work properly.
   *                        For a carrier freq of 48 KHz, the min value 
   *                        is 80us*48KHz = 3.84. For proper GC100 operation,
   *                        all on/off values in the timing pattern must be 
   *                        4 or higher
   *
   */

  //
  // Test code
  //
  public static void main( String args[] )
  {
    GCCodeConverter converter = new GCCodeConverter();
    
    try 
    {
      BufferedReader reader = 
        new BufferedReader( new FileReader( args[0] ) );

      while( true )
      {
        String prontoCode = reader.readLine();
        System.out.println("ProntoCode: " + prontoCode );

        //        String cmd = "sendir,2:1,1,40000,1,1,44,45,46,45";
        //        System.out.println("Cmd is: " + cmd );
        //        driver.sendMsg( cmd );
        //        String cmd2 = "sendir,2:1,1,40000,1,1,4,5,6,5";
        //        System.out.println("Cmd2 is: " + cmd2 );

        System.out.println("GCCode: " + 
                           converter.convertProntoCode( prontoCode ) );
      }
    }
    catch( Exception e )
    {
      System.out.println( e );
      System.exit(-1);
    }
    
  }
  
}
