/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ShoutcastMediaRenderer.java,v $
 *
 */

package com.cidero.bridge.shoutcast;

import java.util.logging.Logger;

import org.cybergarage.upnp.device.InvalidDescriptionException;

import com.cidero.bridge.MediaRendererException;
import com.cidero.bridge.AbstractMediaRendererBridge;
import com.cidero.upnp.AVTransport;
import com.cidero.upnp.ConnectionManager;
import com.cidero.upnp.RenderingControl;

/**
 * This class contains the Shoutcast implementation of the 
 * AbstractMediaRenderer* interface. Since there is no way to
 * control devices listing to the shoutcast stream, most of
 * the methods in AbstractMediaRendere are not implemented, 
 * and the methods default to the no-ops in the base class.
 * The exception is the getFriendlyName method, which is implemented.
 *
 */
public class ShoutcastMediaRenderer extends AbstractMediaRendererBridge
{
  private static Logger logger = 
     Logger.getLogger("com.cidero.bridge.shoutcast");

  private final static String DESCRIPTION_FILE_NAME = 
     "com/mediarush/bridge/shoutcast/description/MediaRenderer.xml";

  // Shoutcast versions of UPnP services
  //ShoutcastRenderingControl  renderingControl;
  //ShoutcastAVTransport       avTransport;
  //ShoutcastConnectionManager connectionManager;

  /**
   * Constructor
   *
   * @param  friendlyName     Friendly name (e.g. 'HomeCast')
   *                          This is assigned in property file
   */
  public ShoutcastMediaRenderer( String friendlyName )
    throws InvalidDescriptionException
  {
    super( DESCRIPTION_FILE_NAME, "UnknownAddr", friendlyName );

    logger.fine("ShoutcastMediaRenderer constructor: Entered");

    getProperties();
  }
  
  /** 
   *  Get properties from propery file 
   */
  public void getProperties()
  {
    logger.fine("Loading properties for shoutcast renderer");
    
    //Properties props = 
    //  MrUtil.loadProperties("ShoutcastMediaRenderer.properties");
	}

  public String getProxyUrlPath()
  {
    return "/Shoutcast/" + getFriendlyName();
  }
  
  //
  //  Not yet implemented
  //

  public RenderingControl getRenderingControl() {
    return null;
    //    return renderingControl;
  }
  public AVTransport getAVTransport() {
    return null;
    //    return avTransport;
  }
  public ConnectionManager getConnectionManager() {
    return null;
    //    return connectionManager;
  }


  public void avTransportSetTransportURI( String uri )
    throws MediaRendererException
  {
    //avTransport.setTransportURI( uri );
  }
  public void avTransportPlay( String speed ) throws MediaRendererException
  {
    //avTransport.play( speed );
  }
  public void avTransportPause() throws MediaRendererException
  {
    //avTransport.pause();
  }
  public void avTransportStop() throws MediaRendererException
  {
    //avTransport.stop();
  }


}
