/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ShoutcastDeviceManager.java,v $
 *
 */

package com.cidero.bridge.shoutcast;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import org.cybergarage.upnp.device.InvalidDescriptionException;

import com.cidero.bridge.BridgeDeviceManager;
import com.cidero.util.MrUtil;
import com.cidero.util.AppPreferences;

/**
 * This class handles the management of multiple ShoutcastMediaRenderer
 * bridge devices (UPnP-controllable home radio stations).  A properties
 * file is used to control how many stations are started up, and what 
 * URL's to broadcast on.  UPnP control points can control each shoutcast
 * station independently.
 *
 */
public class ShoutcastDeviceManager implements BridgeDeviceManager
{
  private static Logger logger = 
    Logger.getLogger("com.cidero.bridge.shoutcast");

  private final static int SHOUTCAST_MAX_STATIONS = 10;

  List   activeDeviceList = new ArrayList();

  static AppPreferences pref;
  

  public ShoutcastDeviceManager()
  {
    loadPreferences();

    instantiateDeviceBridges();
  }
  

  /*
   *  Load Audiotron property file. File contains list of audiotron 
   *  IP addresses and logical names. 
   */
  public static void loadPreferences()
  {
    // Load shared & user-specific preferences for this application
    pref = new AppPreferences(".cidero");

    if( ! pref.load( "Bridge", "ShoutcastDeviceManager" ) )
    {
      logger.severe("Missing preferences file - exiting");
      System.exit(-1);
    }
  }

  public void savePreferences()
  {
    pref.saveUserPreferences( "Bridge", "ShoutcastDeviceManager" );
  }

  public static AppPreferences getPreferences()
  {
    if( pref == null )
      loadPreferences();
    
    return pref;
  }


  public void instantiateDeviceBridges()
  {
    for( int n = 1 ; n <= SHOUTCAST_MAX_STATIONS ; n++ )
    {
      String propPrefix = "station" + n + "_";
      
      String enabled = pref.get( propPrefix + "enabled" );
      if( (enabled == null) || (! enabled.equals("true")) ) 
        continue;
      
      String name = pref.get( propPrefix + "name" );

      if( name == null )
        continue;
      
      logger.info("Creating station '" + name + "'" );

      try 
      {
        ShoutcastMediaRenderer bridgeDev = new ShoutcastMediaRenderer( name );
        
        activeDeviceList.add( bridgeDev );

        bridgeDev.start();
      }
      catch( InvalidDescriptionException e )
      {
        logger.severe("ShoutcastDeviceManager: Invalid UPnP device description" );
      }
    }

    logger.fine("Leaving getProperties " );
  }

  public void start()
  {
    logger.fine("Starting ShoutcastDeviceManager " );
  }

  public void stop()
  {
    logger.fine("Stopping ShoutcastDeviceManager " );
  }


  public static void main(String args[])
  {
    ShoutcastDeviceManager devManager = new ShoutcastDeviceManager();

    devManager.start();
  }

}

