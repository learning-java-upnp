/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: MediaRendererBridgeSession.java,v $
 *
 */
package com.cidero.bridge;

import com.cidero.http.HTTP;
import com.cidero.http.HTTPConnection;
import com.cidero.renderer.IMediaRenderer;
import com.cidero.upnp.UPnPException;
import com.cidero.util.NetUtil;
import com.cidero.util.ShoutcastOutputStream;

/**
 * Class to hold information associated with a MediaRenderer 'session' which may
 * consist of playback of a single file, or multiple files (playlist case)
 * 
 * A playback session may be shared by several media renderers, allowing for
 * 'synchronized' playback. (The degree of synchronization will probably range
 * from 'pretty good' for devices of the same type, to 'marginal' for devices
 * with wildly different buffering strategies.
 * 
 */
public class MediaRendererBridgeSession extends AbstractMediaRendererSession
		implements Runnable {

	public MediaRendererBridgeSession(IMediaRenderer rendererBridge,
			String resourceURL, String metaData, String userAgent,
			int syncWaitMillisec) throws UPnPException {
		super(rendererBridge, resourceURL, metaData, userAgent,
				syncWaitMillisec);
	}

	@Override
	protected void play(IMediaRenderer renderer) throws MediaRendererException {
		final String hostAddr = NetUtil.getDefaultLocalIPAddress();

		final IMediaRendererBridge rb = (IMediaRendererBridge) renderer;
		rb.avTransportSetTransportURI("http://" + hostAddr + ":8081"
				+ rb.getProxyUrlPath());

		// returns immediately after starting playback...
		rb.avTransportPlay(playSpeed);
	}

	/**
	 * Join the SynchronizedShoutcastGroup associated with this session. If no
	 * group is active, create one
	 * 
	 * @return Reference to new SyncShoutcastGroup, or null if joined existing
	 *         group
	 */
	public synchronized SyncShoutcastGroup joinSyncShoutcastGroup(
			ShoutcastOutputStream outStream, HTTPConnection connection) {
		if (syncShoutcastGroup == null) // First requester for this URL ?
		{
			String userAgent = connection.getRequest().getHeaderValue(
					HTTP.USER_AGENT);
			// System.out.println("join - userAgent = " + userAgent );
			// AppPreferences pref = RadioServer.getPreferences();

			syncShoutcastGroup = new SyncShoutcastGroup(this, outStream,
					connection);
			return syncShoutcastGroup;
		} else {
			syncShoutcastGroup.addStream(outStream, connection);

			// Return null to let caller know this thread was 'joined' with
			// existing shoutcast group, and the thread should be terminated
			// without
			// closing the HTTP socket session
			return null;
		}
	}

}
