/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: PrismiqAsyncNotifyListener.java,v $
 *
 */

package com.cidero.bridge.prismiq;

/**
 *  The Prismiq outputs a set of asynchronous messages on the media agent
 *  TCP connection. This listener interface allows them to be acted upon.
 *
 *  The message codes are:
 *
 *   905       Automatic timestamp/PTS messages
 *   907/908   Notify on new Shoutcast song/title
 *   909       Notify when media length known
 *   910/911   Notify on audio/video underflow
 *   912       Notify on invalid audio data
 *    
 *   Note that additional text follows the message code. See Prismiq
 *   media agent doc for more details
 *
 *   Sample msg:   "910 Audio underflow"
 */

public interface PrismiqAsyncNotifyListener
{
  public void asyncNotifyHandler( String msg );
}


