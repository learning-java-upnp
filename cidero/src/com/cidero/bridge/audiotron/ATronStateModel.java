/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ATronStateModel.java,v $
 *
 */

package com.cidero.bridge.audiotron;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Logger;

import com.cidero.upnp.AVTransport;
import com.cidero.upnp.RendererStateModel;

/**
 *  Audiotron renderer state class.  The Audiotron dumps a bunch of 
 *  status info upon request.  Below is a sample.  The only fields 
 *  of interest (at the moment) to the UPnP renderer bridge package
 *  are the volume & mute values, and the play state, since they are
 *  things that may be modified via the remote, and that info needs 
 *  to be fed back to the UPnP control point.
 * 
 *  Response to status request:   ( "http://<ATronIP>:80/apigetstatus.asp" )
 *
 *  [Status]
 *  CurrTime=295795
 *  PlayQUpdateTime=293094
 *  CurrVolume=-2
 *  STOP_LED=1
 *  PLAY_LED=0
 *  PAUSE_LED=0
 *  RANDOM_LED=0
 *  GROUP_LED=0
 *  MUTE_LED=0
 *  REPEAT_LED=1
 *  State=Inactive
 *  [End Status]
 *
 */
public class ATronStateModel extends RendererStateModel
{
  private static Logger logger = 
    Logger.getLogger("com.cidero.bridge.audiotron");

  public ATronStateModel()
  {
  }
  
  public void setChanged()
  {
    super.setChanged();
  }
  
  /**
   *  Parse a status response
   */
  public boolean parse( String status )
  {
    BufferedReader reader = 
      new BufferedReader( new StringReader( status ) );
    
    try 
    {
      // First line should be '[Status]'
      String line = reader.readLine();
      if( line == null || ! line.startsWith("[Status") )
      {
        logger.warning("Status response syntax error (doesn't start with [Status]");
        return false;
      }
    
      while( (line = reader.readLine()) != null )
      {
        logger.finest("Line: " + line );
        
        if( line.startsWith("[End") )
        {
          return true;
        }
        
        // Split up field into name,value pair 

        String[] tmp = line.split("=");
        
        String name = tmp[0];
        String value = tmp[1];
        
        if( name.startsWith("CurrVol") )
        {
          //logger.fine("RendererStateModel - CurrVol: " + value );
          setVolume( value );
        }
        else if( name.startsWith("MUTE") )
        {
          //logger.fine("RendererStateModel - Mute: " + value );
          setMute( value );
        }
        else if( name.startsWith("STOP") )
        {
          //logger.fine("RendererStateModel - StopLED: " + value );
          if( value.equals("1") )
            setTransportState( AVTransport.STATE_STOPPED );
        }
        else if( name.startsWith("PLAY") )
        {
          //logger.fine("RendererStateModel - PlayLED: " + value );
          if( value.equals("1") )
            setTransportState( AVTransport.STATE_PLAYING );
        }
        else if( name.startsWith("PAUSE") )
        {
          //logger.fine("RendererStateModel - PauseLED: " + value );
          if( value.equals("1") )
            setTransportState( AVTransport.STATE_PAUSED_PLAYBACK );
        }
      }
    }
    catch( IOException e )
    {
      logger.warning("Exception parsing ATron status msg '" + 
                     status + "'" + e );
      return false;
    }
    
    return true;
  }

  /**
   * Simple test code
   */
  public static void main( String args[] )
  {
    StringBuffer buf = new StringBuffer();

    buf.append( "[Status]\n" );
    buf.append( "CurrVolume=-2\n" );    
    buf.append( "[End Status]\n" );

    ATronStateModel status = new ATronStateModel();
    
    status.parse( buf.toString() );

    System.out.println("Volume, mute = " + status.getVolume() + 
                       " " + status.getMute() );
  }

}


