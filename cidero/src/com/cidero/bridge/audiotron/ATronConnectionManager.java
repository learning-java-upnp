/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ATronConnectionManager.java,v $
 *
 */

package com.cidero.bridge.audiotron;

import java.util.logging.Logger;

import org.cybergarage.upnp.device.InvalidDescriptionException;

import com.cidero.upnp.ConnectionManager;


/**
 * Audiotron ConnectionManager class
 *
 * This class doesn't really do anything - all the actions use the 
 * default handler methods in the ConnectionManger superclass. This
 * is due to the fact that all the supported actions are just get's
 * of state variables - there aren't any that change any state info
 */
public class ATronConnectionManager  extends ConnectionManager
{
  private static Logger logger = 
    Logger.getLogger("com.cidero.bridge.audiotron");

  ATronMediaRenderer    mediaRenderer;
  
  /**
   * Creates a new <code>ConnectionManager</code> instance.
   *
   */
  public ATronConnectionManager( ATronMediaRenderer mediaRenderer )
    throws InvalidDescriptionException
  {
    super( mediaRenderer );

    logger.fine("Entered ATronConnectionManager constructor");

    this.mediaRenderer = mediaRenderer;

    initializeStateVariables();

    logger.fine("Leaving ATronConnectionManager constructor");
  }

  /**
   *  Initialize state variables. Note that required state variables 
   *  have been given 'reasonable' default values in the base class
   *  version of this routine 
   */
  public void initializeStateVariables()
  {
    // Audiotron acts as a sink for audio data only
    setStateVariable("SourceProtocolInfo", "" );
    setStateVariable("SinkProtocolInfo",
                     "http-get:*:audio/mpeg:*,http-get:*:audio/mpegurl:*" );
    // TODO: Add WMA mime type to above
  }
	
}

