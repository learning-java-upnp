/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: AbstractMediaRendererBridge.java,v $
 *
 */

package com.cidero.bridge;

import java.io.IOException;
import java.util.logging.Logger;

import org.cybergarage.upnp.device.InvalidDescriptionException;

import com.cidero.renderer.AbstractMediaRenderer;


/**
 * Base class for UPnP bridges for non-UPnP-compliant media renderer
 * devices (most commonly music playback devices that support HTTP-GET) 
 */
public abstract class AbstractMediaRendererBridge
                extends AbstractMediaRenderer
//                implements NotifyListener, EventListener
 implements IMediaRendererBridge
{
  private static Logger logger = Logger.getLogger("com.cidero.bridge");

  // HTTP Proxy server that sits inbetween the device and the UPNP MediaServer
  // A single HTTP server is shared by all instances of this class
  private String ipAddr = "UnknownIPAddr";

  //  HTTP Proxy server that sits inbetween the device and the UPNP MediaServer
  //  A single HTTP server is shared by all instances of this class
  static HTTPProxyServer httpServer;
  static int httpServerPort;   // configurable via property file

  /**
   * Constructor
   *
   * @param     description file name
   *
   * @exception InvalidDescriptionException if there is an error parsing
   *            the UPnP XML device description
   */
  public AbstractMediaRendererBridge( String descriptionFileName,
                                  String ipAddr, String friendlyName )
    throws InvalidDescriptionException
  {
    super(descriptionFileName, friendlyName);
    this.ipAddr = ipAddr;
	
    // Start HTTP server if this is the first bridge device instantiated
    if( httpServer == null )
    {
      try 
      {
        // Constructor starts server up on separate thread
        httpServer = new HTTPProxyServer( this, httpServerPort );
      }
      catch( IOException e )
      {
        logger.fine("Exception creating HTTP server: " + e );
        System.exit(-1);
      }
    }
    
    logger.fine("Leaving AbstractMediaRendererBridge base class constructor " );
  }

  /**
   *  Load Bridge preferences. Preference information comes from 
   *  2 sources - the default set of preferences that come with the program,
   *  and the (optional) user-specific set stored in the user's home
   *  directory. The default set is located in the Java classpath under 
   *  the 'properties' subdirectory. The user-specific set is stored 
   *  in the user's home directory, under the '.cidero' subdirectory 
   */
  public static void loadPreferences()
  {
	AbstractMediaRenderer.loadPreferences();

    // Default server port is 8081
    httpServerPort = pref.getInt("httpServerPort", 8081);
    logger.info("HTTP server port: " + httpServerPort );

    logger.fine("Leaving loadPreference " );
  }

	/**
	 * Set/Get the IP address of the underlying non-UPnP device
	 */
	public void setIPAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cidero.bridge.IMediaRendererBridge#getIPAddr()
	 */
	public String getIPAddr() {
		return ipAddr;
	}

}

