package com.cidero.bridge;

import com.cidero.renderer.IMediaRenderer;


public interface IMediaRendererBridge extends IMediaRenderer {

	/**
	 *  Get the pathname portion of the proxy URL for underlying device.
	 *  This is the URL the device will use to do HTTP-GET's from the 
	 *  UPnP bridge's HTTP server
	 *
	 *  Examples:
	 *
	 *     "/UPnPBridge/device/Prismiq-1"
	 *     "/UPnPBridge/shoutcast/Homecast1"
	 */
	String getProxyUrlPath();

	String getIPAddr();

}