/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: BridgeDeviceManager.java,v $
 *
 */

package com.cidero.bridge;


/**
 * The BridgeDeviceManager interface class provides the interface 
 * for managing one or more devices of a given type. Typical
 * operations include scanning for the presence of devices of 
 * a given type, initializing any located devices, and starting
 * them running.
 */

public interface BridgeDeviceManager
{
  /** 
   *  Scan for all instances of a given device type
   */
  //abstract public void scan();

  /**
   *  Start all devices of a given type
   */
  abstract public void start();
  abstract public void stop();

  // Maybe need something like this??
  //abstract public List getDevices();

}

