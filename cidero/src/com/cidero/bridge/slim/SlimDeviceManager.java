/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: SlimDeviceManager.java,v $
 *
 */

package com.cidero.bridge.slim;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import org.cybergarage.upnp.device.InvalidDescriptionException;

import com.cidero.bridge.BridgeDeviceManager;
import com.cidero.util.MrUtil;
import com.cidero.util.AppPreferences;

/**
 *
 *  This class handles the management of multiple SqueezeMediaRenderer 
 *  'bridge' device instances. The real work with regard to the translation
 *  of UPnP commands to their Squeeze 'media agent' equivalents is done in the 
 *  SqueezeMediaRenderer class.
 * 
 * 
 *  Notes:
 *
 */
public class SlimDeviceManager implements BridgeDeviceManager,
                                          SlimUdpBroadcastListener
{
  private static Logger logger = Logger.getLogger("com.cidero.bridge.slim");

  private final static int SLIM_MAX_UNITS = 10;

  //int   heartbeatPort;
  List  activeDeviceList = new ArrayList();

  static AppPreferences pref;

  SlimUdpReadThread udpReadThread;
  DatagramSocket udpSocket;


  public SlimDeviceManager()
  {
    //loadPreferences();
    //instantiateDeviceBridges();

  }



  public static void loadPreferences()
  {
    // Load shared & user-specific preferences for this application
    pref = new AppPreferences(".cidero");

    if( ! pref.load( "Bridge", "SlimDeviceManager" ) )
    {
      logger.severe("Missing preferences file - exiting");
      System.exit(-1);
    }
  }

  public void savePreferences()
  {
    pref.saveUserPreferences( "Bridge", "SlimDeviceManager" );
  }

  public static AppPreferences getPreferences()
  {
    if( pref == null )
      loadPreferences();
    
    return pref;
  }

  public void udpBroadcastPacketReceived( DatagramPacket packet )
  {
    try 
    {
      byte[] data = packet.getData();
      if( data[0] == 'd' )
      {
        SlimDiscoveryReq discoveryReq = new SlimDiscoveryReq( packet );
        logger.info( discoveryReq.toString() );

        // Respond to the discovery request with a UDP unicast to the port
        // of the requestor
        
        SlimDiscoveryResponse response =
          new SlimDiscoveryResponse("onrush", discoveryReq );

        logger.info("responding to request" );

        udpSocket.send( response.buildDatagramPacket() );
      }
      else
      {
        logger.info("Discarding unknown packet type");
      }

    }
    catch( Exception e )
    {
      logger.info("Exception processing UDP packet");
    }
    
  }


  /** 
   *  Load device managers specified in property file
   */
  public void instantiateDeviceBridges()
  {
    logger.fine("Entering getProperties " );

    // Default heartbeat port is 3650
    //heartbeatPort = props.getProperty( "heartbeatPort", 3650 );

    for( int n = 1 ; n <= SLIM_MAX_UNITS ; n++ )
    {
      String propPrefix = "slim" + n + "_";
      
      String enabled = pref.get( propPrefix + "enabled" );
      if( (enabled == null) || (! enabled.equals("true")) ) 
        continue;
      
      String ipAddr = pref.get( propPrefix + "ipaddr" );
      String name = pref.get( propPrefix + "name" );

      if( ipAddr == null || name == null )
        continue;
      
      logger.info("Creating UPnP bridge for Slim Unit '" + name + "'" );
      logger.info("IPAddr = '" + ipAddr + "'" );

      try 
      {
        SlimMediaRenderer bridgeDev = 
          new SlimMediaRenderer( ipAddr, name );

        activeDeviceList.add( bridgeDev );

        bridgeDev.start();
      }
      catch( InvalidDescriptionException e )
      {
        logger.severe("SlimDeviceManager: Invalid UPnP device description" + e );
      }
    }

    logger.fine("Leaving instantiateDeviceBridges " );
  }

  public void start()
  {
    logger.info("Starting SlimDeviceManager " );

    // Create a datagram socket and bind to free address chosen by OS
    try 
    {
      udpSocket = new DatagramSocket( 3483 );
    }
    catch( SocketException e )
    {
      logger.severe("Failed to create socket for outbound UDP traffic");
      System.exit(-1);
    }
    
    // Start up Slim devices UDP broadcast listener thread
    udpReadThread = new SlimUdpReadThread( this, udpSocket );
    udpReadThread.start();

  }

  public void stop()
  {
    //System.out.println("Stopping SlimDeviceManager " );
    logger.info("Stopping SlimDeviceManager " );

    /*
    for( int n = 0 ; n < activeDeviceList.size(); n++ )
    {
      SlimMediaRenderer bridgeDev = 
         (SlimMediaRenderer)activeDeviceList.get(n);
      bridgeDev.stop();
    }
    */
  }


  /**
   *  Method invoked by SlimDeviceMonitor whenever a UDP packet
   *  arrives on port 3650. If this is a new device, create a 
   *  UPnP <--> Slim bridge device instance and add it to the
   *  list of managed devices.
   */

  /*  Disabled for now...
  public void heartbeatPacketReceived( String ipAddr )
  {
    //logger.finest("processing heartbeat packet from " + ipAddr );

    AbstractMediaRendererBridge bridgeDev;

    for( int n = 0 ; n < activeDeviceList.size() ; n++ )
    {
      bridgeDev = (AbstractMediaRendererBridge)activeDeviceList.get(n);

      if( bridgeDev.getIPAddr().equals( ipAddr ) )
      {
        //logger.finest("Slim device at IP " + ipAddr + " updated" );

        return;
      }

    }
    
    //
    // No existing device for this IP - add one and start it running
    //

    logger.info("Found new Slim device at IP: " + ipAddr +
                " - creating UPnP bridge" );

    try 
    {
      bridgeDev = 
        new AbstractMediaRendererBridge( new SlimMediaRenderer( ipAddr ) );

      activeDeviceList.add( bridgeDev );

      bridgeDev.start();
    }
    catch( InvalidDescriptionException e )
    {
      logger.severe("SlimDeviceManager: Invalid UPnP device description" );
    }
    
  }
  */


  public static void main(String args[])
  {
    SlimDeviceManager devManager = new SlimDeviceManager();

    devManager.start();
  }
  

}

