/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: SlimDiscoveryResponse.java,v $
 *
 */

package com.cidero.bridge.slim;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

/**
 *  Slim UDP Discovery response (Modified SliMP3 format) 
 * 
 *  18 byte message
 *
 *  The 'normal' SliMP3 discovery response is as follows, according
 *  to the Slim 'SLIMP3ClientProtocol' document
 *
 *    Field    Value/Description
 *    --------------------------------------------------------------
 *    0        'D' as in discovery  
 *    1        Reserved
 *    2..5     Servers IP Addr, or 0 if same as in UDP packet hdr
 *    6..7     Server port
 *    8..17    Reserved
 *
 *  The above format is apparently not followed in the Squeezebox version
 *  of Slimserver. Instead, it appears to be this:
 *
 *    Field    Value/Description
 *    --------------------------------------------------------------
 *    0        'D' as in discovery  
 *    1..17    Server hostname (or server logical name?)
 *
 */
public class SlimDiscoveryResponse
{
  private static Logger logger = Logger.getLogger("com.cidero.bridge.slim");

  String   serverName;
  SlimDiscoveryReq request;

  public SlimDiscoveryResponse( String serverName,
                                SlimDiscoveryReq request )
  {
    if( serverName.length() > 17 )
    {
      logger.warning("server name length exceeded - truncating to 17 bytes");
      this.serverName = serverName.substring(0,17);
    }
    else
    {
      this.serverName = serverName;
    }

    this.request = request;
  }
  
  /**
   *  Build a datagram packet for the discovery response
   */
  public DatagramPacket buildDatagramPacket()
  throws UnknownHostException
  {
    byte[] data = new byte[18];
    
    data[0] = 'D';

    byte[] serverNameBytes = serverName.getBytes();
    int n;
    for( n = 1 ; (n <= serverNameBytes.length) && (n < 18); n++ )
      data[n] = serverNameBytes[n-1];
      //      data[n+1] = 0;

    while( n < 18 )
      data[n++] = 0;
    
    DatagramPacket dgmPacket = 
        new DatagramPacket( data, data.length,
                            request.getSrcInetAddress(),
                            request.getSrcPort() );

    logger.info("DatagramPacket len: " + data.length + " Inet" +
                request.getSrcInetAddress().toString() + " Port: " +
                request.getSrcPort() );

    return dgmPacket;
  }
  
  public String toString()
  {
    return "DiscoveryResponse: serverName: " + serverName;
  }
  
}
