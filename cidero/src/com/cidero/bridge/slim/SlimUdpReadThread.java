/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: SlimUdpReadThread.java,v $
 *
 */

package com.cidero.bridge.slim;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Logger;

/**
 *  Thread that reads incoming UDP packets on port 3483 and takes 
 *  appropriate action.
 *
 *  When a squeezebox client starts up, it does a UDP broadcast of
 *  a SLIMP3 discover request, and waits for a response containing
 *  the server address 
 *
 */
public class SlimUdpReadThread implements Runnable
{
  private static Logger logger = Logger.getLogger("com.cidero.bridge.slim");

  DatagramSocket udpSocket;
  private static int udpPort = 3483;

  SlimUdpBroadcastListener udpBroadcastListener;

  public SlimUdpReadThread( SlimUdpBroadcastListener udpBroadcastListener,
                            DatagramSocket udpSocket )
  {
    this.udpBroadcastListener = udpBroadcastListener;
    this.udpSocket = udpSocket;
  }
  
  private Thread thread = null;  // for clean shutdown via stop()
  
  public void run()
  {
    logger.info("Running...");

    Thread thisThread = Thread.currentThread();
    
    byte[] buf = new byte[8192];

    /*
    try 
    {
      ds = new DatagramSocket( udpPort );  // default: 3483
      ds.setBroadcast( true );
    }
    catch( SocketException e )
    {
      logger.warning("Error: " + e );
      return;
    }
    */
    
    DatagramPacket recvPacket = new DatagramPacket( buf, buf.length );

    while( thread == thisThread )
    {
      //
      // Read packets forever
      //
      try 
      {
        udpSocket.receive( recvPacket );

        logger.info("Got UDP packet, length = " 
                    + recvPacket.getLength() 
                    + " IPAddress = " + recvPacket.getAddress() );

        // For some reason, getAddress() can return address with a leading
        // '/'. Strip it off if present
        String ipAddr = recvPacket.getAddress().getHostAddress();

        //        int index = ipAddr.lastIndexOf("/");
        
        //        if( index >= 0 )
        //          ipAddr = ipAddr.substring( index );
        
        if( udpBroadcastListener != null )
          udpBroadcastListener.udpBroadcastPacketReceived( recvPacket );

      }
      catch( Exception e )
      {
        logger.warning("Error: " + e );
        return;
      }
      
    }
    
    logger.info("Shutting down...");

  }

  public void start()
  {
    thread = new Thread( this );
    thread.start();  // Invokes run()
  }
  
  public void stop()
  {
    thread = null;
  }
  
  
}
