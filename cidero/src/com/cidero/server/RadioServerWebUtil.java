/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: RadioServerWebUtil.java,v $
 *
 */
package com.cidero.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.logging.Logger;

import com.cidero.util.*;
import com.cidero.upnp.*;

/**
 *  Web interface utility class for Radio Server
 */
public class RadioServerWebUtil
{
  private final static Logger logger = Logger.getLogger("com.cidero.server");

  
  /**
   *  Write the HTML header to the specified PrintWriter (output stream)
   */
  public static void writeHtmlHeader( PrintWriter writer, String pageTitle )
  {
    writer.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
    writer.println("<html>");
    writer.println("<head>");
    writer.println("<meta http-equiv=\"content-type\" content=\"text/html; charset=ISO-8859-1\">");
    writer.println("<title>" + pageTitle + "</title>");
    writer.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet.css\" />");
    writer.println("</head>");

    writer.println("<body>");

    writer.println("<div id=\"pageLogo\" title=\"banner\">");
    writer.println("<img src=\"cideroLogo96.gif\" alt=\"Logo\" align=\"left\" />");
    writer.println("</div>");

    writer.flush();
  }
  

  /**
   * Main routine
   * 
   */
  public static void main( String args[] )
  {
    try
    {
    }
    catch( Exception e )
    {
      logger.severe( "Error starting RadioServer" + e );
      System.exit(-1);
    }
  }

}
