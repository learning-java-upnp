/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: HTTPTestClient.java,v $
 *
 */

package com.cidero.util;

import java.net.MalformedURLException;
import java.net.URL;

import com.cidero.http.HTTP;
import com.cidero.http.HTTPConnection;
import com.cidero.http.HTTPRequest;
import com.cidero.http.HTTPResponse;
import com.cidero.http.HTTPStatus;


/**
 * Simple HTTP test client useful for a variety of debugging purposes
 *
 */
public class HTTPTestClient
{
  /**
   * Creates a new <code>ShoutcastOutputStream</code> instance.
   *
   */
  public HTTPTestClient()
  {
  }

  /** 
   * Read data from  URL
   */
  public void readURL( String urlString ) throws MalformedURLException
  {
    System.out.println( "!!!!!!!!!!Session: readAndQueueURL " + urlString + "\n\n");

    URL url = new URL( urlString );

    // Following are all amped out entries
    //   URL url = new URL( "http://rs4.radiostreamer.com:9230/" );
    //URL url = new URL( "http://war.str3am.com:7090/" );

    // Following are all radio paradise entries
    //URL url = new URL( "http://64.236.34.196:80/stream/2001" );
    //URL url = new URL( "http://64.236.34.97:80/stream/2001" );
    //URL url = new URL( "http://64.236.34.67:80/stream/2001" );
    //URL url = new URL( "http://64.236.34.196:80/stream/2001" );
    //URL url = new URL( "http://64.236.34.4:80/stream/2001" );
    //URL url = new URL( "http://207.200.96.226:8048" );

    try 
    {
      System.out.println( "Session: Opening connection to: " +
                          url );

      HTTPConnection connection = new HTTPConnection();

      HTTPRequest request = new HTTPRequest( HTTP.GET, url );
      System.out.println("Adding headers");
      request.addHeader("Host", "64.236.34.196");
      request.addHeader("User-Agent", "Cidero_Bridge");
      request.addHeader("Accept", "*/*");
      request.addHeader( "Icy-Metadata", "1" );
      System.out.println("Added header");
      System.out.println("Request is:\n" + request.toString() );

      HTTPResponse response = connection.sendRequest( request, false );
      
      System.out.println("Response first line: " + response.getFirstLine() );
      System.out.println("Response code: " + response.getStatusCode() );
      for( int n = 0 ; n < response.getNumHeaders() ; n++ )
      {
        System.out.println( response.getHeader(n).toString() );
      }

      if( response.getStatusCode() == HTTPStatus.OK )
      {
        /*
        byte[] content = response.getContent(); 
        if (content == null)
          System.out.println("No content " );
        else
          System.out.println("Got content - size = " + content.length );
        */

        String content = response.getContentString(); 
        System.out.println(content);

        // Processing...
        response.releaseConnection();

      }
    }
		catch( Exception e )
    {
      e.printStackTrace();
      System.out.println("Session: readURL: Exception" + e );
		}
  }

  public static void main( String args[] )
  {
    try 
    {
      HTTPTestClient client = new HTTPTestClient();
      client.readURL( args[0] );
    }
		catch( Exception e )
    {
      System.out.println("Session: readURL: Exception" + e );
		}
  }

}

