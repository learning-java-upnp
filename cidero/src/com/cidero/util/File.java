/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: File.java,v $
 *
 */

package com.cidero.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;


/**
 *  Drop-in replacement class for java.io.File that supports
 *  both the 'standard' java.io.File class and the CIFS (SMB) 
 *  SmbFile class.
 *
 *  If a package uses java.io.File, just change the 
 *  import statements in the package from java.io.File to 
 *  com.cidero.util.File to make the switch. No
 *  other code changes should be necessary.
 *
 *  Notes: 
 *
 *   Not *all* file methods are fully supported by the 
 *   underlying SmbFile implementation
 *
 *   This class, along with the RandomAccessFile class, were
 *   original created to allow the helliker MP3 ID3 package to
 *   work with SMB filesystems as well as local filesystems 
 */
public class File
{
  static final int STD_FILE = 1;
  static final int SMB_FILE = 2;

  AbstractFile file;

  /**
   *  Constructor. Invoke the constructor of the wrapped class, trap
   *  the SMB-specific exceptions and throw the 'standard' File
   *  equivalents (as close as we can)
   */
  public File( File parent, String child )
  {
    if( parent.getFileType() == SMB_FILE ) 
      file = new SMBFileWrapper( parent, child );
    else
      file = new StdFileWrapper( parent, child );
  }

  public File( String pathname )
  {
    System.out.print("Using wrapper for File( pathnaem )");
    
    if( pathname.startsWith("smb:") )
      file = new SMBFileWrapper( pathname );
    else  
      file = new StdFileWrapper( pathname );
  }

  public File( String parent, String child )
  {
    System.out.print("Using wrapper for File( parent,child )");

    if( parent.startsWith("smb:") )
      file = new SMBFileWrapper( parent, child );
    else
      file = new StdFileWrapper( parent, child );
  }

  public File( URI uri ) throws IllegalArgumentException
  {
    System.out.print("Using wrapper for File( uri )");

    if( uri.toString().startsWith("smb") )
      file = new SMBFileWrapper( uri );
    else
      file = new StdFileWrapper( uri );
  }
  
  public Object getUnderlyingFileObj() 
  {
    return file.getUnderlyingFileObj();
  }

  public int getFileType() 
  {
    return file.getFileType();
  }
  
  public boolean canRead() throws SecurityException {
    return file.canRead();
  }
  public boolean canWrite() throws SecurityException {
    return file.canWrite();
  }
  /*
  public int compareTo( File pathname ) {
    return file.compareTo( pathname );
  }
  public int compareTo( Object o ) {
    return file.compareTo( o );
  }
  */
  public boolean createNewFile() throws IOException {
    return file.createNewFile();
  }
  public boolean delete() throws SecurityException {
    return file.delete();
  }
  public void deleteOnExit() throws SecurityException {
    file.deleteOnExit();
  }
  public boolean equals( Object o ) {
    return file.equals( o );
  }
  public boolean exists() throws SecurityException {
    return file.exists();
  }
  /*
  public File getAbsoluteFile() throws SecurityException {
    return file.getAbsoluteFile();
  }
  */
  public String getAbsolutePath() throws SecurityException {
    return file.getAbsolutePath();
  }
  /*
  public File getCanonicalFile() throws SecurityException {
    return file.getCanonicalFile();
  }
  */
  public String getCanonicalPath() throws IOException {
    return file.getCanonicalPath();
  }
  public String getName() {
    return file.getName();
  }
  public String getParent() {
    return file.getParent();
  }
  /*
  public File getParentFile() {
    return file.getParentFile();
  }
  */
  public String getPath() {
    return file.getPath();
  }
  public int hashCode() {
    return file.hashCode();
  }
  public boolean isAbsolute() {
    return file.isAbsolute();
  }
  public boolean isDirectory() throws SecurityException {
    return file.isDirectory();
  }
  public boolean isFile() throws SecurityException {
    return file.isFile();
  }
  public boolean isHidden() throws SecurityException {
    return file.isHidden();
  }
  public long lastModified() throws SecurityException {
    return file.lastModified();
  }
  public long length() throws SecurityException {
    return file.length();
  }
  public String[] list() throws SecurityException {
    return file.list();
  }
  /*
  public String[] list( FilenameFilter filter ) throws SecurityException {
    return file.list( filter );
  }
  public File[] listFiles() throws SecurityException {
    return file.listFiles();
  }
  public File[] listFiles( FileFilter filter ) throws SecurityException {
    return file.listFiles( filter );
  }
  public File[] listFiles( FilenameFilter filter ) throws SecurityException {
    return file.listFiles( filter );
  }
  public static File[] listRoots() {
    return file.listRoots();
  }
  */
  public boolean mkdir() throws SecurityException {
    return file.mkdir();
  }
  public boolean mkdirs() throws SecurityException {
    return file.mkdirs();
  }
  /*
  public boolean renameTo( File dest ) throws SecurityException {
    return file.renameTo( dest );
  }
  */
  public boolean setLastModified( long time ) throws SecurityException {
    return file.setLastModified( time );
  }
  public boolean setReadOnly() throws SecurityException {
    return file.setReadOnly();
  }
  public String toString() {
    return file.toString();
  }
  public URI toURI() {
    return file.toURI();
  }
  public URL toURL() throws MalformedURLException {
    return file.toURL();
  }
}

