/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: RandomAccessFile.java,v $
 *
 */

package com.cidero.util;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *  Drop-in replacement class for java.io.RandomAccessFile that supports
 *  both 'normal' filesystem and CIFS (SMB) filesystem access.
 *
 *  If a package uses java.io.RandomAccessFile, just change the 
 *  import statements in the package from java.io.RandomAccessFile to 
 *  com.cidero.util.RandomAccessFile to make the switch. No
 *  other code changes should be necessary.
 */
public class RandomAccessFile implements DataOutput, DataInput
{
  AbstractRandomAccessFile raf;
  
  /**
   *  Constructor.
   *  NEED CLEVER WAY TO MAKE THIS CONSTRUCTOR WORK
   */
  public RandomAccessFile( com.cidero.util.File file, String mode )
    throws FileNotFoundException
  {
    if( file.getFileType() == com.cidero.util.File.SMB_FILE )
      raf = new SMBRandomAccessFileWrapper( file, mode );
    else  
      raf = new StdRandomAccessFileWrapper( file, mode );
  }


  /**
   *  Constructor. Invoke the constructor of the wrapped class, trap
   *  the SMB-specific exceptions and throw the 'standard' RandomAccessFile
   *  equivalents (as close as we can)
   */
  public RandomAccessFile( String filename, String mode )
    throws FileNotFoundException
  {
    if( filename.startsWith("smb") )
      raf = new SMBRandomAccessFileWrapper( filename, mode );
    else  
      raf = new StdRandomAccessFileWrapper( filename, mode );
  }
  
  public int read() throws IOException {
    return raf.read();
  }
  public int read( byte b[] ) throws IOException {
    return raf.read( b );
  }
  public int read( byte b[], int off, int len ) throws IOException {
    return raf.read( b, off, len );
  }
  public long getFilePointer() throws IOException {
    return raf.getFilePointer();
  }
  public void seek( long pos ) throws IOException {
    raf.seek( pos );
  }
  public long length() throws IOException {
    return raf.length();
  }
  public void setLength( long newLength ) throws IOException {
    raf.setLength( newLength );
  }
  public void close() throws IOException {
    raf.close();
  }
  
  //
  // DataInput interface methods 
  //
  public boolean readBoolean() throws IOException {
    return raf.readBoolean();
  }
  public byte readByte() throws IOException {
    return raf.readByte();
  }
  public int readUnsignedByte() throws IOException {
    return raf.readUnsignedByte();
  }
  public short readShort() throws IOException {
    return raf.readShort();
  }
  public int readUnsignedShort() throws IOException {
    return raf.readUnsignedShort();
  }
  public char readChar() throws IOException {
    return raf.readChar();
  }
  public int readInt() throws IOException {
    return raf.readInt();
  }
  public long readLong() throws IOException {
    return raf.readLong();
  }
  public float readFloat() throws IOException {
    return raf.readFloat();
  }
  public double readDouble() throws IOException {
    return raf.readDouble();
  }
  public void readFully( byte b[] ) throws IOException{
    raf.readFully( b );
  }
  public void readFully( byte b[], int off, int len ) throws IOException {
    raf.readFully( b, off, len );
  }
  public String readLine() throws IOException {
    return raf.readLine();
  }
  public String readUTF() throws IOException {
    return raf.readUTF();
  }
  public int skipBytes( int n ) throws IOException {
    return raf.skipBytes( n );
  }
  
  //
  // DataOutput interface methods 
  //
  public void write( int b ) throws IOException {
    raf.write( b );
  }
  public void write( byte b[] ) throws IOException {
    raf.write( b );
  }
  public void write( byte b[], int off, int len ) throws IOException {
    raf.write( b, off, len );
  }
  public void writeBoolean( boolean v ) throws IOException {
    raf.writeBoolean( v );
  }
  public void writeByte( int v ) throws IOException {
    raf.writeByte( v );
  }
  public void writeShort( int v ) throws IOException {
    raf.writeShort( v );
  }
  
  public void writeChar( int v ) throws IOException {
    raf.writeChar( v );
  }
  
  public void writeInt( int v ) throws IOException {
    raf.writeInt( v );
  }
  
  public void writeLong( long v ) throws IOException {
    raf.writeLong( v );
  }
  
  public void writeFloat( float v ) throws IOException  {
    raf.writeFloat( v );
  }
  
  public void writeDouble( double v ) throws IOException {
    raf.writeDouble( v );
  }
  
  public void writeBytes( String s ) throws IOException {
    raf.writeBytes( s );
  }
  
  public void writeChars( String s ) throws IOException {
    raf.writeChars( s );
  }
  
  public void writeUTF( String str ) throws IOException {
    raf.writeUTF( str );
  }

}

