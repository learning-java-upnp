/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: AbstractRandomAccessFile.java,v $
 *
 */


package com.cidero.util;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 *  Class to allow use of multiple mechanisms to access files in a 
 *  random manner.  Used to hide details of RandomAccessFile (local filesystem)
 *  and SmbRandomAccessFile (CIFS filesystem) from applications.
 *
 *  The Java class library probably should have placed an abstract
 *  class below java.io.RandomAccessFile (as with InputStream), 
 *  but it didn't - hence the need for this class to support random access 
 *  to filesystems other than the default OS filesystem (like SMB).
 *
 */
public interface AbstractRandomAccessFile extends DataOutput, DataInput
{
  public int read() throws IOException;
  public int read( byte b[] ) throws IOException;
  public int read( byte b[], int off, int len ) throws IOException;

  public long getFilePointer() throws IOException;
  public void seek( long pos ) throws IOException;
  public long length() throws IOException;
  public void setLength( long newLength ) throws IOException;
  public void close() throws IOException;

  // DataInput methods 
  //public boolean readBoolean() throws IOException;
  //public byte readByte() throws IOException;
  //public int readUnsignedByte() throws IOException;
  //public short readShort() throws IOException;
  //public int readUnsignedShort() throws IOException;
  //public char readChar() throws IOException;
  //public int readInt() throws IOException;
  //public long readLong() throws IOException;
  //public float readFloat() throws IOException;
  //public double readDouble() throws IOException;
  //public void readFully( byte b[] ) throws IOException;
  //public void readFully( byte b[], int off, int len ) throws IOException;
  //public String readLine() throws IOException;
  //public String readUTF() throws IOException;
  //public int skipBytes( int n ) throws IOException;

  // DataOutput methods 
  //public void write( int b ) throws IOException;
  //public void write( byte b[] ) throws IOException;
  //public void write( byte b[], int off, int len ) throws IOException;
  //public void writeBoolean( boolean v ) throws IOException;
  //public void writeByte( int v ) throws IOException;
  //public void writeShort( int v ) throws IOException;
  //public void writeChar( int v ) throws IOException;
  //public void writeInt( int v ) throws IOException;
  //public void writeLong( long v ) throws IOException;
  //public void writeFloat( float v ) throws IOException;
  //public void writeDouble( double v ) throws IOException;
  //public void writeBytes( String s ) throws IOException;
  //public void writeChars( String s ) throws IOException;
  //public void writeUTF( String str ) throws IOException;

}

