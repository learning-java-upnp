/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: SMBFileWrapper.java,v $
 *
 */

package com.cidero.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

/**
 *  Wrapper class for SMBFile. This allows it to be used
 *  within the custom File implementation framework.
 *  (Need an object that implements the AbstractFile
 *  interface)
 */
public class SMBFileWrapper implements AbstractFile
{
  SmbFile file;
  
  /**
   *  Constructor. Invoke the constructor of the wrapped class, trap
   *  the SMB-specific exceptions and throw the 'standard' File
   *  equivalents (as close as we can)
   */
  public SMBFileWrapper( com.cidero.util.File parent, String child )
  {
    try
    {
      file = new SmbFile( (SmbFile)parent.getUnderlyingFileObj(), child );
    }
    catch( MalformedURLException e )
    {
      e.printStackTrace();
      //System.exit(-1);
    }
    catch( UnknownHostException e )
    {
      e.printStackTrace();
      System.exit(-1);
    }
  }

  public SMBFileWrapper( String pathname )
  {
    try
    {
      file = new SmbFile( pathname );
    }
    catch( MalformedURLException e )
    {
      e.printStackTrace();
      //System.exit(-1);
    }
  }

  public SMBFileWrapper( String parent, String child )
  {
    try 
    {
      file = new SmbFile( parent, child );
    }
    catch( MalformedURLException e )
    {
      e.printStackTrace();
      //System.exit(-1);
    }
  }

  public SMBFileWrapper( URI uri ) throws IllegalArgumentException
  {
    try
    {
      file = new SmbFile( uri.toURL() );
    }
    catch( MalformedURLException e )
    {
      throw new IllegalArgumentException( "Bad URL in SMBFile constructor" );
    }
    
  }
  
  public Object getUnderlyingFileObj() 
  {
    return (Object)file;
  }

  public int getFileType()
  {
    return com.cidero.util.File.SMB_FILE;
  }
  

  public boolean canRead() throws SecurityException {
    try 
    {
      return file.canRead();
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  public boolean canWrite() throws SecurityException {
    try 
    {
      return file.canWrite();
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  /*
  public int compareTo( File pathname ) {
    return file.compareTo( pathname );
  }
  public int compareTo( Object o ) {
    return file.compareTo( o );
  }
  */
  public boolean createNewFile() throws IOException {
    try 
    {
      file.createNewFile();
      return true;
    }
    catch( SmbException e )
    {
      throw new IOException();
    }
  }
  public boolean delete() throws SecurityException {
    try
    {
      file.delete();
      return true;
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  public void deleteOnExit() throws SecurityException {
    System.out.println("SMBFile.deleteOnExit() not supported");
    throw new SecurityException();
  }
  public boolean equals( Object o ) {
    return file.equals( o );
  }
  public boolean exists() throws SecurityException {
    try
    {
      return file.exists();
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  /*
  public File getAbsoluteFile() throws SecurityException {
    return file.getAbsoluteFile();
  }
  */
  public String getAbsolutePath() throws SecurityException {
    System.out.println("SMBFile.getAbsolutePath() not supported");
    throw new SecurityException();
  }
  /*
  public File getCanonicalFile() throws SecurityException {
    return file.getCanonicalFile();
  }
  */
  public String getCanonicalPath() throws IOException {
    return file.getCanonicalPath();
  }
  public String getName() {
    return file.getName();
  }
  public String getParent() {
    return file.getParent();
  }
  /*
  public File getParentFile() {
    return file.getParentFile();
  }
  */
  public String getPath() {
    return file.getPath();
  }
  public int hashCode() {
    return file.hashCode();
  }
  public boolean isAbsolute() {
    System.out.println("SMBFile.isAbsolute() not supported");
    return false;
  }
  public boolean isDirectory() throws SecurityException {
    try 
    {
      return file.isDirectory();
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  public boolean isFile() throws SecurityException {
    try 
    {
      return file.isFile();
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  public boolean isHidden() throws SecurityException {
    try 
    {
      return file.isHidden();
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  public long lastModified() throws SecurityException {
    try 
    {
      return file.lastModified();
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  public long length() throws SecurityException {
    try 
    {
      return file.length();
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  public String[] list() throws SecurityException {
    try 
    {
      return file.list();
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  /*
  public String[] list( FilenameFilter filter ) throws SecurityException {
    return file.list( filter );
  }
  public File[] listFiles() throws SecurityException {
    return file.listFiles();
  }
  public File[] listFiles( FileFilter filter ) throws SecurityException {
    return file.listFiles( filter );
  }
  public File[] listFiles( FilenameFilter filter ) throws SecurityException {
    return file.listFiles( filter );
  }
  public static File[] listRoots() {
    return file.listRoots();
  }
  */
  public boolean mkdir() throws SecurityException {
    try
    {
      file.mkdir();
      return true;
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  public boolean mkdirs() throws SecurityException {
    try
    {
      file.mkdirs();
      return true;
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  /*
  public boolean renameTo( File dest ) throws SecurityException {
    return file.renameTo( dest );
  }
  */
  public boolean setLastModified( long time ) throws SecurityException {
    try
    {
      file.setLastModified( time );
      return true;
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  public boolean setReadOnly() throws SecurityException {
    try
    {
      file.setReadOnly();
      return true;
    }
    catch( SmbException e )
    {
      throw new SecurityException();
    }
  }
  public String toString() {
    System.out.println("SMBFile.toString() not supported");
    return null;
  }
  public URI toURI() {
    System.out.println("SMBFile.toURI() not supported");
    return null;
  }
  public URL toURL() throws MalformedURLException {
    return file.toURL();
  }
}

