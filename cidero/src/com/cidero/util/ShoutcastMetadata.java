/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ShoutcastMetadata.java,v $
 *
 */

package com.cidero.util;

import java.util.logging.Logger;

/**
 * Simple Shoutcast metadata helper class.  Parses the metadata and 
 * makes it accessible via getArtist(),getTitle() methods. 
 *
 * Shoutcast metadata typically looks like:
 *
 *   StreamTitle='U2 - Gloria';StreamURL='www.radioparadise.com';
 *
 */
public class ShoutcastMetadata
{
  private static Logger logger = Logger.getLogger("com.cidero.util");

  String    metadata;   // Raw metadata string
  
  String    artist;
  String    title;
  String    streamURL;
  
  /**
   *  Constructor
   */
  public ShoutcastMetadata( String metadata )
  {
    // trim off random data following last ';' if present
    int index = metadata.lastIndexOf(';');
    if( index > 0 )
      metadata = metadata.substring(0,index);

    this.metadata = metadata;
    
    String[] fields = metadata.split(";");

    for( int n = 0 ; n < fields.length ; n++ )
    {
      String[] nameValuePair = fields[n].split("=");
      
      if( nameValuePair.length != 2 )
      {
        logger.warning("Metadata syntax error");
        continue;
      }

      if( nameValuePair[0].toLowerCase().indexOf("title") >= 0 )
      {
        // Strip single quotes if present
        String artistTitle = removeSingleQuotes( nameValuePair[1].trim() );
        index = artistTitle.indexOf(" - ");
        if( index >= 0 )
        {
          artist = artistTitle.substring(0,index).trim();
          title = artistTitle.substring(index+3).trim();
        }
        else
        {
          artist = null;
          title = artistTitle.trim();
        }
      }
      else if( nameValuePair[0].toLowerCase().indexOf("url") >= 0 )
      {
        streamURL = removeSingleQuotes( nameValuePair[1].trim() );
      }
    }
  }
  
  public String getArtist() {
    return artist;
  }

  public String getTitle() {
    return title;
  }

  public String getStreamURL() {
    return streamURL;
  }
  
  public String removeSingleQuotes( String str )
  {
    if( str.startsWith("'") && str.endsWith("'") )
      return str.substring(1, str.length()-1);
    else
      return str;
  }
  
  public static void main( String args[] )
  {
    String meta = "StreamTitle='U2 - Gloria';StreamURL='www.radio.com';XX***";

    ShoutcastMetadata shoutMetadata = new ShoutcastMetadata( meta );
    System.out.println("Artist: " + shoutMetadata.getArtist() );
    System.out.println("Title: " + shoutMetadata.getTitle() );
    System.out.println("URL: " + shoutMetadata.getStreamURL() );
  }
  
}
