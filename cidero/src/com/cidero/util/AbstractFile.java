/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: AbstractFile.java,v $
 *
 */

package com.cidero.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;


/**
 *  Class to allow use of multiple mechanisms to access files in a 
 *  random manner.  Used to hide details of File (local filesystem)
 *  and SmbFile (CIFS filesystem) from applications.
 *
 *  The Java class library perhaps should have placed an abstract
 *  class below java.io.File (as with InputStream), 
 *  but it didn't - hence the need for this class to support random access 
 *  to filesystems other than the default OS filesystem (like SMB).
 *
 */
public interface AbstractFile
{
  public Object getUnderlyingFileObj();
  public int    getFileType();
  
  public boolean canRead() throws SecurityException;
  public boolean canWrite() throws SecurityException;
  //public int compareTo( File pathname );
  //public int compareTo( Object o );
  public boolean createNewFile() throws IOException;
  public boolean delete() throws SecurityException;
  public void deleteOnExit() throws SecurityException;
  public boolean equals( Object o );
  public boolean exists() throws SecurityException;
  //public File getAbsoluteFile() throws SecurityException;
  public String getAbsolutePath() throws SecurityException;
  //public File getCanonicalFile() throws SecurityException;
  public String getCanonicalPath() throws IOException;
  public String getName();
  public String getParent();
  //public File getParentFile();
  public String getPath();
  public int hashCode();
  public boolean isAbsolute();
  public boolean isDirectory() throws SecurityException;
  public boolean isFile() throws SecurityException;
  public boolean isHidden() throws SecurityException;
  public long lastModified() throws SecurityException;
  public long length() throws SecurityException;
  public String[] list() throws SecurityException;
  //public String[] list( FilenameFilter filter ) throws SecurityException;
  //public File[] listFiles() throws SecurityException;
  //public File[] listFiles( FileFilter filter ) throws SecurityException;
  //public File[] listFiles( FilenameFilter filter ) throws SecurityException;
  //public static File[] listRoots();
  public boolean mkdir() throws SecurityException;
  public boolean mkdirs() throws SecurityException;
  //public boolean renameTo( File dest ) throws SecurityException;
  public boolean setLastModified( long time ) throws SecurityException;
  public boolean setReadOnly() throws SecurityException;
  public String toString();
  public URI toURI();
  public URL toURL() throws MalformedURLException;

}

