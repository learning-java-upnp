/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: TimeUtil.java,v $
 *
 */
package com.cidero.util;

import java.text.NumberFormat;
import java.util.logging.Logger;

public class TimeUtil
{
  private static Logger logger = Logger.getLogger("com.cidero.util");

  static NumberFormat nf2 = null;

  static 
  {
    nf2 = NumberFormat.getInstance();
    nf2.setMinimumIntegerDigits(2);
    nf2.setGroupingUsed(false);
  }
  
  public static String toHHMMSS( int secs )
  {
    int hour = (int)(secs/3600);
    int minute = (int)((secs % 3600)/60);
    int sec = (int)secs % 60;

    try 
    {
      return nf2.format(hour) + ":" + nf2.format(minute) + ":" + 
             nf2.format(sec);
    }
    catch( NumberFormatException e )
    {
      logger.warning("Exception converting '" + secs +
                     "' seconds to HHMMSS");
      return "**:**:**";
    }
  }

}
