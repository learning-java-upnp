/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: UTF8Util.java,v $
 *
 */
package com.cidero.util;

import java.io.UnsupportedEncodingException;


/**
 * 
 */
public class UTF8Util
{
  public static boolean checkForMultiByteChars( String str )
  {
    try 
    {
      byte[] b = str.getBytes("UTF-8");
      if( b.length > str.length() )
      {
        System.out.println("bytes > String.length");
      }
      else
      {
        System.out.println("checkForMultiByteChars: None found");
        return false;
      }
      
      int n = 0;
      
      for( ; n < b.length ; n++ )
      {
        if( (b[n] & 0x80) != 0 )
        {
          System.out.println(" n: " + n + " val: " + b[n] );
        }
      }
    }
    catch( Exception e )
    {
    }
    return true;
  }

  public static String replaceMultiByteChars( String str, char replaceChar )
  {
    try 
    {
      byte[] b = str.getBytes("UTF-8");
      if( b.length == str.length() )
        return str;

      byte[] out = new byte[b.length];

      for( int n = 0; n < b.length ; n++ )
      {
        if( (b[n] & 0x80) != 0 )
          out[n] = (byte)replaceChar;
        else
          out[n] = b[n];
      }

      return new String( out );
    }
    catch( Exception e )
    {
      return str;
    }
  }
}

