/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: TaggedMsg.java,v $
 *
 */
package com.cidero.util;


/**
 * Tagged message class. Originally created to associate a sequence number
 * with command messages so message responses can paired with message
 * requests
 */
public class TaggedMsg
{
  int    id;
  Object obj;

  public TaggedMsg( int id, Object obj )
  {
    this.id = id;
    this.obj = obj;
  }
  
  public int getId()
  {
    return id;
  }
  public Object getObject()
  {
    return obj;
  }
  
}

