/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: JLabelledComboBox.java,v $
 *
 */

package com.cidero.swing;

import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;

/**
 * Convenience class for combo box with label. Defaults to using Box
 * layout, with a small spacer strut between label and combo box.
 */
public class JLabelledComboBox extends JPanel
{
  JLabel label;
  JComboBox comboBox;

  public JLabelledComboBox( String labelText, int gapInPixels )
  {
    setLayout( new BoxLayout( this, BoxLayout.X_AXIS ) );
    label = new JLabel( labelText );
    add( label );

    if( gapInPixels > 0 )
      add( Box.createHorizontalStrut( gapInPixels ) );

    comboBox = new JComboBox();
    add( comboBox );
  }

  public JComboBox getComboBox()
  {
    return comboBox;
  }

  public void addItem( String itemString )
  {
    comboBox.addItem( itemString );
  }
  
  public void addActionListener( ActionListener actionListener )
  {
    comboBox.addActionListener( actionListener );
  }
  
  public void setComponentBackground( Color color )
  {
    setBackground( color );
    //label.setOpaque( true );
    //label.setBackground( color );
    comboBox.setOpaque( false );
    comboBox.setBackground( color );
  }
  
  public void setSelectedIndex( int index )
  {
    comboBox.setSelectedIndex( index );
  }
  
}
