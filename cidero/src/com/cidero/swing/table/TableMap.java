/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: TableMap.java,v $
 *
 */

/* Code originally from Sun tutorials, but we probably will modify, so put
 * in mediarush namespace for now
 */
package com.cidero.swing.table;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.event.TableModelListener;
import javax.swing.event.TableModelEvent;

public class TableMap extends AbstractTableModel
implements TableModelListener {
  protected TableModel model;

  public TableModel getModel() {
    return model;
  }

  public void setModel(TableModel model) {
    //System.out.println("TableMap: setModel");
    this.model = model;
    model.addTableModelListener(this);
  }

  // By default, implement TableModel by forwarding all messages
  // to the model.

  public Object getValueAt(int aRow, int aColumn) {
    return model.getValueAt(aRow, aColumn);
  }

  public void setValueAt(Object aValue, int aRow, int aColumn) {
    model.setValueAt(aValue, aRow, aColumn);
  }

  public int getRowCount() {
    int rowCount = 0;
      
    if( model != null )
      rowCount = model.getRowCount();

    //      System.out.println("TableMap: getRowCount " + rowCount );

    return rowCount;
  }

  public int getColumnCount() {
    int colCount = 0;
      
    if( model != null )
      colCount = model.getColumnCount();

    //      System.out.println("TableMap: getColumnCount = " + colCount );

    return colCount;
  }

  public String getColumnName(int aColumn) {
    return model.getColumnName(aColumn);
  }

  public Class getColumnClass(int aColumn) {
    return model.getColumnClass(aColumn);
  }

  public boolean isCellEditable(int row, int column) {
    return model.isCellEditable(row, column);
  }
  //
  // Implementation of the TableModelListener interface,
  //
  // By default forward all events to all the listeners.
  public void tableChanged(TableModelEvent e) {
    //System.out.println("TableMap: tableChanged");
    fireTableChanged(e);
  }
}
