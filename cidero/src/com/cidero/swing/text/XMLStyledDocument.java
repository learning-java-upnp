/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: XMLStyledDocument.java,v $
 *
 */

package com.cidero.swing.text;

import com.sun.org.apache.xerces.internal.parsers.*;

import java.io.*;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.StyleConstants;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Segment;
import javax.swing.text.Style;
import javax.swing.text.StyleContext;
import javax.swing.text.BadLocationException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Logger;
import java.awt.Color;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 *  Subclass of StyledDocument that provides support for parsing an
 *  XML document and formatting the output so it is readable. Allows
 *  for configuration of the colors/fontStyles/etc... for the various
 *  parts of an XML document.
 *
 *  TODO: Not quite done with adding usable API for this class...
 */
public class XMLStyledDocument extends DefaultStyledDocument
                               implements ContentHandler
{
  private final static Logger logger = Logger.getLogger("com.cidero.util");

  private final static int START_ELEMENT = 1;
  private final static int END_ELEMENT = 2;

  XMLReader parser;
  int nodeDepth;
  int lastElement;
  String indentString = "  ";
  String baseIndentString = "";

  Style defaultStyle;
  Style normalTextStyle;
  Style normalErrorStyle;
  Style xmlNodeStyle;
  Style xmlAttrStyle;
  Style xmlTextStyle;


  public XMLStyledDocument()
  {
    initStyles();
    
    parser = new SAXParser();
    parser.setContentHandler( this );
  }
  
  /**
   *  Set number of spaces for each level of XML node indentation
   */
  public void setXMLIndent( int nSpaces )
  {
    StringBuffer buf = new StringBuffer();
    for( int n = 0 ; n < nSpaces ; n++ )
      buf.append(" ");
    indentString = buf.toString();
  }
  
  /**
   *  Set indentation for root level XML indentation 
   */
  public void setBaseXMLIndent( int nSpaces )
  {
    StringBuffer buf = new StringBuffer();
    for( int n = 0 ; n < nSpaces ; n++ )
      buf.append(" ");
    baseIndentString = buf.toString();
  }

  /**
   * Set the style for normal (non-XML) text
   */
  public void setNormalTextStyle( Style style )
  {
    // Remove existing document style
    removeStyle( "normalText" );
    
    normalTextStyle = addStyle("normalText", defaultStyle );
    normalTextStyle.addAttributes( style.copyAttributes() );
  }
  
  /**
   * Set the style for the XML nodes
   *
   *   <nodeName id="10"> Node Text </nodeName>
   *   ^^^^^^^^^^       ^           ^^^^^^^^^^^
   */
  public void setXMLNodeStyle( Style style )
  {
    removeStyle( "xmlNode" );
    
    xmlNodeStyle = addStyle("xmlNode", defaultStyle );
    xmlNodeStyle.addAttributes( style.copyAttributes() );
  }
  
  /**
   * Set the style for the XML node attributes ( <nodeName id=attr> )
   *                                                       ^^^^^^^ 
   *   <nodeName id="10"> Node Text </nodeName>
   *             ^^^^^^^ 
   */
  public void setXMLAttrStyle( Style style )
  {
    removeStyle( "xmlAttr" );
    
    xmlAttrStyle = addStyle("xmlAttr", defaultStyle );
    xmlAttrStyle.addAttributes( style.copyAttributes() );
  }

  /**
   * Set the style for the XML text fragments
   *
   *   <nodeName id="10"> Node Text </nodeName>
   *                     ^^^^^^^^^^^ 
   */
  public void setXMLTextStyle( Style style )
  {
    removeStyle( "xmlText" );
    
    xmlTextStyle = addStyle("xmlText", defaultStyle );
    xmlTextStyle.addAttributes( style.copyAttributes() );
  }

  /**
   * Setup some default styles.  All styles share a common default style parent
   */
  public void initStyles()
  {
    defaultStyle = StyleContext.getDefaultStyleContext().
      getStyle(StyleContext.DEFAULT_STYLE);
    StyleConstants.setFontFamily( defaultStyle, "SansSerif" );

    normalTextStyle = addStyle("normalText", defaultStyle);
    StyleConstants.setBold( normalTextStyle, true );

    normalErrorStyle = addStyle("normalError", defaultStyle);
    StyleConstants.setBold( normalErrorStyle, true );
    StyleConstants.setForeground( normalErrorStyle, Color.red.darker() );

    xmlNodeStyle = addStyle("xmlNode", defaultStyle);
    StyleConstants.setBold( xmlNodeStyle, true );
    StyleConstants.setForeground( xmlNodeStyle, Color.green.darker().darker());

    xmlAttrStyle = addStyle( "xmlAttr", defaultStyle );
    //StyleConstants.setItalic( xmlAttrStyle, true );
    StyleConstants.setBold( xmlAttrStyle, true );
    StyleConstants.setForeground( xmlAttrStyle, Color.red.darker().darker() );

    xmlTextStyle = addStyle("xmlText", defaultStyle);
    StyleConstants.setBold( xmlTextStyle, true);
    StyleConstants.setForeground( xmlTextStyle, Color.blue.darker() );

  }
  
  public void appendString( String str )
  {
    try 
    {
      insertString(getLength(), str, getStyle("normalText") );
    }
    catch( BadLocationException e )
    {
      logger.warning("Error inserting string in doc" + e );
    }
  }

  /** 
   * Insert a string using the error style (red text)
   */
  public void appendErrorString( String str )
  {
    try 
    {
      insertString(getLength(), str, getStyle("normalError") );
    }
    catch( BadLocationException e )
    {
      logger.warning("Error inserting string in doc" + e );
    }
  }
  
  /**
   * Append an XML string to the document, inserting formatting as necessary.
   * If there is an XML parsing error, 
   */
  public void appendXMLString( String xmlString )
  {
    nodeDepth = 0;  // reset
    
    //System.out.println( "Parsing" + xmlString );
    
    try
    {
      byte[] xmlByteArray = xmlString.getBytes("UTF-8");
      ByteArrayInputStream xmlStream = new ByteArrayInputStream(xmlByteArray);

      InputSource inputSource = new InputSource( xmlStream );
      inputSource.setEncoding( "UTF-8" );
      parser.parse( inputSource );
    }
    catch( UnsupportedEncodingException e )
    {
      logger.warning("Error parsing XML: " + xmlString + "\n" +
                     e.toString() );
      appendErrorString( "Error parsing XML - Raw XML string: \n" + 
                         xmlString );
    }
    catch( SAXException e )
    {
      logger.warning("Error parsing XML: " + xmlString + "\n" +
                     e.toString() );
      appendErrorString( "Error parsing XML - Raw XML string: \n" + 
                         xmlString );
    }
    catch( IOException e )
    {
      logger.warning("Error parsing XML: " + xmlString + "\n" +
                     e.toString() );
      appendErrorString( "Error parsing XML - Raw XML string: \n" +
                         xmlString );
    }
  }

  /** 
   *  Clear document (replace whole document with empty string)
   */
  public void clear()
  {
    try 
    {
      replace( 0, getLength(), "", null );
    }
    catch( BadLocationException e )
    {
      logger.warning("Error inserting string in doc" + e );
    }
  }
  
  public void setDocumentLocator( Locator locator )
  {
    //logger.finest("setDocumentLocator:" + "\n" );
  }

  public void startDocument() throws SAXException
  {
    //logger.finest("startDocument:" + "\n" );
  }

  public void endDocument() throws SAXException
  {
    //logger.finest("endDocument:" + "\n" );

  }

  public void processingInstruction( String target, String data )
    throws SAXException
  {
    logger.finest("processingInstruction: target: " + target +
                           " data: " + data );
  }

  public void startPrefixMapping( String prefix, String uri )
    throws SAXException
  {

    //logger.finest("startPrefixMapping: prefix: " + prefix +
    //                  " uri: " + uri );
  }

  public void endPrefixMapping( String prefix )
    throws SAXException
  {
    //logger.finest("endPrefixMapping: prefix: " + prefix );
  }

  public void startElement( String namespaceURI, String localName,
                            String rawName, Attributes atts )
    throws SAXException
  {
    //    System.out.println( indent + "depth: " + nodeDepth + " startElement: " +
    //                        " localName: " + localName +
    //                        " rawName: " + rawName );

    try 
    {
      StringBuffer buf = new StringBuffer();
    
      if( nodeDepth > 0 )
        buf.append("\n");

      buf.append( baseIndentString );
      for( int n = 0 ; n < nodeDepth ; n++ )
        buf.append( indentString );
    
      buf.append("<" + rawName );

      if( atts.getLength() > 0 )
      {
        insertString(getLength(), buf.toString(), getStyle("xmlNode") );

        StringBuffer attrBuf = new StringBuffer();
        for( int n = 0 ; n < atts.getLength() ; n++ )
        {
          attrBuf.append( " " + atts.getLocalName(n) + "=\"" +
                          atts.getValue(n) + "\"" );
        }
        insertString(getLength(), attrBuf.toString(), getStyle("xmlAttr") );

        insertString(getLength(), ">", getStyle("xmlNode") );
      }
      else
      {
        buf.append(">");
        insertString(getLength(), buf.toString(), getStyle("xmlNode") );
      }
    }
    catch( BadLocationException e )
    {
      throw new SAXException( e );
    }
      
    nodeDepth++;
    lastElement = START_ELEMENT;
  }

  public void endElement( String namespaceURI, String localName,
                          String rawName )
    throws SAXException
  {
    nodeDepth--;

    //    System.out.println("endElement: " +
    //                       " localName: " + localName +
    //                       " rawName: " + rawName  );

    StringBuffer buf = new StringBuffer();

    // If last element processed was also an END element, want a newline
    if( lastElement == END_ELEMENT )
    {
      buf.append("\n");
      buf.append( baseIndentString );
      for( int n = 0 ; n < nodeDepth ; n++ )
        buf.append( indentString );
    }
    
    buf.append("</" + rawName + ">");

    try 
    {
      insertString(getLength(), buf.toString(), getStyle("xmlNode") );
    }
    catch( BadLocationException e )
    {
      throw new SAXException( e );
    }

    lastElement = END_ELEMENT;
  }

  public void characters( char[] ch, int start, int end )
    throws SAXException
  {
    String s = new String( ch, start, end );
    s = s.trim();

    try 
    {
      if( s.length() > 0 ) 
        insertString(getLength(), s, getStyle("xmlText") );
    }
    catch( BadLocationException e )
    {
      throw new SAXException( e );
    }

  }

  public void ignorableWhitespace( char[] ch, int start, int end )
    throws SAXException
  {
    //String s = new String( ch, start, end );
    //System.out.println( "whitespace: [" + s + "]" );
  }
  
  public void skippedEntity( String name )
    throws SAXException
  {
    //    System.out.println("skippedEntity: name: " + name );
  }
  
  /**
   * Simple test program
   */
  public static void main( String[] args )
  {
    StringBuffer xml = new StringBuffer();
    
    try 
    {
      xml.append("<DIDL-Lite xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\">\n");
      
      xml.append("<item id=\"0\"><upnp:artist>Me</upnp:artist>");
      xml.append("<dc:title>Whatever</dc:title>\n");
      xml.append("</item> \n" );

      xml.append("</DIDL-Lite>\n");
    
      XMLStyledDocument doc = new XMLStyledDocument();
      doc.setXMLIndent( 8 );

      doc.appendXMLString( xml.toString() );

      System.out.println( "XML Document as plain text" );
      Segment segment = new Segment();
      doc.getText( 0, doc.getLength(), segment );
      System.out.println( segment.toString() );
    }
    catch( Exception e )
    {
      e.printStackTrace();
    }

  }
  
}
