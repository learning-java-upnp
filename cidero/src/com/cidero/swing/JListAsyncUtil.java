/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: JListAsyncUtil.java,v $
 *
 */

package com.cidero.swing;

import javax.swing.JList;
import javax.swing.SwingUtilities;

/**
 * Convenience class for updating JList properties from non-event threads
 * Puts necessary action(s) on Swing event queue.
 */
public class JListAsyncUtil
{
  public static void ensureIndexIsVisible( JList jList, int index )
  {
    SwingUtilities.invokeLater( new EnsureIndexIsVisibleThread(jList, index) );
  }
  
  static class EnsureIndexIsVisibleThread implements Runnable
  {
    JList jList;
    int   index;
    public EnsureIndexIsVisibleThread( JList jList, int index )
    {
      this.jList = jList;
      this.index = index;
    }

    public void run() 
    {
      if( index >= 0 )
        jList.ensureIndexIsVisible( index );
    }
  }
}
