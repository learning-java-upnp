/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: IconUtil.java,v $
 *
 */

package com.cidero.swing;

import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.ImageIcon;

/**
 * Misc Icon related methods
 */
public class IconUtil
{
  public final static int ROTATE_0_DEG_CW   = 0;
  public final static int ROTATE_90_DEG_CW  = 90;
  public final static int ROTATE_180_DEG_CW = 180;
  public final static int ROTATE_270_DEG_CW = 270;

  /**
   *  Generate a rotated copy of an image icon. Allowable rotations are 
   *  0, 90, 180, 270.  Integer used to allow for abitrary degree rotation
   *  in future (just need to compute the right values for the 3rd
   *  translate step to support that - TODO
   *
   */
  public static ImageIcon rotateImageIcon( ImageIcon icon, int rotation )
  {
    if( rotation == ROTATE_0_DEG_CW )
      return icon;
    
    // Create rotated version of image icon
    Image image = icon.getImage();
    BufferedImage bufImage = null;

    // Create a buffered image with a format that's compatible with the screen
    // Note - screen compatibility code commented out for now (code 
    // borrowed from Java Almanac). There were problems on Linux 
    // laptop running RH9.0

    boolean hasAlpha = false;

    /*
    //
    // Determine if the image has transparent pixels; for this method's
    // implementation, see e661 Determining If an Image Has Transparent Pixels
    boolean hasAlpha = hasAlpha(image);

    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    try
    {
      // Determine the type of transparency of the new buffered image
      int transparency = Transparency.OPAQUE;
      if (hasAlpha)
        transparency = Transparency.BITMASK;
    
      // Create the buffered image
      GraphicsDevice gs = ge.getDefaultScreenDevice();
      GraphicsConfiguration gc = gs.getDefaultConfiguration();
      bufImage = gc.createCompatibleImage(
           image.getWidth(null), image.getHeight(null), transparency) ;
    }
    catch (HeadlessException e)
    {
      // The system does not have a screen
    }
    */
    
    // Headless system or Graphics environment problems
    // Create a buffered image using the default color model
    if( bufImage == null )
    {
      int type = BufferedImage.TYPE_INT_RGB;
      if (hasAlpha)
        type = BufferedImage.TYPE_INT_ARGB;

      bufImage = new BufferedImage( image.getWidth(null),
                                    image.getHeight(null), type );
    }

    // Copy image to buffered image
    Graphics g = bufImage.createGraphics();
    
    // Paint the image onto the buffered image
    g.drawImage(image, 0, 0, null);
    g.dispose();
    
    //
    // Remember transformation steps specified below are actually applied in
    // reverse order! (Confusing API...) 
    //
    AffineTransform rotationTransform = new AffineTransform();
    //    rotationTransform.rotate( 0.0,

    // Step 3 - shift rotated image back so upper left is at 0,0
    if( rotation == ROTATE_180_DEG_CW )
    {
      rotationTransform.translate( bufImage.getWidth()/2,
                                   bufImage.getHeight()/2 );  // Step 3
    }
    else // 90 or 270
    {
      rotationTransform.translate( bufImage.getHeight()/2,
                                   bufImage.getWidth()/2 );  // Step 3
    }
  
    // Step 2 - do the rotation about the center point
    rotationTransform.rotate( Math.PI/2.0 ); 
    
    // Step 1 - shift original image so middle is at 0,0
    rotationTransform.translate( -bufImage.getWidth()/2,
                                 -bufImage.getHeight()/2 );

    AffineTransformOp op = 
     new AffineTransformOp( rotationTransform,
                            AffineTransformOp.TYPE_NEAREST_NEIGHBOR );
    //                            AffineTransformOp.TYPE_BILINEAR );

    BufferedImage destImage;
    destImage = op.filter( bufImage, null );

    System.out.println("srcImage width, height = " +
                       bufImage.getWidth() + " " +
                       bufImage.getHeight() );
    System.out.println("destImage width, height = " +
                       destImage.getWidth() + " " + 
                       destImage.getHeight() );

    Image rotatedImage = Toolkit.getDefaultToolkit().createImage( destImage.getSource() );
    ImageIcon rotatedIcon = new ImageIcon( rotatedImage );

    return rotatedIcon;
  }

}
