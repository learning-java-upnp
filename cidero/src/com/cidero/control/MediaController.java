/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: MediaController.java,v $
 *
 */

package com.cidero.control;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Properties;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.Handler;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import org.cybergarage.http.*;
import org.cybergarage.upnp.*;
import org.cybergarage.upnp.device.*;
import org.cybergarage.upnp.event.*;
import org.cybergarage.upnp.ssdp.SSDPPacket;

import com.ha.common.windows.*;

import com.cidero.proxy.*;
import com.cidero.util.MrUtil;
import com.cidero.util.NetUtil;
import com.cidero.util.URLUtil;
import com.cidero.util.AppPreferences;
import com.cidero.upnp.*;


/**
 *  Main UPnP Media Controller class. Discovers UPnP MediaServer and 
 *  MediaRenderer devices on a network and interacts with them via a 
 *  Swing-based GUI
 */
public class MediaController extends ControlPoint 
                             implements NotifyListener,
                                        EventListener,
                                        SearchResponseListener,
                                        DeviceChangeListener,
                                        SubscriptionChangeListener,
                                        WindowListener,
                                        StandByRequestListener
{
  private final static Logger logger = 
       Logger.getLogger("com.cidero.control");

  private final static String TITLE = "Cidero UPnP A/V Controller";
  public static int DEFAULT_WIDTH = 820;
  public final static int DEFAULT_HEIGHT = 480;

  // Use 180 seconds subcription duration for NMPR compatibility
  public static int REQUESTED_SUBSCRIPTION_PERIOD_SEC = 180;
  
  private JFrame frame;
  private MenuBar menuBar;

  JPanel avServerPanel;
  JPanel avRendererPanel;

  JButton mediaServerButton;

  // List of servers/renderers currently active
  static MediaDeviceList mediaDeviceList;
  
  JPanel serverDevicePanel;
  RendererDevicePanel rendererDevicePanel;
  MediaBrowserPanel mediaBrowserPanel;

  DebugWindow debugWindow;

  /**
   * List model for set of debug objects (actions,events,notifications)
   * in debug window
   */
  DefaultListModel debugListModel = new DefaultListModel();
  static int       debugHistorySize = 100;
  boolean          debugAutoFormatXML = true;


  static AppPreferences preferences;

  /**
   * Proxy server for synchronous playback 
   */
  HTTPProxyServer proxyServer = null;
  
  /**
   * Detector for windows standby events (uses JNI and DLL library)
   */
  StandByDetector standByDetector;

  /** 
   *  Background color that overrides UI Look&Feel background
   */
  static Color bgColor = new Color( 220, 220, 220 );

  public static Color getBackground() 
  {
    return bgColor;
  }

  static MediaController controller = null;

  public static MediaController getInstance()
  {
    if( controller == null )
      controller = new MediaController();

    return controller;
  }
  
  /**
   * Constructor. Private for singleton object
   */
  private MediaController()
  {
    loadPreferences();
    
    createDefaultPlaylistDirs();
    
    mediaDeviceList = new MediaDeviceList();
    
    addNotifyListener( this );
    addSearchResponseListener( this );
    addEventListener( this );
    addDeviceChangeListener( this );
    addSubscriptionChangeListener( this );

    // Install optional listener for non-notify HTTP requests
    // Use separate listener class here (as opposed to 'this') since
    // base ControlPoint class already implements HTTPRequestListener,
    // and we don't want to override it (TODO may want to change base
    // class instead)
    addHttpRequestListener( new ControllerHTTPRequestListener(this) );

    setSubscriptionPeriodSec( 
          preferences.getInt("subscriptionPeriodSec",
                             REQUESTED_SUBSCRIPTION_PERIOD_SEC));

    // Use NMPR mode (enables auto-resubscribe in CyberLink)
    setNMPRMode( true );

    // If configured to run a synchronized proxy server in the same
    // JVM as the controller, start it up.
    startLocalProxyServer();
    
    // If windows, run standby detector
    // Also run simple monitoring thread that detects the waking up condition
    // by looking for time jumps
    //
    if( isRunningOnWindows() )
    {
      logger.info("Installing Windows standby mode handler");
      standByDetector = new StandByDetector( this );
      standByDetector.setAllowStandby( true );

      WakeupMonitorThread wakeupMonitorThread = new WakeupMonitorThread(this);
      wakeupMonitorThread.start();
    }

    initFrame();
  }

  /**
   *  Load MediaController preferences. Preference information comes from 
   *  2 sources - the default set of preferences that come with the program,
   *  and the (optional) user-specific set stored in the user's home
   *  directory. The default set is located in the Java classpath under 
   *  the 'properties' subdirectory. The user-specific set is stored 
   *  in the user's home directory, under the '.cidero' subdirectory 
   */
  public static void loadPreferences()
  {
    // Load shared & user-specific preferences for this application
    preferences = new AppPreferences(".cidero");

    if( ! preferences.load( "MediaController", "MediaController" ) )
    {
      logger.severe("Missing preferences file - exiting");
      System.exit(-1);
    }

    setDebugFlagsFromPreferences();

    setLogLevelFromPreferences();

  }


  public static void setDebugFlagsFromPreferences()
  {
    // Initialized enable flags for all the different debug object type
    boolean enableFlag = preferences.getBoolean( "debug.search", true );
    DebugSearchRequest.setEnabled( enableFlag );
    DebugSearchResponse.setEnabled( enableFlag );

    enableFlag = preferences.getBoolean( "debug.alive", false );
    DebugNotifyMsg.setAliveEnabled( enableFlag );
    enableFlag = preferences.getBoolean( "debug.byeBye", true );
    DebugNotifyMsg.setByeByeEnabled( enableFlag );

    enableFlag = preferences.getBoolean( "debug.subscribe", true );
    DebugSubscriptionRequest.setEnabled( enableFlag );
    DebugSubscriptionResponse.setEnabled( enableFlag );

    DebugAction.setEnabled( preferences.getBoolean( "debug.action", true ) );
    DebugEvent.setEnabled( preferences.getBoolean( "debug.event", true ) );

    setDebugHistorySize( preferences.getInt( "debug.historySize", 100 ) );
    DebugObj.setAutoFormatXML( preferences.getBoolean( "debug.autoFormatXML",
                                                       true ) );
  }

  public static void setLogLevelFromPreferences()
  {
    String logLevel = preferences.get( "logLevel", "INFO" );

    if( logLevel.equalsIgnoreCase("WARNING") )
      setLogLevel( Level.WARNING );
    else if( logLevel.equalsIgnoreCase("INFO") )
      setLogLevel( Level.INFO );
    else if( logLevel.equalsIgnoreCase("FINE") )
      setLogLevel( Level.FINE );
    else if( logLevel.equalsIgnoreCase("FINER") )
      setLogLevel( Level.FINER );
    else if( logLevel.equalsIgnoreCase("FINEST") )
      setLogLevel( Level.FINEST );
  }
  

  private static Logger topLevelCideroLogger = null;
  private static Logger topLevelCybergarageLogger = null;

  public static void setLogLevel( Level logLevel )
  {
    //
    // If first time called, get the global logger, and enable all levels
    // for all the *handlers*.  (Level of printout is controlled by the
    // logger's level, not the handler's level)
    //
    if( topLevelCideroLogger == null )
    {
      topLevelCideroLogger = Logger.getLogger("com.cidero");    

      topLevelCybergarageLogger = Logger.getLogger("org.cybergarage");    

      Handler[] handlers = topLevelCideroLogger.getParent().getHandlers();
      if( handlers.length < 1 )
      {
        logger.warning("nHandlers < 1");
      }
      else
      {
        for( int n = 0 ; n < handlers.length ; n++ )
          handlers[n].setLevel(Level.ALL);
      }

    }

    topLevelCideroLogger.setLevel( logLevel );
    topLevelCybergarageLogger.setLevel( logLevel );

  }

  public void savePreferences()
  {
    preferences.saveUserPreferences( "MediaController", "MediaController" );
  }

  public static AppPreferences getPreferences()
  {
    if( preferences == null )
      loadPreferences();
    
    return preferences;
  }

  public void createDefaultPlaylistDirs()
  {
    String userHome = System.getProperty("user.home");
    File xmlDir = new File( userHome +
                            "/.cidero/MediaController/playlists/xml" );
    if( ! xmlDir.exists() )
    {
      if( ! xmlDir.mkdirs() )
      {
        logger.warning("Error creating default playlist directory" +
                       xmlDir.getName() );
      }
    }

    File m3uDir = new File( userHome +
                            "/.cidero/MediaController/playlists/m3u" );
    if( ! m3uDir.exists() )
    {
      if( ! m3uDir.mkdirs() )
      {
        logger.warning("Error creating default playlist directory" +
                       m3uDir.getName() );
      }
    }
  }
  

  public void startLocalProxyServer()
  {
    if( proxyServer != null )
    {
      proxyServer.stop();
      proxyServer = null;
    }
    
    String syncProxyType = preferences.get( "syncProxyType", "local" );

    logger.fine("Proxy server type is '" + syncProxyType + "'");

    if( syncProxyType.equals("local") )
    {
      int syncProxyPort = preferences.getInt( "syncProxyPort", 18081 );
      int syncProxyWaitMs = 
        preferences.getInt( "syncProxyWaitMs", 2000 );

      logger.info("(Re)Starting sync proxy server on local host, port: " + 
                  syncProxyPort + " syncProxyWaitMs: " + syncProxyWaitMs );
      try 
      {
        // Start set of server threads (one per network interface)
        proxyServer = new HTTPProxyServer( syncProxyPort, syncProxyWaitMs );
        proxyServer.start();
      }
      catch( IOException e )
      {
        logger.warning("Failure starting synchronous proxy server: " + e);
      }
    }
  }

  public void stopLocalProxyServer()
  {
    if( proxyServer != null )
    {
      logger.info("Stopping local proxy server");
      proxyServer.stop();
      proxyServer = null;
    }
  }

  private void initFrame()
  {
    frame = new JFrame( TITLE );

    frame.getContentPane().setLayout( new BorderLayout() );
    
    // Menubar
    menuBar = new MenuBar( this );
    frame.setJMenuBar( menuBar );

    //
    // Panels for holding A/V renderer/server device icons
    //
    FlowLayout serverLayout = new FlowLayout( FlowLayout.LEADING );
    serverLayout.setHgap(0);
    serverLayout.setVgap(0);

    //avServerPanel = new JPanel();
    //avServerPanel.setLayout( new BoxLayout( avServerPanel, BoxLayout.X_AXIS));
    avServerPanel = new JPanel( serverLayout );
    TitledBorder serversBorder = new TitledBorder("Media Servers");
    avServerPanel.setBorder( serversBorder );
    //avServerPanel.setPreferredSize( new Dimension(100,80) );
    avServerPanel.setBackground( bgColor );

    serverDevicePanel = new JPanel( new BorderLayout() );
    //serverDevicePanel.add( avRendererPanel, BorderLayout.NORTH );
    //serverDevicePanel.add( new JScrollPane(avServerPanel),
    serverDevicePanel.add( avServerPanel,
                           BorderLayout.CENTER );
    serverDevicePanel.setBackground( bgColor );

    rendererDevicePanel = new RendererDevicePanel( this );

    JSplitPane splitPane = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT,
                                     new JScrollPane(serverDevicePanel),
                                     new JScrollPane(rendererDevicePanel) );
    splitPane.setOneTouchExpandable(true);
    splitPane.setDividerLocation(300);

    // Create brower panel with tree on left, tabbed media items pane on right
    mediaBrowserPanel = new MediaBrowserPanel( this );

    frame.getContentPane().add( splitPane, BorderLayout.NORTH );
    frame.getContentPane().add( mediaBrowserPanel, BorderLayout.CENTER );


    frame.setSize(new Dimension(DEFAULT_WIDTH,DEFAULT_HEIGHT));

    // Set things up so that control point 'stop' action gets called
    // if the window is closed. When DefaultCloseOperation is set to
    // DISPOSE_ON_CLOSE, the registered window listeners are invoked
    // prior to the Frame being disposed
    
    frame.addWindowListener( this );
    frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );

    frame.setVisible(true);
  }

  public JFrame getFrame() {
    return frame;
  }
  
  public Container getContentPane() {
    return getFrame().getContentPane();
  }
  
  public MediaBrowserPanel getMediaBrowserPanel() {
    return mediaBrowserPanel;
  }

  public DebugWindow getDebugWindow() {
    return debugWindow;
  }

  public DefaultListModel getDebugListModel() {
    return debugListModel;
  }

  public static MediaDeviceList getMediaDeviceList() {
    return mediaDeviceList;
  }

  /**
   * Add a debug object to the debug window
   *
   * Changes to the debug list model need to all be done in the context of
   * the Swing event thread
   */
  class SwingAddDebugObj implements Runnable
  {
    DebugObj debugObj;
    int debugHistorySize;

    public SwingAddDebugObj( DebugObj debugObj, int historySize )
    {
      this.debugObj = debugObj;
      this.debugHistorySize = historySize;
    }

    public void run() 
    {
      if( debugListModel.getSize() >= debugHistorySize )
      {
        // Delete oldest one(s) to make room. May be more than 1 removal here
        // if history size changed. If history size unchanged, 2nd arg will
        // evaluate to 0, which results in a removal of item 0
        //
        debugListModel.removeRange(0,
                             debugListModel.getSize() - debugHistorySize);
      }
    
      debugListModel.addElement( debugObj );
    }
  }
  
  synchronized public void addDebugObj( DebugObj debugObj )
  {
    if( DebugObj.getEnabled() == true )
    {
     SwingAddDebugObj swingAddDebugObj =
        new SwingAddDebugObj( debugObj, debugHistorySize );

      SwingUtilities.invokeLater( swingAddDebugObj );
    }
  }

  public void clearDebugHistory()
  {
    debugListModel.clear();
  }
  
  public static void setDebugHistorySize( int historySize )
  {
    debugHistorySize = historySize;
  }
  public static int getDebugHistorySize()
  {
    return debugHistorySize;
  }


  public void printConsole(String msg)
  {
    logger.finer( msg );
    
    //    ctrlPointPane.printConsole(msg); 
  }

  public void clearConsole()
  {
    //    ctrlPointPane.clearConsole();
  }

  /**
   *  This callback is invoked whenever CLink discovers a new device
   *
   */
  public void deviceAdded( final Device dev )
  {
    logger.info("Adding device - Type: " + dev.getDeviceType() + 
                " Name: " + dev.getFriendlyName() + 
                " Location: " + dev.getLocation() );

    // If debug enabled, save action in debug object list
    if ( DebugMsg.getEnabled() )
    {
      DebugMsg debugMsg = new DebugMsg( "CyberLink Event: Device Added: " +
                                        " Type: " + dev.getDeviceType() + 
                                        " Name: " + dev.getFriendlyName(),
                                        "" );
      addDebugObj( debugMsg );
    }

    // One early Sony device advertised itself as a 'ContentDirectory'...

    if( (dev.getDeviceType().indexOf("device:MediaServer") >= 0) ||
        (dev.getDeviceType().indexOf("device:ContentDirectory") >= 0) )
    {
      addMediaServer( this, dev );
    }
    else if( dev.getDeviceType().indexOf("device:MediaRenderer") >= 0 ) 
    {
      addMediaRenderer( this, dev );
    }
    
    // Check embedded devices
		DeviceList devList = dev.getDeviceList();
		int devCnt = devList.size();

		for (int n=0; n<devCnt; n++)
    {
			Device embeddedDev = devList.getDevice(n);

      logger.fine("  Checking embedded device " + n );
      logger.fine(" deviceType: " + embeddedDev.getDeviceType() );
      logger.fine(" friendlyName: " + embeddedDev.getFriendlyName() );

      // Set location field in embedded device for benefit of URLBase logic
      embeddedDev.setLocation( dev.getLocation() );

      if( embeddedDev.getDeviceType().indexOf("MediaServer") >= 0 )
      {
        addMediaServer( this, embeddedDev );
      }
      else if( embeddedDev.getDeviceType().indexOf("MediaRenderer") >= 0 ) 
      {
        addMediaRenderer( this, embeddedDev );
      }
		}

    logger.fine(" --------- deviceAdded callback (Leaving) ----------");

  }

  public void addMediaServer( final MediaController controller,
                              final Device dev )
  {
    logger.fine(" Adding Media Server! " );
    logger.fine(" URLBase: " + dev.getURLBase() );

    // Check for content directory service
      
    ServiceList serviceList = dev.getServiceList();
      
    Service service;

    int serviceCnt = serviceList.size();

    for( int n=0; n<serviceCnt; n++ )
    {
      service = serviceList.getService(n);
        
      logger.fine(" Service Type: " + service.getServiceType() );
      logger.finest(" Service Id: " + service.getServiceID() );
      logger.finest(" Service Description URL: " + 
                         service.getDescriptionURL() );
      logger.finest(" Service SCPD URL: " + 
                         service.getSCPDURL() );
      logger.finest(" Service Control URL: " + 
                         service.getControlURL() );

      // service.setControlURL("/PhotoServer/" + service.getControlURL() );
        
      if( service.getServiceType().indexOf("ContentDirectory") >= 0 )
        logger.finest(" Matched content director service!!!!");
      else
        logger.finest(" Didn't match content directory service!!!!");
    }
      

    // Update the GUI asynchronously
    Runnable swingUpdateThread = new Runnable() 
    {
      public void run() 
      {
        //
        // Instantiate new MediaServerDevice. Audio, Video, and Image models
        // are created along with device (initially empty)
        // 
        try
        {
          MediaServerDevice mediaServer = 
            new MediaServerDevice( controller, dev );

          controller.getMediaDeviceList().add( mediaServer );

          JButton button = mediaServer.createButton();

          avServerPanel.add( button );

          frame.pack();
        }
        catch( InvalidDescriptionException e )
        {
          logger.warning("Invalid device description " + e );
          e.printStackTrace();
        }
      }

    };
      
    SwingUtilities.invokeLater( swingUpdateThread );

  }


  public void addMediaRenderer( MediaController controller, 
                                Device dev )
  {
    logger.finer(" Adding Media Renderer! " );
    logger.finest(" URLBase: " + dev.getURLBase() );

    //
    // Instantiate new MediaRendererDevice. Pass controller parent and
    // low-level device as args.  Note that constructor sets up event
    // listeners, so subscribe command (below) shouldn't be issued until 
    // *after* this constructor has executed, otherwise some events may
    // arrive too early
    // 
    final MediaRendererDevice mediaRenderer;
    try 
    {
      mediaRenderer = new MediaRendererDevice( controller, dev );
      mediaDeviceList.add( mediaRenderer );
    }
    catch( InvalidDescriptionException e )
    {
      logger.warning("Invalid device description " + e );
      return;
    }

    // Update the GUI asynchronously
    Runnable swingUpdateThread = new Runnable() 
    {
      public void run() 
      {
        rendererDevicePanel.add( mediaRenderer );
        logger.finer("addMediaRenderer: RUNNING ASYNC GUI UPDATE!!!!!");
        frame.pack();
      }
    };
      
    try 
    {
      SwingUtilities.invokeLater( swingUpdateThread );
    }
    catch( Exception e )
    {
      logger.warning("InvokeAndWait Interrupted! " + e );
      return;
    }


    // Subscribe to known MediaRenderer services ConnectionManager, 
    // AVTransport, and RenderingControl. Some devices may have other
    // vendor-specific services - skip over those
    ServiceList serviceList = dev.getServiceList();
    Service service;
    int serviceCnt = serviceList.size();
    for( int n=0; n<serviceCnt; n++ )
    {
      service = serviceList.getService(n);
        
      logger.finest(" Service Type: " + service.getServiceType() );
      logger.finest(" Service Id: " + service.getServiceID() );
      logger.finest(" Service Description URL: " + 
                         service.getDescriptionURL() );
      logger.finest(" Service SCPD URL: " + service.getSCPDURL() );
      logger.finest(" Service Control URL: " + service.getControlURL() );

      if( service.getServiceType().indexOf("ConnectionManager") >= 0 ||
          service.getServiceType().indexOf("AVTransport") >= 0 ||
          service.getServiceType().indexOf("RenderingControl") >= 0 )
      {
        // OJN - changed this to request a non-infinte duration subscription 
        // 180 sec is NMPR 120-sec renew rate + 60 sec leeway
        if( ! subscribe( service, getSubscriptionPeriodSec() ) )
          logger.warning("Error subscribing to service");
      }
    }

  }

  /**
   *  Device removal handler. This method is invoked by CLink whenever
   *  CLink removes a device from it's list of devices, either due to
   *  receiving a 'bye-bye' notificaton from the device, or due to a
   *  device timeout. 
   *
   *  In order that the application can clean up, this method is invoked
   *  prior to the destruction of the underlying CLink Device object, so all
   *  Device methods may be invoked here.
   *
   *  @param  dev    Underlying CLink device
   */
  public void deviceRemoved( final IDevice dev )
  {
    logger.info("Removing device - Type: " + dev.getDeviceType() + 
                " Name: " + dev.getFriendlyName() );

    // If debug enabled, save action in debug object list
    if ( DebugMsg.getEnabled() )
    {
      DebugMsg debugMsg = new DebugMsg( "CyberLink Event: Device Removed: " +
                                        " Type: " + dev.getDeviceType() + 
                                        " Name: " + dev.getFriendlyName(),
                                        "" );
      addDebugObj( debugMsg );
    }


    if( dev.getDeviceType().indexOf("MediaServer") >= 0 )
    {
      logger.finer(" Removing Media Server! " );

      // Update the GUI asynchronously
      Runnable swingUpdateThread = new Runnable() 
      {
        public void run() 
        {
          // Find server device in list
          MediaDevice mediaServerDevice = 
            mediaDeviceList.getMediaDevice( dev );
          
          if( mediaServerDevice != null )
          {
            avServerPanel.remove( mediaServerDevice.getButton() );
            avServerPanel.repaint();
            serverDevicePanel.repaint();
          }

          // Remove device from list
          mediaDeviceList.remove( mediaServerDevice );

          mediaServerDevice.destroy();

          logger.finer("RUNNING GUI UPDATE!!!!!");
          //frame.pack();
        }

      };
      
      SwingUtilities.invokeLater( swingUpdateThread );
      
    }
    else if( dev.getDeviceType().indexOf("MediaRenderer") >= 0 )
    {
      logger.finer(" Removing Media Renderer! " );

      // Update the GUI asynchronously
      Runnable swingUpdateThread = new Runnable() 
      {
        public void run() 
        {
          // Find renderer device in list
          MediaRendererDevice mediaRenderer = 
            (MediaRendererDevice)mediaDeviceList.getMediaDevice( dev );
          
          if( mediaRenderer != null )
            rendererDevicePanel.remove( mediaRenderer );
          
          // Remove device from list
          mediaDeviceList.remove( mediaRenderer );

          mediaRenderer.destroy();

          logger.finer("RUNNING GUI UPDATE!!!!!");
          frame.pack();
        }
      };
      
      SwingUtilities.invokeLater( swingUpdateThread );
    }
  }

  /**
   *  Handle an incoming UPnP event. Events for *all* services are funneled 
   *  to this routine at the moment. From here they are passed to service-
   *  specific event listener routines (CLink customization)
   *
   *  A better implementation might be to install EventListener's on 
   *  a per-service basis (further Clink mod - TODO)
   *
   *  @param uuid
   *  @param seq
   *  @param name
   *  @param value
   */
  public void eventNotifyReceived( String uuid, long seq, String name,
                                   String value)
  {
    //logger.info("--------------------Event---------------------------");
    //logger.info("event notify : uuid = " + uuid + ", seq = " + 
    //                     seq + ", name = " + name + ", value =" + value); 

    

    //
    // Dispatch event to service-specific listener (OJN CLink mod).
    //
    Service service = getSubscriberService( uuid );
    if( service == null )
    {
      logger.warning("Can't find service for event");
    }
    else
    {
      logger.fine("Service SID = " + service.getSID() );
      service.performEventListener( uuid, seq, name, value );

    }

    // If debug enabled, save action in debug object list
    if ( DebugEvent.getEnabled() )
    {
      DebugEvent debugEvent = new DebugEvent( service,
                                              uuid, seq, name, value );
      addDebugObj( debugEvent );
    }

    //logger.finer("--------------------Event end-----------------------");

  }
  
  /**
   *  Handle an incoming device notification. This routine is actually
   *  invoked for all packets arriving on the SSDP multicast port. 
   *  Packets come in three flavors:
   *
   *  - Search requests  (ssdp:discover)    
   *
   *  - Announce/Alive packets  (ssdp:alive)
   *
   *  - ByeBye packets   (ssdp:bye-bye)
   *
   */
  public void deviceNotifyReceived( SSDPPacket packet )
  {
    DebugObj debugObj;

    // Filter out message based on user settings
    if ( packet.isDiscover() )
    {
      if ( ! DebugSearchRequest.getEnabled() )
        return;

      debugObj = new DebugSearchRequest( packet );
      addDebugObj( debugObj );
    }
    else if ( packet.isByeBye() )
    {
      if ( ! DebugNotifyMsg.getByeByeEnabled() )
        return;

      debugObj = new DebugNotifyMsg( packet );
      addDebugObj( debugObj );
    }
    else if ( packet.isAlive() )
    {
      if ( ! DebugNotifyMsg.getAliveEnabled() )
        return;

      debugObj = new DebugNotifyMsg( packet );
      addDebugObj( debugObj );
    }
    
  }

  /**
   * Handle an incoming response to a search request.  These responses
   * are sent to the unicast SSDP address, not the multicast address
   * used by the above messages (hence the use of a separate callback)
   */
  public void deviceSearchResponseReceived( SSDPPacket packet )
  {
    DebugSearchResponse response = new DebugSearchResponse( packet );
    addDebugObj( response );
  }

  /**
   * Handle subscription requests/responses. These callbacks are only 
   * intended to be used to snoop (debug) the underlying subscription
   * activity, which is managed internally by the CLink UPnP library.
   * 
   * In this application, simply create debug objects out of the 
   * subscription request/response packets.
   */
  public void subscriptionRequestSent( Service service, 
                                       SubscriptionRequest subReq )
  {
    DebugSubscriptionRequest request =
      new DebugSubscriptionRequest( service, subReq );
    addDebugObj( request );
  }

  public void subscriptionResponseReceived( Service service,
                                            SubscriptionResponse subRes )
  {
    DebugSubscriptionResponse response =
      new DebugSubscriptionResponse( service, subRes );
    addDebugObj( response );
  }


  public ArrayList getActiveMediaRenderers()
  {
    return mediaDeviceList.getActiveMediaRenderers();
  }

  /** 
   * Close down controller.
   */
  public void close()
  {
    stop();   // Does an unsubscribe of all subscribed services
    System.exit(-1);
  }
  
  /**
   *  Window event listeners
   */
  public void windowClosing(WindowEvent e) {
    //System.out.println("WindowListener method called: windowClosing.");
    close();
  }

  public void windowClosed(WindowEvent e) {
    //This will only be seen on standard output.
    //System.out.println("WindowListener method called: windowClosed.");
    close();
  }

  public void windowOpened(WindowEvent e) {
    //System.out.println("WindowListener method called: windowOpened.");
  }

  public void windowIconified(WindowEvent e) {
    //System.out.println("WindowListener method called: windowIconified.");
  }

  public void windowDeiconified(WindowEvent e) {
    //System.out.println("WindowListener method called: windowDeiconified.");
  }

  public void windowActivated(WindowEvent e) {
    //System.out.println("WindowListener method called: windowActivated.");
  }

  public void windowDeactivated(WindowEvent e) {
    //System.out.println("WindowListener method called: windowDeactivated.");
  }

  /**
   *  If OS issues a standby request (Windows-only at the moment), check to see
   *  if the controller is actively controlling any playback sessions. If not,
   *  allow the system to go into standby. If controller is 'active', refuse
   *  to be put into standby.
   *
   *  In standby mode, the UPnP 'alive' messages and service resubscription
   *  messages no longer flow, so it is best to issue a 'stop' to the 
   *  control point so other UPnP devices aren't trying to pass events
   *  to a control point that is no longer listening.
   */
  public void standByRequested()
  {
    logger.info("Standby requested");

    if( isActivelyControllingRenderer() )
    {
      logger.info("Not allowing standby, controller in use");
      standByDetector.setAllowStandby( false );
    }
    else
    {
      logger.info("Allowing Standby after stopping controller");
      stop();
      standByDetector.setAllowStandby( true );
    }
    MrUtil.sleep(2000);
  }
  
  public boolean isActivelyControllingRenderer()
  {
    ArrayList mediaRendererList = getActiveMediaRenderers();

    for( int dev = 0 ; dev < mediaRendererList.size() ; dev++ )
    {
      MediaRendererDevice mediaRenderer =  
      (MediaRendererDevice)mediaRendererList.get(dev);
          
      if( mediaRenderer.isRunning() )
        return true;
    }
    
    return false;
  }

  static String os = null;
  
  public static boolean isRunningOnMacOSX()
  {
    if( os == null )
    {
      os = System.getProperty("os.name");
    }
    
    logger.fine("OS = " + os );
    if( os.toLowerCase().indexOf("os x") >= 0 )
      return true;

    return false;
  }

  public static boolean isRunningOnWindows()
  {
    if( os == null )
    {
      os = System.getProperty("os.name");
    }
    
    logger.fine("OS = " + os );
    if( os.toLowerCase().indexOf("window") >= 0 )
      return true;

    return false;
  }

  public static String getProxyIPAddress()
  {
    if( preferences.get("syncProxyType","local").equals("remote") )
    {
      return preferences.get("syncProxyIPAddress",
                             NetUtil.getDefaultLocalIPAddress() );
    }
    else
    {
      String addr = NetUtil.getDefaultLocalIPAddress();
      logger.fine("************Returning default local IP of " + addr );
      return addr;
    }
  }


  public static void main( String args[] ) 
  {
    try 
    {
      //Schedule a job for the event-dispatching thread:
      //creating and showing this application's GUI.
      javax.swing.SwingUtilities.invokeAndWait(
      new Runnable()
      {
        public void run() {
          MediaController.getInstance();
        }
      });
      
    }
    catch( Exception e )
    {
      e.printStackTrace();
      System.exit(-1);
    }
    
    //
    // Invoke inherited control point start method. This is not done in
    // createAndShowGUI() since we want to run the controller threads
    // that get fired off at the main thread priority (not the Swing 
    // priority) 
    //
    controller.start();

    //
    // Alternative startup - only register interest in MediaRenderer and 
    // MediaServer devices (not yet TODO)
    //
    //controller.start( "urn:schemas-upnp-org:device:MediaServer:1" );
    //controller.start( "urn:schemas-upnp-org:device:MediaRenderer:1" );

    //
    // Make sure control point stop routine is invoked at shutdown. It's
    // important to unsubscribe to all device services if possible (some
    // UPnP devices never time out subscriptions properly, so the
    // subscriptions get left hanging and take up resources)
    //
    Runtime.getRuntime().addShutdownHook( new Thread() 
    {
      public void run() 
      {
        System.out.println("Java runtime shutting down");
        controller.stop();
      }
    });
    
    //
    // Start device search thread that issues search requests every 5 
    // minutes, 2 at a time.  This is to handle some devices that don't
    // transmit multiple 'alive' notifications in a wireless environment,
    // which can cause device problems if the single packets are lost.
    //

    DeviceSearchThread devSearchThread =
      new DeviceSearchThread( controller, 5*60, 2 );

    devSearchThread.start();
    
    //
    // Windows Media Connect doesn't respond to search requests from a 
    // controller running on the same host. If user has enabled the
    // workaround patch, and there is a WMC device location stored from
    // the last run, attempt to manually 'discover' the device using the 
    // stored device location URL
    //
    if( preferences.getBoolean("wmcDiscoveryBugPatchEnable") )
    {
      String devURL = preferences.get("wmcDeviceDescriptionURL","");
      logger.info("WMC Device description URL = '" + devURL + "'");
      if( ! devURL.trim().equals("") )
      {
        logger.info("Attempting to add device!!!!!!!!!");
        logger.info(" !!!!!!!!!");

        controller.addDevice( devURL, "max-age=1800" );

        logger.info("After attempting to add device!!!!!!!!!");
        logger.info(" !!!!!!!!!");
      }
    }
    
  }

  
}
