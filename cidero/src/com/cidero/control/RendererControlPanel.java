/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: RendererControlPanel.java,v $
 *
 */

package com.cidero.control;

import java.awt.*;
import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.awt.event.ItemEvent;
//import java.awt.event.ItemListener;
import java.awt.event.*;

import java.util.Observable;
import java.util.Observer;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;

import com.cidero.upnp.AVTransport;
import com.cidero.upnp.RendererStateModel;
import com.cidero.util.MrUtil;
import com.cidero.util.URLUtil;

/**
 *  Control panel for an instance of a Media Renderer.  A single 
 *  control point may use multiple instances of this class if
 *  controlling multiple renderers
 */
public class RendererControlPanel extends JPanel implements Observer
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  RendererControlWindow controlWindow;
  MediaRendererDevice mediaRenderer;  // parent renderer

  // State model that reflects state of actual device
  RendererStateModel  stateModel;     

  // 'Pseudo' state model for use with image slideshow playback (Don't
  // want play controls to follow device level state, but instead remain
  // in the PLAYING state for the duration of the slideshow
  RendererStateModel  pseudoStateModel;

  JPanel  transportPanel;
  JPanel  statusPanel;
  JPanel  linkPanel;

  JButton stopButton;
  JButton pauseButton;
  JButton playButton;
  JButton prevButton;
  JButton nextButton;
  JButton repeatOneButton;
  JButton repeatAllButton;
  JButton expandButtonLeft;
  JButton expandButtonCenter;
  JButton expandButtonRight;
  JButton linkButton;
  JButton unlinkButton;

  JRadioButton shuffleButton;

  JSlider  playSlider;

  ImageIcon playIcon;          // Normal play icon
  ImageIcon playPressedIcon;  // Play icon when selected

  JLabel trackInfoLabel;
  JLabel trackTimeLabel;

  Color bgColor = new Color( 210, 210, 210 );
  
  int viewMode = RendererControlWindow.AUDIO_QUEUE_VIEW;
  
  /**
   *  Constructor. All button presses currently dispatched to single
   *  Action listener instance (shared handler). All slider changes 
   *  dispatched to ChangeListener instance (shared handler)
   */
  public RendererControlPanel( RendererControlWindow controlWindow,
                               MediaRendererDevice mediaRenderer )
  {
    this.controlWindow = controlWindow;
    this.mediaRenderer = mediaRenderer;

    BoxLayout layout = new BoxLayout( this, BoxLayout.Y_AXIS );
    setLayout(layout);


    //
    // Upper frame contains play/stop/pause/ff/fr buttons on left, 
    // volume controls on right (free space grabber)
    //
    JPanel topPanel = new JPanel( new BorderLayout() );
    topPanel.setBackground( bgColor );
    add( topPanel );
    
    transportPanel = new JPanel();
    topPanel.add( transportPanel, BorderLayout.WEST );
    transportPanel.setBorder( BorderFactory.createEmptyBorder(4,4,2,4) );
    transportPanel.setBackground( bgColor );
    
    // Use box layout for transport buttons so struts (spacers) can be
    // inserted
    BoxLayout transportLayout = new BoxLayout( transportPanel,
                                               BoxLayout.X_AXIS );
    transportPanel.setLayout( transportLayout );
    

    // Note: Use 'null' tooltips for commonly known buttons (avoid unnecessary
    // tooltips where possible)

    // Stop
    stopButton = createTransportButton( "stop36.gif",
                                        null,
                                        "stopPressed36.gif",
                                        "stopSel36.gif" );
    stopButton.addActionListener( new StopActionListener() );
    transportPanel.add( stopButton );

    // Pause
    pauseButton = createTransportButton( "pause36.gif",
                                         null,
                                         "pausePressed36.gif",
                                         "pauseSel36.gif" );
    pauseButton.addActionListener( new PauseActionListener() );
    transportPanel.add( pauseButton );

    // Play
    playButton = createTransportButton( "play36.gif",
                                        null,
                                        "playPressed36.gif",
                                        "playSel36.gif" );
    playButton.addActionListener( new PlayActionListener() );
    transportPanel.add( playButton );

    // Insert a little space before prev/next buttons
    //transportPanel.add( Box.createHorizontalStrut(8) );

    // Previous
    prevButton = createTransportButton( "prev36.gif",
                                        null,
                                        "prevPressed36.gif",
                                        null );
    prevButton.addActionListener( new PrevActionListener() );
    transportPanel.add( prevButton );

    // Next
    nextButton = createTransportButton( "next36.gif",
                                        null,
                                        "nextPressed36.gif",
                                        null );
    nextButton.addActionListener( new NextActionListener() );
    transportPanel.add( nextButton );


    // Insert a little space before repeat mode buttons
    transportPanel.add( Box.createHorizontalStrut(8) );


    // Repeat one
    repeatOneButton = createTransportButton( "repeatOne36.gif",
                                             "Repeat single track",
                                             "repeatOnePressed36.gif",
                                             "repeatOneSel36.gif" );
    repeatOneButton.addActionListener( new RepeatOneActionListener() );

    transportPanel.add( repeatOneButton );

    // Repeat all
    repeatAllButton = createTransportButton( "repeatAll36.gif",
                                             "Repeat all tracks",
                                             "repeatAllPressed36.gif",
                                             "repeatAllSel36.gif" );
    repeatAllButton.addActionListener( new RepeatAllActionListener() );
    transportPanel.add( repeatAllButton );

    

    // Group volume slider & mute in separate panel.  Volume controls are
    // optional depending on whether the device supports it (Philips 
    // Streamium 300i, for example, doesn't have volume control feature)
    if( mediaRenderer != null && 
        mediaRenderer.supportsAction( "RenderingControl", "SetVolume" ) )
    {
      VolumeControlPanel volumeControlPanel;
      volumeControlPanel = new VolumeControlPanel( mediaRenderer );
      volumeControlPanel.setBorder( BorderFactory.createEmptyBorder(4,8,4,8) );
      volumeControlPanel.setComponentBackground( bgColor );

      topPanel.add( volumeControlPanel, BorderLayout.CENTER );
    }


    // Link/unlink buttons on right side of panel
    linkPanel = new JPanel();
    topPanel.add( linkPanel, BorderLayout.EAST );

    //    BoxLayout linkLayout = new BoxLayout( linkPanel,
    //                                          BoxLayout.Y_AXIS );
    BorderLayout linkLayout = new BorderLayout();
    linkPanel.setLayout( linkLayout );

    linkPanel.setBorder( BorderFactory.createEmptyBorder(4,4,2,4) );
    linkPanel.setBackground( bgColor );

    linkButton = createTransportButton( "linkButton.gif",
                                        "Link renderers",
                                        "linkButton.gif",
                                        "linkButtonSel.gif" );

    MouseListener popupListener = new PopupListener();
    linkButton.addMouseListener(popupListener);


    //linkButton.addActionListener( new PlayActionListener() );
    //linkPanel.add( linkButton );
    linkPanel.add( linkButton, BorderLayout.SOUTH );



    //
    // Status display line panel (Artist - Album  Time)
    //
    statusPanel = new JPanel( new BorderLayout() );

    statusPanel.setBorder( BorderFactory.createEmptyBorder(2,2,0,2) );
    statusPanel.setBackground( bgColor );

    trackInfoLabel= new JLabel("Unknown Artist - Unknown Title");
    trackInfoLabel.setBackground( Color.black );
    trackInfoLabel.setForeground( Color.cyan );
    trackInfoLabel.setOpaque( true );

    trackInfoLabel.setBorder( BorderFactory.createEmptyBorder(8,4,8,4) );

    statusPanel.add( trackInfoLabel, BorderLayout.CENTER );

    trackTimeLabel= new JLabel("00:00/00:00");
    trackTimeLabel.setBackground( Color.black );
    trackTimeLabel.setForeground( Color.cyan );
    trackTimeLabel.setOpaque( true );
    trackTimeLabel.setBorder( BorderFactory.createEmptyBorder(8,4,8,4) );

    statusPanel.add( trackTimeLabel, BorderLayout.EAST );

    add( statusPanel );

    //
    // PlayQueue panel expand/collapse button
    //
    JPanel expandPanel = new JPanel( new BorderLayout() );
    add( expandPanel );

    expandPanel.setBorder( new CompoundBorder( 
                   BorderFactory.createEmptyBorder(2,0,0,0),
                   BorderFactory.createLineBorder( Color.gray,1) ) );

    expandButtonLeft = createExpandButton( "expand4.gif",
                                           "Expose/hide playlist panel",
                                           "expand4.gif",
                                           "collapse4.gif" );
    expandButtonLeft.addActionListener( new ExpandActionListener() );

    // Center button is invisible - expand icon looks weird in middle
    // of panel (still need button tho...)
    expandButtonCenter = createExpandButton( null,
                                            "Expose/hide playlist panel",
                                             null,
                                             null );
    expandButtonCenter.addActionListener( new ExpandActionListener() );

    expandButtonRight = createExpandButton( "expand4.gif",
                                            "Expose/hide playlist panel",
                                            "expand4.gif",
                                            "collapse4.gif" );
    expandButtonRight.addActionListener( new ExpandActionListener() );

    expandPanel.add( expandButtonLeft, BorderLayout.WEST );
    expandPanel.add( expandButtonCenter, BorderLayout.CENTER );
    expandPanel.add( expandButtonRight, BorderLayout.EAST );

    // 
    // Now that all panel buttons have been created, become observer of
    // renderer's state model. (if addObserver was called earlier, 
    // there's a race condition where update() could get invoked before
    // the buttons exist)
    //
    if( mediaRenderer != null )
    {
      stateModel = mediaRenderer.getStateModel();
      pseudoStateModel = mediaRenderer.getPseudoStateModel();
    }
    else
    {
      stateModel = new RendererStateModel();
      pseudoStateModel = new RendererStateModel();
    }
    
    stateModel.addObserver( this );
    pseudoStateModel.addObserver( this );
    
    // Do intial read of state from model 
    update( stateModel, null );
  }

  /**
   * Controls are set up a bit different for audio/video vs. image items
   */
  public void setViewMode( int mode )
  {
    if( viewMode == mode )  
      return; // no change
    
    viewMode = mode;
    
    if( (viewMode == RendererControlWindow.AUDIO_QUEUE_VIEW) ||
        (viewMode == RendererControlWindow.VIDEO_QUEUE_VIEW) )
    {
      transportPanel.remove( repeatAllButton );
      transportPanel.add( repeatOneButton );
      transportPanel.add( repeatAllButton );
      transportPanel.revalidate();

      enableAllTransportControls();
    }
    else if( viewMode == RendererControlWindow.IMAGE_QUEUE_VIEW )
    {
      transportPanel.remove( repeatOneButton );
      transportPanel.revalidate();

      if( controlWindow.getImageQueuePanel().getSlideshowMode() ==
          ImageQueuePanel.COMPOSE_SLIDESHOW )
        disableAllTransportControls();
      else
        disableAllTransportControls();

    }
  }

  public void enableAllTransportControls()
  {
    stopButton.setEnabled( true );
    pauseButton.setEnabled( true );
    playButton.setEnabled( true );
    nextButton.setEnabled( true );
    prevButton.setEnabled( true );
    repeatOneButton.setEnabled( true );
    repeatAllButton.setEnabled( true );
  }
  
  public void disableAllTransportControls()
  {
    stopButton.setEnabled( false );
    pauseButton.setEnabled( false );
    playButton.setEnabled( false );
    nextButton.setEnabled( false );
    prevButton.setEnabled( false );
    repeatOneButton.setEnabled( false );
    repeatAllButton.setEnabled( false );
  }
  
  static Insets transportButtonInsets = new Insets( 2, 2, 2, 2 );
  static Border transportButtonBorder = 
    BorderFactory.createEmptyBorder(4,2,4,2);

  public static JButton createTransportButton( String iconName,
                                               String toolTipText,
                                               String pressedIconName,
                                               String selectedIconName )
  {
    ImageIcon icon = MrUtil.createImageIcon( iconName );
    JButton button = new JButton( "", icon );

    if( toolTipText != null )
      button.setToolTipText( toolTipText );
      
    if( pressedIconName != null )
    {
      icon = MrUtil.createImageIcon( pressedIconName );
      button.setPressedIcon( icon );
    }

    if( selectedIconName != null )
    {
      icon = MrUtil.createImageIcon( selectedIconName );
      button.setSelectedIcon( icon );
      button.setRolloverSelectedIcon( icon );
    }

    button.setMargin( transportButtonInsets );
    button.setContentAreaFilled( false );
    button.setBorderPainted( false );
    button.setFocusPainted( false );
    button.setBorder( transportButtonBorder );

    return button;
  }

  static Border expandButtonBorder = 
    BorderFactory.createEmptyBorder(1,10,1,10);

  public static JButton createExpandButton( String iconName,
                                            String toolTipText,
                                            String pressedIconName,
                                            String selectedIconName )
  {
    ImageIcon icon = MrUtil.createImageIcon( iconName );
    JButton button;
    if( icon != null )
      button = new JButton( "", icon );
    else
      button = new JButton( "" );

    if( toolTipText != null )
      button.setToolTipText( toolTipText );
      
    if( pressedIconName != null )
    {
      icon = MrUtil.createImageIcon( pressedIconName );
      button.setPressedIcon( icon );
    }

    if( selectedIconName != null )
    {
      icon = MrUtil.createImageIcon( selectedIconName );
      button.setSelectedIcon( icon );
      button.setRolloverSelectedIcon( icon );
    }

    // Margin is distance between button border and button text/icon 
    // Ignored if button.setBorder is used
    //button.setMargin( expandButtonInsets );
    button.setContentAreaFilled( true );
    button.setBorderPainted( false );
    button.setFocusPainted( false );
    button.setBorder( expandButtonBorder );
    button.setBackground( new Color( 180, 180, 180 ) );
    //    button.setBackground( Color.gray );
    //button.setForeground( Color.blue );

    return button;
  }

  
  public void update()
  {
    update( stateModel, null );
  }
  

  /**
   *  Update UI elements based on state change. Note that
   *  volume control panel registers separately with stateModel
   *  and has its own version of update()
   */

  boolean inUpdate = false;

  public void update( Observable rendererStateModel, Object arg )
  {
    RendererStateModel model = (RendererStateModel)rendererStateModel;

    if( viewMode == RendererControlWindow.IMAGE_QUEUE_VIEW )
    {
      if( model == stateModel )  
        return; // Ignore true state model updates in image mode
    }
    else
    {
      if( model == pseudoStateModel )
        return; // Ignore pseudo state model updates in normal mode
    }

    logger.fine("RendererControlUpdate - transportState = " + 
                model.getTransportState() );

    inUpdate = true;
    
    // Code to Enable/disable buttons to reflect current transport state.
    // Not yet used, since it is sometimes useful to send a device a 
    // 'stop' if the control point already thinks it's stopped (one example)
    // May want to revisit this when UPnP device implementations become
    // more uniform TODO
    /* 
    String transportState = model.getTransportState();
    if( transportState.equals( AVTransport.STATE_STOPPED ) )
    {
      stopButton.setEnabled( false );
      pauseButton.setEnabled( false );
      playButton.setEnabled( true );
    }
    else if( transportState.equals( AVTransport.STATE_PAUSED_PLAYBACK ) )
    {
      stopButton.setEnabled( true );
      pauseButton.setEnabled( false );
      playButton.setEnabled( true );
    }
    else if( transportState.equals( AVTransport.STATE_PLAYING ) )
    {
      stopButton.setEnabled( true );
      pauseButton.setEnabled( true );
      playButton.setEnabled( false );
    }
    */

    String transportState = model.getTransportState();

    if( transportState.equals( AVTransport.STATE_STOPPED ) )
    {
      stopButton.setSelected( true );
      pauseButton.setSelected( false );
      playButton.setSelected( false );
    }
    else if( transportState.equals( AVTransport.STATE_PAUSED_PLAYBACK ) )
    {
      stopButton.setSelected( false );
      pauseButton.setSelected( true );
      playButton.setSelected( false );
    }
    else if( transportState.equals( AVTransport.STATE_PLAYING ) )
    {
      stopButton.setSelected( false );
      pauseButton.setSelected( false );
      playButton.setSelected( true );
    }
    else if( transportState.equals( AVTransport.STATE_NO_MEDIA ) )
    {
      stopButton.setSelected( false );
      pauseButton.setSelected( false );
      playButton.setSelected( false );
    }

    String playMode = model.getPlayMode();
    if( playMode.equals( AVTransport.PLAY_MODE_NORMAL ) )
    {
      //shuffleButton.setSelected( false );
      repeatOneButton.setSelected( false );
      repeatAllButton.setSelected( false );
    }
    else if( playMode.equals( AVTransport.PLAY_MODE_RANDOM ) ||
             playMode.equals( AVTransport.PLAY_MODE_SHUFFLE ) )
    {
      //shuffleButton.setSelected( true );
      repeatOneButton.setSelected( false );
      repeatAllButton.setSelected( false );
    }
    else if( playMode.equals( AVTransport.PLAY_MODE_REPEAT_ONE ) )
    {
      //shuffleButton.setSelected( false );
      repeatOneButton.setSelected( true );
      repeatAllButton.setSelected( false );
    }
    else if( playMode.equals( AVTransport.PLAY_MODE_REPEAT_ALL ) )
    {
      //shuffleButton.setSelected( false );
      repeatOneButton.setSelected( false );
      repeatAllButton.setSelected( true );
    }

    //
    // TrackInfo & trackTime Status line. 
    // 
    //  Line looks like:
    // 
    //    'Artist - Title                   Pos/Duration'
    // 
    // Note: Protect against null track info fields in state model
    //

    // 1st line
    StringBuffer buf = new StringBuffer();
  
    if( transportState.equals( AVTransport.STATE_NO_MEDIA ) )
    {
      buf.append(" -- NO MEDIA PRESENT -- ");
    }
    else
    {
      // Not bothering with Artist for photos yet - it's not present very
      // often in the photo metadata yet
      if( viewMode != RendererControlWindow.IMAGE_QUEUE_VIEW )
      {
        if( model.getTrackArtist() == null )
          buf.append( "Unknown Artist - " );
        else
          buf.append( model.getTrackArtist() + " - " );
      }
      
      String title = model.getTrackTitle();
      if( (title == null) || (title.length() == 0) )
      {
        // No explicit title - create one from tail end of URL if possible
        title = model.getTrackURI();
        if( title != null )
        {
          if( (title = URLUtil.getPathTail( title )) == null )
            buf.append( "Untitled" );
          else
            buf.append( title );
        }
        else
        {
          buf.append( "Untitled" );
        }
      }
      else
      {
        buf.append( title );
      }
    }
    
    trackInfoLabel.setText( buf.toString() );
    

    buf = new StringBuffer();

    if( model.getTrackRelTime() == null )
      buf.append( "0:00:00 /" );
    else
      buf.append( model.getTrackRelTime() + " / ");

    String duration = model.getTrackDuration();
    if( duration == null )
      buf.append( "0:00:00" );
    else
      buf.append( duration );

    trackTimeLabel.setText( buf.toString() );

    // Track number not currently displayed (not too meaningful in playlists)
    // TODO: May want   <N> of <NNN> type display
    /*
    if( model.getTrackNum() == null )
      trackNumLabel.setText( "?:" );
    else
      trackNumLabel.setText( model.getTrackNum() + ":");
    */
    
    MediaRendererDevice masterRenderer = mediaRenderer.getMasterRenderer();
    if( masterRenderer != null )
    {
      controlWindow.setTitle( mediaRenderer.getFriendlyName() + 
                              " (Sync'd to '" +
                              masterRenderer.getFriendlyName() + "')" );
      linkButton.setSelected(true);
    }
    else if( mediaRenderer.getSlaveRendererList().size() > 0 )
    {
      buf = new StringBuffer();
      buf.append( mediaRenderer.getFriendlyName() + "(" );
      
      for( int n = 0 ; n < mediaRenderer.getSlaveRendererList().size() ; n++ )
      {
        MediaRendererDevice slaveRenderer = 
        (MediaRendererDevice)mediaRenderer.getSlaveRendererList().get(n);

        buf.append( " +" + slaveRenderer.getFriendlyName() );
      }
      
      buf.append( ")" );
      controlWindow.setTitle( buf.toString() );
      linkButton.setSelected(true);
    }
    else
    {
      controlWindow.setTitle( mediaRenderer.getFriendlyName() );
      linkButton.setSelected(false);
    }

    inUpdate = false;

  }

  /**
   *  Button action listeners
   */
  public class StopActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.fine("StopActionListener: Entered" );

      // Following check enables UI look & feel testing w/o real renderer
      if( mediaRenderer != null )   
      {
        if( viewMode == RendererControlWindow.IMAGE_QUEUE_VIEW )
          mediaRenderer.stopImagePlayback();
        else
          mediaRenderer.stopPlayback();
      }
    }
  }

  public class PauseActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.fine("Entered" );
      if( mediaRenderer != null )
      {
        if( viewMode == RendererControlWindow.IMAGE_QUEUE_VIEW )
          mediaRenderer.pauseImagePlayback();
        else
          mediaRenderer.pausePlayback();
      }
    }
  }

  public class PlayActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.fine("Entered" );
      if( mediaRenderer != null )
      {
        if( viewMode == RendererControlWindow.IMAGE_QUEUE_VIEW )
        {
          // Image queue panel has most of the info needed to run playback
          // so delegate to it
          controlWindow.getImageQueuePanel().
            startImagePlayback( AVTransport.PLAY_MODE_NORMAL );
        }
        else
        {
          mediaRenderer.startPlayback();
        }
      }
    }
  }

  public class PrevActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.fine("Entered" );
      if( mediaRenderer != null )
      {
        if( viewMode == RendererControlWindow.AUDIO_QUEUE_VIEW )
          mediaRenderer.prev();
        else if( viewMode == RendererControlWindow.IMAGE_QUEUE_VIEW )
          controlWindow.getImageQueuePanel().selectPrev();
      }
    }
  }

  public class NextActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.fine("Entered" );
      if( mediaRenderer != null )
      {
        if( viewMode == RendererControlWindow.AUDIO_QUEUE_VIEW )
          mediaRenderer.next();
        else if( viewMode == RendererControlWindow.IMAGE_QUEUE_VIEW )
          controlWindow.getImageQueuePanel().selectNext();
      }
    }
  }

  public class RepeatOneActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.fine("Entered" );
      if( mediaRenderer != null )
      {
        if( repeatOneButton.isSelected() )
        {
          // Toggle repeatOne OFF
          repeatOneButton.setSelected( false );
          mediaRenderer.setPlayMode( AVTransport.PLAY_MODE_NORMAL );
        }
        else
        {
          // Toggle repeatOne ON
          repeatOneButton.setSelected( true );
          mediaRenderer.setPlayMode( AVTransport.PLAY_MODE_REPEAT_ONE );
        }
      }
    }
  }

  public class RepeatAllActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.fine("Entered" );

      if( repeatAllButton.isSelected() )
      {
        // Toggle repeatOne OFF
        repeatAllButton.setSelected( false );
        if( mediaRenderer != null )
        {
          if( viewMode == RendererControlWindow.IMAGE_QUEUE_VIEW )
          {
            pseudoStateModel.setPlayMode(AVTransport.PLAY_MODE_NORMAL);
            pseudoStateModel.notifyObservers();
          }
          else
          {
            mediaRenderer.setPlayMode( AVTransport.PLAY_MODE_NORMAL );
          }
        }
      }
      else
      {
        // Toggle repeatOne ON
        repeatAllButton.setSelected( true );

        if( mediaRenderer != null )
        {
          if( viewMode == RendererControlWindow.IMAGE_QUEUE_VIEW )
          {
            pseudoStateModel.setPlayMode( AVTransport.PLAY_MODE_REPEAT_ALL );
            pseudoStateModel.notifyObservers();
          }
          else
          {
            mediaRenderer.setPlayMode( AVTransport.PLAY_MODE_REPEAT_ALL );
          }
        }
      }
      repeatOneButton.setSelected( false );
    }
  }

  public void setExpandButtonsSelected( boolean flag )
  {
    expandButtonLeft.setSelected(flag);
    expandButtonRight.setSelected(flag);
  }
  

  public class ExpandActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.fine("Entered" );

      if( expandButtonLeft.isSelected() )
      {
        // Collapse playlist window (remove from control panel container)
        controlWindow.collapsePlayQueuePanel();
      }
      else
      {
        controlWindow.expandPlayQueuePanel();
      }
    }
  }

  class PopupListener extends MouseAdapter
  {
    PopupListener() {
    }

    public void mousePressed(MouseEvent e) {
      maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) {
      maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e)
    {
      //      if (e.isPopupTrigger()) {

      JPopupMenu popup = new JPopupMenu();

      JMenuItem rendererNone = new JMenuItem("Break Link...");
      rendererNone.addActionListener( 
                  new LinkedRendererActionListener( null ) );
      // If this renderer is linked (master OR slave), then 'Break Link'
      // item is active, otherwise it isn't
      if( ! mediaRenderer.isLinked() )
        rendererNone.setEnabled(false);

      popup.add( rendererNone );

      // Get the list of all renderers of the same type as name
      // (just use all renderers for now)

      ArrayList rendererList =
        MediaController.getInstance().getActiveMediaRenderers();

      for( int n = 0 ; n < rendererList.size() ; n++ )
      {
        MediaRendererDevice renderer = 
          (MediaRendererDevice)rendererList.get(n);
        
        if( !renderer.getFriendlyName().
            equals( mediaRenderer.getFriendlyName() ) ) // Exclude curr rend
        {
          JMenuItem rendererItem = new JMenuItem("Link to '" + 
                           renderer.getFriendlyName() + "'" );

          rendererItem.addActionListener( 
                  new LinkedRendererActionListener( renderer ) );

          if( mediaRenderer.isLinked() )
            rendererItem.setEnabled(false);

          popup.add( rendererItem );
        }
      }
      
      popup.show(e.getComponent(),
                 e.getX(), e.getY());
        //      }
    }
  }

  public class LinkedRendererActionListener implements ActionListener
  {
    MediaRendererDevice masterRenderer;

    public LinkedRendererActionListener( MediaRendererDevice masterRenderer )
    {
      this.masterRenderer = masterRenderer;
    }

    public void actionPerformed( ActionEvent e )
    {
      if( masterRenderer == null )
      {
        // Break link selected...

        if( mediaRenderer.getMasterRenderer() != null )
        {
          mediaRenderer.getMasterRenderer().
                        removeSlaveRenderer( mediaRenderer );
          //mediaRenderer.setMasterRenderer(null);
          enableAllTransportControls();
          
          Rectangle pos = controlWindow.getBounds();
          controlWindow.setLocation( pos.x, pos.y+8 );

          // Invoke update to set titles of master & slave renderers
          update( stateModel, null );
        }
        else if( mediaRenderer.getSlaveRendererList().size() > 0 )
        {
          for( int n = 0 ; n < mediaRenderer.getSlaveRendererList().size() ;
               n++ )
          {
            MediaRendererDevice slaveRenderer = 
            (MediaRendererDevice)mediaRenderer.getSlaveRendererList().get(n);
            
            mediaRenderer.removeSlaveRenderer( slaveRenderer );

            Rectangle pos = slaveRenderer.
                            getRendererControlWindow().getBounds();
            slaveRenderer.getRendererControlWindow().
                          setLocation( pos.x, pos.y+8*(n+1) );

            slaveRenderer.getRendererControlWindow().
                          getControlPanel().enableAllTransportControls();

            // Invoke update to set titles of master & slave renderers
            slaveRenderer.getRendererControlWindow().
                          getControlPanel().update();
          }

          // Invoke update to set titles of master & slave renderers
          update( stateModel, null );
        }
        
      }
      else
      {
        disableAllTransportControls();
        //linkButton.setSelected(true);
        controlWindow.collapsePlayQueuePanel();

        // Get the position of the master renderer window and move this
        // slave renderer directly below it and any existing slaves
        Rectangle pos = masterRenderer.getRendererControlWindow().getBounds();
        System.out.println("master window x,y,w,h: " + pos.x + "," + pos.y +
                           "," + pos.width + "," + pos.height );
        
        for( int n = 0 ; n < masterRenderer.getSlaveRendererList().size() ;
             n++ )
        {
          MediaRendererDevice slaveRenderer = 
            (MediaRendererDevice)masterRenderer.getSlaveRendererList().get(n);

          Rectangle slavePos = slaveRenderer.
                               getRendererControlWindow().getBounds();
          pos.height += slavePos.height;
        }
        
        // Add this renderer to masters slave renderer list and disable 
        // all the transport controls
        masterRenderer.addSlaveRenderer( mediaRenderer );
        mediaRenderer.setMasterRenderer( masterRenderer );

        mediaRenderer.getRendererControlWindow().
                      setLocation( pos.x, pos.y+pos.height );

        // Invoke update to set titles of master & slave renderers
        update( stateModel, null );
      }

		}
	}

  /*
  public class PlayModeItemListener implements ItemListener
  {
    public void itemStateChanged( ItemEvent e )
    {
      logger.info("PlayModeItemListener: itemStateChanged(): Entered" );

      JRadioButton button = (JRadioButton)e.getItem();
      
      if( ! button.isSelected() )
      {
        if( !inUpdate )
        {
          if( mediaRenderer != null )
            mediaRenderer.actionSetPlayMode( AVTransport.PLAY_MODE_NORMAL );
        }
      }
      else
      {
        if( button == shuffleButton )
        {
          rptOneButton.setSelected( false );
          rptAllButton.setSelected( false );
          if( !inUpdate )
          {
            if( mediaRenderer != null )
              mediaRenderer.actionSetPlayMode( AVTransport.PLAY_MODE_SHUFFLE );
          }
        }
        else if( button == rptOneButton )
        {
          shuffleButton.setSelected( false );
          rptAllButton.setSelected( false );
          if( !inUpdate )
          {
            if( mediaRenderer != null )
              mediaRenderer.actionSetPlayMode( AVTransport.PLAY_MODE_REPEAT_ONE );
          }
        }
        else if( button == rptAllButton )
        {
          shuffleButton.setSelected( false );
          rptOneButton.setSelected( false );
          if( !inUpdate )
          {
            if( mediaRenderer != null )
              mediaRenderer.actionSetPlayMode( AVTransport.PLAY_MODE_REPEAT_ALL );
          }
        }
      }

    }
  }
  */


}

