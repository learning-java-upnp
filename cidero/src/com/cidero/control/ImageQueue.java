/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ImageQueue.java,v $
 *
 */

package com.cidero.control;

import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.AbstractListModel;
import javax.swing.ListSelectionModel;
import javax.swing.DefaultListSelectionModel;

import com.cidero.upnp.*;

/**
 *  Image list model for grid or list of images (image display window
 *  supports different views). Can be used with JList's. Selection model
 *  is part of this class so selections can be made programatically without
 *  knowing about the selection listeners at the UI level
 */
public class ImageQueue extends AbstractListModel
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  DefaultListSelectionModel selectionModel = new DefaultListSelectionModel();
  
  Vector imageList = new Vector();

  //
  // Currently, a dummy item is always maintained at end for rendering
  // a blank item in the JList (enable sorter insertion bar at end of
  // list)  TODO - figure out different way to do this (extend JList
  // behaviour?)
  //
  ImageQueueElem dummyItem;

  String playMode = AVTransport.PLAY_MODE_NORMAL;

  // Index of current image (current queue position)
  int currIndex = 0;

  public ImageQueue()
  {
    dummyItem = new ImageQueueElem();
    imageList.add( dummyItem );
  }

  public ListSelectionModel getSelectionModel() {
    return selectionModel;
  }
  
  /**
   *  Clear model. Leave dummy item at end
   */
  public void clear()
  {
    int size = imageList.size();
    imageList.clear();
    imageList.add( dummyItem );
    if( size > 1 )
      fireIntervalRemoved( this, 0, size-2 );
  }
  
  /**
   *  Set the current queue position.
   */
  public synchronized void setCurrentPosition( int index )
  {
    if( (index < 0) || (index >= size()) )
      logger.warning("index argument (" + index + ") out of bounds" );
    else
      currIndex = index;

    //fireActiveItemChanged();  selected is not same as active (busy)
  }

  /**
   *  Create an imageModelElem object from image and add it to the model
   */ 
  public void add( CDSImageItem image )
  {
    ImageQueueElem elem = new ImageQueueElem( image );

    // Get the icon for the image so it is cached for later rapid display 
    ImageIcon icon = elem.getImageIcon();

    // Insert at end, just before dummy item
    imageList.insertElementAt( elem, imageList.size()-1 );

    // Now doing fireContentsChanged after adding a whole set of images
    // (one event is more efficient)
    //fireIntervalAdded( this, imageList.size()-1, imageList.size()-1 );
  }

  public CDSObject getCDSObject( int index )
  {
    ImageQueueElem imageElem = (ImageQueueElem)imageList.get(index);
    return imageElem.getCDSObject();
  }
  
  public boolean remove( ImageQueueElem elem )
  {
    return imageList.remove( elem );
  }

  public ImageQueueElem remove( int index )
  {
    return (ImageQueueElem)imageList.remove( index );
  }

  /**
   *  Remove a set of items from queue, specified by index
   */
  public void remove( int[] indices )
  {
    for( int n = 0 ; n < indices.length ; n++ )
    {
      int index = indices[n];
      imageList.remove( index );

      // Need to adjust other indices to reflect new list numbering
      for( int j = n+1 ; j < indices.length ; j++ )
      {
        if( index < indices[j] )
          indices[j]--;
      }
    }
  }
  
  public void insertElementAt( ImageQueueElem elem, int index )
  {
    imageList.insertElementAt( elem, index );
    // Now doing fireContentsChanged after adding a whole set of inserts
    // (one event is more efficient)
    //fireIntervalAdded( this, index, index );
  }
  
  /**
   * Fire off a contents change event for the whole element list
   * (not be the most efficient for small list changes, but ok
   * for now - TODO - fix)
   */
  public void fireAllContentsChanged()
  {
    fireContentsChanged( this, 0, imageList.size()-1 );
  }
  
  /**
   * Methods required by ListModel interface
   */
  public Object getElementAt( int index ) {
    return imageList.get( index );
  }
  
  public int getSize() {
    return imageList.size();
  }

  /**
   * Return true size (minus 1 for dummy object - yeech - fix TODO)
   */
  public int size() {
    return imageList.size()-1;
  }


  /**
   *  Set the play mode for the queue.
   *
   *  @param  playMode    AVTransport.PLAY_MODE_NORMAL or
   *                      AVTransport.PLAY_MODE_REPEAT_ONE or
   *                      AVTransport.PLAY_MODE_REPEAT_ALL or
   *                      AVTransport.PLAY_MODE_DIRECT_1
   *
   *  Note: Some MediaRenderer devices support these play modes, some
   *  don't. Hence the support for it here to provide consistent behavior
   *  across multiple devices
   */
  public void setPlayMode( String playMode )
  {
    this.playMode = playMode;
  }

  boolean selectFlag = false;
  
  public void setSelectFlag( boolean selectFlag )
  {
    this.selectFlag = selectFlag;
  }
  

  /**
   * Get the item at current queue pointer position
   *
   * @return CDSObject reference, or null if empty queue or invalid position
   */
  public synchronized CDSObject getCurrItem()
  {
    if( size() > 0 )
    {
      if( currIndex < 0 || currIndex >= size() )
      {
        logger.warning("Invalid queue index");
        return null;
      }
      
      ImageQueueElem item = (ImageQueueElem)imageList.get( currIndex ); 

      // Mark as busy and let listeners know the list model changed
      /*
      firstItem.setBusy(true);  
      fireActiveItemChanged();
      fireContentsChanged( this, 0, getSize()-1 );
      */

      if( selectFlag )
        selectionModel.setSelectionInterval( currIndex, currIndex );
        
      return item.getCDSObject();
    }
    return null;
  }
  
  /**
   * Get first item in play queue. 
   */
  public synchronized CDSObject getFirstItem()
  {
    if( size() > 0 )
    {
      currIndex = 0;
      ImageQueueElem item = (ImageQueueElem)imageList.get( currIndex ); 

      // Mark as busy and let listeners know the list model changed
      /*
      firstItem.setBusy(true);  
      fireActiveItemChanged();
      fireContentsChanged( this, 0, getSize()-1 );
      */

      if( selectFlag )
        selectionModel.setSelectionInterval( currIndex, currIndex );

      return item.getCDSObject();
    }
    else
    {
      return null;
    }
  }
  
  public synchronized boolean isEmpty()
  {
    return (size() == 0);
  }
  
  /**
   * Get next item in play queue. 
   */
  public synchronized CDSObject getNextItem()
  {
    ImageQueueElem currItem;  
    CDSObject cdsObj = null;
    
    if( (currIndex < 0) || (currIndex >= size()) )
      return null;
    
    int lastCurrIndex = currIndex;
    
    logger.fine("getNextItem: Entered, currIndex = " + currIndex + 
                " playMode = " + playMode );
    
    currItem = (ImageQueueElem)imageList.get( currIndex ); 

    if( ++currIndex == size() )
    {
      if( playMode.equals(AVTransport.PLAY_MODE_REPEAT_ALL) )
        currIndex = 0;
      else
        currIndex = -1;
    }
    
    if( currIndex == -1 )
    {
      if( selectFlag )
        selectionModel.clearSelection();

      //fireContentsChanged( this, 0, getSize()-1 );
      return null;
    }
    
    if( selectFlag )
      selectionModel.setSelectionInterval( currIndex, currIndex );

    currItem = (ImageQueueElem)imageList.get( currIndex ); 

    if( currItem == null )
    {
      //fireContentsChanged( this, 0, getSize()-1 );
      return null;
    }
    
    // Mark as busy and let listeners know the list model changed
    /*
    currItem.setBusy(true);
    fireContentsChanged( this, lastCurrIndex, currIndex );
    fireActiveItemChanged();
    */

    return currItem.getCDSObject();
  }

}
