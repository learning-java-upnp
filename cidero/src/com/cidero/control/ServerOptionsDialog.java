/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ServerOptionsDialog.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.Vector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;


/**
 *  Server options configuration dialog
 *
 *  Supported Options:
 *
 *  - Patch for Windows Media Connect discovery bug
 *    WMC (due to bug in Windows XP UPnP stack actually) doesn't respond
 *    to search requests from a control point on the same host as the server.
 *    The workaround is to enable manual search on the local host
 *
 */
public class ServerOptionsDialog extends JDialog
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaController controller;   // parent controller
  AppPreferences pref;

  JPanel       serverSpecificPanel = new JPanel();

  JCheckBox    wmcManualSearchCheckBox =
                   new JCheckBox("Enable discovery bug patch for Windows Media Connect on local host");

  //JLabel       wmcDeviceURLLabel = new JLabel("WMC Device URL:");
  //JTextField   wmcDeviceURLText = new JTextField(80);

  JPanel buttonPanel = new JPanel();
  JButton okButton = new JButton("OK");
  JButton applyButton = new JButton("Apply");
  JButton cancelButton = new JButton("Cancel");

  /** 
   * Dialog is a singleton
   */
  static ServerOptionsDialog dialog = null;
  
  public static ServerOptionsDialog getInstance()
  {
    if( dialog == null )
      dialog = new ServerOptionsDialog( MediaController.getInstance() );

    return dialog;
  }

  /** 
   * Constructor
   */
  private ServerOptionsDialog( MediaController controller )
  {
    super( controller.getFrame(), "Server Options", false );

    this.controller = controller;
    pref = controller.getPreferences();

    // Initialize UI components based on preferences
    setUIFromPreferences();
    
    // Set position to right-hand side of parent window (more or less)
    setLocationRelativeTo( controller.getFrame() );
    Container cp = getContentPane();

    cp.setLayout( new BorderLayout() );
    
		GridBagConstraints 	gbc = new GridBagConstraints();

    //--------- Server-Specific Options ---------

    GridBagLayout serverSpecificLayout = new GridBagLayout();

    serverSpecificPanel.setLayout( serverSpecificLayout );
    
		serverSpecificPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Server-Specific Options"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    //gbCon.insets.set( 14, 8, 4, 2 );
    //gbc.anchor = GridBagConstraints.CENTER;
    gbc.anchor = GridBagConstraints.WEST;
    serverSpecificLayout.setConstraints( wmcManualSearchCheckBox, gbc );
    serverSpecificPanel.add( wmcManualSearchCheckBox );


    /*
    gbc.gridx = 0;
    gbc.gridy = 1;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    serverSpecificLayout.setConstraints( wmcDeviceURLLabel, gbc );
    serverSpecificPanel.add( wmcDeviceURLLabel );


    gbc.gridx = 1;
    gbc.gridy = 1;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    serverSpecificLayout.setConstraints( wmcDeviceURLText, gbc );
    serverSpecificPanel.add( wmcDeviceURLText );
    */

    cp.add( serverSpecificPanel, BorderLayout.CENTER );


    //--------------------------------------------------------------
    // 'OK', 'Apply', 'Cancel' button panel at bottom of dialog
    //--------------------------------------------------------------

    okButton.addActionListener( new OkActionListener() );
    applyButton.addActionListener( new ApplyActionListener() );
    cancelButton.addActionListener( new CancelActionListener() );

    buttonPanel.setLayout( new FlowLayout() );
    buttonPanel.add( okButton );
    buttonPanel.add( applyButton );
    buttonPanel.add( cancelButton );
    cp.add( buttonPanel, BorderLayout.SOUTH );
    
    pack();
    setVisible( true );
  }


  /**
   *  Button action listeners
   */
  public class OkActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();  // in-memory copy
      setVisible( false );
    }
  }

  public class ApplyActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();
    }
  }

  public class CancelActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      // Restore settings from preferences
      setUIFromPreferences();

      setVisible( false );
    }
  }
  
  public void setUIFromPreferences()
  {
    wmcManualSearchCheckBox.setSelected(
              pref.getBoolean( "wmcDiscoveryBugPatchEnable", false ) );

    //wmcDeviceURLText.setText( pref.get( "wmcDeviceDescriptionURL",
    //                                         "" ) );
  }

  public void updatePreferences()
  {
    if( wmcManualSearchCheckBox.isSelected() )
    {
      pref.putBoolean( "wmcDiscoveryBugPatchEnable", true );
    }
    else
    {
      pref.putBoolean( "wmcDiscoveryBugPatchEnable", false );
      // Clear device description URL
      pref.put( "wmcDeviceDescriptionURL", "" );
    }
  }
  
}

  
