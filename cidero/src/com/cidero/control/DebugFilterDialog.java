/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugFilterDialog.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.Vector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;


/**
 *  Debugger window filtering dialog
 */
public class DebugFilterDialog extends JDialog
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  DebugWindow debugWindow;
  DebugPanel  debugPanel;

  JPanel filterPanel = new JPanel();

  JCheckBox searchCheckBox = new JCheckBox("Search");
  JCheckBox actionCheckBox = new JCheckBox("Actions");
  JCheckBox eventCheckBox = new JCheckBox("Events");
  JCheckBox subscribeCheckBox = new JCheckBox("Subscription");
  JCheckBox notifyAliveCheckBox = 
    new JCheckBox("Alive Notifications");
  JCheckBox notifyByeByeCheckBox =
    new JCheckBox("ByeBye Notifications");

  JCheckBox avServerCheckBox = new JCheckBox("A/V Media Servers");
  JCheckBox avRendererCheckBox = new JCheckBox("A/V Media Renderer");
  JCheckBox otherDeviceCheckBox = new JCheckBox("Other devices");

  JPanel buttonPanel = new JPanel();
  JButton okButton = new JButton("OK");
  JButton applyButton = new JButton("Apply");
  JButton cancelButton = new JButton("Cancel");

  AppPreferences pref;

  /** 
   * Dialog is a singleton
   */
  static DebugFilterDialog dialog = null;
  
  public static DebugFilterDialog getInstance()
  {
    if( dialog == null )
    {
      dialog = new DebugFilterDialog( DebugWindow.getInstance() );
    }
    return dialog;
  }

  /** 
   * Constructor
   */
  private DebugFilterDialog( DebugWindow debugWindow )
  {
    super( debugWindow, "Message Filtering", false );

    this.debugWindow = debugWindow;
    this.debugPanel = debugWindow.getDebugPanel();  // shorthand reference

    pref = MediaController.getPreferences();
    
    // Set position to right-hand side of parent debug window (more or less)
    setLocationRelativeTo( debugWindow );

    Container cp = getContentPane();
    cp.setLayout( new BorderLayout() );
    
    filterPanel.setLayout( new BoxLayout( filterPanel, BoxLayout.Y_AXIS ) );

		GridBagConstraints 	gbc = new GridBagConstraints();

    //--------------- Message type filtering options ----------------------

    GridBagLayout msgTypeLayout = new GridBagLayout();
    JPanel msgTypePanel = new JPanel( msgTypeLayout );
    
		msgTypePanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("UPnP Message Types"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );
    filterPanel.add( msgTypePanel );

    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.gridx = 0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    //gbCon.insets.set( 14, 8, 4, 2 );
    gbc.anchor = GridBagConstraints.CENTER;

    gbc.gridy = 0;
    msgTypeLayout.setConstraints( searchCheckBox, gbc );
    searchCheckBox.setSelected( pref.getBoolean("debug.search", true ) );
    msgTypePanel.add( searchCheckBox );

    gbc.gridy = 1;
    msgTypeLayout.setConstraints( subscribeCheckBox, gbc );
    subscribeCheckBox.setSelected( pref.getBoolean("debug.subscribe", true ) );
    msgTypePanel.add( subscribeCheckBox );

    gbc.gridy = 2;
    msgTypeLayout.setConstraints( notifyAliveCheckBox, gbc );
    notifyAliveCheckBox.setSelected( pref.getBoolean("debug.alive", false ) );
    msgTypePanel.add( notifyAliveCheckBox );

    gbc.gridy = 3;
    msgTypeLayout.setConstraints( notifyByeByeCheckBox, gbc );
    notifyByeByeCheckBox.setSelected( pref.getBoolean("debug.byeBye", true ) );
    msgTypePanel.add( notifyByeByeCheckBox );

    gbc.gridy = 4;
    msgTypeLayout.setConstraints( actionCheckBox, gbc );
    actionCheckBox.setSelected( pref.getBoolean("debug.action", true ) );
    msgTypePanel.add( actionCheckBox );

    gbc.gridy = 5;
    msgTypeLayout.setConstraints( eventCheckBox, gbc );
    eventCheckBox.setSelected( pref.getBoolean("debug.event", true ) );
    msgTypePanel.add( eventCheckBox );


    //---------------- Device type Panel  ----------------------

    GridBagLayout deviceTypeLayout = new GridBagLayout();
    JPanel deviceTypePanel = new JPanel( deviceTypeLayout );
    
		deviceTypePanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Source Device Types"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );
    filterPanel.add( deviceTypePanel );
    
    gbc.gridy = 0;
    deviceTypeLayout.setConstraints( avServerCheckBox, gbc );
    avServerCheckBox.setSelected( DebugObj.getMediaServerDevicesEnabled());
    avServerCheckBox.setEnabled( false );  // Not yet implemented
    deviceTypePanel.add( avServerCheckBox );

    gbc.gridy = 1;
    deviceTypeLayout.setConstraints( avRendererCheckBox, gbc );
    avRendererCheckBox.setSelected( DebugObj.getMediaRendererDevicesEnabled());
    avRendererCheckBox.setEnabled( false );  // Not yet implemented
    deviceTypePanel.add( avRendererCheckBox );

    gbc.gridy = 2;
    deviceTypeLayout.setConstraints( otherDeviceCheckBox, gbc );
    otherDeviceCheckBox.setSelected( DebugObj.getOtherDevicesEnabled());
    otherDeviceCheckBox.setEnabled( false );  // Not yet implemented
    deviceTypePanel.add( otherDeviceCheckBox );

    //--------- Add config panel to dialog as the free space hog ---------

    cp.add( filterPanel, BorderLayout.CENTER );


    //--------------------------------------------------------------
    // 'OK', 'Apply', 'Cancel' button panel at bottom of dialog
    //--------------------------------------------------------------

    okButton.addActionListener( new OkActionListener() );
    applyButton.addActionListener( new ApplyActionListener() );
    cancelButton.addActionListener( new CancelActionListener() );

    buttonPanel.setLayout( new FlowLayout() );
    buttonPanel.add( okButton );
    buttonPanel.add( applyButton );
    buttonPanel.add( cancelButton );
    cp.add( buttonPanel, BorderLayout.SOUTH );
    
    pack();
    setVisible( true );
  }

  /**
   *  Button action listeners
   */
  public class OkActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e ) {
      updatePreferences();
      setVisible( false );
    }
  }

  public class ApplyActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e ) {
      updatePreferences();
    }
  }

  public class CancelActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e ) {
      setVisible( false );
    }
  }

  /**
   * Update in-memory copy of preferences, and reset Debug object 
   * enable flags based on the new preferences
   */
  public void updatePreferences()
  {
    if( searchCheckBox.isSelected() )
      pref.putBoolean( "debug.search", true );
    else
      pref.putBoolean( "debug.search", false );

    if( subscribeCheckBox.isSelected() )
      pref.putBoolean( "debug.subscribe", true );
    else
      pref.putBoolean( "debug.subscribe", false );

    if( notifyAliveCheckBox.isSelected() )
      pref.putBoolean( "debug.alive", true );
    else
      pref.putBoolean( "debug.alive", false );

    if( notifyByeByeCheckBox.isSelected() )
      pref.putBoolean( "debug.byeBye", true );
    else
      pref.putBoolean( "debug.byeBye", false );

    if( actionCheckBox.isSelected() )
      pref.putBoolean( "debug.action", true );
    else
      pref.putBoolean( "debug.action", false );

    if( eventCheckBox.isSelected() )
      pref.putBoolean( "debug.event", true );
    else
      pref.putBoolean( "debug.event", false );

    debugWindow.getParentController().setDebugFlagsFromPreferences();
  }
  
}

  
