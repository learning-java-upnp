/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: RendererDevicePanel.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.BorderFactory;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import javax.swing.WindowConstants;

import com.cidero.upnp.CDSAudioItem;
import com.cidero.upnp.CDSObject;
import com.cidero.upnp.CDSObjectList;

/**
 *  This class implements the renderer device sub-panel at the top right
 *  of the browser panel.  List of discovered renderer devices and buttons
 *  for adding selected items to the active renderer's playlist, etc...
 */
public class RendererDevicePanel extends JPanel
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  protected MediaController parent;

  JPanel avRendererPanel;

  /**
   * Constructor
   *
   */
  public RendererDevicePanel( MediaController parent )
  {
    this.parent = parent;

    setBackground( MediaController.getBackground() );

    setLayout( new BorderLayout() );
    setBorder( new TitledBorder( "Media Renderers" ) );

    //----------------------------------------------------------------
    // List of discovered renderer devices at top
    //----------------------------------------------------------------

    FlowLayout rendererLayout = new FlowLayout( FlowLayout.LEADING );
    rendererLayout.setHgap(0);
    rendererLayout.setVgap(0);
    avRendererPanel = new JPanel( rendererLayout );
    avRendererPanel.setBackground( MediaController.getBackground() );
    
    //avRendererPanel.setPreferredSize( new Dimension(100,80) );
    add( avRendererPanel, BorderLayout.CENTER );

  }

  public void add( MediaRendererDevice renderer )
  {
    JButton button = renderer.createButton();
    avRendererPanel.add( button );
  }

  public void remove( MediaRendererDevice renderer )
  {
    avRendererPanel.remove( renderer.getButton() );
    avRendererPanel.repaint();
  }

  public MediaBrowserPanel getMediaBrowserPanel()
  {
    return parent.getMediaBrowserPanel();
  }

  public class PlaylistMembershipModeActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      JComboBox cb = (JComboBox)e.getSource();
      String mode = (String)cb.getSelectedItem();
      logger.info("Membership Mode:" + mode );
    }
  }

}

