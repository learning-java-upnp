/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: MediaTreePanel.java,v $
 *
 */

package com.cidero.control;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.WindowConstants;
import javax.swing.BorderFactory;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.cidero.upnp.CDSObject;

/**
 *  Media tree panel component for user interface (sits on left side
 *  of MediaBrowserPanel)
 */ 
public class MediaTreePanel extends JPanel
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  protected MediaBrowserPanel browserPanel;
  protected DefaultTreeModel defaultTreeModel;
  protected DefaultTreeModel treeModel;
  protected JTree tree;

  /**
   *  Construct a MediaTreePanel object, using a default tree model
   *  with a single node - 'No Media Server Selected'.  The tree model
   *  is switched whenever a different content directory service 
   *  (MediaServer) is selected
   */
  public MediaTreePanel( MediaBrowserPanel browserPanel )
  {
    super( new GridLayout( 1, 0 ) );
        
    this.browserPanel = browserPanel;
    
    // Save default UI background for use in tree list window (default
    // tree background is pure white, apparently, and its too bright
    Color uiLookAndFeelBackground = getBackground();

    // Set the background of this container to a slightly darker gray
    setBackground( MediaController.getBackground() );
    EmptyBorder emptyBorder = 
      (EmptyBorder)BorderFactory.createEmptyBorder( 3, 3, 3, 3 );
    setBorder( emptyBorder );

    //
    // Set up default tree model for no currently selected server
    // Single node with title of "No Media Server Selected"
    //
    DefaultMutableTreeNode rootNode =
      new DefaultMutableTreeNode( "No Media Server Selected" );
    defaultTreeModel = new DefaultTreeModel( rootNode );
    treeModel = defaultTreeModel;

    tree = new JTree( treeModel );
    tree.setEditable( false );
    tree.getSelectionModel().setSelectionMode
      ( TreeSelectionModel.SINGLE_TREE_SELECTION );
    tree.setShowsRootHandles( true );
    tree.setScrollsOnExpand( true );

    //tree.setForeground( Color.green );
    tree.setBackground( uiLookAndFeelBackground );

    DefaultTreeCellRenderer cellRenderer = 
      (DefaultTreeCellRenderer)tree.getCellRenderer();
    
    cellRenderer.setBackgroundNonSelectionColor( uiLookAndFeelBackground );
    Font font = cellRenderer.getFont();
    Font boldFont = font.deriveFont( Font.BOLD );
    if( boldFont != null )
      cellRenderer.setFont( boldFont );

    tree.addTreeExpansionListener( new MediaTreeExpansionListener() );

    tree.addMouseListener( new MediaTreeMouseListener( this ) );

    JScrollPane scrollPane = new JScrollPane( tree );
    add( scrollPane );
  }

  /** 
   * Set the current content directory tree model. The model changes 
   * depending on which MediaServer is selected
   */
  public void setTreeModel( DefaultTreeModel treeModel )
  {
    this.treeModel = treeModel;
    treeModel.addTreeModelListener( new MediaTreeModelListener() );
    tree.setModel( treeModel );
  }
  public void setTreeModelToDefault()
  {
    setTreeModel( defaultTreeModel );
  }
  public DefaultTreeModel getTreeModel()
  {
    return treeModel;
  }
  public JTree getTree()
  {
    return tree;
  }
  
  public MediaBrowserPanel getMediaBrowserPanel()
  {
    return browserPanel;
  }
  public MediaItemPanel getMediaItemPanel()
  {
    return browserPanel.getMediaItemPanel();
  }

  public CDSObject getSelectedObject()
  {
    TreePath currentSelection = tree.getSelectionPath();
    if( currentSelection != null )
    {
      //DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode)
      MediaNode currentNode = 
        (MediaNode)currentSelection.getLastPathComponent();

      return (CDSObject)currentNode.getUserObject();
    }
    else
    {
      return null;
    }
  }
  
  public void updateTreeView( TreePath path, boolean expandPath )
  {
    MediaNode node = (MediaNode)path.getLastPathComponent();
    logger.fine("treeExpanded: path = " + node );

    if( node.needsRefresh() )
    {
      node.expand();  // Refreshes info via UPnP if needed
      treeModel.nodeStructureChanged( node );
      
      if( node.getChildCount() > 0 )
      {
        // Make just-expanded nodes (if any) visible
        MediaNode childNode = (MediaNode)node.getChildAt(0);
        tree.scrollPathToVisible( new TreePath(childNode.getPath()) );
      }
    }

    if( expandPath )
      tree.expandPath( path );

    //
    // Display any UPNP objects of class 'item' in MediaItemPanel
    // The audio/video/image models associated with the panel 
    // are updated
    //
    logger.fine("Displaying node child items");

    browserPanel.getMediaItemPanel().displayNodeChildItems( node );
  }


  /**
   * Nested class to handle tree model changes
   * (Currently a no-op)
   */

  class MediaTreeModelListener implements TreeModelListener
  {
    public void treeNodesChanged(TreeModelEvent e) {
    }
    public void treeNodesInserted(TreeModelEvent e) {
    }
    public void treeNodesRemoved(TreeModelEvent e) {
    }
    public void treeStructureChanged(TreeModelEvent e) {
    }
  }

  /**
   * Nested class to handle tree expansion events
   */
  class MediaTreeExpansionListener implements TreeExpansionListener
  {
    //public MediaTreeExpansionListener() {
    //    }
    
    public void treeCollapsed( TreeExpansionEvent e )
    {
      logger.fine("treeCollapsed");
    }

    public void treeExpanded( TreeExpansionEvent e )
    {
      TreePath path = e.getPath();
      updateTreeView( path, false );
    }
  }

  
  /*
   * Basic test code
   */

  public static void main(String[] args)
  {
    //Schedule a job for the event-dispatching thread:
    //creating and showing this application's GUI.
    javax.swing.SwingUtilities.invokeLater(
      new Runnable()
      {
        public void run() {
          createAndShowGUI();
        }
      });
  }

  private static void createAndShowGUI()
  {
    JFrame window = new JFrame("Media Tree Panel Test");
    Container cp = window.getContentPane();

    window.setBounds( 0, 0, 600, 200 );
    window.setDefaultCloseOperation( WindowConstants.DISPOSE_ON_CLOSE );

    DefaultMutableTreeNode rootNode = 
      new DefaultMutableTreeNode( "MediaMine" );

    DefaultMutableTreeNode musicNode = 
      new DefaultMutableTreeNode( "Music" );
    musicNode.setAllowsChildren( true );

    DefaultMutableTreeNode artistsNode = 
      new DefaultMutableTreeNode( "Artists" );
    artistsNode.setAllowsChildren( true );

    DefaultMutableTreeNode moviesNode = 
      new DefaultMutableTreeNode( "Movies" );

    DefaultMutableTreeNode photosNode = 
      new DefaultMutableTreeNode( "Photos" );

    rootNode.add( musicNode );
    musicNode.add( artistsNode );
    rootNode.add( moviesNode );
    rootNode.add( photosNode );

    DefaultTreeModel treeModel = new DefaultTreeModel( rootNode );
    treeModel.setAsksAllowsChildren( true );

    // Pass null for parent browser window for simple test
    MediaTreePanel mediaTreePanel = new MediaTreePanel( null );
    mediaTreePanel.setTreeModel( treeModel );

    cp.add( mediaTreePanel, BorderLayout.CENTER ); 

    window.setVisible(true);
  }


  /** 
   * Remove the currently selected node.
   */
  //  public void removeCurrentNode()
  //  {
  //    TreePath currentSelection = tree.getSelectionPath();
  //    if( currentSelection != null )
  //    {
  //      DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode)
  //        (currentSelection.getLastPathComponent());
  //      MutableTreeNode parent = (MutableTreeNode)( currentNode.getParent() );
  //
  //      if( parent != null ) {
  //        treeModel.removeNodeFromParent( currentNode );
  //        return;
  //      }
  //    } 

    // Either there was no selection, or the root was selected.
    //toolkit.beep();
  //  }

  /** 
   * Add child to the currently selected node.
   */
  //  public DefaultMutableTreeNode addObject( Object child )
  //  {
  //    DefaultMutableTreeNode parentNode = null;
  //    TreePath parentPath = tree.getSelectionPath();
    
  //    if (parentPath == null) {
  //      parentNode = rootNode;
  //    } else {
  //      parentNode = (DefaultMutableTreeNode)
  //      (parentPath.getLastPathComponent());
  //    }

  //    return addObject(parentNode, child, true);
  //  }

  //  public DefaultMutableTreeNode addObject( DefaultMutableTreeNode parent,
  //                                            Object child )
  //  {
  //    return addObject(parent, child, false);
  //  }

  //  public DefaultMutableTreeNode addObject( DefaultMutableTreeNode parent,
  //                                           Object child, 
  //                                           boolean shouldBeVisible )
  //  {
  //    DefaultMutableTreeNode childNode = 
  //      new DefaultMutableTreeNode( child );

  //    if (parent == null) {
  //      parent = rootNode;
  //    }

  //    treeModel.insertNodeInto( childNode, parent, 
  //                             parent.getChildCount() );

    //Make sure the user can see the new node.
  //    if( shouldBeVisible ) {
  //      tree.scrollPathToVisible(new TreePath(childNode.getPath()));
  //    }
  //    return childNode;
  //  }

}

