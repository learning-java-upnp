/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: SyncOptionsDialog.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.Vector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;


/**
 *  Synchronization options configuration dialog
 */
public class SyncOptionsDialog extends JDialog
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaController controller;
  AppPreferences pref;

  JPanel       proxyServerPanel = new JPanel();

  JLabel       proxyTypeLabel = new JLabel("Proxy Type:");
  JRadioButton localButton = new JRadioButton("Local");
  JRadioButton remoteButton = new JRadioButton("Remote");
  ButtonGroup proxyTypeButtonGroup = new ButtonGroup();

  JLabel       proxyPortLabel = new JLabel("Proxy Port:");
  JTextField   proxyPortText = new JTextField(12);

  JLabel       proxyAddrLabel = new JLabel("Remote Proxy IPAddress:");
  JTextField   proxyAddrText = new JTextField(12);


  JPanel       syncTimingPanel = new JPanel();

  JCheckBox    resyncOnAlbumTransitionsCheckBox =
                   new JCheckBox("Resync on Album Transitions");

  JLabel       resyncPeriodLabel = new JLabel("Resync Period (min):");
  JTextField   resyncPeriodText = new JTextField(6);

  /*
  SpinnerNumberModel autoAlphaThreshModel;
  JPanel    selectOptPanel = new JPanel();
  JCheckBox autoSelectMusicCheckBox =new JCheckBox("Auto select music tracks");
  JCheckBox autoSelectImagesCheckBox = new JCheckBox("Auto select pictures");
  JCheckBox autoSelectVideosCheckBox = new JCheckBox("Auto select movies");
  */

  JPanel buttonPanel = new JPanel();
  JButton okButton = new JButton("OK");
  JButton applyButton = new JButton("Apply");
  JButton cancelButton = new JButton("Cancel");

  /** 
   * Dialog is a singleton
   */
  static SyncOptionsDialog dialog = null;
  
  public static SyncOptionsDialog getInstance()
  {
    if( dialog == null )
      dialog = new SyncOptionsDialog( MediaController.getInstance() );

    return dialog;
  }

  /** 
   * Constructor
   */
  private SyncOptionsDialog( MediaController controller )
  {
    super( controller.getFrame(), "Synchronization Options", false );

    this.controller = controller;
    pref = controller.getPreferences();

    // Initialize UI components based on preferences
    setUIFromPreferences();
    

    // Set position to right-hand side of parent debug window (more or less)
    setLocationRelativeTo( controller.getFrame() );
    Container cp = getContentPane();

    //configPanel.setLayout( new BoxLayout( configPanel, BoxLayout.Y_AXIS ) );

    cp.setLayout( new BorderLayout() );
    
		GridBagConstraints 	gbc = new GridBagConstraints();

    //--------- Proxy server Options ---------

    GridBagLayout proxyServerLayout = new GridBagLayout();

    proxyServerPanel.setLayout( proxyServerLayout );
    
		proxyServerPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Proxy Server Options"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    //gbCon.insets.set( 14, 8, 4, 2 );
    //gbc.anchor = GridBagConstraints.CENTER;
    gbc.anchor = GridBagConstraints.WEST;
    proxyServerLayout.setConstraints( proxyTypeLabel, gbc );
    proxyServerPanel.add( proxyTypeLabel );


    //    autoFolderCheckBox.setSelected(
    //               pref.getBoolean( "autoAlphaFoldersEnabled", true ) );

    localButton.setActionCommand( "local" );
    localButton.addActionListener( new ProxyTypeActionListener() );
    remoteButton.setActionCommand( "remote" );
    remoteButton.addActionListener( new ProxyTypeActionListener() );

    
    // Add buttons to button group
    proxyTypeButtonGroup = new ButtonGroup();
    proxyTypeButtonGroup.add( localButton );
    proxyTypeButtonGroup.add( remoteButton );

    gbc.gridx = 1;
    gbc.gridy = 0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.CENTER;
    proxyServerLayout.setConstraints( localButton, gbc );
    proxyServerPanel.add( localButton );

    gbc.gridx = 2;
    gbc.gridy = 0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.CENTER;
    proxyServerLayout.setConstraints( remoteButton, gbc );
    proxyServerPanel.add( remoteButton );


    gbc.gridx = 0;
    gbc.gridy = 1;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    proxyServerLayout.setConstraints( proxyPortLabel, gbc );
    proxyServerPanel.add( proxyPortLabel );


    gbc.gridx = 1;
    gbc.gridy = 1;
    gbc.gridwidth = 2;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    proxyServerLayout.setConstraints( proxyPortText, gbc );
    proxyServerPanel.add( proxyPortText );

    gbc.gridx = 0;
    gbc.gridy = 2;
    gbc.gridwidth = 1;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    proxyServerLayout.setConstraints( proxyAddrLabel, gbc );
    proxyServerPanel.add( proxyAddrLabel );

    gbc.gridx = 1;
    gbc.gridy = 2;
    gbc.gridwidth = 2;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    proxyServerLayout.setConstraints( proxyAddrText, gbc );
    proxyServerPanel.add( proxyAddrText );

    gbc.gridwidth = 1;

    /*
    int currThresh = pref.getInt("autoAlphaFoldersThresh", 500 );
    autoAlphaThreshModel = new SpinnerNumberModel( currThresh, 0, 1000, 10 );
    */

    cp.add( proxyServerPanel, BorderLayout.NORTH );

    //--------- Sync Timing Options ---------

    GridBagLayout syncTimingLayout = new GridBagLayout();

    syncTimingPanel.setLayout( syncTimingLayout );
    
		syncTimingPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Sync/Resync Timing Options"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    //gbCon.insets.set( 14, 8, 4, 2 );
    //gbc.anchor = GridBagConstraints.CENTER;
    gbc.anchor = GridBagConstraints.WEST;
    syncTimingLayout.setConstraints( resyncOnAlbumTransitionsCheckBox, gbc );
    syncTimingPanel.add( resyncOnAlbumTransitionsCheckBox );


    gbc.gridx = 0;
    gbc.gridy = 1;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    syncTimingLayout.setConstraints( resyncPeriodLabel, gbc );
    syncTimingPanel.add( resyncPeriodLabel );


    gbc.gridx = 1;
    gbc.gridy = 1;
    //gbc.gridwidth = 2;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    syncTimingLayout.setConstraints( resyncPeriodText, gbc );
    syncTimingPanel.add( resyncPeriodText );

    cp.add( syncTimingPanel, BorderLayout.CENTER );



    //--------------------------------------------------------------
    // 'OK', 'Apply', 'Cancel' button panel at bottom of dialog
    //--------------------------------------------------------------

    okButton.addActionListener( new OkActionListener() );
    applyButton.addActionListener( new ApplyActionListener() );
    cancelButton.addActionListener( new CancelActionListener() );

    buttonPanel.setLayout( new FlowLayout() );
    buttonPanel.add( okButton );
    buttonPanel.add( applyButton );
    buttonPanel.add( cancelButton );
    cp.add( buttonPanel, BorderLayout.SOUTH );
    
    pack();
    setVisible( true );
  }

  class ProxyTypeActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      String proxyType = 
        proxyTypeButtonGroup.getSelection().getActionCommand();

      //String choice = e.getActionCommand();
      System.out.println("ProxyType now: " + proxyType );

      if( proxyType.equals("remote") ) 
        proxyAddrText.setEnabled(true);
      else
        proxyAddrText.setEnabled(false);
    }
  }



  /**
   *  Button action listeners
   */
  public class OkActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();  // in-memory copy
      setVisible( false );
    }
  }

  public class ApplyActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();
    }
  }

  public class CancelActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      // Restore settings from preferences
      setUIFromPreferences();

      setVisible( false );
    }
  }
  
  public void setUIFromPreferences()
  {
    String proxyType = pref.get( "syncProxyType", "local" );
    if( proxyType.equals("remote") )
    {
      remoteButton.setSelected(true);
      proxyAddrText.setEnabled(true);
    }
    else
    {
      localButton.setSelected(true);
      proxyAddrText.setEnabled(false);
    }
    
    proxyPortText.setText( pref.get( "syncProxyPort", "18081" ) );
    proxyAddrText.setText( pref.get( "syncProxyIPAddress", "192.168.1.100" ) );

    resyncOnAlbumTransitionsCheckBox.setSelected(
               pref.getBoolean( "resyncOnAlbumTransitions", true ) );

    resyncPeriodText.setText( pref.get( "resyncPeriodMin", "60" ) );
    
  }

  public void updatePreferences()
  {
    int savedPort = pref.getInt( "syncProxyPort", 18081 );
    String savedProxyType = pref.get( "syncProxyType", "local" );
    
    pref.put( "syncProxyPort", proxyPortText.getText().trim() );
    pref.put( "syncProxyIPAddress", proxyAddrText.getText().trim() );

    String proxyType = proxyTypeButtonGroup.getSelection().getActionCommand();
    if( proxyType.equals("remote") ) 
    {
      controller.stopLocalProxyServer();
      pref.put( "syncProxyType", "remote" );
    }
    else
    {
      pref.put( "syncProxyType", "local" );

      // If switching to local mode, or if in local mode but port changed,
      // start/restart the local proxy server
      if( (! savedProxyType.equals("local") ) || 
          (savedPort != pref.getInt( "syncProxyPort", 18081 ))) 
      {
        controller.startLocalProxyServer();
      }
    }


    if( resyncOnAlbumTransitionsCheckBox.isSelected() )
      pref.putBoolean( "resyncOnAlbumTransitions", true );
    else
      pref.putBoolean( "resyncOnAlbumTransitions", false );

    pref.put( "resyncPeriodMin", resyncPeriodText.getText().trim() );

  }
  
}

  
