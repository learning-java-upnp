/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugPanel.java,v $
 *
 */

package com.cidero.control;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.SwingUtilities;

import com.cidero.util.MrUtil;
import com.cidero.swing.text.XMLStyledDocument;


/**
 *  Debug panel class. Separate from window to allow debug panel to be
 *  incorporated in main controller window if desired
 */
class DebugPanel extends JPanel
{
  private final static Logger logger = 
       Logger.getLogger("com.cidero.control");

  final static ImageIcon redIcon = MrUtil.createImageIcon( "redball.gif" );
  final static ImageIcon blueIcon = MrUtil.createImageIcon( "blueball.gif" );
  final static ImageIcon greenIcon = MrUtil.createImageIcon( "greenball.gif" );
  final static ImageIcon yellowIcon = MrUtil.createImageIcon( "yellowball.gif" );

  MediaController mediaController;

  JTextPane    textPane;
  JList        dataList;
  JScrollPane  textScrollPane;
  JButton      filterButton;
  JButton      optionsButton;
  JButton      clearButton;
  JButton      startButton;
  JButton      stopButton;

  XMLStyledDocument doc = new XMLStyledDocument();

  /**
   * Constructor
   */
  public DebugPanel( MediaController mediaController )
  {
    this.mediaController = mediaController;

    setLayout( new BorderLayout() );

    //----------------------------------------------------------------
    // Small control area at top of window for 'clear', 'pause',
    // and 'resume' buttons
    //----------------------------------------------------------------

    JPanel debugControlPanel = new JPanel( new BorderLayout() );
    add( debugControlPanel, BorderLayout.NORTH );

    FlowLayout configControlLayout = new FlowLayout( FlowLayout.LEFT );
    configControlLayout.setVgap(2);
    JPanel configControlPanel = new JPanel( configControlLayout );
    debugControlPanel.add( configControlPanel, BorderLayout.WEST );

    filterButton = new JButton("UPnP Message Filtering...");
    filterButton.addActionListener( new FilterActionListener() );
    filterButton.setFocusPainted( false );
    configControlPanel.add( filterButton );

    optionsButton = new JButton("Options...");
    optionsButton.addActionListener( new OptionsActionListener() );
    optionsButton.setFocusPainted( false );
    configControlPanel.add( optionsButton );


    FlowLayout captureControlLayout = new FlowLayout( FlowLayout.RIGHT );
    captureControlLayout.setVgap(2);
    JPanel captureControlPanel = new JPanel( captureControlLayout );
    debugControlPanel.add( captureControlPanel, BorderLayout.EAST );

    clearButton = new JButton("Clear");
    clearButton.addActionListener( new ClearActionListener() );
    clearButton.setFocusPainted( false );
    captureControlPanel.add( clearButton );

    stopButton = new JButton("Pause Capture");
    stopButton.addActionListener( new StopActionListener() );
    stopButton.setFocusPainted( false );
    //stopButton.setBorder( border);
    captureControlPanel.add( stopButton );
    
    startButton = new JButton("Resume Capture");
    startButton.addActionListener( new StartActionListener() );
    startButton.setEnabled( false );
    startButton.setFocusPainted( false );
    //startButton.setBorder( border);
    captureControlPanel.add( startButton );


    //----------------------------------------------------------------
    // List of logged actions, events, etc...
    //----------------------------------------------------------------

    dataList = new JList( mediaController.getDebugListModel() );
    dataList.setCellRenderer( new MyCellRenderer() );
    dataList.addListSelectionListener( new MyListSelectionListener() );

    //----------------------------------------------------------------
    // Text window for expanded detail of Actions,Events, etc...
    //----------------------------------------------------------------

    JPanel textPanel = new JPanel( new BorderLayout() );

    doc.setBaseXMLIndent(2);
    doc.setXMLIndent(2);
    
    textPane = new JTextPane( doc );
    textScrollPane = new JScrollPane( textPane );
    textScrollPane.setPreferredSize( new Dimension( 400, 400 ) );

    textPanel.add( textScrollPane, BorderLayout.CENTER );


    //----------------------------------------------------------------
    // Put the upper and lower debug panes in ScrollPanes
    //----------------------------------------------------------------

    JScrollPane scrollPane = new JScrollPane( dataList );
    scrollPane.setPreferredSize( new Dimension( 400, 400 ) );

    //Put the list and text scroll panes in a split pane.
    JSplitPane splitPane = new JSplitPane( JSplitPane.VERTICAL_SPLIT,
                                           scrollPane,
                                           textPanel );
    splitPane.setOneTouchExpandable(true);
    splitPane.setResizeWeight(0.5);

    add( splitPane, BorderLayout.CENTER );
  }

  /**
   *  Button action listeners
   */

  public class FilterActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      DebugFilterDialog.getInstance().setVisible(true);
    }
  }

  public class OptionsActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      DebugConfigDialog.getInstance().setVisible(true);
    }
  }

  public class ClearActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
    	clearList();
    }
  }

  public class StartActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      DebugObj.setEnabled( true );
      startButton.setEnabled( false );
      stopButton.setEnabled( true );
      logger.fine("Starting capture");
    }
  }

  public class StopActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      DebugObj.setEnabled( false );
      startButton.setEnabled( true );
      stopButton.setEnabled( false );
      logger.fine("Stopping capture");
    }
  }

  public void clearList()
  {
    mediaController.clearDebugHistory();
    doc.clear();
  }
  
  DebugObj lastDbgObj = null;
  
  class MyListSelectionListener implements ListSelectionListener
  {
    public void valueChanged( ListSelectionEvent e )
    {
      if( !e.getValueIsAdjusting() ) 
      {
        DebugObj dbgObj = (DebugObj)dataList.getSelectedValue();
        if( dbgObj == null )
          return;  // no object selected

        //System.out.println("Selected value is: " + 
        //                 dbgObj.toSingleLineString() );

        if( dbgObj == lastDbgObj )
          return;
        
        lastDbgObj = dbgObj;
        
        doc.clear();

        // Append the object's string representation to the XMLStyledDocument 
        // Needs to be done inside debug object because object has a mixture of
        // normal and XML text fragments, and the document class needs to
        // be told which is which
        dbgObj.append( doc );
        
        // Reposition to top of scrolled textArea after append. Need to put
        // off this command until *after* textArea.append events are all
        // processed (I think the append must put a few events on the 
        // queue that get processed after this listener returns)
        Runnable swingUpdateThread = new Runnable() 
        {
          public void run() 
          {
            textScrollPane.getViewport().setViewPosition(new Point(0, 0));
          }
        };
        SwingUtilities.invokeLater( swingUpdateThread );
      }
    }
  }
  
  class MyCellRenderer extends JLabel implements ListCellRenderer
  {
    // This is the only method defined by ListCellRenderer.
    // We just reconfigure the JLabel each time we're called.
    
    public Component getListCellRendererComponent(
       JList list,
       Object value,            // value to display
       int index,               // cell index
       boolean isSelected,      // is the cell selected
       boolean cellHasFocus)    // the list and the cell have the focus
    {
      DebugObj debugObj = (DebugObj)value;

      setText( debugObj.toSingleLineString() );

      if( debugObj.getStatus() == DebugObj.STATUS_OK )
        setIcon( greenIcon );
      else if( debugObj.getStatus() == DebugObj.STATUS_WARNING )
        setIcon( yellowIcon );
      else
        setIcon( redIcon );

      if (isSelected)
      {
        setBackground( list.getSelectionBackground() );
        setForeground( list.getSelectionForeground() );
      }
      else
      {
        setBackground( list.getBackground() );
        setForeground( list.getForeground() );
      }
      setEnabled(list.isEnabled());
      setFont(list.getFont());
      setOpaque(true);
      return this;
    }
  }
}

 

