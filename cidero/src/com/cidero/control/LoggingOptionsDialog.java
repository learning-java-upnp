/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: LoggingOptionsDialog.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.Vector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;


/**
 *  Logging options configuration dialog
 */
public class LoggingOptionsDialog extends JDialog
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaController controller;
  AppPreferences pref;

  JPanel       logLevelPanel = new JPanel();
  JLabel       logLevelLabel = new JLabel("Logging Level:");
  JRadioButton warnButton = new JRadioButton("WARNING");
  JRadioButton infoButton = new JRadioButton("INFO");
  JRadioButton fineButton = new JRadioButton("FINE");
  JRadioButton finerButton = new JRadioButton("FINER");
  JRadioButton finestButton = new JRadioButton("FINEST");
  ButtonGroup  logLevelButtonGroup = new ButtonGroup();

  JPanel buttonPanel = new JPanel();
  JButton okButton = new JButton("OK");
  JButton applyButton = new JButton("Apply");
  JButton cancelButton = new JButton("Cancel");

  /** 
   * Dialog is a singleton
   */
  static LoggingOptionsDialog dialog = null;
  
  public static LoggingOptionsDialog getInstance()
  {
    if( dialog == null )
      dialog = new LoggingOptionsDialog( MediaController.getInstance() );

    return dialog;
  }

  /** 
   * Constructor
   */
  private LoggingOptionsDialog( MediaController controller )
  {
    super( controller.getFrame(), "Logging Options", false );

    this.controller = controller;
    pref = controller.getPreferences();

    // Initialize UI components based on preferences
    setUIFromPreferences();
    
    // Set position to right-hand side of parent debug window (more or less)
    setLocationRelativeTo( controller.getFrame() );
    Container cp = getContentPane();

    //configPanel.setLayout( new BoxLayout( configPanel, BoxLayout.Y_AXIS ) );

    cp.setLayout( new BorderLayout() );
    
		GridBagConstraints 	gbc = new GridBagConstraints();

    //--------- Proxy server Options ---------

    GridBagLayout logLevelLayout = new GridBagLayout();

    logLevelPanel.setLayout( logLevelLayout );
    
		logLevelPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Logging Level"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;

    gbc.anchor = GridBagConstraints.CENTER;

    gbc.gridy = 0;
    logLevelLayout.setConstraints( warnButton, gbc );
    logLevelPanel.add( warnButton );

    gbc.gridy = 1;
    logLevelLayout.setConstraints( infoButton, gbc );
    logLevelPanel.add( infoButton );

    gbc.gridy = 2;
    logLevelLayout.setConstraints( fineButton, gbc );
    logLevelPanel.add( fineButton );

    gbc.gridy = 3;
    logLevelLayout.setConstraints( finerButton, gbc );
    logLevelPanel.add( finerButton );

    gbc.gridy = 4;
    logLevelLayout.setConstraints( finestButton, gbc );
    logLevelPanel.add( finestButton );

    
    // Add buttons to button group
    logLevelButtonGroup.add( warnButton );
    logLevelButtonGroup.add( infoButton );
    logLevelButtonGroup.add( fineButton );
    logLevelButtonGroup.add( finerButton );
    logLevelButtonGroup.add( finestButton );

    cp.add( logLevelPanel, BorderLayout.NORTH );


    //--------------------------------------------------------------
    // 'OK', 'Apply', 'Cancel' button panel at bottom of dialog
    //--------------------------------------------------------------

    okButton.addActionListener( new OkActionListener() );
    applyButton.addActionListener( new ApplyActionListener() );
    cancelButton.addActionListener( new CancelActionListener() );

    buttonPanel.setLayout( new FlowLayout() );
    buttonPanel.add( okButton );
    buttonPanel.add( applyButton );
    buttonPanel.add( cancelButton );
    cp.add( buttonPanel, BorderLayout.SOUTH );
    
    pack();
    setVisible( true );
  }

  /**
   *  Button action listeners
   */
  public class OkActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();  // in-memory copy
      setVisible( false );
    }
  }

  public class ApplyActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();
    }
  }

  public class CancelActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      // Restore settings from preferences
      setUIFromPreferences();

      setVisible( false );
    }
  }
  
  public void setUIFromPreferences()
  {
    String logLevel = pref.get( "logLevel", "INFO" );

    if( logLevel.equalsIgnoreCase("WARNING") )
      warnButton.setSelected(true);
    else if( logLevel.equalsIgnoreCase("INFO") )
      infoButton.setSelected(true);
    else if( logLevel.equalsIgnoreCase("FINE") )
      fineButton.setSelected(true);
    else if( logLevel.equalsIgnoreCase("FINER") )
      finerButton.setSelected(true);
    else if( logLevel.equalsIgnoreCase("FINEST") )
      finestButton.setSelected(true);
  }

  public void updatePreferences()
  {
    Level logLevel;
    
    if( warnButton.isSelected() )
    {
      pref.put( "logLevel", "WARNING" );
      logLevel = Level.WARNING;
    }
    else if( infoButton.isSelected() )
    {
      pref.put( "logLevel", "INFO" );
      logLevel = Level.INFO;
    }
    else if( fineButton.isSelected() )
    {
      pref.put( "logLevel", "FINE" );
      logLevel = Level.FINE;
    }
    else if( finerButton.isSelected() )
    {
      pref.put( "logLevel", "FINER" );
      logLevel = Level.FINER;
    }
    else // if( finestButton.isSelected() )
    {
      pref.put( "logLevel", "FINEST" );
      logLevel = Level.FINEST;
    }
    
    MediaController.setLogLevel( logLevel );
      
  }
  
}

  
