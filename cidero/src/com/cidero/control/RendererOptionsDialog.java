/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: RendererOptionsDialog.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.Vector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;


/**
 *  Browse options configuration dialog
 */
public class RendererOptionsDialog extends JDialog
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaController controller;
  AppPreferences pref;

  JPanel    configPanel = new JPanel();
  JLabel    monIntervalLabel = new JLabel("Monitoring Interval (ms)");
  SpinnerNumberModel monIntervalSpinnerModel;
  JLabel    playTransLabel = new JLabel("Play Transition Timeout (ms)");
  SpinnerNumberModel playTransSpinnerModel;
  JLabel    slideIntervalLabel = new JLabel("Slideshow interval (sec)");
  SpinnerNumberModel slideIntervalSpinnerModel;

  /*
  JPanel    mediaSelectHintsPanel = new JPanel();
  JCheckBox transcodeWMACheckBox = new JCheckBox("Always use transcoded WMA if available");
  JLabel    monIntervalLabel = new JLabel("Monitoring Interval (ms)");
  SpinnerNumberModel monIntervalSpinnerModel;
  */

  JPanel buttonPanel = new JPanel();
  JButton okButton = new JButton("OK");
  JButton applyButton = new JButton("Apply");
  JButton cancelButton = new JButton("Cancel");

  /** 
   * Dialog is a singleton
   */
  static RendererOptionsDialog dialog = null;
  
  public static RendererOptionsDialog getInstance()
  {
    if( dialog == null )
      dialog = new RendererOptionsDialog( MediaController.getInstance() );

    return dialog;
  }

  /** 
   * Constructor
   */
  private RendererOptionsDialog( MediaController controller )
  {
    super( controller.getFrame(), "Renderer Options", false );

    this.controller = controller;
    pref = controller.getPreferences();

    // Set position to right-hand side of parent debug window (more or less)
    setLocationRelativeTo( controller.getFrame() );
    Container cp = getContentPane();
    cp.setLayout( new BorderLayout() );

    
		GridBagConstraints 	gbc = new GridBagConstraints();
    GridBagLayout gbLayout = new GridBagLayout();

    //configPanel.setLayout( new BoxLayout( configPanel, BoxLayout.Y_AXIS ) );
    configPanel.setLayout( gbLayout );
    
		configPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Renderer Control Options"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.CENTER;

    //
    // Monitoring interval
    //
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbLayout.setConstraints( monIntervalLabel, gbc );
    configPanel.add( monIntervalLabel );

    int currVal = pref.getInt("renderer.monitorIntervalMs", 3000 );
    monIntervalSpinnerModel = new SpinnerNumberModel( currVal, 500,
                                                      20000, 500 );
    JSpinner monIntervalSpinner = new JSpinner( monIntervalSpinnerModel );
    gbc.gridx = 1;
    gbc.gridy = 0;
    gbLayout.setConstraints( monIntervalSpinner, gbc );
    configPanel.add( monIntervalSpinner );

    //
    // Play transition timeout
    //
    gbc.gridx = 0;
    gbc.gridy = 1;
    gbLayout.setConstraints( playTransLabel, gbc );
    configPanel.add( playTransLabel );

    currVal = pref.getInt("renderer.playTransitionTimeoutMs", 6000 );
    playTransSpinnerModel = new SpinnerNumberModel( currVal, 0,
                                                    20000, 500 );
    JSpinner playTransSpinner = new JSpinner( playTransSpinnerModel );
    gbc.gridx = 1;
    gbc.gridy = 1;
    gbLayout.setConstraints( playTransSpinner, gbc );
    configPanel.add( playTransSpinner );

    //
    // Slideshow interval
    //
    gbc.gridx = 0;
    gbc.gridy = 2;
    gbLayout.setConstraints( slideIntervalLabel, gbc );
    configPanel.add( slideIntervalLabel );

    currVal = pref.getInt("renderer.slideshowIntervalSec", 15 );
    slideIntervalSpinnerModel = new SpinnerNumberModel( currVal, 0,
                                                        3600, 1 );
    JSpinner slideIntervalSpinner = new JSpinner( slideIntervalSpinnerModel );
    gbc.gridx = 1;
    gbc.gridy = 2;
    gbLayout.setConstraints( slideIntervalSpinner, gbc );
    configPanel.add( slideIntervalSpinner );
    

    //--------- Add config panel to dialog as the free space hog ---------

    cp.add( configPanel, BorderLayout.CENTER );


    //--------- Media Selection options ---------

    /*
    GridBagLayout selectOptLayout = new GridBagLayout();
    selectOptPanel.setLayout( selectOptLayout );
    
		selectOptPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Media Selection Hints"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    autoSelectMusicCheckBox.setSelected(
               pref.getBoolean( "autoSelectMusicTracks", false ) );

    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.anchor = GridBagConstraints.WEST;
    selectOptLayout.setConstraints( autoSelectMusicCheckBox, gbc );
    selectOptPanel.add( autoSelectMusicCheckBox );
    */

    //--------------------------------------------------------------
    // 'OK', 'Apply', 'Cancel' button panel at bottom of dialog
    //--------------------------------------------------------------

    okButton.addActionListener( new OkActionListener() );
    applyButton.addActionListener( new ApplyActionListener() );
    cancelButton.addActionListener( new CancelActionListener() );

    buttonPanel.setLayout( new FlowLayout() );
    buttonPanel.add( okButton );
    buttonPanel.add( applyButton );
    buttonPanel.add( cancelButton );
    cp.add( buttonPanel, BorderLayout.SOUTH );
    
    pack();
    setVisible( true );
  }

  /**
   *  Button action listeners
   */
  public class OkActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();  // in-memory copy
      setVisible( false );
    }
  }

  public class ApplyActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();
    }
  }

  public class CancelActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      setVisible( false );
    }
  }
  
  public void updatePreferences()
  {
    Integer currValue = (Integer)monIntervalSpinnerModel.getValue();
    pref.putInt( "renderer.monitorIntervalMs", currValue.intValue() );

    currValue = (Integer)playTransSpinnerModel.getValue();
    pref.putInt( "renderer.playTransitionTimeoutMs",
                 currValue.intValue() );

    currValue = (Integer)slideIntervalSpinnerModel.getValue();
    pref.putInt("renderer.slideshowIntervalSec", currValue.intValue() );
  }
}

  
