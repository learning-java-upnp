/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: PlayQueuePanel.java,v $
 *
 */

package com.cidero.control;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import java.awt.*;
import java.awt.event.*;
import java.awt.dnd.*;
import java.awt.datatransfer.*;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.border.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.cidero.swing.*;
import com.cidero.upnp.*;
import com.cidero.util.AppPreferences;
import com.cidero.util.MrUtil;

/**
 *  Class for renderer audio/video play queue panel at bottom of renderer 
 *  control window. An instance of this panel or its image equivalent 
 *  is attached to the renderer window, depending on mode.
 */
public class PlayQueuePanel extends JPanel
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  protected MediaRendererDevice mediaRenderer;

  JList      playQueueJList;
  PlayQueue playQueue;   // Model for JList

  DragSource dragSource;
  DragSourceListener dragSourceListener;
  int dragStartIndex = -1;

  JPanel  controlPanel;
  JButton shuffleButton = new JButton("Shuffle");
  JButton deleteButton = new JButton("Delete");
  JButton clearButton = new JButton("Clear");


  Color bgColor = new Color( 210, 210, 210 );

  AppPreferences pref;

  /**
   * Constructor
   */
  public PlayQueuePanel( MediaRendererDevice mediaRenderer )
  {
    this.mediaRenderer = mediaRenderer;

    pref = MediaController.getPreferences();

    setLayout( new BorderLayout() );

		setBorder( BorderFactory.createEmptyBorder(2,2,2,2) );

    // Support case of no 'real' MediaRenderer (convenient for testing minor
    // changes to UI component layout)
    if( mediaRenderer != null )
      playQueue = mediaRenderer.getPlayQueue();
    else
      playQueue = new PlayQueue();


    playQueueJList = new JList( playQueue );
    playQueue.addActiveItemListener( new PlayQueueActiveItemListener() );

    playQueueJList.setCellRenderer( new PlayQueueCellRenderer() );

    // These call sets the JList cell fixedHeight & fixedWidth properties,
    // which improves JList performance for long lists - See Sun docs.
    // TODO: Not yet enabled - need to compute height based on font
    //playQueueJList.setFixedCellHeight( 20 );
    //playQueueJList.setFixedCellWidth( 20 );

    playQueueJList.setCellRenderer( new PlayQueueCellRenderer() );
    playQueueJList.addListSelectionListener( new PlayQueueSelectionListener());

    PlayQueueMouseListener mouseListener = new PlayQueueMouseListener();
    playQueueJList.addMouseListener( mouseListener );
    //playQueueJList.addMouseMotionListener( mouseListener );
    playQueueJList.setBackground( getBackground() );

    //
    // Configure JList to be a drag source & drop target
    //
    dragSource = new DragSource();

    dragSourceListener = new PlayQueueDragSourceListener();

    // Set up listener for drag gesture (beginning of drag)
    dragSource.createDefaultDragGestureRecognizer( 
                              playQueueJList,
                              DnDConstants.ACTION_MOVE,
                              new PlayQueueDragGestureListener() );

    DropTarget dropTarget = new DropTarget( playQueueJList,
                                    new PlayQueueDropTargetListener() );


    //
    // Put JList in ScrollPane
    //
    JScrollPane scrollPane = new JScrollPane( playQueueJList );
    scrollPane.setPreferredSize( new Dimension( 400, 400 ) );

    add( scrollPane, BorderLayout.CENTER );

    //------------------------------------------------------------------
    // Controls below queue list (shuffle, clear, etc...)
    //------------------------------------------------------------------

    FlowLayout buttonLayout = new FlowLayout( FlowLayout.CENTER );
    buttonLayout.setHgap(12);
    buttonLayout.setVgap(6);
    controlPanel = new JPanel( buttonLayout );


    // Currently 3 different insert modes:
    //  1. Normal  - Insert w/normal priority at end of current list  (default)
    //  2. Jukebox - Insert w/high priority at current position (after
    //               current track if one is playing)
    JLabelledComboBox modeCombo = new JLabelledComboBox("Insert Mode:", 4 );
    modeCombo.addItem("Normal"); 
    modeCombo.setSelectedIndex( 0 ); // Make Normal the default
    modeCombo.addItem("Jukebox");
    modeCombo.addActionListener( new ModeActionListener() );
    controlPanel.add( modeCombo );

    Insets buttonMargin = new Insets( 2, 8, 2, 8 );
    
    shuffleButton.addActionListener( new ShuffleActionListener() );
    shuffleButton.setMargin( buttonMargin );
    controlPanel.add( shuffleButton );

    deleteButton.addActionListener( new DeleteActionListener() );
    deleteButton.setMargin( buttonMargin );
    controlPanel.add( deleteButton );

    clearButton.addActionListener( new ClearActionListener() );
    clearButton.setMargin( buttonMargin );
    controlPanel.add( clearButton );

    // Small customizations to handle MAC OSX widget differences 
    if( MediaController.isRunningOnMacOSX() )
    {
      shuffleButton.setFocusPainted( false );
      shuffleButton.setContentAreaFilled( false );
      deleteButton.setFocusPainted( false );
      deleteButton.setContentAreaFilled( false );
      clearButton.setFocusPainted( false );
      clearButton.setContentAreaFilled( false );
    }
    else
    {
       setBackground( bgColor );
       controlPanel.setBackground( bgColor );
       modeCombo.setBackground( bgColor );
    }

    add( controlPanel, BorderLayout.SOUTH );
  }

  /**
   *  Get currently selected queue item index.  -1 if no selection
   */
  public void setSelectedIndex( int index )
  {
    playQueueJList.setSelectedIndex( index );
  }
  public int getSelectedIndex()
  {
    return playQueueJList.getSelectedIndex();
  }

  /**
   * Clear current selection in queue JList
   */
  public void clearSelection()
  {
    playQueueJList.clearSelection();
  }


  /**
   * CellRenderer for the play queue JList. Gets the CDSObject for the cell
   * and makes a string out of the Artist/Title info
   */
  class PlayQueueCellRenderer extends JLabel implements ListCellRenderer
  {
    public Component getListCellRendererComponent(
       JList list,
       Object value,            // value to display
       int index,               // cell index
       boolean isSelected,      // is the cell selected
       boolean cellHasFocus)    // the list and the cell have the focus
    {
      PlayQueueElem qItem = (PlayQueueElem)value;
      CDSObject obj = qItem.getCDSObject();

      String artist = null;

      if( obj instanceof CDSVideoItem )
        artist = ((CDSVideoItem)obj).getActor();

      if( artist == null )
        artist = obj.getArtist();
      if( artist == null )
      {
        if( obj.getCreator() != null )
          artist = obj.getCreator();
        else
          artist = "Unknown Artist";
      }
      setText( artist + " - " + obj.getTitle() );

      if ( qItem.isBusy() )
      {
        // Cell that is currently playing is green
        setBackground( Color.green );
        setForeground( list.getForeground() );
      }
      else if (isSelected)
      {
        setBackground( list.getSelectionBackground() );
        setForeground( list.getSelectionForeground() );
      }
      else if ( index == playQueue.getCurrentPosition() )
      {
        // Cell at current queue position (not running) is gray
        setBackground( new Color(210,210,210) );
        setForeground( list.getForeground() );
      }
      else if ( qItem.getPriority() == PlayQueue.PRIORITY_HIGH )
      {
        // High priority cells (added in interactive jukebox mode) are yellow
        setBackground( Color.yellow );
        setForeground( list.getForeground() );
      }
      else
      {
        setBackground( list.getBackground() );
        setForeground( list.getForeground() );
      }
      setEnabled( list.isEnabled() );
      setFont( list.getFont() );
      setOpaque( true );
      return this;
    }
  }

  /**
   *  Listener for change in the list selection
   */
  class PlayQueueSelectionListener implements ListSelectionListener
  {
    public void valueChanged( ListSelectionEvent e )
    {
      if( !e.getValueIsAdjusting() ) 
      {
        PlayQueueElem qItem = 
          (PlayQueueElem)playQueueJList.getSelectedValue();
        if( qItem == null )
          return;  // no object selected

        CDSObject obj = qItem.getCDSObject();

        //System.out.println("Selected value is: " + obj.getTitle() );
        /*
        if( dbgObj == lastDbgObj )
          return;
        lastDbgObj = dbgObj;
        */
      }
    }
  }

  class PlayQueueMouseListener extends MouseAdapter
                               implements MouseMotionListener
  {
    public void mouseClicked( MouseEvent e )
    {
      int buttonId = e.getButton();
    
      int index = playQueueJList.locationToIndex( e.getPoint() );

      if( e.getClickCount() == 1 )
      {
        //logger.info("Click count = 1, index = " + index );
      }
      else if( e.getClickCount() == 2 )
      {
        //System.out.println("Click count = 2, index = " + index );

        // If track double-clicked, do one of two things:
        //   - If in normal mode, play track immediately (stopping
        //     current track if necessary
        //   - If in jukebox mode, do ???  (TODO - not done here)
        playQueue.setCurrentPosition( index );
        
        if( mediaRenderer != null )   
        {
          mediaRenderer.actionStop();  // No-op if not running
          mediaRenderer.startPlayback();
        }
      }
    }

    public void mousePressed(MouseEvent e)
    {
      int buttonId = e.getButton();

      // isPopupTrigger doesn't seem to work consistently across
      // Windows/MacOSX/Linux. Just punt and do popup for buttons 2 OR 3
      //if( e.isPopupTrigger() )
      if( buttonId == MouseEvent.BUTTON1 )
      {
        dragStartIndex = playQueueJList.locationToIndex( e.getPoint() );
        //logger.info("mousePressed: dragStartIndex = " + dragStartIndex );
      }
      else
      {
        maybeShowPopup( e );
      }
    }

    public void maybeShowPopup( MouseEvent e )
    {
      JPopupMenu popup = new JPopupMenu();

      JMenuItem shuffleItem = new JMenuItem("Shuffle");
      shuffleItem.addActionListener( new ShuffleActionListener() );
      popup.add( shuffleItem );

      JMenuItem albumShuffleItem = new JMenuItem("Album Shuffle");
      albumShuffleItem.addActionListener( 
                  new AlbumShuffleActionListener() );
      popup.add( albumShuffleItem );

      popup.addSeparator();

      JMenuItem saveQueueItem = new JMenuItem("Save Play Queue...");
      saveQueueItem.addActionListener( 
                  new SaveQueueActionListener() );
      popup.add( saveQueueItem );

      JMenuItem loadQueueItem = new JMenuItem("Load Saved Play Queue...");
      loadQueueItem.addActionListener( 
                  new LoadQueueActionListener() );
      popup.add( loadQueueItem );

      popup.addSeparator();

      JMenuItem exportQueueItem = 
        new JMenuItem("Export Play Queue as Playlist...");
      exportQueueItem.addActionListener( 
                  new ExportQueueActionListener() );

      String twonkyDbFile = pref.get("twonkyDbFile").trim();
      if( (twonkyDbFile == null) || (twonkyDbFile.length() == 0) ) 
        exportQueueItem.setEnabled(false);

      popup.add( exportQueueItem );

      popup.show( e.getComponent(), e.getX(), e.getY() );
    }


    public void mouseMoved( MouseEvent e )
    {
      //logger.info("Mouse moved " );
    }

    public void mouseDragged( MouseEvent e )
    {
      //logger.info("mouseDragged: Entered");

      /*
      if( !dragEnabled )
        return;
    
      if( startDragIndex == -1 )
      {
        int index = imageJList.locationToIndex( e.getPoint() );
      }
      */
    }

  }

  class PlayQueueDragGestureListener implements DragGestureListener
  {
    /*
     *  Called when a drag gesture is recognized
     */
    public void dragGestureRecognized( DragGestureEvent dge )
    {
      logger.info( "dragGestureRecognized");  

      // Don't need to create a real string or object here since we
      // are just shuffling items within the list and can track list indicies

      // StringSelection implements Transferable
      // Build a dummy StringSelection object that the Drag Source
      // can use to transport a string to the Drop Target

      StringSelection text = new StringSelection( "Drag Text"); 

      // Ask the DragSource to start the drag of our text
      dragSource.startDrag( dge, DragSource.DefaultMoveDrop, 
                            text, dragSourceListener );
    }
    
  }
  

  /*
   * Drag source listener
   */
  class PlayQueueDragSourceListener extends DragSourceAdapter
  {
    /*
     * Called as the cursor's hotspot enters a platform-dependent drop site
     */
    public void dragEnter( DragSourceDragEvent dsde )
    {
      //logger.info("DragSource:dragEnter");
    }

    /*
     * This method is invoked to signify that the Drag and Drop operation
     * is complete
     */
    public void dragDropEnd( DragSourceDropEvent dsde )
    {
      /*
      logger.info("DragSource:dropEnd");

      if( dsde.getDropSuccess() )
      {
        logger.info( "Object successfully dropped" );
      }
      else
      {
        logger.info( "Object not successfully dropped" );
      }
      */
    }

    /*
     *  Called as the cursor's hotspot exits a platform-dependent drop site.
     */
    public void dragExit( DragSourceEvent dse )
    {
      //logger.info("DragSource:dragExit");
    }
    
    /*
     * Called as the cursor's hotspot moves over a platform-dependent drop
     * site.
     */
    public void dragOver( DragSourceDragEvent dsde )
    {
      //logger.info("DragSource:dragOver");
    }

    /*
     * Called when the user has modified the drop gesture.
     */
    public void dropActionChanged( DragSourceDragEvent dsde )
    {
      //logger.info("DragSource:dropActionChanged");
    }

  }
  
  /*
   * Drop target listener
   */
  class PlayQueueDropTargetListener extends DropTargetAdapter
  {
    /*
     * Called while a drag operation is ongoing, when the mouse pointer 
     * enters the operable part of the drop site for the DropTarget 
     * registered with this listener.
     */
    public void dragEnter( DropTargetDragEvent dtde )
    {
      //logger.info("DropTarget:dragEnter");

      int index = 
        playQueueJList.locationToIndex( dtde.getLocation() );

      setSelectedIndex( index );
    }
    
    /*
     * Called while a drag operation is ongoing, when the mouse pointer has
     * exited the operable part of the drop site for the DropTarget
     * registered with this listener.
     */
    public void dragExit( DropTargetEvent dte )
    {
      //logger.info("DropTarget:dragExit");
    }
    
    /*
     * Called when a drag operation is ongoing, while the mouse pointer is
     * still over the operable part of the drop site for the DropTarget
     * registered with this listener.
     */
    public void dragOver( DropTargetDragEvent dtde )
    {
      //logger.info("DropTarget:dragOver");

      int index = playQueueJList.locationToIndex( dtde.getLocation() );

      setSelectedIndex( index );
    }

    /*
     * Called if the user has modified the current drop gesture.
     */
    public void dropActionChanged( DropTargetDragEvent dtde )
    {
      //logger.info("DropTarget:dropActionChanged");
    }
    
    /*
     *  Handle the drop
     */
    public void drop( DropTargetDropEvent dtde )
    {
      //logger.info( "DropTarget: drop" );

      int index = playQueueJList.locationToIndex( dtde.getLocation() );

      if( dragStartIndex < 0 )
        return;
      
      // Move the dragged element so that it falls before the last selected
      // item
      playQueue.move( dragStartIndex, index );

      try
      {
        Transferable transferable = dtde.getTransferable();

        if( transferable.isDataFlavorSupported( 
                                   DataFlavor.stringFlavor ) )
        {
          dtde.acceptDrop( DnDConstants.ACTION_MOVE );

          // Step 6: Extract the Transferable String
          String s = ( String )transferable.getTransferData( 
                                              DataFlavor.stringFlavor );
          //addElement( s );
          // Step 7: Complete the drop and notify the DragSource 
          //         (will receive a dragDropEnd() notification)
          dtde.getDropTargetContext().dropComplete( true );
        }
        else
        {
          // Step 5: Reject dropped objects that are not Strings
          dtde.rejectDrop();
        }
      }
      catch( IOException e )
      {
        e.printStackTrace();
        logger.warning( "Exception" + e.getMessage());
        dtde.rejectDrop();
      }
      catch( UnsupportedFlavorException e )
      {
        e.printStackTrace();
        logger.warning( "Exception" + e.getMessage());
        dtde.rejectDrop();
      }

      dragStartIndex = -1;

      playQueueJList.clearSelection();
    }

  }


  /**
   *  Listener for change in the 'active item'. The active item corresponds
   *  to the 'now playing' item in the list, which may be separate from
   *  the selected item in the Jlist.  
   *
   *  Notes:  This is a PlayQueue-specific custom listener, not 
   *  supported by the JList class 
   */
  class PlayQueueActiveItemListener implements ActiveItemListener
  {
    public void activeItemChanged( int activeItemIndex )
    {
      // shift the JList view to keep active item visible. This needs
      // to get done on the Swing event queue
      logger.fine("Shifting JList view to item: " + activeItemIndex );
      if( activeItemIndex >= 0 && playQueue.size() > 0 )
        JListAsyncUtil.ensureIndexIsVisible( playQueueJList, 
                                             activeItemIndex );
    }
  }
  
  public class ShuffleActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      playQueue.shuffle();
    }
  }

  public class AlbumShuffleActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      playQueue.albumShuffle();
    }
  }

  public class ClearActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      playQueue.clear();
    }
  }

  public class DeleteActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      int selectedIndex = getSelectedIndex();
      if( selectedIndex >= 0 )
        playQueue.remove( selectedIndex );
    }
  }

  public class ModeActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      JComboBox cb = (JComboBox)e.getSource();
      String mode = (String)cb.getSelectedItem();
      logger.info("Audio/Video Play Queue Mode:" + mode );

      if( mode.equals("Normal") )
      {
        // Normal mode. Insert track at end of list
        playQueue.setInsertItemMode( PlayQueue.MODE_BUILDING_LIST );
      }
      else if( mode.equals("Jukebox") )
      {
        playQueue.setInsertItemMode( PlayQueue.MODE_JUKEBOX );
      }
    }
  }

  public class DidlLiteFileFilter extends FileFilter
  {
    public String getDescription()
    {
      return "UPnP Formatted Playlists (*.xml)";
    }

    public boolean accept(File file)
    {
      if (file.isDirectory()) {
        return true;
      }

      if( file.getName().toLowerCase().endsWith(".xml") )
        return true;

      return false;
    }
  }

  public class M3UFileFilter extends FileFilter
  {
    public String getDescription()
    {
      return "M3U Formatted Playlists (*.m3u)";
    }

    public boolean accept(File file)
    {
      if (file.isDirectory()) {
        return true;
      }

      if( file.getName().toLowerCase().endsWith(".m3u") )
        return true;

      return false;
    }
  }
  
  public class SaveQueueActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.info("Saving play queue");

      String userHome = MrUtil.getUserHomeUnix();
      String xmlSaveDir = pref.get("xmlSaveDirectory");
      logger.info("xmlSaveDir from pref: " + xmlSaveDir );
      if( xmlSaveDir == null || (xmlSaveDir.trim().length() == 0) )
        xmlSaveDir = userHome + "/.cidero/MediaController/playlists/xml";

      logger.info("xmlSaveDir final: " + xmlSaveDir );

      JFileChooser fileChooser = new JFileChooser( xmlSaveDir );
      fileChooser.setFileHidingEnabled( false );
      DidlLiteFileFilter didlLiteFileFilter = new DidlLiteFileFilter();
      fileChooser.setFileFilter( didlLiteFileFilter );

      int retVal = fileChooser.showSaveDialog( playQueueJList );
      if( retVal == JFileChooser.APPROVE_OPTION )
      {
        File file = fileChooser.getSelectedFile();
        playQueue.saveToDidlLite( file );
      }
    }
  }

  
  public class LoadQueueActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.info("Loading play queue");

      String userHome = MrUtil.getUserHomeUnix();
      String xmlSaveDir = pref.get("xmlSaveDirectory");
      if( xmlSaveDir == null || (xmlSaveDir.trim().length() == 0) )
        xmlSaveDir = userHome + "/.cidero/MediaController/playlists/xml";

      JFileChooser fileChooser = new JFileChooser( xmlSaveDir );
      fileChooser.setFileHidingEnabled( false );
      DidlLiteFileFilter didlLiteFileFilter = new DidlLiteFileFilter();
      fileChooser.setFileFilter( didlLiteFileFilter );

      int retVal = fileChooser.showOpenDialog( playQueueJList );
      if( retVal == JFileChooser.APPROVE_OPTION )
      {
        File file = fileChooser.getSelectedFile();
        logger.info("Loading from file " + file.getName() );
        playQueue.loadFromDidlLite( file );
      }
    }
  }

  public class ExportQueueActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.info("Exporting play queue as playlist");

      String userHome = MrUtil.getUserHomeUnix();
      String m3uSaveDirectory = pref.get("m3uSaveDirectory");
      if( m3uSaveDirectory == null || (m3uSaveDirectory.trim().length() == 0) )
        m3uSaveDirectory = userHome + "/.cidero/MediaController/playlists/m3u";

      JFileChooser exportFileChooser = new JFileChooser( m3uSaveDirectory );
      M3UFileFilter m3uFileFilter = new M3UFileFilter();
      exportFileChooser.setFileFilter( m3uFileFilter );

      int retVal = exportFileChooser.showSaveDialog( playQueueJList );
      if( retVal == JFileChooser.APPROVE_OPTION )
      {
        File file = exportFileChooser.getSelectedFile();
        if( ! playQueue.exportToM3U( pref.get("twonkyDbFile"), file ) )
        {
          try 
          {
          JOptionPane.showMessageDialog(
                 MediaController.getInstance().getFrame(),
                 "<html>" +
                 "Export of playlist to '" + file.getCanonicalPath() +
                 "' failed.<br>Check that Twonky DB file exists at '" +
                 pref.get("twonkyDbFile") + "'</html>" );
          }
          catch( IOException ex )
          {
            logger.warning("Non-existent save file '" + file.getName() +
                           "'" + ex );
          }
        }
      }
    }
  }

}

