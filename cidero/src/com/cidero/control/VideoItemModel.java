/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: VideoItemModel.java,v $
 *
 */

package com.cidero.control;

import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

import com.cidero.upnp.CDSVideoItem;
import com.cidero.upnp.CDSObject;

/**
 * Video item model for table in VideoItem tabbed window
 *
 */
public class VideoItemModel extends AbstractTableModel
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  private String[] columnNames =
  { "Artist", "Title", "Time", "Genre" };

  ArrayList itemList;

  public VideoItemModel()
  {
    itemList = new ArrayList();
  }
  
  /* Clear model (remove all track info references) */
  public void clear()
  {
    itemList.clear();
  }
  
  public void add( CDSVideoItem item )
  {
    itemList.add( item );
  }

  public int getColumnCount()
  {
    //    logger.fine("AudioItemModel: getColumnCount: " + 
    //                       columnNames.length);
    return columnNames.length;
  }

  public int getRowCount()
  {
    //    logger.fine("AudioItemModel: getRowCount");
    return itemList.size();
  }

  public String getColumnName( int col )
  {
    return columnNames[col];
  }

  public Object getValueAt( int row, int col )
  {
    CDSVideoItem obj = (CDSVideoItem)itemList.get( row );

    //if( obj instanceof CDSMusicTrack )
    switch( col )
    {
      case 0:  // Actor
        // Some CDS implementations use the 'Creator' field instead of
        // the 'Actor' field in the CDSMusicTrack object.
        if( obj.getActor() == null )
          return obj.getCreator();
        else
          return obj.getActor();
      case 1:  // Track
        return obj.getTitle();
      case 2:  // Time
        return obj.getDurationNoMillisec();
      case 3:  // Genre
        return obj.getGenre();

      default:
        logger.fine("No obj found for row, col " + row + " " + col );
        return null;
    }
  }

  public CDSObject getObjectAtRow( int row )
  {
    return (CDSObject)itemList.get( row );
  }

  /** 
   * Convenience function that doesn't require 'source' arg
   */ 
  public void fireTableChanged()
  {
    fireTableChanged( new TableModelEvent( this ) );
  }
  
  /*
   * Don't need to implement this method unless your table's
   * data can change.
   */
  //public void setValueAt(Object value, int row, int col) {

  //    logger.fine("Setting value at " + row + "," + col
  //                       + " to " + value
  //                       + " (an instance of "
  //                       + value.getClass() + ")");

  //    data[row][col] = value;
  //    fireTableCellUpdated( row, col );

  //  }

  
}
