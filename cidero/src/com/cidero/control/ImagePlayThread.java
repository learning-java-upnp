/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ImagePlayThread.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import com.cidero.util.MrUtil;
import com.cidero.util.AppPreferences;
import com.cidero.upnp.*;

/**
 * Image playback thread. Image playback is a bit different than audio/video
 * playback, which is handled by the PlayThread class.  
 *
 */
public class ImagePlayThread implements Runnable
{
  private static Logger logger = Logger.getLogger("com.cidero.control");

  MediaRendererDevice renderer;

  String              playMode;
  int                 queueStartIndex;

  RendererStateModel stateModel;
  RendererStateModel pseudoStateModel;
  ImageQueue         imageQueue;
  AppPreferences     pref;

  private boolean interruptFlag = false;  

  // Return states for slideshowWait()
  private static final int NORMAL = 1;
  private static final int PLAYBACK_TERMINATED = 2;
  private static final int PLAYBACK_INTERRUPTED = 3;


  /**
   * Constructor
   *
   */
  public ImagePlayThread( MediaRendererDevice renderer )
  {
    this.renderer = renderer;

    // shorthand
    stateModel = renderer.getStateModel();
    pseudoStateModel = renderer.getPseudoStateModel();
    imageQueue = renderer.getImageQueue();
    pref = MediaController.getPreferences();

  }
  
  private Thread playThread = null;  // for clean shutdown via stop()

  
  /**
   * @param  playMode  Supported modes are:
   *  
   *   AVTransport.PLAY_MODE_NORMAL      Play to end of queue, from curr pos
   *   AVTransport.PLAY_MODE_DIRECT_1    Play image at curr pos
   *   AVTransport.PLAY_MODE_REPEAT_ALL  Play endless loop, from curr pos
   *
   * @param  startIndex  Starting queue index
   *
   */
  public void start( String playMode, int queueStartIndex )
  {
    if( playThread == null )
    {
      this.playMode = playMode;
      this.queueStartIndex = queueStartIndex;

      playThread = new Thread( this );
      playThread.start();
    }
    else
    {
      logger.info("start: Image play thread already running!");

      //
      // Two cases here:
      // 1.  Play thread is paused and user issued a play command to resume
      // 2.  Play thread is running but user changed the selection manually
      //     by clicking on an image - want to jump to that image
      //

      if( pauseFlag )
      {
        setPauseFlag( false );  // Just un-pause
      }
      else
      {
        imageQueue.setCurrentPosition( queueStartIndex );

        // Interrupt current playback wait state so new queue position is
        // recognized
        interruptFlag = true;  
      }
    }
  }

  public boolean isRunning()
  {
    return (playThread != null);
  }

  boolean busy = false;
  
  public boolean isBusy()
  {
    return busy;
  }
  
  public void stop()
  {
    if( playThread == null )
      logger.warning("stop: Image play thread not currently running");
      
    playThread = null;
    busy = false;
  }
  
  public void run()
  {
    logger.fine("ImagePlayThread: Running... isEventThread = " + 
                SwingUtilities.isEventDispatchThread() );

    if( imageQueue.isEmpty() )
    {
      logger.warning("Empty image play queue");
      playThread = null;
      return;
    }

    pauseFlag = false;

    int playCount = 0;


    // Set this flag to true to cause images to be selected in JList 
    // as they are played
    imageQueue.setSelectFlag( true );

    imageQueue.setCurrentPosition( queueStartIndex );
    CDSObject obj = imageQueue.getCurrItem();   // mark as selected

    if( obj == null )
    {
      logger.warning("Empty image play queue" );
      playThread = null;
      return;
    }

    busy = true;
    
    while( (playThread != null) && (obj != null) )
    {
      // Save start time so we can wait the proper amount of 
      // time before displaying next slide in slideshow 
      long startTimeMs = System.currentTimeMillis();

      //
      // Get the 'best matching resource' for the given target renderer 
      // device. This is based on the preferred/supported protocols of 
      // both the source (server) and sink (renderer) devices
      //
      String reqIpAddr = null; // Not yet impl
      CDSResource res =
        obj.getBestMatchingResource( renderer.getProtocolInfoList(),
                                     reqIpAddr,
                                     "684x456",
                                     -1 );
      if( res == null )
      {
        logger.warning("No matching resource for this server/renderer combo");
        logger.warning("Supported renderer protocols: " + 
                       renderer.protocolInfoListToString() );
        break;
      }
      logger.fine("Resource name: " + res.getName() );

      // Do basic connection setup for this protocol. This is expensive Op 
      // (several UPnP calls), so only do 1st time through (TODO: May need
      // to do whenever protocolInfo changes also) 
      if( playCount == 0 )
        renderer.setupConnection( res.getProtocolInfo() );
      playCount++;
      
      if( renderer.actionSetAVTransportURI( res.getName(),
                                   CDS.toDIDL( obj, "*", res ) ) == false )
      {
        logger.warning("play:Error returned from setting URI");
        break;
      }

      //
      // Send play action to renderer. Note that a successful play action
      // should result in an incoming LastChange event with TransportState
      // set to 'PLAYING'. When the event is received, the UI is updated
      // and a monitoring thread is started to issue periodic getPostionInfo
      // actions
      //

      // To deal with DLink image playback behavior - see below
      stateModel.setTransportStatus( AVTransport.STATUS_OK );

      if( renderer.actionPlay( "1.0" ) == false )
      {
        logger.warning("Error occurred starting item playback - " +
                       " Stopping playback session" );
        break;
      }

      // Test of image rotation action provided by Philips 300i/400i
      // Ideally this call would change rotation state for subsequently
      // loaded images...
      //
      // Note: Test indicated that Streamium rotation happened before image
      // became visible, so if the UI had a rotate button that flagged 
      // images for rotation, this would work!  TODO: Add req'd UI controls
      //
      //if( renderer.actionX_Rotate( "+90" ) == false )
      //{
      //  logger.fine("rotation anot play:Error returned from setting URI");
      //}

      // Set transportState of pseudoStateModel to PLAYING for the 
      // benefit of the play controls in the renderer window. When in
      // image playback mode, the renderer window observes the
      // pseudoStateModel, not the state model of the device itself
      pseudoStateModel.setTransportState( AVTransport.STATE_PLAYING );
      pseudoStateModel.notifyObservers();


      //
      // Wait up to 6000 ms for transport state to shift out of STOPPED
      // (exact period here is user-configurable. Need to wait here to 
      // give transportState time to transition, otherwise logic below could
      // see the 'STOPPED' state and think it's time to move to the next
      // track.
      //
      int maxWaitMs = pref.getInt("renderer.playTransitionTimeoutMs", 6000);
      //System.out.println("Waiting for playTransition - millis = " +
      //        maxWaitMs );
      int waitMs;
      for( waitMs = 0 ; waitMs < maxWaitMs ; waitMs += 500 ) 
      {
        MrUtil.sleep( 500 );
        if( !stateModel.getTransportState().equals(AVTransport.STATE_STOPPED) )
        {
          //          System.out.println("TransportState = " +
          //                             stateModel.getTransportState() + "after " +
          //                             waitMs + " ms");
          break;
        }
      }
      if( waitMs >= maxWaitMs )
        logger.warning("Media playback transition timeout");
      
      // 
      // Waiting for transition to the STOPPED TransportState, up to 10
      // sec per image. Also check periodically for *TransportStatus* (not
      // TransportState) of "STATUS_MEDIA_END" (That's what DLink DSM-320
      // reports after an image is loaded - it never transitions to
      // STOPPED state)
      //
      for( int n = 0 ; n < 20 ; n++ )
      {
        if( stateModel.waitForTransportState( AVTransport.STATE_STOPPED,
                                              500 ) )
        {
          logger.fine("waitForTransportState - STOPPED");
          break;
        }
        else if( stateModel.getTransportStatus().equals("STATUS_MEDIA_END") )
        {
          // DLink DSM-320 hack
          logger.fine("TransportStatus = " + stateModel.getTransportStatus());
          renderer.actionStop();
          break;
        }
      }
      
      // Call GetPositionInfo once to update image info (Artist,Title,etc...)
      // Periodically call GetPositionInfo to update the playback time.
      // The call updates the state model, which in turn notifies the UI
      if( renderer.actionGetPositionInfo() == false )
      {
        logger.warning("Error getting image info");
        break;
      }
      
      // Copy track info to pseudoStateModel
      pseudoStateModel.setTrackArtist( stateModel.getTrackArtist() );
      pseudoStateModel.setTrackTitle( stateModel.getTrackTitle() );
      pseudoStateModel.notifyObservers();

      if( playMode.equals(AVTransport.PLAY_MODE_DIRECT_1) )
        break;

      busy = false;
      
      int waitStatus = slideshowWait();
      logger.fine("Done waiting" );
      pauseFlag = false;  // Reset 

      if( waitStatus == NORMAL )
      {
        obj = imageQueue.getNextItem();
      }
      else if( waitStatus == PLAYBACK_INTERRUPTED )
      {
        obj = imageQueue.getCurrItem();
      }
      else if( waitStatus == PLAYBACK_TERMINATED )
      {
        break;
      }
      
      if( obj == null ) 
      {
        if( pseudoStateModel.getPlayMode().equals(AVTransport.PLAY_MODE_REPEAT_ALL) )
        {
          obj = imageQueue.getFirstItem();
        }
      }

    } // while( (playThread != null) && (obj != null) )

    busy = false;
    pseudoStateModel.setTransportState( AVTransport.STATE_STOPPED );
    pseudoStateModel.notifyObservers();

    if( obj == null )
      logger.fine("No more resources in play queue - stopping");

    if( playThread == null )
      logger.fine("Play Thread stopped by user");

    logger.fine("Leaving imageplaythread");

    playThread = null;  // Reset for next start() call 
  }


  public int slideshowWait()
  {
    // Use millisec in timing loop to get sub-second response to
    // stop/next/prev buttons

    pseudoStateModel.setTrackDurationSec(
                  pseudoStateModel.getSlideshowIntervalMs()/1000 );
    pseudoStateModel.setTrackRelTimeSec( 0 );
    pseudoStateModel.notifyObservers();

    int lastWaitSec = 0;

    for( int waitMs = 0 ; waitMs < pseudoStateModel.getSlideshowIntervalMs() ;)
    {
      MrUtil.sleep( 200 );

      if( playThread == null )
        return PLAYBACK_TERMINATED;
      if( interruptFlag )
      {
        interruptFlag = false;
        logger.info("Slideshow sleep INTERRUPTED!!!!");
        return PLAYBACK_INTERRUPTED;
      }
      
      if( !pauseFlag )
      {
        waitMs += 200;
        int waitSec = waitMs/1000;
        if( waitSec != lastWaitSec )
        {
          pseudoStateModel.setTrackDurationSec( 
                  pseudoStateModel.getSlideshowIntervalMs()/1000 );
          pseudoStateModel.setTrackRelTimeSec( waitSec );
          pseudoStateModel.notifyObservers();
        }
        lastWaitSec = waitSec;
      }
    }

    return NORMAL;
  }


  boolean pauseFlag = false;

  public void setPauseFlag( boolean flag )
  {
    logger.fine("Setting PAUSE flag to " + flag );
    pauseFlag = flag;
    if( flag )
      pseudoStateModel.setTransportState( AVTransport.STATE_PAUSED_PLAYBACK );
    else
      pseudoStateModel.setTransportState( AVTransport.STATE_PLAYING );
    pseudoStateModel.notifyObservers();

  }

}

