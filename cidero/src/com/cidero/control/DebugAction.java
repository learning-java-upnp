/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugAction.java,v $
 *
 */

package com.cidero.control;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.cybergarage.upnp.Action;
import org.cybergarage.upnp.Argument;
import org.cybergarage.upnp.ArgumentList;
import org.cybergarage.upnp.UPnPStatus;

import com.cidero.swing.text.XMLStyledDocument;
import com.cidero.upnp.IService;


/**
 *  Debug version of UPnP action objects (transactions).  This object
 *  is lighter weight than the Cybergarage Action class - good for 
 *  storing a bunch of actions
 */
class DebugAction extends DebugObj
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  String    name;
  ArrayList inputArgList = new ArrayList();
  ArrayList outputArgList = new ArrayList();
  int       errorCode;   // UPnP error code for failed actions
  
  // Each UPnP service has it's own action error codes, in addition to the 
  // standard UPnP codes that apply to all services. Associate a 
  // service instance with each debug message so it's errorToString()
  // method is available.
  IService service;

  /**
   * Constructor
   *
   * @param action   UPnP action object (Cybergarage)
   * @param service  UPnP service 'helper' class object (Cidero). This class
   *                 knows about the service-specific UPnP error codes
   */
  public DebugAction( Action action, IService service )
  {
    this.name = action.getName();
    this.service = service;
    
    setStatus( DebugObj.STATUS_OK );
    errorCode = 0;

    ArgumentList argList = action.getArgumentList();
    
    int argCount = argList.size();
    for( int n = 0 ; n < argCount ; n++ )
    {
      Argument arg = argList.getArgument(n);

      DebugActionArg debugArg = new DebugActionArg( arg.getName(),
                                                    arg.getValue() );
      if( arg.isInDirection() == true )
        inputArgList.add( debugArg );
      else
        outputArgList.add( debugArg );
    }
  }

  /**
   * Constructor for DebugAction error object when no action with specified
   * name exists in a UPnP service (shouldn't cybergarage generate this
   * error automatically? - TODO:
   */
  public DebugAction( String actionName )
  {
    this.name = actionName;
    setStatus( DebugObj.STATUS_ERROR );
    setErrorCode( UPnPStatus.INVALID_ACTION );
  }

  public void setErrorCode( int code )
  {
    errorCode = code;
  }
  
  class DebugActionArg
  {
    String name;
    String value;

    public DebugActionArg( String name, String value )
    {
      this.name = name;
      this.value = value;
    }
    public String getName() {
      return name;
    }
    public String getValue() {
      return value;
    }
    public String toString() 
    {
      StringBuffer buf = new StringBuffer();
      buf.append( name + " = " + value + "\n" );
      return buf.toString();
    }
  }

  /**
   * Single line string represention. Used in top pane of debug window
   */
  public String toSingleLineString()
  {
    StringBuffer buf = new StringBuffer();
    
    // If debug object status non-zero, print out UPnP action error code.
    // (assume it was set properly)

    String deviceName = service.getDevice().getFriendlyName();

    if( getStatus() < 0 )
    {
      buf.append( getTimeString() );
      buf.append( " Action: " );
      if( deviceName != null )
        buf.append( deviceName + ": " );
      buf.append( name );
      buf.append( "  Status: " + getStatusString() );
      buf.append( "  ErrorCode: " + errorCode );
      buf.append( " (" + service.errorToString( errorCode ) + ")" );
    }
    else
    {
      buf.append( getTimeString() );
      buf.append( " Action: " );
      if( deviceName != null )
        buf.append( deviceName + ": " );
      buf.append( name );
      buf.append( "  Status: " + getStatusString() );
    }
    
    return buf.toString();
  }
  
  /**
   * Full (may be multiple lines) string represention. Used in bottom
   * pane of debug window
   */
  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    
    buf.append("Action: " + name + " ErrorCode: " +
               service.errorToString( errorCode ) );

    int argCount = inputArgList.size();
    for( int n = 0 ; n < argCount ; n++ )
    {
      DebugActionArg arg = (DebugActionArg)inputArgList.get(n);
      buf.append(" InArg: " + arg.toString() );
    }

    argCount = outputArgList.size();
    for( int n = 0 ; n < argCount ; n++ )
    {
      DebugActionArg arg = (DebugActionArg)outputArgList.get(n);
      buf.append(" OutArg: " + arg.toString() );
    }
    
    return buf.toString();
  }

  /** 
   *  Append a text representation of the object to an XML-capable
   *  StyledDocument class.  Fragments known to be XML are put to the
   *  the document using the special appendXML() method (applies XML-
   *  appropriate formatting), while normal text just uses the normal
   *  append() method (default formatting).
   *
   *  Fragment know to be formatted using XML are Action arguments that
   *  end in 'Metadata', and the Result field of the Browse action
   *
   * @param doc          XMLStyledDocument instance (normally displayed
   *                     in JTextPane)
   * @param autoFormat   Enable XML auto-formatting. Setting this to
   *                     false disables all the special XML-sensitive logic
   */ 
  public void append( XMLStyledDocument doc, boolean autoFormat )
  {
    doc.appendString("Action: " + name + "\n" );

    int argCount = inputArgList.size();
    for( int n = 0 ; n < argCount ; n++ )
    {
      DebugActionArg arg = (DebugActionArg)inputArgList.get(n);

      doc.appendString( " InArg: " + arg.getName() );

      if( autoFormat && arg.getName().endsWith("MetaData") )
      {
        doc.appendString( "\n" );
        doc.appendXMLString( arg.getValue() );
        doc.appendString( "\n" );
      }
      else
      {
        doc.appendString( " " + arg.getValue() + "\n" );
      }
    }

    argCount = outputArgList.size();
    for( int n = 0 ; n < argCount ; n++ )
    {
      DebugActionArg arg = (DebugActionArg)outputArgList.get(n);

      doc.appendString(" OutArg: " + arg.getName() );

      if( autoFormat &&
          ( arg.getName().endsWith("MetaData") || 
            (name.equals("Browse") && arg.getName().equals("Result"))) )
      {
        doc.appendString( "\n" );
        doc.appendXMLString( arg.getValue() + "\n" );
        doc.appendString( "\n" );
      }
      else
      {
        doc.appendString( " " + arg.getValue() + "\n" );
      }
    }
  }

  static boolean enabled = true;

  public static void setEnabled( boolean flag )
  {
    enabled = flag;
  }
  public static boolean getEnabled()
  {
    return enabled;
  }

  public boolean isDisplayable()
  {
    return enabled;
  }


}

 

