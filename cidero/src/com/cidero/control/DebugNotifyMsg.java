/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugNotifyMsg.java,v $
 *
 */

package com.cidero.control;

import java.util.ArrayList;
import java.util.logging.Logger;
import java.net.URL;
import java.net.MalformedURLException;

import org.cybergarage.upnp.Action;
import org.cybergarage.upnp.Argument;
import org.cybergarage.upnp.ArgumentList;
import org.cybergarage.upnp.ssdp.SSDPPacket;

import com.cidero.swing.text.XMLStyledDocument;

/**
 *  Debug container for notify messages. Notify messages come in two
 *  flavors - 'alive' messages and 'bye-bye' messages. Both message types
 *  are sent out to the SSDP multicast address 239.255.255.250:1900
 *
 *  Alive messages notes:
 *
 *   Alive messages are sent out when a device first connects to the 
 *   network, and periodically after that
 *
 *   Example:
 *
 *     NOTIFY * HTTP/1.1
 *     HOST: 239.255.255.250:1900
 *     CACHE-CONTROL: max-age=1801
 *     LOCATION: http://192.168.1.100:49152/description.xml
 *     NT: urn:schemas-upnp-org:service:ContentDirectory:1
 *     NTS: ssdp:alive
 *     SERVER: Microsoft Windows/5.1, UPnP/1.0, Intel SDK for UPnP devices /1.2
 *     USN: uuid:MUSICMATCHMediaServer:onrush::urn:schemas-upnp-org:service:ContentDirectory:1
 *
 *  Bye-Bye messages notes:
 *
 *   Bye-Bye messages are sent whenever a device disconnects from the
 *   network
 *
 *   Example:
 *
 *     NOTIFY * HTTP/1.1
 *     NTS: ssdp:byebye
 *     NT: upnp:rootdevice
 *     USN: uuid:OfficePrismiq_UUID::upnp:rootdevice
 *     HOST: 239.255.255.250:1900
 *   
 *  
 */
class DebugNotifyMsg extends DebugObj
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  String    ssdpPacket;      // Entire SSDP packet as string
  
  // ----------- Required fields for incoming response -----------
  String    usn;
  String    location;        // Location of XML device description (URL)
  String    server;
  String    nt;              // Notification type
  String    nts;              // Notification type
  String    cacheControl;

  // ---------- Recommended fields - may not be present ---------
  String    date;
  
  // -- Local variables

  String    device;
  String    host;
  int       port;
  
  
  /**
   * Constructor
   */
  public DebugNotifyMsg( SSDPPacket packet )
  {
    setStatus( DebugObj.STATUS_OK );


    ssdpPacket = packet.toString();
    
    usn = packet.getUSN();
    nt = packet.getNT();
    nts = packet.getNTS();
    location = packet.getLocation();

    // If alive packet, parse the location URL and get the host & port
    if( nts.indexOf("alive") > 0 )
    {
      try
      {
        URL url = new URL( location );
        host = url.getHost();
        port = url.getPort();
      }
      catch( MalformedURLException e )
      {
        logger.warning("Invalid location field: " + location );
        setStatus( DebugObj.STATUS_ERROR );
      }
    }
    
    /*
    // Parse the device type from the USN for convenience
    int index = usn.indexOf("device:");
    if( index < 0 )
    {
      logger.warning("Syntax error in search response USN field: " + usn );
      device = "Unknown device type";
    }
    else
    {
      device = usn.substring( index+7 );
    }
    */
  }
    
  /**
   * Single line string represention. Used in top pane of debug window
   */
  public String toSingleLineString()
  {
    if( nts.indexOf("alive") > 0 )
      return getTimeString() + " Notify: NTS: " + nts + 
            " Host=" + host + ":" + port + " NT=" + nt;
    else
      return getTimeString() + " Notify: NTS: " + nts + 
            " NT=" + nt;
  }
  
  /**
   * Full (may be multiple lines) string represention. Used in bottom
   * pane of debug window
   */
  public String toString()
  {
    return ssdpPacket;
  }

  /** 
   *  Append a text representation of the object to an XML-capable
   *  StyledDocument class.
   *
   * @param doc          XMLStyledDocument instance (normally displayed
   *                     in JTextPane)
   * @param autoFormat   Enable XML auto-formatting. Setting this to
   *                     false disables all the special XML-sensitive logic
   */ 
  public void append( XMLStyledDocument doc, boolean autoFormat )
  {
    doc.appendString("Notify Raw SSDP Packet:\n\n" + ssdpPacket );
  }


  static boolean   byeByeEnabled = true;     // enabled for display
  static boolean   aliveEnabled = false;      // enabled for display
  
  public static void setAliveEnabled( boolean flag )
  {
    aliveEnabled = flag;
  }
  public static boolean getAliveEnabled()
  {
    return aliveEnabled;
  }

  public static void setByeByeEnabled( boolean flag )
  {
    byeByeEnabled = flag;
  }
  public static boolean getByeByeEnabled()
  {
    return byeByeEnabled;
  }


  public boolean isDisplayable()
  {
    return aliveEnabled;
  }


}

 

