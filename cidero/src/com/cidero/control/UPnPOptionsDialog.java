/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: UPnPOptionsDialog.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.Vector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;


/**
 *  UPnP protocol options configuration dialog
 */
public class UPnPOptionsDialog extends JDialog
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaController controller;
  AppPreferences pref;

  JPanel       upnpPanel = new JPanel();

  JLabel       subReqLabel = new JLabel("Subscription Request Duration (sec):");
  JTextField   subReqText = new JTextField(8);

  JPanel buttonPanel = new JPanel();
  JButton okButton = new JButton("OK");
  JButton applyButton = new JButton("Apply");
  JButton cancelButton = new JButton("Cancel");

  /** 
   * Dialog is a singleton
   */
  static UPnPOptionsDialog dialog = null;
  
  public static UPnPOptionsDialog getInstance()
  {
    if( dialog == null )
      dialog = new UPnPOptionsDialog( MediaController.getInstance() );

    return dialog;
  }

  /** 
   * Constructor
   */
  private UPnPOptionsDialog( MediaController controller )
  {
    super( controller.getFrame(), "UPnP Protocol Options", false );

    this.controller = controller;
    pref = controller.getPreferences();

    // Initialize UI components based on preferences
    setUIFromPreferences();
    

    // Set position to right-hand side of parent debug window (more or less)
    setLocationRelativeTo( controller.getFrame() );
    Container cp = getContentPane();

    //configPanel.setLayout( new BoxLayout( configPanel, BoxLayout.Y_AXIS ) );

    cp.setLayout( new BorderLayout() );
    
		GridBagConstraints 	gbc = new GridBagConstraints();

    //--------- Proxy server Options ---------

    GridBagLayout gbLayout = new GridBagLayout();

    upnpPanel.setLayout( gbLayout );
    
		upnpPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("UPnP Options"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    //gbCon.insets.set( 14, 8, 4, 2 );
    //gbc.anchor = GridBagConstraints.CENTER;
    gbc.anchor = GridBagConstraints.WEST;
    gbLayout.setConstraints( subReqLabel, gbc );
    upnpPanel.add( subReqLabel );

    gbc.gridx = 1;
    gbc.gridy = 0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    gbLayout.setConstraints( subReqText, gbc );
    upnpPanel.add( subReqText );

    cp.add( upnpPanel, BorderLayout.CENTER );

    //--------------------------------------------------------------
    // 'OK', 'Apply', 'Cancel' button panel at bottom of dialog
    //--------------------------------------------------------------

    okButton.addActionListener( new OkActionListener() );
    applyButton.addActionListener( new ApplyActionListener() );
    cancelButton.addActionListener( new CancelActionListener() );

    buttonPanel.setLayout( new FlowLayout() );
    buttonPanel.add( okButton );
    buttonPanel.add( applyButton );
    buttonPanel.add( cancelButton );
    cp.add( buttonPanel, BorderLayout.SOUTH );
    
    pack();
    setVisible( true );
  }

  /**
   *  Button action listeners
   */
  public class OkActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();  // in-memory copy
      setVisible( false );
    }
  }

  public class ApplyActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();
    }
  }

  public class CancelActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      // Restore settings from preferences
      setUIFromPreferences();

      setVisible( false );
    }
  }
  
  public void setUIFromPreferences()
  {
    subReqText.setText( pref.get( "subscriptionPeriodSec", "180" ) );
  }

  public void updatePreferences()
  {
    pref.put( "subscriptionPeriodSec", subReqText.getText().trim() );

    controller.setSubscriptionPeriodSec( 
                      pref.getInt("subscriptionPeriodSec",
                      MediaController.REQUESTED_SUBSCRIPTION_PERIOD_SEC) );
  }
  
}

  
