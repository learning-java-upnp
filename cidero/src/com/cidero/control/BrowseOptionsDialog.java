/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: BrowseOptionsDialog.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.Vector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;


/**
 *  Browse options configuration dialog
 */
public class BrowseOptionsDialog extends JDialog
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaController controller;
  AppPreferences pref;

  JPanel    largeFolderPanel = new JPanel();
  JCheckBox autoFolderCheckBox = new JCheckBox("Use Alphabetical Sub-Folders");
  JLabel    autoThreshLabel = new JLabel("Alphabetical Sub-Folders Threshold");
  SpinnerNumberModel autoAlphaThreshModel;

  JPanel    selectOptPanel = new JPanel();
  JCheckBox autoSelectMusicCheckBox = new JCheckBox("Auto select music tracks");
  JCheckBox autoSelectImagesCheckBox = new JCheckBox("Auto select pictures");
  JCheckBox autoSelectVideosCheckBox = new JCheckBox("Auto select movies");

  JPanel buttonPanel = new JPanel();
  JButton okButton = new JButton("OK");
  JButton applyButton = new JButton("Apply");
  JButton cancelButton = new JButton("Cancel");

  /** 
   * Dialog is a singleton
   */
  static BrowseOptionsDialog dialog = null;
  
  public static BrowseOptionsDialog getInstance()
  {
    if( dialog == null )
      dialog = new BrowseOptionsDialog( MediaController.getInstance() );

    return dialog;
  }

  /** 
   * Constructor
   */
  private BrowseOptionsDialog( MediaController controller )
  {
    super( controller.getFrame(), "Browse Options", false );

    this.controller = controller;
    pref = controller.getPreferences();

    // Set position to right-hand side of parent debug window (more or less)
    setLocationRelativeTo( controller.getFrame() );
    Container cp = getContentPane();

    //configPanel.setLayout( new BoxLayout( configPanel, BoxLayout.Y_AXIS ) );

    cp.setLayout( new BorderLayout() );
    
		GridBagConstraints 	gbc = new GridBagConstraints();

    //--------- Large Folder Options ---------

    GridBagLayout largeFolderLayout = new GridBagLayout();

    largeFolderPanel.setLayout( largeFolderLayout );
    
		largeFolderPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Large Folder Options"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    autoFolderCheckBox.setSelected(
               pref.getBoolean( "autoAlphaFoldersEnabled", true ) );

    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    //gbCon.insets.set( 14, 8, 4, 2 );
    //gbc.anchor = GridBagConstraints.CENTER;
    gbc.anchor = GridBagConstraints.WEST;
    largeFolderLayout.setConstraints( autoFolderCheckBox, gbc );
    largeFolderPanel.add( autoFolderCheckBox );

    gbc.gridx = 0;
    gbc.gridy = 1;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.CENTER;
    largeFolderLayout.setConstraints( autoThreshLabel, gbc );
    largeFolderPanel.add( autoThreshLabel );

    int currThresh = pref.getInt("autoAlphaFoldersThresh", 500 );
    autoAlphaThreshModel = new SpinnerNumberModel( currThresh, 0, 1000, 10 );
    //    autoAlphaThreshModel.addChangeListener(
    //      new AutoAlphaFolderChangeListener() );
    JSpinner autoAlphaThreshSpinner = new JSpinner( autoAlphaThreshModel );

    gbc.gridx = 1;
    gbc.gridy = 1;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    largeFolderLayout.setConstraints( autoAlphaThreshSpinner, gbc );
    largeFolderPanel.add( autoAlphaThreshSpinner );

    cp.add( largeFolderPanel, BorderLayout.NORTH );

    //--------- Selection options ---------

    GridBagLayout selectOptLayout = new GridBagLayout();
    selectOptPanel.setLayout( selectOptLayout );
    
		selectOptPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Selection Options"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    autoSelectMusicCheckBox.setSelected(
               pref.getBoolean( "autoSelectMusicTracks", false ) );
                                        
    autoSelectImagesCheckBox.setSelected(
               pref.getBoolean( "autoSelectPictures", false ) );

    autoSelectVideosCheckBox.setSelected(
               pref.getBoolean( "autoSelectMovies", false ) );

    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.anchor = GridBagConstraints.WEST;
    selectOptLayout.setConstraints( autoSelectMusicCheckBox, gbc );
    selectOptPanel.add( autoSelectMusicCheckBox );

    gbc.gridx = 0;
    gbc.gridy = 1;
    selectOptLayout.setConstraints( autoSelectImagesCheckBox, gbc );
    //selectOptPanel.add( autoSelectImagesCheckBox );

    gbc.gridx = 0;
    gbc.gridy = 2;
    selectOptLayout.setConstraints( autoSelectVideosCheckBox, gbc );
    //selectOptPanel.add( autoSelectVideosCheckBox );

    cp.add( selectOptPanel, BorderLayout.CENTER );

    //--------------------------------------------------------------
    // 'OK', 'Apply', 'Cancel' button panel at bottom of dialog
    //--------------------------------------------------------------

    okButton.addActionListener( new OkActionListener() );
    applyButton.addActionListener( new ApplyActionListener() );
    cancelButton.addActionListener( new CancelActionListener() );

    buttonPanel.setLayout( new FlowLayout() );
    buttonPanel.add( okButton );
    buttonPanel.add( applyButton );
    buttonPanel.add( cancelButton );
    cp.add( buttonPanel, BorderLayout.SOUTH );
    
    pack();
    setVisible( true );
  }

  /**
   *  Button action listeners
   */
  public class OkActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();  // in-memory copy
      setVisible( false );
    }
  }

  public class ApplyActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();
    }
  }

  public class CancelActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      setVisible( false );
    }
  }
  
  public void updatePreferences()
  {
    if( autoFolderCheckBox.isSelected() )
      pref.putBoolean( "autoAlphaFoldersEnabled", true );
    else
      pref.putBoolean( "autoAlphaFoldersEnabled", false );
    
    Integer currValue = (Integer)autoAlphaThreshModel.getValue();
    
    pref.putInt( "autoAlphaFoldersThresh", currValue.intValue() );

    if( autoSelectMusicCheckBox.isSelected() )
      pref.putBoolean( "autoSelectMusicTracks", true );
    else
      pref.putBoolean( "autoSelectMusicTracks", false );

    if( autoSelectImagesCheckBox.isSelected() )
      pref.putBoolean( "autoSelectPictures", true );
    else
      pref.putBoolean( "autoSelectPictures", false );

    if( autoSelectVideosCheckBox.isSelected() )
      pref.putBoolean( "autoSelectMovies", true );
    else
      pref.putBoolean( "autoSelectMovies", false );
  }
  
}

  
