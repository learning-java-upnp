/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: MediaItemPanel.java,v $
 *
 */

package com.cidero.control;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Cursor;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;
import javax.swing.BorderFactory;
import javax.swing.border.EmptyBorder;

import com.cidero.upnp.*;
import com.cidero.util.MrUtil;
import com.cidero.util.AppPreferences;
import com.cidero.swing.MySwingUtil;

/**
 *  This class implements a tabbed container panel to hold the panels
 *  that display the audio, video, and image items (AudioTrackPanel,
 *  VideoItemPanel, ImageItemPanel)
 */
public class MediaItemPanel extends JPanel implements Runnable
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  protected MediaBrowserPanel parent;

  JTabbedPane tabbedPane;
  
  AudioItemPanel audioPanel;
  ImageItemPanel imagePanel;
  VideoItemPanel videoPanel;

  /**
   * Constructor
   */
  public MediaItemPanel( MediaBrowserPanel parent )
  {
    this.parent = parent;
    
    setLayout( new BoxLayout( this, BoxLayout.Y_AXIS ) );

    setBackground( MediaController.getBackground() );

    EmptyBorder emptyBorder = 
      (EmptyBorder)BorderFactory.createEmptyBorder( 4, 0, 0, 0 );
    setBorder( emptyBorder );
    
    tabbedPane = new JTabbedPane();

    audioPanel = new AudioItemPanel( this );

    tabbedPane.addTab( "Music", null, audioPanel,
                       "Music Items For Selected Folder");

    imagePanel = new ImageItemPanel( this );
    
    tabbedPane.addTab( "Pictures", null, imagePanel,
                       "Image Items For Selected Folder");

    videoPanel = new VideoItemPanel( this );
    
    tabbedPane.addTab( "Movies", null, videoPanel,
                       "Video Items For Selected Folder");

    //Add the tabbed pane to this panel.
    add( tabbedPane );
  }

  /**
   * Set the models used to display audio, image, and video data in the
   * various panels. The models are associated with a MediaServerDevice
   */
  public void setAudioItemModel( AudioItemModel model ) {
    audioPanel.setModel( model );
  }
  public AudioItemModel getAudioItemModel() {
    return audioPanel.getModel();
  }

  public void setImageItemModel( ImageQueue model ) {
    imagePanel.setModel( model );
  }
  public ImageQueue getImageItemModel() {
    return imagePanel.getModel();
  }

  public void setVideoItemModel( VideoItemModel model ) {
    videoPanel.setModel( model );
  }
  public VideoItemModel getVideoItemModel() {
    return videoPanel.getModel();
  }

  public MediaBrowserPanel getMediaBrowserPanel() {
    return parent;
  }

  public MediaController getMediaController() {
    return parent.getMediaController();
  }


  //  public void setVideoItemModel( VideoItemModel videoItemModel )
  //  {
  //    videoPanel.setModel( videoItemModel );
  //  }

  //  public void setImageItemModel( ImageItemModel imageItemModel )
  //  {
  //    imagePanel.setModel( imageItemModel );
  //  }
  
  public CDSObject getSelectedObject()
  {
    int tabIndex = tabbedPane.getSelectedIndex();
    
    if( tabIndex == 0 )  // music tab
    {
      return audioPanel.getSelectedObject();      
    }
    else if( tabIndex == 2 )  // video tab
    {
      return audioPanel.getSelectedObject();      
    }
    else
    {
      // Image selection works a bit differently at the moment...
      logger.fine("MediaItemPanel: getSelectedObject: Not yet supp!\n");
    }

    return null;
  }
  
  CDSObjectList itemList;

  /**
   * Display the child nodes of type item (non-containers) in the media
   * item panel. Audio, image, and video items are each displayed in their
   * own tabbed pane. Model/View/Controller approach used, so this 
   * routine just adds the items to the relevant models, and fires change
   * events 
   *
   * This method is invoked when the user clicks on a node in the media
   * browse tree for the active media server, and the node expands itself 
   *
   */
  public void displayNodeChildItems( MediaNode parentNode )
  {
    AudioItemModel audioModel = audioPanel.getModel();
    ImageQueue imageModel = imagePanel.getModel();
    VideoItemModel videoModel = videoPanel.getModel();

    //logger.info("displaying Items");

    //int rowCount = audioModel.getRowCount();
    //audioModel.fireTableRowsDeleted( 0, rowCount-1 );
    //logger.fine("displayNodeChildItems: childCount = " +
    //                       parentNode.getChildCount() );

    itemList = parentNode.getItemList();
    int numAudioItems = 0;
    int numImageItems = 0;
    int numVideoItems = 0;
    
    for( int n = 0 ; n < itemList.size() ; n++ )
    {
      CDSObject upnpObj = itemList.getObject(n);

      if( upnpObj.isContainer() )  
        continue;  // not an item
      
      if( upnpObj instanceof CDSAudioItem )
      {
        // If this is first audio item, clear model before adding to it
        if( numAudioItems == 0 )
          audioModel.clear();
          
        logger.fine("Adding audio item to audio model");
        audioModel.add( (CDSAudioItem)upnpObj );
        numAudioItems++;
      }
      else if( upnpObj instanceof CDSImageItem )
      {
        if( numImageItems == 0 )
          imageModel.clear();

        numImageItems++;
      }
      else if( upnpObj instanceof CDSVideoItem )
      {
        if( numVideoItems == 0 )
          videoModel.clear();
          
        logger.fine("Adding video item to video model");
        videoModel.add( (CDSVideoItem)upnpObj );
        numVideoItems++;
      }
    }

    //
    // If only one type of item present in container, switch item panel 
    // tab to the appropriate one (Music or Pictures). If both types
    // of items are present, just leave selected tab unchanged
    //
    if( (numAudioItems == 0) && (numImageItems > 0) )
      tabbedPane.setSelectedIndex( 1 );
    else if( (numAudioItems > 0) && (numImageItems == 0) )
      tabbedPane.setSelectedIndex( 0 );
    else if( numVideoItems > 0 )
      tabbedPane.setSelectedIndex( 2 );

    audioModel.fireTableChanged();
    imageModel.fireAllContentsChanged();
    videoModel.fireTableChanged();


    AppPreferences pref = MediaController.getPreferences();

    if( numAudioItems > 0 )
    {
      if( pref.getBoolean( "autoSelectMusicTracks", false ) == true )
      {
        logger.fine("Auto-selecting all tracks");
        audioPanel.selectAll();
      }
      else
      {
        logger.fine("NOT Auto-selecting all tracks");
      }
    }
    
    //
    // Image item addition done in separate thread to allow for incremental
    // loading/display of icons in panel (provides continual user feedback)
    //
    if( numImageItems > 0 )
    {
      // Set the cursor to wait cusor using invokeLater
      MySwingUtil.setCursorAsync( 
        MediaController.getInstance().getMediaBrowserPanel(),
        Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) );

      Thread imageAddThread = new Thread( this );
      imageAddThread.start();
    }
    
    // Don't bother auto-selecting videos for playback

  }

  // Image add thread
  public void run()
  {
    ImageQueue imageModel = imagePanel.getModel();
    int numImageItems = 0;

    for( int n = 0 ; n < itemList.size() ; n++ )
    {
      CDSObject upnpObj = itemList.getObject(n);

      if( upnpObj instanceof CDSImageItem )
      {
        logger.fine("Adding image item to image model");

        // Icon retrieved from server (significant latency) within add() call
        imageModel.add( (CDSImageItem)upnpObj );
        numImageItems++;

        // fire event every 4 images to refresh JList 
        if( (numImageItems > 0) && ((numImageItems % 4) == 0) )
        {
          imageModel.fireAllContentsChanged();
        }
      }
    }
    
    // Restore cursor
    MySwingUtil.setCursorAsync(
        MediaController.getInstance().getMediaBrowserPanel(),
        Cursor.getDefaultCursor() );  

    // This need to be in event dispatch thread
    //if( numImageItems > 0 )
    //{
    //  if( pref.getBoolean( "autoSelectPictures", false ) == true )
    //    imagePanel.selectAll();
    //}
  }
  
}

