/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ImageQueueElem.java,v $
 *
 */
package com.cidero.control;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

import com.cidero.upnp.CDSObject;
import com.cidero.upnp.CDSImageItem;

/**
 *  Simple class to allow addition of an image icon to ImageItem object
 *  (for use with ImageItemPanel JList)
 */
public class ImageQueueElem
{
  private static Logger logger = Logger.getLogger("com.cidero.control");

  CDSImageItem  itemObj;
  ImageIcon     cachedIcon;

  static int iconWidthConstraint = 128;
  static int iconHeightConstraint = 128;

  int cachedIconWidthConstraint;
  int cachedIconHeightConstraint;
    
  public ImageQueueElem()
  {
  }
  
  /**
   * Construct a play queue item with the default PERMANENT membership
   * attribute set
   */
  public ImageQueueElem( CDSImageItem itemObj )
  {
    this.itemObj = itemObj;
  }

  public CDSObject getCDSObject() {
    return itemObj;
  }

  /**
   * Set the preferred size of image icons for the JList panel
   */
  public static void setIconPreferredSize( int width, int height )
  {
    /*
    boolean iconSizeChanged = false;

    if( (width != iconWidth) || (height != iconHeight) )
      iconSizeChanged = true;
    */
  
    iconWidthConstraint = width;
    iconHeightConstraint = height;
  }
  
  /**
   *  Get icon of requested size. If no icon cached, or size of cached
   *  icon is different than the current icon size, create a new one.
   *  CDSResource may 
   */
  public ImageIcon getImageIcon()
  {
    if( cachedIcon == null ||
        (cachedIconWidthConstraint != iconWidthConstraint) ||
        (cachedIconHeightConstraint != iconHeightConstraint) )
    {
      logger.fine(" modelElem: getting image icon - width, height = " +
                  iconWidthConstraint + " " + iconHeightConstraint );

      // Get smallest raw icon within 20% of requested size and
      // resample it to match specified width, height
      cachedIcon = itemObj.getImageIcon( iconWidthConstraint,
                                         iconHeightConstraint );
    }

    cachedIconWidthConstraint = iconWidthConstraint;
    cachedIconHeightConstraint = iconHeightConstraint;

    return cachedIcon;
  }
  
}
  
