/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ImageItemPanel.java,v $
 *
 */

package com.cidero.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.cidero.upnp.*;
import com.cidero.util.AppPreferences;
import com.cidero.util.AppPreferencesChangeListener;

/**
 *  This class implements a panel that displays a set of image items as
 *  thumbnails.
 */
public class ImageItemPanel extends JPanel
                            implements AppPreferencesChangeListener
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaItemPanel parent;

  ImageQueue defaultModel;
  ImageQueue model;

  JButton addSelectedButton = new JButton("Add Images To Slideshow");
  JButton selectAllButton = new JButton("Select All");
  JButton clearSelectionButton = new JButton("Clear Selection");

  JList imageJList;
  GridBagLayout gbLayout = new GridBagLayout();
  GridBagConstraints gbc = new GridBagConstraints();

  AppPreferences pref;

  /**
   * Constructor
   */
  public ImageItemPanel( MediaItemPanel parent )
  {
    this.parent = parent;
    
    //setLayout( new BoxLayout( this, BoxLayout.Y_AXIS ) );
    setLayout( new BorderLayout() );

    pref = MediaController.getPreferences();
    pref.addChangeListener( this );

    Color uiLookAndFeelBackground = getBackground();
    
    setBackground( MediaController.getBackground() );
    
    EmptyBorder emptyBorder = 
      (EmptyBorder)BorderFactory.createEmptyBorder( 0, 2, 2, 2 );
    setBorder( emptyBorder );

    //----------------------------------------------------------------
    // Button panel at top
    //----------------------------------------------------------------

    FlowLayout buttonLayout = new FlowLayout( FlowLayout.CENTER );
    buttonLayout.setHgap(10);
    buttonLayout.setVgap(8);

    selectAllButton.addActionListener( new SelectAllActionListener() );
     clearSelectionButton.addActionListener( new ClearSelectionActionListener() );
    addSelectedButton.addActionListener( new AddSelectedActionListener() );
    
    JPanel buttonPanel = new JPanel( buttonLayout );

    // Small customizations to handle MAC OSX widget differences 
    if( MediaController.isRunningOnMacOSX() )
    {
      addSelectedButton.setFocusPainted( false );
      addSelectedButton.setContentAreaFilled( false );
      selectAllButton.setFocusPainted( false );
      selectAllButton.setContentAreaFilled( false );
      clearSelectionButton.setFocusPainted( false );
      clearSelectionButton.setContentAreaFilled( false );
    }
    else
    {
      buttonPanel.setBackground( MediaController.getBackground() );
    }

    buttonPanel.add( addSelectedButton );
    buttonPanel.add( selectAllButton );
    buttonPanel.add( clearSelectionButton );

    add( buttonPanel, BorderLayout.NORTH );

    //----------------------------------------------------------------
    // Scrolling pane with JList of image thumbnails at bottom
    // JList set up using HORIZONTAL layout
    //----------------------------------------------------------------

    defaultModel = new ImageQueue();

    imageJList = new JList( defaultModel );
    imageJList.setLayoutOrientation( JList.HORIZONTAL_WRAP );
    // To make horizontal wrap work the way I want...
    imageJList.setVisibleRowCount( 0 );  

    ImageQueueViewerHelper viewerHelper = 
       new ImageQueueViewerHelper( imageJList );
    imageJList.setCellRenderer( viewerHelper );
    imageJList.addMouseListener( viewerHelper );
    imageJList.addMouseMotionListener( viewerHelper );

    JScrollPane scrollPane = new JScrollPane( imageJList );
    add( scrollPane, BorderLayout.CENTER );

    appPreferencesChanged();
  }
  
  /*
   * Set the list model observed by this panel. Need to fire a model
   * changed event after setting it to trigger a refresh 
   */
  public void setModel( ImageQueue model )
  {
    this.model = model;
    imageJList.setModel( model );
  }
  public ImageQueue getModel() {
    return model;
  }
  
  public MediaController getMediaController() {
    return parent.getMediaController();
  }

  public void appPreferencesChanged()
  {
    Color color = pref.getColor("ImageItemPanel.background");
    if( color != null )
      imageJList.setBackground( color );  

    color = pref.getColor("ImageItemPanel.foreground");
    if( color != null )
      imageJList.setForeground( color );  
    imageJList.revalidate();
  }
  

  public void selectAll()
  {
    imageJList.setSelectionInterval( 0, model.size()-1 );
  }

  public class SelectAllActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      selectAll();
    }
  }

  public class ClearSelectionActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      imageJList.clearSelection();
    }
  }

  public class AddSelectedActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      int[] selectedIndices = imageJList.getSelectedIndices();

      if( selectedIndices.length <= 0 )
        return;
      
      ArrayList mediaRendererList = 
        getMediaController().getActiveMediaRenderers();

      // 
      // Add selected items to all renderers that are visible
      // Put up info dialog if none are visible
      //
      int visibleRendererCount = 0;

      for( int dev = 0 ; dev < mediaRendererList.size() ; dev++ )
      {
        MediaRendererDevice mediaRenderer =  
          (MediaRendererDevice)mediaRendererList.get(dev);
          
        if( ! mediaRenderer.isVisible() )
          continue;
          
        visibleRendererCount++;

        for( int n = 0 ; n < selectedIndices.length ; n++ )
        {
          if( selectedIndices[n] >= model.getSize() )
          {
            logger.warning("Error - JList reported out of range selection");
          }
          else
          {
            CDSImageItem item = (CDSImageItem)model.getCDSObject( selectedIndices[n] );
            logger.fine("Adding obj " + item.getTitle() + 
                        " to renderer play queue" );
            mediaRenderer.addToImageQueue( item );
          }
        }

        // Done updating queue - now fire single change event to update 
        // the whole thing (as opposed to single event for each add())
        mediaRenderer.getImageQueue().fireAllContentsChanged();
      }

      if( visibleRendererCount == 0 )
      {
        JOptionPane.showMessageDialog( getMediaController().getFrame(),
           "Please select one or more renderers to add items to their play queue(s)");
        return;
      }
    }
  }

}

