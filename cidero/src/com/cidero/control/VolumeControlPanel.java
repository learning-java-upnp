/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: VolumeControlPanel.java,v $
 *
 */

package com.cidero.control;

import java.awt.Dimension;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.cidero.upnp.RendererStateModel;

/**
 *  Simple volume control class with a horizontal slider and a mute 
 *  button. The slider takes up the free space
 */
public class VolumeControlPanel extends JPanel implements Observer
{
  private final static Logger logger = 
       Logger.getLogger("com.cidero.control");

  MediaRendererDevice mediaRenderer;  // parent
  RendererStateModel  stateModel;

  JSlider      volumeSlider = null;
  JButton      muteButton = null;
  boolean      inUpdate = false;
  boolean      muteActionSupported = true;
  int          muteLatchedVolume = 25;

  public VolumeControlPanel( MediaRendererDevice mediaRenderer )
  {
    this.mediaRenderer = mediaRenderer;

    if( mediaRenderer != null )
      stateModel = mediaRenderer.getStateModel();
    else
      stateModel = new RendererStateModel();  // For UI testing

    stateModel.addObserver( this );

    //GridBagLayout layout = new GridBagLayout();
    setLayout( new BorderLayout() );

    //setBackground( Color.orange );

    //
    // Mute button
    //
    muteButton = RendererControlPanel.
      createTransportButton( "mute36.gif",
                             "Volume mute",
                             "mutePressed36.gif",
                             "muteSel36.gif" );
    muteButton.addActionListener( new MuteActionListener() );
    add( muteButton, BorderLayout.WEST );

    // If device doesn't natively support Mute action, set flag so it
    // can be simulated as needed by setting volume to 0 (and latching
    // volume for the 'unmute' operation)
    if( ! mediaRenderer.supportsAction( "RenderingControl", "SetMute" ) )
      muteActionSupported = false;
    

    //
    // Volume slider
    //
    int initialVolume = 0;
    volumeSlider = new JSlider( JSlider.HORIZONTAL, 0, 100, initialVolume );

    // Can't figure out how to set color of slider bar - below just affects
    // ticks

    //volumeSlider.setForeground( Color.red );
    //volumeSlider.setForeground( new Color( 136, 163, 206 ) );
    //volumeSlider.setBackground( new Color( 165, 197, 250 ) );
    volumeSlider.addChangeListener( new VolumeChangeListener() );
    volumeSlider.setPreferredSize( new Dimension( 100, 36 ) );
    volumeSlider.setMajorTickSpacing( 50 );
    //volumeSlider.setMinorTickSpacing( 0 );
    //volumeSlider.setPaintTicks( true );
    //volumeSlider.setPaintLabels( true );
    add( volumeSlider, BorderLayout.CENTER );

  }

  public void setComponentBackground( Color bgColor )
  {
    setBackground( bgColor );
    volumeSlider.setBackground( bgColor );
  }
  
  /**
   * Observer callback for stateModel changes
   */
  public synchronized void update( Observable model, Object arg )
  {
    RendererStateModel stateModel = (RendererStateModel)model;

    // Set flag to inhibit invocation of actions that would otherwise
    // result from the component callbacks
    inUpdate = true;
    
    volumeSlider.setValue( stateModel.getVolume() );
    if( muteButton != null )
      muteButton.setSelected( stateModel.getMute() );

    inUpdate = false;
  }

  /**
   *  Button action listeners
   */
  public class MuteActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      logger.fine("Entered" );

      if( muteButton.isSelected() )
      {
        muteButton.setSelected(false);
        // Following check enables UI look & feel testing w/o real renderer
        if( mediaRenderer != null )   
        {
          if( muteActionSupported )
          {
            mediaRenderer.actionSetMute( false );  // Toggle 
          }
          else
          {
            // Simulated Mute
            stateModel.setMute( false );
            mediaRenderer.actionSetVolume( muteLatchedVolume );  // Toggle 
          }
        }
      }
      else
      {
        muteButton.setSelected(true);
        if( mediaRenderer != null )   
        {
          if( muteActionSupported )
          {
            mediaRenderer.actionSetMute( true );   // Toggle 
          }
          else
          {
            // Simulated Mute
            muteLatchedVolume = stateModel.getVolume();
            stateModel.setMute( true );
            mediaRenderer.actionSetVolume( 0 );  // Toggle 
          }
        }
      }
    }
  }

  public class VolumeChangeListener implements ChangeListener
  {
    public void stateChanged( ChangeEvent e )
    {
      JSlider source = (JSlider)e.getSource();

      if( inUpdate )
        return;

      if( !source.getValueIsAdjusting() )
      {
        logger.fine("JSlider listener: Changing volume to " + 
                    (int)source.getValue() );

        // Volume range is 0-100
        // Shouldn't have to do the following if device eventing is working
        // properly for all devices - an event should arrive that updates
        // the state model (revisit - TODO)
        stateModel.setVolume( (int)source.getValue() );
        //stateModel.notifyObservers();
        if( mediaRenderer != null )
          mediaRenderer.actionSetVolume( (int)source.getValue() );
      }
    }
  }

  /*
  public class MuteItemListener implements ItemListener
  {
    public void itemStateChanged( ItemEvent e )
    {
      logger.fine("Mute itemStateChanged(): Entered" );

      if( inUpdate )
        return;

      if( mediaRenderer != null )
      {
        if( muteButton.isSelected() )
          mediaRenderer.actionSetMute( true );
        //stateModel.setMute( true );
        else
          mediaRenderer.actionSetMute( false );
        //        stateModel.setMute( false );
      }
      
      //stateModel.notifyObservers();
    }
  }
  */

}

