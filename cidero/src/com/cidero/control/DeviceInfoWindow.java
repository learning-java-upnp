/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DeviceInfoWindow.java,v $
 *
 */

package com.cidero.control;

import java.awt.*;
import java.awt.event.*;
import java.util.logging.Logger;
import java.util.Vector;
import java.util.Observer;
import java.util.Observable;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.cidero.util.AppPreferences;
import com.cidero.util.ExternalBrowser;

/**
 *  Device information/configuration window. This window shows (eventually)
 *  all the device details made available via the device's XML description,
 *  including the link to the device's 'presentation' page, which on some
 *  devices allows for simple Web-based device configuration.
 */
public class DeviceInfoWindow extends JFrame
  implements WindowListener, Observer
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaController controller;
  Container cp;
  JPanel panel;

  /** 
   * Window is a singleton
   */
  static DeviceInfoWindow deviceInfoWindow = null;
  
  public static DeviceInfoWindow getInstance()
  {
    if( deviceInfoWindow == null )
    {
      deviceInfoWindow = new DeviceInfoWindow( MediaController.getInstance() );
    }
    return deviceInfoWindow;
  }

  /**
   *
   */
  private DeviceInfoWindow( MediaController mediaController )
  {
    super( "UPnP A/V Device Information & Configuration" );

    this.controller = mediaController;
    
    MediaController.getMediaDeviceList().addObserver(this);

    // Position window near upper left of parent.
    // Make the initial size dependent on the number of devices present
    Point location = controller.getFrame().getLocation();
    //Dimension size = controller.getFrame().getSize();
    
    int nRows = MediaController.getMediaDeviceList().size();
    if( nRows > 8 )
      nRows = 8;  // max size
    int ySize = 80 + nRows * 60;
    setBounds( location.x+20, location.y+60, 400, ySize );

    addWindowListener( this );
    
    cp = getContentPane();

    panel = buildPanel( MediaController.getMediaDeviceList() );
    
    cp.add( panel, BorderLayout.CENTER ); 
    setVisible( true );
  }

  public MediaController getParentController()
  {
    return controller;
  }

  /**
   *  Update method invoked due to change in device list (device added/removed)
   *  Since this window isn't used that frequently, just destroy the 
   *  previous panel containing *all* the device widgets, and rebuild it
   *  from scratch. (Simplest code for rarely used feature)
   */
  public void update( Observable model, Object arg )
  {
    MediaDeviceList devList = (MediaDeviceList)model;

    logger.fine("DeviceInfoWindow:update - nDevices = " + devList.size() );

    cp.remove( panel ); 
    //panel.dispose();
    cp.repaint();
    
    panel = buildPanel( devList );
    cp.add( panel, BorderLayout.CENTER ); 
  }
  
  /**
   *  Build device info panel. Layout is grid with one row per device
   *  containing:
   *   
   *   Icon   FriendlyName   IPAddr  WebPage
   *
   *  TODO: Might want to add access to 'DeviceXML' and 'ServiceXML' here
   */
  public JPanel buildPanel( MediaDeviceList devList )
  {
    logger.fine("buildPanel: Entered, nDev = " + devList.size() );
    
    JPanel p = new JPanel();
    
    GridBagLayout gbLayout = new GridBagLayout();
    p.setLayout( gbLayout );

		GridBagConstraints 	gbc = new GridBagConstraints();
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    //gbCon.insets.set( 14, 8, 4, 2 );
    gbc.anchor = GridBagConstraints.WEST;

    EmptyBorder emptyBorder = 
      (EmptyBorder)BorderFactory.createEmptyBorder(2,4,2,4);

    for( int n = 0 ; n < devList.size() ; n++ )
    {
      MediaDevice dev = devList.getMediaDevice(n);

      gbc.gridx = 0;
      gbc.gridy = n;
      gbc.weightx = 0.0;
      gbc.weighty = 1.0;

      JButton iconButton = new JButton( dev.getIcon() );
      iconButton.setBorderPainted( false );
      iconButton.setFocusPainted( false );
      iconButton.setContentAreaFilled( false );
      iconButton.setBorder( emptyBorder );

      iconButton.addActionListener( 
            new WebInterfaceActionListener("http://www.cidero.com") );

      gbLayout.setConstraints( iconButton, gbc );
      p.add( iconButton );

      gbc.gridx = 1;
      gbc.weightx = 1.0;
      String labelString = "<html>" + 
                            dev.getFriendlyName() + "<br>" +
                            dev.getHost() + ":" + dev.getPort() + 
                           "</html>" ;
      
      JLabel nameLabel = new JLabel( labelString );
      gbLayout.setConstraints( nameLabel, gbc );
      p.add( nameLabel );

      String baseURL = dev.getURLBase();
      String presentationURL = dev.getPresentationURL();
      //System.out.println("BaseURL: " + baseURL + 
      //                   "PresentationURL = " + presentationURL );

      // Prepend the baseURL to the presentationURL. Some devices just
      // jammed a Web site address into presentationURL field, so try 
      // and deal with that by checking for leading "http" or "www."
      // (kludge)  
      if( (presentationURL != null) && !presentationURL.trim().equals("") )
      {
        if( presentationURL.startsWith("http://") )
        {
          // leave it be
        }
        else if( presentationURL.startsWith("www.") )
        {
          presentationURL = "http://" + presentationURL;
        }
        else
        {
          if( baseURL != null )
          {
            if( !baseURL.endsWith("/") && !presentationURL.startsWith("/") )
              presentationURL = baseURL + "/" + presentationURL;
            else
              presentationURL = baseURL + presentationURL;
          }
        }
        
        gbc.gridx = 2;
        gbc.weightx = 1.0;
        //JLabel presLabel = new JLabel( dev.getPresentation() );
        JButton presentationButton = new JButton( 
           "<html><a href=\"" + presentationURL + "\">Web Page</a></html>");
        presentationButton.setBorderPainted( false );
        presentationButton.setFocusPainted( false );
        presentationButton.setContentAreaFilled( false );
        presentationButton.setBorder( emptyBorder );
        
        presentationButton.addActionListener( 
               new WebInterfaceActionListener( presentationURL ) ); 

        gbLayout.setConstraints( presentationButton, gbc );
        p.add( presentationButton );
      }
    }

    return p;
  }
  
  /**
   *  Button action listeners
   */
  public class WebInterfaceActionListener implements ActionListener
  {
    String url;
    
    public WebInterfaceActionListener( String url )
    {
      this.url = url;
    }
    
    public void actionPerformed( ActionEvent e )
    {
      logger.fine("Displaying URL '" + url + "' in browser");
      ExternalBrowser extBrowser = new ExternalBrowser();
      
      // Mozilla profile support
      AppPreferences pref = MediaController.getPreferences();
      String profileOptions = pref.get("ExternalBrowser.mozillaProfileOptions");
      if( profileOptions != null )
        extBrowser.setMozillaProfileOptions( profileOptions );

      extBrowser.displayURL( url );
    }
  }


  /**
   *  Window listener methods
   */
  public void windowClosing(WindowEvent e) {
    //System.out.println("WindowListener method called: windowClosing.");
    // TODO: May want to shut down debugging when window not up
  }

  public void windowClosed(WindowEvent e) {
    //System.out.println("WindowListener method called: windowClosed.");
  }

  public void windowOpened(WindowEvent e) {
    //System.out.println("WindowListener method called: windowOpened.");
  }

  public void windowIconified(WindowEvent e) {
    //System.out.println("WindowListener method called: windowIconified.");
  }

  public void windowDeiconified(WindowEvent e) {
    //System.out.println("WindowListener method called: windowDeiconified.");
  }

  public void windowActivated(WindowEvent e) {
    //System.out.println("WindowListener method called: windowActivated.");
  }

  public void windowDeactivated(WindowEvent e) {
    //System.out.println("WindowListener method called: windowDeactivated.");
  }

}

  
