/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugSubscriptionResponse.java,v $
 *
 */

package com.cidero.control;

import java.util.ArrayList;
import java.util.logging.Logger;
import java.net.URL;
import java.net.MalformedURLException;

import org.cybergarage.http.HTTPStatus;
import org.cybergarage.upnp.Device;
import org.cybergarage.upnp.Service;
import org.cybergarage.upnp.event.*;
import com.cidero.swing.text.XMLStyledDocument;

/**
 *  Debug container for subscription response messages. See 
 *  DebugSubscriptionRequest.java comments for message details
 */      
class DebugSubscriptionResponse extends DebugObj
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  SubscriptionResponse   subRes;  // Original response
  String                 friendlyName;   // target device's friendlyName
  String                 serviceType;    // target device's service type
  
  /**
   * Constructor
   */
  public DebugSubscriptionResponse( Service service,
                                    SubscriptionResponse subRes )
  {
    this.subRes = subRes;

    if( subRes.getStatusCode() == HTTPStatus.OK )
      setStatus( DebugObj.STATUS_OK );
    else
      setStatus( DebugObj.STATUS_ERROR );

    // skip over 1st 29 chars (urn:schemas-upnp-org:service:)
    serviceType = service.getServiceType().substring(29);
    friendlyName = service.getRootDevice().getFriendlyName();

    //logger.info("Subscription response for dev: " + friendlyName + 
    //                " = " + subRes.toString() );

  }

    
  /**
   * Single line string represention. Used in top pane of debug window
   */
  public String toSingleLineString()
  {
    return getTimeString() + " SubscribeResponse: " + 
      friendlyName + ":" + serviceType +
      " Status: " + HTTPStatus.code2String( subRes.getStatusCode() );
  }
  
  /**
   * Full (may be multiple lines) string represention. Used in bottom
   * pane of debug window
   */
  public String toString()
  {
    return subRes.toString();
  }

  /** 
   *  Append a text representation of the object to an XML-capable
   *  StyledDocument class.
   *
   * @param doc          XMLStyledDocument instance (normally displayed
   *                     in JTextPane)
   * @param autoFormat   Enable XML auto-formatting. Setting this to
   *                     false disables all the special XML-sensitive logic
   */ 
  public void append( XMLStyledDocument doc, boolean autoFormat )
  {
    doc.appendString("Subscription Response:\n\n" + subRes.toString() );
  }

  static boolean enabled = true;

  public static void setEnabled( boolean flag )
  {
    enabled = flag;
  }
  public static boolean getEnabled()
  {
    return enabled;
  }

  public boolean isDisplayable()
  {
    return true;
  }

}

 

