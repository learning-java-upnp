/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugConfigDialog.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.Vector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;


/**
 *  Debugger window basic configuration dialog
 */
public class DebugConfigDialog extends JDialog
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  DebugWindow debugWindow;
  DebugPanel  debugPanel;

  JPanel configPanel = new JPanel();
  JCheckBox autoFormatCheckBox = new JCheckBox("Auto Format XML Fragments");

  SpinnerNumberModel histSpinnerModel;

  JPanel buttonPanel = new JPanel();
  JButton okButton = new JButton("OK");
  JButton applyButton = new JButton("Apply");
  JButton cancelButton = new JButton("Cancel");

  AppPreferences pref;

  /** 
   * Dialog is a singleton
   */
  static DebugConfigDialog dialog = null;
  
  public static DebugConfigDialog getInstance()
  {
    if( dialog == null )
    {
      dialog = new DebugConfigDialog( DebugWindow.getInstance() );
    }
    return dialog;
  }

  /** 
   * Constructor
   */
  private DebugConfigDialog( DebugWindow debugWindow )
  {
    super( debugWindow, "Debug Window Options", false );

    this.debugWindow = debugWindow;
    this.debugPanel = debugWindow.getDebugPanel();  // shorthand reference
    
    pref = MediaController.getPreferences();

    // Set position to right-hand side of parent debug window (more or less)
    setLocationRelativeTo( debugWindow );

    Container cp = getContentPane();
    
    cp.setLayout( new BorderLayout() );
    
    configPanel.setLayout( new BoxLayout( configPanel, BoxLayout.Y_AXIS ) );

		GridBagConstraints 	gbc = new GridBagConstraints();

    //---------------- Single-Line Info Config Options  ----------------------

    GridBagLayout singleLineLayout = new GridBagLayout();
    JPanel singleLineConfig = new JPanel( singleLineLayout );
    
		singleLineConfig.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Upper Panel Options"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );
    configPanel.add( singleLineConfig );

    JLabel histLabel = new JLabel("History size (lines)");

    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    //gbCon.insets.set( 14, 8, 4, 2 );
    gbc.anchor = GridBagConstraints.CENTER;

    singleLineLayout.setConstraints( histLabel, gbc );
    singleLineConfig.add( histLabel );

    histSpinnerModel = new SpinnerNumberModel( 
                                  pref.getInt( "debug.historySize", 100 ),
                                  0, 1000, 10 );
    JSpinner histSpinner = new JSpinner( histSpinnerModel );
    gbc.gridx = 1;
    gbc.gridy = 0;
    singleLineLayout.setConstraints( histSpinner, gbc );
    singleLineConfig.add( histSpinner );
    

    //---------------- Multi-Line Info Config Panel  ----------------------

    GridBagLayout multiLineLayout = new GridBagLayout();
    JPanel multiLineConfig = new JPanel( multiLineLayout );
    
		multiLineConfig.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Lower Panel Options"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );
    configPanel.add( multiLineConfig );
    
    autoFormatCheckBox.setSelected( pref.getBoolean( "debug.autoFormatXML",
                                                     true ) );
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    //gbCon.fill = gbCon.VERTICAL;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    //gbCon.insets.set( 14, 8, 4, 2 );
    gbc.anchor = GridBagConstraints.CENTER;
    multiLineLayout.setConstraints( autoFormatCheckBox, gbc );
    multiLineConfig.add( autoFormatCheckBox );

    //--------- Add config panel to dialog as the free space hog ---------

    cp.add( configPanel, BorderLayout.CENTER );


    //--------------------------------------------------------------
    // 'OK', 'Apply', 'Cancel' button panel at bottom of dialog
    //--------------------------------------------------------------

    okButton.addActionListener( new OkActionListener() );
    applyButton.addActionListener( new ApplyActionListener() );
    cancelButton.addActionListener( new CancelActionListener() );

    buttonPanel.setLayout( new FlowLayout() );
    buttonPanel.add( okButton );
    buttonPanel.add( applyButton );
    buttonPanel.add( cancelButton );
    cp.add( buttonPanel, BorderLayout.SOUTH );
    
    pack();
    setVisible( true );
  }

  /**
   *  Button action listeners
   */
  public class OkActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();
      setVisible( false );
    }
  }

  public class ApplyActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();
    }
  }

  public class CancelActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      setVisible( false );
    }
  }
  
  /**
   * Update in-memory copy of preferences, and reset Debug object 
   * enable flags based on the new preferences
   */
  public void updatePreferences()
  {
    Integer currValue = (Integer)histSpinnerModel.getValue();
    pref.putInt( "debug.historySize", currValue.intValue() );

    if( autoFormatCheckBox.isSelected() )
      pref.putBoolean( "debug.autoFormatXML", true );
    else
      pref.putBoolean( "debug.autoFormatXML", false );

    debugWindow.getParentController().setDebugFlagsFromPreferences();
  }
  
}

  
