/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: RemoteURLRequestor.java,v $
 *
 */

package com.cidero.control;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import java.net.URL;
import java.net.MalformedURLException;

import com.cidero.http.*;
import com.cidero.util.*;


/**
 * Simple class to take a Firefox URL request from the 'Launchy' extension
 * and pass it on to the Media Controller via HTTP.
 *
 * The MediaController runs an HTTP server to service UPnP events from 
 * devices. Non-UPnP requests are passed through the UPnP layer to the
 * MediaController class.  The server runs on port 8058 by default
 *
 * The convention used here is 
 */
class RemoteURLRequestor
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  boolean isAudioBroadcast = true;  // Default - assume net radio URL
  String  deviceName = "All";
  String  urlString;
  

  public RemoteURLRequestor( String[] args )
  {
    processCmdLineArgs( args );
    
    
    //AppPreferences pref = MediaController.getPreferences();
    //    int port = pref.getInt("httpServerPort", 8058);
    int port = 8058;

    String proxyURLString;


    logger.info("Sending URL to controller: " + urlString );

    
    if( isAudioBroadcast )
    {
      proxyURLString = URLUtil.urlToProxy( urlString,
                                           NetUtil.getDefaultLocalIPAddress(),
                                           port,
                                           "remoteURL-stream-" + deviceName );
    }
    else
    {
      proxyURLString = URLUtil.urlToProxy( urlString,
                                           NetUtil.getDefaultLocalIPAddress(),
                                           port,
                                           "remoteURL-file-" + deviceName );
    }
    
    logger.info("Proxy URL: " + proxyURLString );
    
    HTTPConnection connection = new HTTPConnection();
    try
    {
      URL url = new URL( proxyURLString );
      HTTPRequest request = new HTTPRequest( HTTP.GET, url );

      // Override use of keep-alive connection with 'close' header for now 
      request.setHeader("Connection", "close" );

      HTTPResponse response = connection.sendRequest( request, false );  

      if( (response == null) || 
          (response.getStatusCode() != HTTPStatus.OK) )
      {
        logger.warning("Error sending URL");
      }
      else
      {
        logger.info("Success sending URL");
      }
    }
    catch( MalformedURLException e )
    {
      logger.warning("Exception for URL: " + e );
    }
    catch( IOException e )
    {
      logger.warning("Exception for URL: " + e );
    }

  }

  public void usage()
  {
    logger.info("Usage: RemoteURLRequestor [-f] [-d devicename] url\n");

    logger.info(" [-f]  Interpret URL as playlist of files. Default is");
    logger.info("       to assume a streaming Internet radio playlist\n");
    logger.info(" [-d]  Device name. Specify the (UPnP) device name to");
    logger.info("       send the URL to. Default is 'All', which sends");
    logger.info("       the URL to all devices with active (open)");
    logger.info("       controller windows.");
  }
  
  public void processCmdLineArgs( String[] args )
  {
    int optind;

    for( optind = 0; optind < args.length; optind++ )
    {
      if( args[optind].equals("-f") )
      {
        isAudioBroadcast = false;
      }
      else if( args[optind].equals("-d") )
      {
        optind++;
        deviceName = args[optind];
      }
      else
      {
        break;
      }
    }

    if( optind != (args.length-1) )
    {
      usage();
      System.exit(-1);
    }

    urlString = args[optind];
  }

  /**
   */
  static public void main( String[] args )
  {

    RemoteURLRequestor requestor = new RemoteURLRequestor( args );
    
    logger.info("Done");

  }
  
}




