/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: VideoItemPanel.java,v $
 *
 */

package com.cidero.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Container;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.WindowConstants;
import javax.swing.ListSelectionModel;
import javax.swing.JOptionPane;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableCellRenderer;

import com.cidero.upnp.CDSVideoItem;
import com.cidero.upnp.CDSObject;
import com.cidero.swing.table.TableSorter;

/**
 *  This class implements a panel that displays a set of video item (movie)
 *  data in table form. It can be used to display all movies associated
 *  with an movie category, or all movies matching a user's search
 *  criteria.  The list can be sorted on all column headings in forward
 *  or reverse order (courtesy of Sun's SortedTable sample code)
 *
 */
public class VideoItemPanel extends JPanel
                            implements MouseListener, ListSelectionListener
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaItemPanel parent;
  JTable         table;
  VideoItemModel model;
  VideoItemModel defaultModel;
  TableSorter    sortedModel;
  ListSelectionModel listSelectionModel;

  JButton addSelectedButton = new JButton("Add Videos To Play Queue");
  JButton selectAllButton = new JButton("Select All");
  JButton clearSelectionButton = new JButton("Clear Selection");

  /**
   * Constructor
   */
  public VideoItemPanel( MediaItemPanel parent )
  {
    this.parent = parent;
    
    setLayout( new BorderLayout() );
    //setLayout( new BoxLayout( this, BoxLayout.Y_AXIS ) );

    Color uiLookAndFeelBackground = getBackground();
    
    setBackground( MediaController.getBackground() );
    
    EmptyBorder emptyBorder = 
      (EmptyBorder)BorderFactory.createEmptyBorder( 0, 2, 2, 2 );
    setBorder( emptyBorder );

    //----------------------------------------------------------------
    // Button panel at top
    //----------------------------------------------------------------

    FlowLayout buttonLayout = new FlowLayout( FlowLayout.CENTER );
    buttonLayout.setHgap(10);
    buttonLayout.setVgap(8);


    selectAllButton.addActionListener( new SelectAllActionListener() );
    clearSelectionButton.addActionListener( new ClearSelectionActionListener() );
    addSelectedButton.addActionListener( new AddSelectedActionListener() );
    
    JPanel buttonPanel = new JPanel( buttonLayout );

    // Start off with no selected items, so disable these buttons...
    addSelectedButton.setEnabled(false);
    clearSelectionButton.setEnabled(false);

    // Small customizations to handle MAC OSX widget differences 
    if( MediaController.isRunningOnMacOSX() )
    {
      addSelectedButton.setFocusPainted( false );
      addSelectedButton.setContentAreaFilled( false );
      selectAllButton.setFocusPainted( false );
      selectAllButton.setContentAreaFilled( false );
      clearSelectionButton.setFocusPainted( false );
      clearSelectionButton.setContentAreaFilled( false );
    }
    else
    {
      buttonPanel.setBackground( MediaController.getBackground() );
    }

    buttonPanel.add( addSelectedButton );
    buttonPanel.add( selectAllButton );
    buttonPanel.add( clearSelectionButton );

    add( buttonPanel, BorderLayout.NORTH );


    //----------------------------------------------------------------
    // Table at bottom
    //----------------------------------------------------------------

    // Create the table with a dummy (empty) audio model

    defaultModel = new VideoItemModel();
    sortedModel = new TableSorter( defaultModel );
  
    table = new JTable( sortedModel );
    sortedModel.addMouseListenerToHeaderInTable( table );

    listSelectionModel = table.getSelectionModel();
    listSelectionModel.addListSelectionListener( this );


    //Set up tool tips for column headers.
    table.getTableHeader().setToolTipText(
         "Click to sort; Shift-Click to sort in reverse order");

    // Set the sizes of the columns
    TableColumn column = null;
    column = table.getColumnModel().getColumn(0);  // Artist
    column.setPreferredWidth(200);
    column = table.getColumnModel().getColumn(1);  // Title
    column.setPreferredWidth(320);
    column = table.getColumnModel().getColumn(2);  // Time
    column.setPreferredWidth(80);
    column.setMaxWidth(80);
    column = table.getColumnModel().getColumn(3);  // Genre
    column.setPreferredWidth(86);
    //column.setMaxWidth(80);

    table.setShowGrid( false );
    table.setBackground( uiLookAndFeelBackground );


    // Allow user to select multiple tracks in table
    table.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );

    Font font = table.getFont();
    Font boldFont = font.deriveFont( Font.BOLD );
    table.setFont( boldFont );
    table.getTableHeader().setFont(boldFont);

    table.addMouseListener( this );

    // Add table to panel within its own scrollable pane
    JScrollPane scroller = 
      new JScrollPane( table,
                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                       JScrollPane.HORIZONTAL_SCROLLBAR_NEVER );
    scroller.setAutoscrolls(true);

    add( scroller, BorderLayout.CENTER );

  }
  
  /**
   * Constructor
   */
  public VideoItemPanel( MediaItemPanel parent, VideoItemModel model )
  {
    this( parent );
    setModel( model );
  }
  
  public MediaController getMediaController()
  {
    return parent.getMediaController();
  }

  /**
   *  Set the audio item model for the panel. This is invoked whenever
   *  the model changes (new server clicked on in the server panel)
   */
  public void setModel( VideoItemModel model )
  {
    if( model == null )
    {
      this.model = defaultModel;
      sortedModel.setModel( defaultModel );
      sortedModel.fireTableChanged( new TableModelEvent( sortedModel ) );
    }
    else
    {
      this.model = model;
      sortedModel.setModel( model );
      sortedModel.fireTableChanged( new TableModelEvent( sortedModel ) );
    }
  }
  public VideoItemModel getModel()
  {
    return model;
  }

  public CDSObject getSelectedObject()
  {
    int row = table.getSelectedRow();
    if( row < 0 )
    {
      logger.fine("No row selected");
      return null;
    }
    
    logger.fine("Selected row = " + row );
    int unsortedRow = sortedModel.getUnsortedRowIndex( row );    

    return model.getObjectAtRow( unsortedRow );
  }

  public void selectAll()
  {
    if( table.getRowCount() > 0 )
    {
      // Select item 0 to force selection model to track changes in 
      // table model (seems to be a bug related to the Table's 
      // selection model getting out of sync with the data model)
      table.setRowSelectionInterval( 0, 0 );
      table.selectAll();
    }
  }

  public void valueChanged( ListSelectionEvent e )
  {
    if( listSelectionModel.isSelectionEmpty() )
    {
      addSelectedButton.setEnabled(false);
      clearSelectionButton.setEnabled(false);
    }
    else
    {
      addSelectedButton.setEnabled(true);
      clearSelectionButton.setEnabled(true);
    }
  }

  public class SelectAllActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      selectAll();
    }
  }

  public class ClearSelectionActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      table.clearSelection();
    }
  }

  public class AddSelectedActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      // Double check that table selection mode is set up right
      if( !table.getRowSelectionAllowed() ||
          table.getColumnSelectionAllowed() )
      {
        logger.warning("Bad Row/Col selection settings");
        return;
      }
      
      int[] rowIndices = table.getSelectedRows();

      if( rowIndices.length > 0 )
      {
        ArrayList mediaRendererList = 
          getMediaController().getActiveMediaRenderers();

        if( mediaRendererList.size() == 0 )
        {
          logger.warning("No active media renderers (select one to bring up window)");
          return;
        }
        
        // 
        // Add selected items to all renderers that are visible
        // Put up info dialog if none are visible
        //
        int visibleRendererCount = 0;

        for( int dev = 0 ; dev < mediaRendererList.size() ; dev++ )
        {
          MediaRendererDevice mediaRenderer =  
            (MediaRendererDevice)mediaRendererList.get(dev);
          
          if( ! mediaRenderer.isVisible() )
            continue;
          
          visibleRendererCount++;

          for( int n = 0 ; n < rowIndices.length ; n++ )
          {
            //System.out.println("Selected row index = " + rowIndices[n] );

            //
            // Sometimes the table 'selectAll' selection can reflect an 
            // older table model selection (Swing bug as far as I can tell),
            // resulting in illegal rows. Filter them out here. This doesn't
            // seem to be a problem for single selections, only 
            // selectAlls after a model change
            //
            if( rowIndices[n] >= table.getRowCount() )
            {
              logger.warning("Error - table reported out of range selected row");
            }
            else
            {
              int unsortedIndex = 
                sortedModel.getUnsortedRowIndex( rowIndices[n] );
              CDSObject obj = model.getObjectAtRow( unsortedIndex );
              
              logger.fine("Adding obj to play queue: " + obj.getTitle() );
              mediaRenderer.addToPlayQueue( obj );
            }
          }
        }

        if( visibleRendererCount == 0 )
        {
          JOptionPane.showMessageDialog( getMediaController().getFrame(),
              "Please select one or more renderers to add items to their play queue(s)");
          return;
        }

      }
      
      table.clearSelection();
    }
  }

  /**
   *  MouseClick event handler. Set flag in browser panel to indicate
   *  most recent click was in ItemPanel region, not the TreePanel
   */
  public void mouseClicked( MouseEvent e )
  {
    //logger.fine("mouseClicked");
    parent.getMediaBrowserPanel().
    setLastMouseClickPanel( MediaBrowserPanel.MEDIA_ITEM_PANEL );
  }
  
  public void mouseEntered(MouseEvent e) {
    //logger.fine("mouseEntered");
  }
  
  public void mouseReleased(MouseEvent e) {
    //logger.fine("mouseReleased");
  }
  
  public void mousePressed(MouseEvent e) {
    logger.fine("mousePressed");
  }
  
  public void mouseExited(MouseEvent e) {
    logger.fine("mouseExited");
  }


}

