/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: RendererControlWindow.java,v $
 *
 */

package com.cidero.control;

import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Point;
import java.awt.event.*;

import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.border.CompoundBorder;

/**
 *  Renderer control window class. Control window has a panel at 
 *  the top with transport & volume controls, and a panel at the 
 *  bottom with a 'play queue' of items to be played. Each renderer
 *  has a separate play queue for music and pictures, and the panels
 *  for each are different. Only one is visible at a time.
 */
public class RendererControlWindow extends JFrame
implements WindowListener, WindowStateListener, ComponentListener
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  // View mode for lower panel
  public final static int AUDIO_QUEUE_VIEW = 1;
  public final static int IMAGE_QUEUE_VIEW = 2;
  public final static int VIDEO_QUEUE_VIEW = 3;

  static int rendererWindowCount = 0;
  
  int playQueueView = AUDIO_QUEUE_VIEW;  // Default view is audio


  Container contentPane;

  JPanel                  panel;   // Main panel 

  MediaRendererDevice mediaRenderer;  // Parent renderer
  RendererControlPanel    controlPanel;
  PlayQueuePanel playQueuePanel;
  ImageQueuePanel imageQueuePanel;

  /**
   * Constructor
   */
  public RendererControlWindow( String title,
                                MediaRendererDevice mediaRenderer )
  {
    super( title );

    // Look for all renderers with open control windows. If there are any,
    // put this new one below the others with a gap of a few pixels
    //MediaDeviceList devList = MediaController.getMediaDeviceList();
    //ArrayList rendererList = devList.getActiveMediaRenderers();
    setBounds( 500+40*rendererWindowCount,
               30*rendererWindowCount, 460, 136 );
    rendererWindowCount++;
    
    //  setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
    contentPane = getContentPane();

    this.mediaRenderer = mediaRenderer;

    // Panel to hold other panels, and host the border
    panel = new JPanel( new BorderLayout() );

		panel.setBorder( 
         new CompoundBorder( BorderFactory.createLineBorder( Color.gray, 1),
           new CompoundBorder(
             BorderFactory.createLineBorder( new Color(180, 180, 180), 6 ),
             BorderFactory.createLineBorder( Color.gray, 1 ) ) ) );

    controlPanel = new RendererControlPanel( this, mediaRenderer );
    playQueuePanel = new PlayQueuePanel( mediaRenderer );
    imageQueuePanel = new ImageQueuePanel( mediaRenderer );
    
    panel.add( controlPanel, BorderLayout.NORTH );
    panel.add( playQueuePanel, BorderLayout.CENTER ); // Default view is audio
    //panel.add( imageQueuePanel, BorderLayout.CENTER ); // Default view is audio

    contentPane.add( panel, BorderLayout.CENTER );

    setVisible( true );

    addWindowListener( this );
    addWindowStateListener( this );

    // Register the listener with the component
    addComponentListener( this );


  }

  public RendererControlPanel getControlPanel() {
    return controlPanel;
  }
  public PlayQueuePanel getPlayQueuePanel() {
    return playQueuePanel;
  }
  public ImageQueuePanel getImageQueuePanel() {
    return imageQueuePanel;
  }

  public void setPlayQueueView( int view ) 
  {
    logger.fine("Setting play queue view to " + view );

    if( playQueueView == view )
    {
      logger.fine("View alread set to " + view + " no action required");
      return;
    }
    
    if( playQueueView == IMAGE_QUEUE_VIEW )
      panel.remove( imageQueuePanel );
    else
      panel.remove( playQueuePanel );

    if( view == IMAGE_QUEUE_VIEW )
    {
      panel.add( imageQueuePanel );
      setSize( 600, getHeight() );
    }
    else
    {
      panel.add( playQueuePanel );
      setSize( 460, getHeight() );
    }
    
    playQueueView = view;

    // Control panel gets a customized look too...
    controlPanel.setViewMode( view );
  }

  public int getPlayQueueView() {
    return playQueueView;
  }
  
  public void setPlayQueueSelectedIndex( int index ) {
    playQueuePanel.setSelectedIndex( index );
  }
  public int getPlayQueueSelectedIndex() {
    return playQueuePanel.getSelectedIndex();
  }
  public void clearPlayQueueSelection() {
    playQueuePanel.clearSelection();
  }

  public int getImageQueueSelectedIndex() {
    return imageQueuePanel.getSelectedIndex();
  }
  public void clearImageQueueSelection() {
    imageQueuePanel.clearSelection();
  }

  /**
   * Expand main window to reveal play queue panel, unless it is already 
   * exposed
   */
  public void expandPlayQueuePanel()
  {
    // If some of play queue is visible, user has probably manually shrunk
    // it, so leave it alone
    if( getHeight() <= 160 )
    {

      if( playQueueView == IMAGE_QUEUE_VIEW )
        setSize( getWidth(), 480 );
      else
        setSize( getWidth(), 300 );

      // Set expand buttons to 'collapse' direction
      controlPanel.setExpandButtonsSelected(true);  
      
    }
  }
  public void collapsePlayQueuePanel()
  {
    setSize( getWidth(), 136 );
    controlPanel.setExpandButtonsSelected(false);
  }

  /*
  public void addPlayQueuePanel()
  {
    contentPane.add( playQueuePanel );
  }
  */

  /**
   *  Window listener methods
   */
  public void windowStateChanged(WindowEvent e) {

    //System.out.println("Window state changed!");

  }
  
  public void windowClosing(WindowEvent e) {
    //System.out.println("WindowListener method called: windowClosing.");
    // TODO: May want to shut down debugging when window not up
  }

  public void windowClosed(WindowEvent e) {
    //System.out.println("WindowListener method called: windowClosed.");
  }

  public void windowOpened(WindowEvent e) {
    //System.out.println("WindowListener method called: windowOpened.");
  }

  public void windowIconified(WindowEvent e)
  {
    //System.out.println("WindowListener method called: windowIconified.");

    for( int n = 0 ; n < mediaRenderer.getSlaveRendererList().size() ; n++ )
    {
      MediaRendererDevice slaveRenderer = 
      (MediaRendererDevice)mediaRenderer.getSlaveRendererList().get(n);

      slaveRenderer.getRendererControlWindow().setVisible(false);
    }
  }

  public void windowDeiconified(WindowEvent e)
  {
    //System.out.println("WindowListener method called: windowDeiconified.");

    Rectangle pos = getBounds();

    for( int n = 0 ; n < mediaRenderer.getSlaveRendererList().size() ; n++ )
    {
      MediaRendererDevice slaveRenderer = 
      (MediaRendererDevice)mediaRenderer.getSlaveRendererList().get(n);

      slaveRenderer.getRendererControlWindow().
        setLocation( pos.x, pos.y+pos.height );

      slaveRenderer.getRendererControlWindow().setVisible(true);

      // Keep running total of height
      Rectangle slavePos = slaveRenderer.
                           getRendererControlWindow().getBounds();
      pos.height += slavePos.height;
    }
  }

  public void windowActivated(WindowEvent e)
  {
    // System.out.println("WindowListener method called: windowActivated.");
  }

  public void windowDeactivated(WindowEvent e) {
    //System.out.println("WindowListener method called: windowDeactivated.");
  }

  public void componentShown(ComponentEvent evt) {
    //System.out.println("ComponentListener method called: shown.");

    // Component is now visible
    Component c = (Component)evt.getSource();

    // If this renderer has any slave renderers, bring up their windows 
    // as well. Make sure the positions are below the master

    if( mediaRenderer.getSlaveRendererList().size() > 0 )
    {
      Rectangle pos = getBounds();

      for( int n = 0 ; n < mediaRenderer.getSlaveRendererList().size() ; n++ )
      {
        MediaRendererDevice slaveRenderer = 
        (MediaRendererDevice)mediaRenderer.getSlaveRendererList().get(n);

        slaveRenderer.getRendererControlWindow().
          setLocation( pos.x, pos.y+pos.height );

        slaveRenderer.getRendererControlWindow().setVisible(true);

        // Keep running total of height
        Rectangle slavePos = slaveRenderer.
           getRendererControlWindow().getBounds();
        pos.height += slavePos.height;
      }
    }
    else if( mediaRenderer.getMasterRenderer() != null )
    {
      // This renderer is linked to master - put it below it if master
      // is visible
      MediaRendererDevice masterRenderer = mediaRenderer.getMasterRenderer();
      if( masterRenderer.getRendererControlWindow().isVisible() )
      {
        Rectangle pos = masterRenderer.getRendererControlWindow().getBounds();
        setLocation( pos.x, pos.y+pos.height );
      }
    }

  }
    
  // This method is called only if the component was visible and setVisible(false) was called
  public void componentHidden(ComponentEvent evt)
  {
    //System.out.println("ComponentListener method called: hidden.");

    // Component is now hidden
    /*
    Component c = (Component)evt.getSource();

    if( mediaRenderer.getSlaveRendererList().size() > 0 )
    {
      for( int n = 0 ; n < mediaRenderer.getSlaveRendererList().size() ; n++ )
      {
        MediaRendererDevice slaveRenderer = 
        (MediaRendererDevice)mediaRenderer.getSlaveRendererList().get(n);

        slaveRenderer.getRendererControlWindow().setVisible(false);
      }
    }
    */
  }
    
  // This method is called after the component's location within its
  // container changes
  public void componentMoved(ComponentEvent evt)
  {
    //System.out.println("ComponentListener method called: moved.");

    Component c = (Component)evt.getSource();
    
    // Get new location
    //Point newLoc = c.getLocation();

    Rectangle pos = c.getBounds();

    // If this is a master renderer, move slave windows along with it
    // Need to keep making slave visible otherwise it 'slides under' 
    // other windows that are higher in window managers stacking order
    for( int n = 0 ; n < mediaRenderer.getSlaveRendererList().size() ; n++ )
    {
      MediaRendererDevice slaveRenderer = 
      (MediaRendererDevice)mediaRenderer.getSlaveRendererList().get(n);

      slaveRenderer.getRendererControlWindow().
                    setLocation( pos.x, pos.y+pos.height );
        
      slaveRenderer.getRendererControlWindow().setVisible(true);

      // Keep running total of height
      Rectangle slavePos = slaveRenderer.
                           getRendererControlWindow().getBounds();
      pos.height += slavePos.height;
    }
      
  }
    
  // This method is called after the component's size changes
  public void componentResized(ComponentEvent evt) {

    //System.out.println("ComponentListener method called: resized");

    Component c = (Component)evt.getSource();
    
    // Get new size
    //Dimension newSize = c.getSize();

    Rectangle pos = c.getBounds();

    // If this is a master renderer, move slave windows along with it
    for( int n = 0 ; n < mediaRenderer.getSlaveRendererList().size() ; n++ )
    {
      MediaRendererDevice slaveRenderer = 
      (MediaRendererDevice)mediaRenderer.getSlaveRendererList().get(n);

        slaveRenderer.getRendererControlWindow().
        setLocation( pos.x, pos.y+pos.height );

        // Keep running total of height
        Rectangle slavePos = slaveRenderer.
           getRendererControlWindow().getBounds();
        pos.height += slavePos.height;
    }

  }


  /**
   * Basic test code
   */
  private static void createAndShowGUI()
  {
    RendererControlWindow window = 
      new RendererControlWindow("Renderer Control Window Test", null );
  }

  public static void main(String[] args)
  {
    //Schedule a job for the event-dispatching thread:
    //creating and showing this application's GUI.
    javax.swing.SwingUtilities.invokeLater(
                                           new Runnable()
                                           {
                                             public void run() {
                                               createAndShowGUI();
                                             }
                                           });
  }

}

  
