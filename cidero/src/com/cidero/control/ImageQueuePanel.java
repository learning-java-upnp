/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ImageQueuePanel.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.cidero.swing.*;
import com.cidero.upnp.*;
import com.cidero.util.AppPreferences;
import com.cidero.util.AppPreferencesChangeListener;
import com.cidero.util.MrUtil;

/**
 *  Class for renderer image queue panel at bottom of renderer control window
 *  An instance of this panel or its audio equivalent is attached to 
 *  the renderer window, depending on mode.
 */
public class ImageQueuePanel extends JPanel 
                             implements AppPreferencesChangeListener
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  protected MediaRendererDevice mediaRenderer;

  AppPreferences pref;

  JList          imageJList;
  ImageQueue     imageQueue;  // Model for queue's JList
  ImageQueueViewerHelper viewerHelper;

  JPanel  listControlPanel;  // Control panel that sits below JList panel

  JButton deleteButton = new JButton("Delete");
  JButton clearButton = new JButton("Clear");
  JComboBox modeCombo = new JComboBox();

  Color bgColor = new Color( 210, 210, 210 );

  JLabel intLabel;
  SpinnerNumberModel slideshowIntSpinnerModel;
  JSpinner slideshowIntSpinner;

  // Slideshow modes
  public final static int COMPOSE_SLIDESHOW = 1;
  public final static int RUN_SLIDESHOW = 2;

  int slideshowMode = COMPOSE_SLIDESHOW;  // Default


  /**
   * Constructor
   */
  public ImageQueuePanel( MediaRendererDevice mediaRenderer )
  {
    this.mediaRenderer = mediaRenderer;

    pref = MediaController.getPreferences();
    pref.addChangeListener( this );

    setLayout( new BorderLayout() );

		setBorder( BorderFactory.createEmptyBorder(2,2,2,2) );

    // Support case of no 'real' MediaRenderer (convenient for testing minor
    // changes to UI component layout)
    if( mediaRenderer != null )
      imageQueue = mediaRenderer.getImageQueue();
    else
      imageQueue = new ImageQueue();

    imageJList = new JList( imageQueue );
    imageJList.setSelectionModel( imageQueue.getSelectionModel() );

    imageJList.setLayoutOrientation( JList.HORIZONTAL_WRAP );
    // To make horizontal wrap work the way I want...
    imageJList.setVisibleRowCount( 0 );  

    viewerHelper = new ImageQueueViewerHelper( imageJList );
    imageJList.setCellRenderer( viewerHelper );
    imageJList.addMouseListener( viewerHelper );
    imageJList.addMouseMotionListener( viewerHelper );

    // These call sets the JList cell fixedHeight & fixedWidth properties,
    // which improves JList performance for long lists - See Sun docs.
    // TODO: Not yet enabled - need to compute height based on font
    // playQueueJList.setFixedCellHeight( 20 );
    // playQueueJList.setFixedCellWidth( 20 );

    //imageJList.addMouseListener( new ImageQueueMouseListener() );

    //imageQueue.addActiveItemListener( new ImageListActiveItemListener() );
    imageJList.addListSelectionListener( new ImageQueueSelectionListener());

    // Add keyboard listeners for right-arrow/left arrow to move selection
    // Default bindings move around in row/col space, not list item index
    // space (just want to be able to hit right-arrow repeatedly to move
    // through slideshow)

    InputMap inputMap1 = imageJList.getInputMap( JComponent.WHEN_FOCUSED );
    InputMap inputMap2 = imageJList.getInputMap( JComponent.WHEN_IN_FOCUSED_WINDOW );
    ActionMap actionMap = imageJList.getActionMap();

    KeyRightAction keyRightAction = new KeyRightAction();
    actionMap.put( keyRightAction.getValue(Action.NAME), keyRightAction );
    inputMap1.put( KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0, false),
                   keyRightAction.getValue(Action.NAME) );
    inputMap1.put( KeyStroke.getKeyStroke(KeyEvent.VK_KP_RIGHT, 0, false),
                   keyRightAction.getValue(Action.NAME) );
    inputMap2.put( KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0, false),
                   keyRightAction.getValue(Action.NAME) );
    inputMap2.put( KeyStroke.getKeyStroke(KeyEvent.VK_KP_RIGHT, 0, false),
                   keyRightAction.getValue(Action.NAME) );

    KeyLeftAction keyLeftAction = new KeyLeftAction();
    actionMap.put( keyLeftAction.getValue(Action.NAME), keyLeftAction );
    inputMap1.put( KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0, false),
                   keyLeftAction.getValue(Action.NAME) );
    inputMap1.put( KeyStroke.getKeyStroke(KeyEvent.VK_KP_LEFT, 0, false),
                   keyLeftAction.getValue(Action.NAME) );
    inputMap2.put( KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0, false),
                   keyLeftAction.getValue(Action.NAME) );
    inputMap2.put( KeyStroke.getKeyStroke(KeyEvent.VK_KP_LEFT, 0, false),
                   keyLeftAction.getValue(Action.NAME) );

    KeyRotateCWAction keyRotateCWAction = new KeyRotateCWAction();
    actionMap.put( keyRotateCWAction.getValue(Action.NAME), keyRotateCWAction);
    inputMap1.put( KeyStroke.getKeyStroke(KeyEvent.VK_R, 0, false),
                   keyLeftAction.getValue(Action.NAME) );
    inputMap2.put( KeyStroke.getKeyStroke(KeyEvent.VK_R, 0, false),
                   keyLeftAction.getValue(Action.NAME) );
    //inputMap2.put( KeyStroke.getKeyStroke(KeyEvent.VK_R, 0, false),
    //               keyLeftAction.getValue(Action.NAME) );

    
    // Put JList in ScrollPane
    JScrollPane scrollPane = new JScrollPane( imageJList );
    scrollPane.setPreferredSize( new Dimension( 400, 400 ) );

    add( scrollPane, BorderLayout.CENTER );
    //scrollPane.setBackground( Color.orange );
    //setBackground( Color.red );

    //------------------------------------------------------------------
    // Controls below queue list (save, clear, etc...)
    //------------------------------------------------------------------

    listControlPanel = new JPanel();
    BoxLayout buttonLayout = new BoxLayout( listControlPanel,
                                            BoxLayout.X_AXIS );
    listControlPanel.setLayout( buttonLayout );

    EmptyBorder emptyBorder = 
     (EmptyBorder)BorderFactory.createEmptyBorder( 4, 4, 4, 4 );
    listControlPanel.setBorder( emptyBorder );
    
    listControlPanel.add( Box.createGlue() );

    JLabel modeLabel = new JLabel("Mode: ");
    listControlPanel.add( modeLabel );

    modeCombo.addItem("Compose Slideshow"); 
    modeCombo.setSelectedIndex( 0 ); // Make compose default
    modeCombo.addItem("Run Slideshow");
    //modeCombo.addItem("Auto Slideshow");
    modeCombo.addActionListener( new ModeActionListener() );
    modeCombo.setPreferredSize( new Dimension( 200, -1 ) );
    modeCombo.setMaximumSize( new Dimension( 200, 48 ) );
    listControlPanel.add( modeCombo );

    listControlPanel.add( Box.createHorizontalStrut(16) );

    intLabel = new JLabel("Interval: ");
    intLabel.setEnabled(false);
    listControlPanel.add( intLabel );

    int currVal = pref.getInt("renderer.slideshowIntervalSec", 15 );

    slideshowIntSpinnerModel = new SpinnerNumberModel( currVal, 0,
                                                       3600, 1 );
    slideshowIntSpinner = new JSpinner( slideshowIntSpinnerModel );
    slideshowIntSpinner.addChangeListener( new SlideshowIntChangeListener() );
    slideshowIntSpinner.setPreferredSize( new Dimension( 60, -1 ) );
    slideshowIntSpinner.setMaximumSize( new Dimension( 60, 48 ) );
    slideshowIntSpinner.setEnabled(false);

    listControlPanel.add( slideshowIntSpinner );


    listControlPanel.add( Box.createHorizontalStrut(24) );
    listControlPanel.add( Box.createGlue() );


    deleteButton.addActionListener( new DeleteActionListener() );
    listControlPanel.add( deleteButton );

    listControlPanel.add( Box.createHorizontalStrut(16) );

    clearButton.addActionListener( new ClearActionListener() );
    listControlPanel.add( clearButton );

    listControlPanel.add( Box.createGlue() );


    // Small customizations to handle MAC OSX widget differences 
    if( MediaController.isRunningOnMacOSX() )
    {
      deleteButton.setFocusPainted( false );
      deleteButton.setContentAreaFilled( false );
      clearButton.setFocusPainted( false );
      clearButton.setContentAreaFilled( false );
    }
    else
    {
       setBackground( bgColor );
       listControlPanel.setBackground( bgColor );
    }

    add( listControlPanel, BorderLayout.SOUTH );

    appPreferencesChanged();
  }

  public void appPreferencesChanged()
  {
    Color color = pref.getColor("ImageQueuePanel.background");
    if( color != null )
      imageJList.setBackground( color );  

    color = pref.getColor("ImageQueuePanel.foreground");
    if( color != null )
      imageJList.setForeground( color );  
    imageJList.revalidate();
  }
  
  public class SlideshowIntChangeListener implements ChangeListener
  {
    public void stateChanged( ChangeEvent e )
    {
      JSpinner spinner = (JSpinner)e.getSource();
      Integer val = (Integer)(spinner.getValue());
      mediaRenderer.getPseudoStateModel().
          setSlideshowIntervalMs( val.intValue()*1000 );

    }
  }
  
  /**
   *  Start image playback.
   * 
   * @param playMode  'AVTransport.PLAY_MODE_NORMAL' if running timed
   *                  slideshow and play button was hit to start it, 
   *                  or 'AVTransport.PLAY_MODE_DIRECT_1' if
   *                  displaying single image due to clicking on it.
   */
  public void startImagePlayback( String playMode )
  {
    if( mediaRenderer == null )
      return;
    
    // If no selection, start playback at first image
    int queueStartIndex = imageJList.getSelectedIndex();
    if( queueStartIndex < 0 )
      queueStartIndex = 0;

    mediaRenderer.startImagePlayback( playMode, queueStartIndex );
  }

  /**
   *  Get currently selected queue item index.  -1 if no selection
   */
  public int getSelectedIndex()
  {
    return imageJList.getSelectedIndex();
  }

  /**
   * Clear current selection in queue JList
   */
  public void clearSelection()
  {
    imageJList.clearSelection();
  }

  public void selectNext()
  {
    // Don't do anything if only dummy element is present in JList
    if( imageJList.getModel().getSize() <= 1 )
      return;
      
    // Ignore if image playback underway
    /*
    if( mediaRenderer.getPlayThread() != null )
    {
      System.out.println("-----!!!!! Select Next PlayThread running---------");
      return;
    }
    */
    
    System.out.println("---------- Select Next -----------------");

    int index = imageJList.getSelectedIndex();
    if( index < 0 )
    {
      index = 0;
    }
    else
    {
      index++;
      if( index >= (imageJList.getModel().getSize()-1) )
        index = 0;
    }
    imageJList.setSelectedIndex( index );
  }

  public void selectPrev()
  {
    if( imageJList.getModel().getSize() <= 1 )
      return;
      
    /*
    // Ignore if image playback underway
    if( mediaRenderer.getPlayThread() != null )
      return;
    */

    System.out.println("Select Previous");
    int index = imageJList.getSelectedIndex();
    if( index < 0 )
    {
      index = imageJList.getModel().getSize()-2;
    }
    else
    {
      index--;
      if( index < 0 )
        index = imageJList.getModel().getSize()-2;
    }
    imageJList.setSelectedIndex( index );
    imageJList.repaint();
  }

  class KeyRightAction extends AbstractAction
  {
    public KeyRightAction() {
      super("keyRightAction");
    }
    public void actionPerformed( ActionEvent e ) {
      selectNext();
    }
  }

  class KeyLeftAction extends AbstractAction
  {
    public KeyLeftAction() {
      super("keyLeftAction");
    }
    public void actionPerformed( ActionEvent e ) {
      selectPrev();
    }
  }

  class KeyRotateCWAction extends AbstractAction
  {
    public KeyRotateCWAction() {
      super("keyRotateCWAction");
    }
    public void actionPerformed( ActionEvent e ) {
      //rotateSelectionCW();
    }
  }

  /*
  class KeyRotateCCWAction extends AbstractAction
  {
    public KeyRotateCCWAction() {
      super("keyRotateCCWAction");
    }
    public void actionPerformed( ActionEvent e ) {
      rotateSelectionCCW();
    }
  }
  */

  /**
   *  Listener for change in the list selection. There are two paths 
   *  that invoke this:
   *
   *  1. The Event thread, when mouse clicks or the arrow keys are used
   *     to change the selection.  In this case the selection change
   *     is interpreted as a *command* when running in slideshow mode.
   *     The cursor is changed to a busy cursor while the play thread
   *     displays the image
   *
   *  2. The Play thread, when a timed slideshow moves on to the next
   *     image. In this case, 
   */
  class ImageQueueSelectionListener implements ListSelectionListener
  {
    public void valueChanged( ListSelectionEvent e )
    {
      if( e.getValueIsAdjusting() ) 
        return;
      
      int index = imageJList.getSelectedIndex();
      logger.fine("SelectionListener index = " + index );

      boolean isEventDispatchThread = SwingUtilities.isEventDispatchThread();
      logger.fine("Event Dispatch Thread = " + isEventDispatchThread );

      if( isEventDispatchThread )
      {
        if( index >= 0 )
          imageJList.ensureIndexIsVisible( index );

        if( (slideshowMode == RUN_SLIDESHOW) && 
            (index >= 0) && (index < imageQueue.size()) )
        {
          logger.fine(" Displaying slide index " + index );

          startImagePlayback( AVTransport.PLAY_MODE_DIRECT_1 );
        
          // Change cursor to the wait cursor until the play thread is
          // no longer busy, or timeout of 5 sec is reached
          int checkPeriodMs = 200;
          int timeoutMs = 5000;
          Thread waitCursorThread =
            new Thread( new WaitCursorThread( checkPeriodMs, timeoutMs ) );
          waitCursorThread.start();
        }
      }
      else
      {
        // selection was changed by playback thread. Need to update the GUI 
        // asynchronously in this case
        JListAsyncUtil.ensureIndexIsVisible( imageJList, index );
      }
    }
  }

  class WaitCursorThread implements Runnable
  {
    int periodMs;
    int timeoutMs;
    
    public WaitCursorThread( int periodMs, int timeoutMs )
    {
      this.periodMs = periodMs;
      this.timeoutMs = timeoutMs;
    }
    
    public void run()
    {
      // Set the cursor to wait cusor using invokeLater
      MySwingUtil.setCursorAsync( imageJList, 
                       Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) );

      // Give playback a bit of time to get going
      MrUtil.sleep( 1000 );

      for( int waitMs = 0 ; waitMs < timeoutMs; waitMs += periodMs ) 
      {
        if( ! mediaRenderer.getImagePlayThread().isBusy() )
          break;
        MrUtil.sleep( periodMs );
      }

      // Restore cursor
      MySwingUtil.setCursorAsync( imageJList, 
                                  Cursor.getDefaultCursor() );  
    }
  }
  

  /*
  class ImageQueueMouseListener extends MouseAdapter
  {
    public void mouseClicked( MouseEvent e )
    {
      int buttonId = e.getButton();

      System.out.println(" mouseClicked 2 -count " + e.getClickCount() );

      int index = imageJList.locationToIndex( e.getPoint() );

      if( (slideshowMode == RUN_SLIDESHOW) && 
          (e.getClickCount() == 1) )
      {
        System.out.println(" Displaying slide index " + index );

        // If image double-clicked, 'play' the single image and then
        // stop

        if( index < imageQueue.size() )
        {
          imageQueue.setCurrentPosition( index );
        
          if( mediaRenderer != null )   
          {
            //mediaRenderer.actionStop();  // No-op if not running
            mediaRenderer.startPlayThread();
          }
        }
      }
    }
  }
  */

  public int getSlideshowMode() {
    return slideshowMode;
  }

  public void setSlideshowMode( int mode )
  {
    if( mode == slideshowMode )
      return;
    
    slideshowMode = mode;

    RendererControlPanel controlPanel = 
      mediaRenderer.getRendererControlWindow().getControlPanel();

    if( mode == COMPOSE_SLIDESHOW )
    {
      slideshowIntSpinner.setEnabled(false);
      intLabel.setEnabled(false);
      deleteButton.setEnabled(true);
      clearButton.setEnabled(true);
      viewerHelper.setDragEnabled(true);
      controlPanel.disableAllTransportControls();
    }
    else if( mode == RUN_SLIDESHOW )
    {
      slideshowIntSpinner.setEnabled(true);
      intLabel.setEnabled(true);
      deleteButton.setEnabled(false);
      clearButton.setEnabled(false);
      viewerHelper.setDragEnabled(false);
      controlPanel.enableAllTransportControls();
    }
  }

  /**
   *  Listener for change in the 'active item'. The active item corresponds
   *  to the 'now playing' item in the list, which may be separate from
   *  the selected item in the Jlist.  
   *
   *  Notes:  This is a AudioQueue-specific custom listener, not 
   *  supported by the JList class 
   */
  class ImageListActiveItemListener implements ActiveItemListener
  {
    public void activeItemChanged( int activeItemIndex )
    {
      // shift the JList view to keep active item visible
      logger.fine("Shifting JList view to item: " + activeItemIndex );

      // Has problems - needs to be put on Swing event queue
      //      if( activeItemIndex >= 0 && playQueue.size() > 0 )
      //        playQueueJList.ensureIndexIsVisible( activeItemIndex );
    }
  }
  
  public class DeleteActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      imageQueue.remove( imageJList.getSelectedIndices() );
      imageQueue.fireAllContentsChanged();
    }
  }

  public class ClearActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      imageQueue.clear();
    }
  }

  public class ModeActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      JComboBox cb = (JComboBox)e.getSource();
      String mode = (String)cb.getSelectedItem();
      logger.fine("Slideshow Mode:" + mode );

      if( mode.equals("Compose Slideshow") )
        setSlideshowMode( COMPOSE_SLIDESHOW );
      else if( mode.equals("Run Slideshow") )
        setSlideshowMode( RUN_SLIDESHOW );

      imageJList.clearSelection();
    }
  }

}

