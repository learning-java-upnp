/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: AboutDialog.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.Vector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;


/**
 *  Help->About dialog
 */
public class AboutDialog extends JDialog
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaController controller;
  AppPreferences pref;

  JLabel aboutLabel = new JLabel("<html>Cidero UPnP Media Controller<br>Version 1.5.3 - 4/24/2007</html>");

  /** 
   * Dialog is a singleton
   */
  static AboutDialog dialog = null;
  
  public static AboutDialog getInstance()
  {
    if( dialog == null )
      dialog = new AboutDialog( MediaController.getInstance() );

    return dialog;
  }

  /** 
   * Constructor
   */
  private AboutDialog( MediaController controller )
  {
    super( controller.getFrame(), "About", false );

    this.controller = controller;
    pref = controller.getPreferences();

    // Set position to right-hand side of parent debug window (more or less)
    //setLocationRelativeTo( controller.getFrame() );
    Container cp = getContentPane();
    cp.setLayout( new BorderLayout() );
    
    cp.add( aboutLabel, BorderLayout.CENTER );

    pack();
    setVisible( true );
  }

}

  
