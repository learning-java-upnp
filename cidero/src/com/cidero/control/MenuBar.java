/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: MenuBar.java,v $
 *
 */

package com.cidero.control;

import java.awt.event.*;
import javax.swing.*;

/**
 * Main menubar for media controller
 */
public class MenuBar extends JMenuBar
{
  MediaController controller;

  JMenu     fileMenu;
  JMenuItem saveSetupItem;
  JMenuItem quitItem;
  JMenuItem searchAVServerItem;
  JMenuItem searchAVRendererItem;
  JMenuItem searchRootItem;
  JMenuItem searchAllItem;
  JMenuItem stopItem;
  JMenuItem restartItem;

  JMenu     optionMenu;
  JMenuItem browseOptionsItem;
  JMenuItem serverOptionsItem;
  JMenuItem rendererOptionsItem;
  JMenuItem syncOptionsItem;
  JMenuItem upnpOptionsItem;
  JMenuItem playlistOptionsItem;
  JMenuItem colorOptionsItem;
  JMenuItem debugWindowItem;
  JMenuItem loggingOptionsItem;
  JMenuItem deviceInfoItem;
  
  JMenu     helpMenu;
  JMenuItem aboutItem;

  public MenuBar( MediaController controller )
  {
    this.controller = controller;
    
    //------------------------------------------------------------------
    fileMenu = new JMenu("File");
    add(fileMenu);

    saveSetupItem = new JMenuItem("Save setup");
    saveSetupItem.addActionListener( new SaveSetupActionListener() );
    fileMenu.add( saveSetupItem );

    fileMenu.addSeparator();

    searchAVServerItem = new JMenuItem("Search for UPnP A/V Servers");
    searchAVServerItem.addActionListener( new SearchActionListener(
      "urn:schemas-upnp-org:device:MediaServer:1") );
    fileMenu.add( searchAVServerItem );

    searchAVRendererItem = new JMenuItem("Search for UPnP A/V Renderers");
    searchAVRendererItem.addActionListener( new SearchActionListener(
      "urn:schemas-upnp-org:device:MediaRenderer:1") );
    fileMenu.add( searchAVRendererItem );

    searchRootItem = new JMenuItem("Search for UPnP root Devices");
    searchRootItem.addActionListener( new SearchActionListener(
      "upnp:rootdevice") );
    fileMenu.add( searchRootItem );

    searchAllItem = new JMenuItem("Search for all UPnP Devices");
    searchAllItem.addActionListener( new SearchActionListener(
      "ssdp:all") );
    fileMenu.add( searchAllItem );

    fileMenu.addSeparator();

    stopItem = new JMenuItem("Stop Controller");
    stopItem.addActionListener( new StopActionListener() );
    fileMenu.add( stopItem );

    restartItem = new JMenuItem("Restart Controller");
    restartItem.addActionListener( new RestartActionListener() );
    fileMenu.add( restartItem );

    fileMenu.addSeparator();

    quitItem = new JMenuItem("Quit");
    quitItem.addActionListener( new QuitActionListener() );
    fileMenu.add(quitItem);

    //------------------------------------------------------------------
    optionMenu = new JMenu("Options");
    add( optionMenu );

    browseOptionsItem = new JMenuItem("Browse Options...");
    browseOptionsItem.addActionListener( new BrowseOptionsActionListener() );
    optionMenu.add( browseOptionsItem );

    serverOptionsItem = new JMenuItem("Server Options...");
    serverOptionsItem.addActionListener( new ServerOptionsActionListener() );
    optionMenu.add( serverOptionsItem );

    rendererOptionsItem = new JMenuItem("Renderer Options...");
    rendererOptionsItem.addActionListener( new RendererOptionsActionListener() );
    optionMenu.add( rendererOptionsItem );


    playlistOptionsItem = new JMenuItem("Playlist Options...");
    playlistOptionsItem.addActionListener( new PlaylistOptionsActionListener() );
    optionMenu.add( playlistOptionsItem );


    syncOptionsItem = new JMenuItem("Synchronization Options...");
    syncOptionsItem.addActionListener( new SyncOptionsActionListener() );
    optionMenu.add( syncOptionsItem );

    deviceInfoItem = new JMenuItem("Device Info/Config...");
    deviceInfoItem.addActionListener( new DeviceInfoActionListener() );
    optionMenu.add( deviceInfoItem );

    upnpOptionsItem = new JMenuItem("UPnP Options...");
    upnpOptionsItem.addActionListener( new UPnPOptionsActionListener() );
    optionMenu.add( upnpOptionsItem );

    //colorOptionsItem = new JMenuItem("Color Options...");
    //colorOptionsItem.addActionListener( new ColorOptionsActionListener() );
    //optionMenu.add( colorOptionsItem );

    debugWindowItem = new JMenuItem("Debug Window...");
    debugWindowItem.addActionListener( new DebugWindowActionListener() );
    optionMenu.add( debugWindowItem );


    loggingOptionsItem = new JMenuItem("Logging Options...");
    loggingOptionsItem.addActionListener( new LoggingOptionsActionListener() );
    optionMenu.add( loggingOptionsItem );

    helpMenu = new JMenu("Help");
    // Not yet implemented
    //    setHelpMenu( helpMenu );
    add( helpMenu );
    aboutItem = new JMenuItem("About");
    aboutItem.addActionListener( new AboutActionListener() );
    helpMenu.add( aboutItem );
  }

  public class QuitActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      controller.close();
    }
  }

  public class SaveSetupActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      controller.savePreferences();
    }
  }
  
  public class StopActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      controller.stop();
    }
  }

  public class RestartActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      controller.start();
    }
  }

  public class SearchActionListener implements ActionListener
  {
    String searchTarget;

    public SearchActionListener( String searchTarget )
    {
      this.searchTarget = searchTarget;
    }

    public void actionPerformed( ActionEvent e )
    {
      controller.search( searchTarget );
    }
  }

  /**
   *  Button action listeners
   */
  public class DebugWindowActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      DebugWindow.getInstance().setVisible(true);
    }
  }

  public class LoggingOptionsActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      LoggingOptionsDialog.getInstance().setVisible(true);
    }
  }

  public class DeviceInfoActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      DeviceInfoWindow.getInstance().setVisible(true);
    }
  }

  public class BrowseOptionsActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      BrowseOptionsDialog.getInstance().setVisible(true);
    }
  }

  public class ServerOptionsActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      ServerOptionsDialog.getInstance().setVisible(true);
    }
  }

  public class RendererOptionsActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      RendererOptionsDialog.getInstance().setVisible(true);
    }
  }

  public class PlaylistOptionsActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      PlaylistOptionsDialog.getInstance().setVisible(true);
    }
  }

  public class SyncOptionsActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      SyncOptionsDialog.getInstance().setVisible(true);
    }
  }

  public class UPnPOptionsActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      UPnPOptionsDialog.getInstance().setVisible(true);
    }
  }

  public class ColorOptionsActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      ColorOptionsDialog.getInstance().setVisible(true);
    }
  }

  public class AboutActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      AboutDialog.getInstance().setVisible(true);
    }
  }

}
