/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugObj.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.text.NumberFormat;

import org.cybergarage.upnp.UPnPStatus;

import com.cidero.swing.text.XMLStyledDocument;

abstract class DebugObj
{
  private final static Logger logger = 
       Logger.getLogger("com.cidero.control");

  final static int STATUS_OK = 0;
  final static int STATUS_ERROR = -1;
  final static int STATUS_WARNING = -2;

  
  MediaController mediaController;
  long timeStampMillis;
  int  status;
  String timeString;    // HH:MM:SS.MMM format


  public DebugObj()
  {
    // Get curr timestamp and make a string out of it up front for
    // efficiency

    GregorianCalendar cal = new GregorianCalendar();
    int hour =     cal.get(Calendar.HOUR);
    int min =      cal.get(Calendar.MINUTE);
    int sec =      cal.get(Calendar.SECOND);
    int milliSec = cal.get(Calendar.MILLISECOND);
    
    NumberFormat nf2 = NumberFormat.getInstance();
    nf2.setMinimumIntegerDigits(2);
    nf2.setGroupingUsed(false);

    NumberFormat nf3 = NumberFormat.getInstance();
    nf3.setMinimumIntegerDigits(3);
    nf3.setGroupingUsed(false);

    timeString = nf2.format(hour) + ":" + nf2.format(min) + ":" +
        nf2.format(sec) + "." + nf3.format(milliSec);
  }

  /*
  public void setTimeStamp( long timeMillis )
  {
    timeStampMillis = timeMillis;
  }
  public long getTimeStamp()
  {
    return timeStampMillis;
  }
  */

  public String getTimeString()
  {
    return timeString;
  }

  public void setStatus( int status )
  {
    this.status = status;
  }
  public int getStatus()
  {
    return status;
  }

  /**
   * Return a string representation of the UPnP status. Note that if
   * there was no error, the UPnP package routine to get status is 
   * never invoked, and the status is set to 0 locally.  So generate
   * an OK here for that case.
   */
  public String getStatusString()
  {
    if( status == STATUS_OK )
      return "OK";
    else if( status == STATUS_ERROR )
      return "ERROR";
    else if( status == STATUS_WARNING )
      return "WARNING";
    else
      return "UNKNOWN";
  }
  
  //--------------------------------------------------------------
  // Filtering methods
  //--------------------------------------------------------------

  static boolean enabled = true;
  static boolean autoFormatXML = true;

  // These not yet really implemented - TODO
  static boolean mediaServerDevicesEnabled = true;
  static boolean mediaRendererDevicesEnabled = true;
  static boolean otherDevicesEnabled = true;

  public static void setMediaServerDevicesEnabled( boolean flag )
  {
    mediaServerDevicesEnabled = flag;
  }
  public static boolean getMediaServerDevicesEnabled()
  {
    return mediaServerDevicesEnabled;
  }

  public static void setMediaRendererDevicesEnabled( boolean flag )
  {
    mediaRendererDevicesEnabled = flag;
  }
  public static boolean getMediaRendererDevicesEnabled()
  {
    return mediaRendererDevicesEnabled;
  }

  public static void setOtherDevicesEnabled( boolean flag )
  {
    otherDevicesEnabled = flag;
  }
  public static boolean getOtherDevicesEnabled()
  {
    return otherDevicesEnabled;
  }

  /**
   * Global enable setting for all debug messages
   */
  public static void setEnabled( boolean flag )
  {
    enabled = flag;
  }
  public static boolean getEnabled()
  {
    return enabled;
  }

  public static void setAutoFormatXML( boolean flag )
  {
    autoFormatXML = flag;
  }
  public static boolean getAutoFormatXML()
  {
    return autoFormatXML;
  }


  abstract public String toSingleLineString();
  abstract public String toString();
  abstract public boolean isDisplayable();

  // Append to document, specifying whether to auto-format XML fragments
  abstract public void append( XMLStyledDocument doc, boolean autoFormatXML );

  // Append to document using current global setting for autoFormatXML flag
  public void append( XMLStyledDocument doc )
  {
    append( doc, autoFormatXML );
  }
  

}

 

