/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugSearchResponse.java,v $
 *
 */

package com.cidero.control;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.cybergarage.upnp.Action;
import org.cybergarage.upnp.Argument;
import org.cybergarage.upnp.ArgumentList;
import org.cybergarage.upnp.ssdp.SSDPPacket;

import com.cidero.swing.text.XMLStyledDocument;

/**
 *  Debug container for incoming search responses.
 *  
 *  Search responses are HTTP responses to multicast search requests. An
 *  example of the syntax is:
 *
 *    HTTP/1.1 200 OK
 *    CACHE-CONTROL: max-age=1801
 *    DATE: Sun, 12 Dec 2004 22:44:40 GMT
 *    EXT:
 *    LOCATION: http://192.168.1.100:49152/description.xml
 *    SERVER: Microsoft Windows/5.1, UPnP/1.0, Intel SDK for UPnP devices /1.2
 *    ST: urn:schemas-upnp-org:device:MediaServer:1
 *    USN: uuid:MUSICMATCHMediaServer:onrush::urn:schemas-upnp-org:device:MediaServer:1
 *
 *
 */
class DebugSearchResponse extends DebugObj
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  String    ssdpPacket;      // Entire SSDP packet as string
  
  // ----------- Required fields for incoming response -----------
  String    usn;
  String    location;        // Location of XML device description (URL)
  String    server;
  String    st;              // Search target - this is an echo back of 
                             // the ST field in the request
  String    cacheControl;    // Device timeout. Device will send ALIVE
                             // message before timeout expires, or control
                             // point should remove device from its list.

  // ---------- Recommended fields - may not be present ---------
  String    date;

  // -- Local variables
  String    device;
  
  
  /**
   * Constructor
   */
  public DebugSearchResponse( SSDPPacket packet )
  {
    ssdpPacket = packet.toString();
    
    usn = packet.getUSN();
    st = packet.getST();
    location = packet.getLocation();

    // Parse the device type from the USN for convenience
    /*
    int index = usn.indexOf("device:");
    if( index < 0 )
    {
      logger.warning("Syntax error in search response USN field: " + usn );
      device = "Unknown device type";
    }
    else
    {
      device = usn.substring( index+7 );
    }
    */    
    setStatus( DebugObj.STATUS_OK );

  }
    
  /**
   * Single line string represention. Used in top pane of debug window
   */
  public String toSingleLineString()
  {
    return getTimeString() + " SearchResponse: USN: " + usn;
  }
  
  /**
   * Full (may be multiple lines) string represention. Used in bottom
   * pane of debug window
   */
  public String toString()
  {
    return ssdpPacket;
  }

  /** 
   *  Append a text representation of the object to an XML-capable
   *  StyledDocument class.
   *
   * @param doc          XMLStyledDocument instance (normally displayed
   *                     in JTextPane)
   * @param autoFormat   Enable XML auto-formatting. Setting this to
   *                     false disables all the special XML-sensitive logic
   */ 
  public void append( XMLStyledDocument doc, boolean autoFormat )
  {
    doc.appendString("SearchResponse Raw SSDP Packet:\n\n" + ssdpPacket );
  }

  public boolean isDisplayable()
  {
    return true;
  }

}

 

