/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugWindow.java,v $
 *
 */

package com.cidero.control;

import java.awt.Point;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.logging.Logger;
import java.util.Vector;

import javax.swing.JFrame;


/**
 *  Standalone window version of renderer control panel. Useful if 
 *  user wants free-floating control panels for multiple renderers
 */
public class DebugWindow extends JFrame implements WindowListener
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  DebugPanel      debugPanel = null;
  MediaController controller;
  
  /** 
   * Debug window is a singleton
   */
  static DebugWindow debugWindow = null;
  
  public static DebugWindow getInstance()
  {
    if( debugWindow == null )
    {
      debugWindow = new DebugWindow( MediaController.getInstance() );
    }
    return debugWindow;
  }

  /**
   *
   */
  public DebugWindow( MediaController mediaController )
  {
    super( "UPnP A/V Controller Debug Monitor" );

    this.controller = mediaController;
    
    // Position debug window just below parent. Use parents width
    Point location = mediaController.getFrame().getLocation();
    Dimension size = mediaController.getFrame().getSize();
    
    setBounds( location.x, location.y+size.height,
               size.width, 300 );
    addWindowListener( this );
    
    // Menu bar 
    //DebugMenuBar menuBar = new DebugMenuBar( this );
    //setJMenuBar( menuBar );

    Container cp = getContentPane();

    debugPanel = new DebugPanel( mediaController );
    
    cp.add( debugPanel, BorderLayout.CENTER ); 

    setVisible( true );
  }

  public DebugPanel getDebugPanel()
  {
    return debugPanel;
  }
  public MediaController getParentController()
  {
    return controller;
  }

  /**
   * Add a debug object to the debug window
   */
  public void addDebugObj( DebugObj debugObj )
  {
    controller.addDebugObj( debugObj );
  }

  public void windowClosing(WindowEvent e) {
    //System.out.println("WindowListener method called: windowClosing.");
    // TODO: May want to shut down debugging when window not up
  }

  public void windowClosed(WindowEvent e) {
    //System.out.println("WindowListener method called: windowClosed.");
  }

  public void windowOpened(WindowEvent e) {
    //System.out.println("WindowListener method called: windowOpened.");
  }

  public void windowIconified(WindowEvent e) {
    //System.out.println("WindowListener method called: windowIconified.");
  }

  public void windowDeiconified(WindowEvent e) {
    //System.out.println("WindowListener method called: windowDeiconified.");
  }

  public void windowActivated(WindowEvent e) {
    //System.out.println("WindowListener method called: windowActivated.");
  }

  public void windowDeactivated(WindowEvent e) {
    //System.out.println("WindowListener method called: windowDeactivated.");
  }

}

  
