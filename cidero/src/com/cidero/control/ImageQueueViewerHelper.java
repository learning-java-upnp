/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ImageQueueViewerHelper.java,v $
 *
 */

package com.cidero.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.cidero.upnp.*;

/**
 * Helper class for viewing image lists. Includes custom CellRenderer and
 * mouse listeners to enable simple sorting via mouse drags.
 */
public class ImageQueueViewerHelper extends ImageListSortableThumb
                                    implements ListCellRenderer,
                                               MouseListener,
                                               MouseMotionListener
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  JList imageJList;
  
  boolean dragEnabled = true;
  int startDragIndex = -1;
  int endDragIndex = -1;

  public ImageQueueViewerHelper( JList jList )
  {
    imageJList = jList;
  }

  public void setDragEnabled( boolean val ) {
    dragEnabled = val;
  }
  public boolean getDragEnabled( ) {
    return dragEnabled;
  }

  public Component getListCellRendererComponent(
       JList list,
       Object value,            // value to display
       int index,               // cell index
       boolean isSelected,      // is the cell selected
       boolean cellHasFocus)    // the list and the cell have the focus
  {
    ImageQueueElem elem = (ImageQueueElem)value;
    CDSImageItem obj = (CDSImageItem)elem.getCDSObject();
    if( obj == null )
    {
      setLabelText( "" );
      setLabelIcon( null );
      setLabelBorder( null );
    }
    else
    {
      setLabelForeground( list.getForeground() );

      String title = obj.getTitle();
      String lowerCaseTitle = title.toLowerCase();

      // Use date for icon label if title is blank or just the JPEG filename
      /*
      if( obj.getDate() != null )
      {
        setLabelText( "<html>" + obj.getTitle() + "<br>" 
                      + obj.getDate() + "</html>" );
      }
      else
      {
      */
        setLabelText( obj.getTitle() );
        /*}*/
      
      ImageIcon icon = elem.getImageIcon();
      //      System.out.println("cellRenderer: index = " + index + 
      //                         " sel " + isSelected );
      if( icon == null )
        logger.warning("NULL image ICON!!");

      setLabelIcon( icon );

      // If dragging, leave selection border at start of drag, otherwise
      // follow JList selection flag
      if( dragOpUnderway() )
      {
        if( index == startDragIndex )
          setSelected( true );
        else
          setSelected( false );
      }
      else
      {
        setSelected( isSelected );
      }
    }
      
    if( dragOpUnderway() && cellHasFocus )
      setSeparatorBackground( Color.blue );
    else
      setSeparatorBackground( list.getBackground() );

    setBackground( list.getBackground() );
    setLabelBackground( list.getBackground() );
    setLabelEnabled( list.isEnabled() );
    setLabelFont( list.getFont() );

    return this;
  }

  /**
   *  mouseClicked = pressed + released
   *
   *  Note: Mac OSX seems to generate only the mouseReleased, not 
   *  mouseClicked...code moved there
   */
  public void mouseClicked( MouseEvent e )
  {
    logger.fine("mouseClicked: Entered");
  }

  public void mousePressed( MouseEvent e )
  {
    if( !dragEnabled )
      return;

    int buttonId = e.getButton();
    
    // Set button as selected (blue background)
    //thumbButton.setBackground( new Color( 0, 0, 192 ) );

    if( buttonId == MouseEvent.BUTTON1 )
    {
      logger.fine("Button1 pressed");
      startDragIndex = -1;
    }
    else if( buttonId == MouseEvent.BUTTON2 )
      logger.fine("Button2 pressed");
    else if( buttonId == MouseEvent.BUTTON3 )
      logger.fine("Button3 pressed");

    // Set button as selected (blue background)
    //thumbButton.setBackground( new Color( 0, 0, 192 ) );

    logger.fine("Click count = " + e.getClickCount() +
                " Shift = " + e.isShiftDown() );
  }

  public void mouseDragged( MouseEvent e )
  {
    //logger.info("mouseDragged: Entered");

    if( !dragEnabled )
      return;
    
    if( startDragIndex == -1 )
    {
      int index = imageJList.locationToIndex( e.getPoint() );

      // Don't drag dummy end element (used for display of insertion
      // point at end of item list)

      ImageQueue model = (ImageQueue)imageJList.getModel();
      if( index != (model.getSize()-1) )
      {
        startDragIndex = index;
        imageJList.setCursor( Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR) );
      }
      else
      {
        return;  
      }
    }
      
    endDragIndex = imageJList.locationToIndex( e.getPoint() );

    //System.out.println("Mouse dragged from index: " + startDragIndex +
    //                         " to: " + endDragIndex );
  }

  public void mouseMoved( MouseEvent e )
  {
    //System.out.println("Mouse moved " );
  }

  public void mouseReleased( MouseEvent e )
  {
    logger.fine("mouseReleased: Entered");

    if( !dragEnabled )
      return;

    int buttonId = e.getButton();
    

    // Set button as selected (blue background)
    //thumbButton.setBackground( new Color( 0, 0, 192 ) );

    if( buttonId == MouseEvent.BUTTON1 )
    {
      logger.fine("Button1 clicked, startDrag = " + startDragIndex +
                  " endDrag = " + endDragIndex );

      //
      // If item was dragged during button press/release, re-order the
      // list model
      //

      ImageQueue model = (ImageQueue)imageJList.getModel();

      if( (startDragIndex != -1) && (endDragIndex != startDragIndex) &&
          (startDragIndex != (model.getSize()-1)) )
      {
        logger.fine("Moving image from index " + startDragIndex +
                    "to " + endDragIndex );

        ImageQueueElem elem = model.remove( startDragIndex );
        if( startDragIndex > endDragIndex )
          model.insertElementAt( elem, endDragIndex );
        else
          model.insertElementAt( elem, endDragIndex-1 );
        model.fireAllContentsChanged();
      }
      startDragIndex = -1;
      imageJList.setCursor( Cursor.getDefaultCursor() );
    }
    else if( buttonId == MouseEvent.BUTTON2 )
      logger.fine("Button2 clicked");
    else if( buttonId == MouseEvent.BUTTON3 )
      logger.fine("Button3 clicked");
    else 
      logger.fine("No button pressed ?? ");

    // Set button as selected (blue background)
    //thumbButton.setBackground( new Color( 0, 0, 192 ) );

    logger.fine("Click count = " + e.getClickCount() +
                " Shift = " + e.isShiftDown() );
  }
  public void mouseExited( MouseEvent e )
  {
  }
  public void mouseEntered( MouseEvent e )
  {
  }
  
  public boolean dragOpUnderway()
  {
    if( !dragEnabled )
      return false;

    return ( startDragIndex != -1 );
  }


}

