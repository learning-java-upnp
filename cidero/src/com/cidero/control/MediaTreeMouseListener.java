/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: MediaTreeMouseListener.java,v $
 *
 */
package com.cidero.control;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Logger;

import javax.swing.tree.TreePath;

/**
 *
 * Note: MouseAdaptor class is convenience class that implements
 * null versions of all MouseListener methods so one can define
 * only the events of interest
 */
public class MediaTreeMouseListener extends MouseAdapter
{
  private final static Logger logger = 
       Logger.getLogger("com.cidero.control");

  MediaTreePanel treePanel;
  
  
  public MediaTreeMouseListener( MediaTreePanel treePanel )
  {
    this.treePanel = treePanel;
  }

  public void mousePressed( MouseEvent e )
  {
    int buttonId = e.getButton();
    
    if( buttonId == MouseEvent.BUTTON1 )
      logger.fine("Button1 pressed");
    else if( buttonId == MouseEvent.BUTTON2 )
      logger.fine("Button2 pressed");
    else if( buttonId == MouseEvent.BUTTON3 )
      logger.fine("Button3 pressed");
    else 
      logger.fine("No button pressed ?? ");

    int selRow = treePanel.getTree().getRowForLocation( e.getX(), e.getY() );
    TreePath selPath = treePanel.getTree().getPathForLocation( e.getX(), e.getY() );

    if( selRow != -1 )
    {
      if( e.getClickCount() == 1 )
      {
        singleClick( buttonId, selRow, selPath );
      }
      else if( e.getClickCount() == 2 )
      {
        doubleClick( buttonId, selRow, selPath );
      }
    }
  }

  public void singleClick( int buttonId, int selRow, TreePath selPath )
  {
    logger.fine("singleClick: row = " + selRow +
                       " path = " + selPath.toString() );

    if( buttonId == 1 ) 
    {
      // get object at this node and print it out  (TODO)

      logger.fine("Setting lastClicked to TREE_PANEL" );
      treePanel.getMediaBrowserPanel().
        setLastMouseClickPanel( MediaBrowserPanel.MEDIA_TREE_PANEL );

    }

    // Update the tree view, using the expandPath flag so the node is 
    // expanded if it isn't already
    treePanel.updateTreeView( selPath, true );
  }

  /**
   *  If double-click, display all child items of selected node in the 
   *  item panel
   */
  public void doubleClick( int buttonId, int selRow, TreePath selPath )
  {
    logger.fine("doubleClick: row = " + selRow +
                       " path = " + selPath.toString() );

  }

}

