/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: PlayQueueElem.java,v $
 *
 */
package com.cidero.control;
import java.util.logging.Logger;

import com.cidero.upnp.CDSObject;

/**
 *  Simple class to allow addition of a few dynamic playlist specific 
 *  tags to a CDSObject
 */
public class PlayQueueElem
{
  CDSObject obj;
  int       membershipType = PlayQueue.MEMBERSHIP_PERMANENT;
  int       priority = PlayQueue.PRIORITY_NORMAL;
  boolean   busy;
    
  /**
   * Construct a play queue item with the default PERMANENT membership
   * attribute set
   */
  public PlayQueueElem( CDSObject obj )
  {
    this.obj = obj;
  }

  public CDSObject getCDSObject() {
    return obj;
  }

  public void setMembershipType( int membershipType ) {
    this.membershipType = membershipType;
  }
  public int getMembershipType() {
    return membershipType;
  }

  public void setPriority( int priority ) {
    this.priority = priority;
  }
  public int getPriority() {
    return priority;
  }

  public void setBusy( boolean busy ) {
    this.busy = busy;
  }
  public boolean isBusy() {
    return busy;
  }
}
  
