/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugMenuBar.java,v $
 *
 */

package com.cidero.control;

import java.awt.event.*;
import javax.swing.*;

/**
 * Menubar class for use with DebugWindow
 */
public class DebugMenuBar extends JMenuBar
{
  DebugWindow debugWindow;
  DebugPanel  debugPanel;
  
	JMenu fileMenu;
	JMenu optionsMenu;

	JMenuItem configItem;
	JMenuItem filterItem;
	JMenuItem testItem;

	JMenuItem saveAsItem;
	JMenuItem testHelpItem;

	public DebugMenuBar( DebugWindow window )
  {
		this.debugWindow = window;
    this.debugPanel = window.getDebugPanel();  // shorthand reference
		
    //--------------------------------------------------------------

		fileMenu = new JMenu("File");
		add(fileMenu);
		saveAsItem = new JMenuItem("Save As");
		saveAsItem.addActionListener( new SaveAsActionListener() );
		fileMenu.add( saveAsItem );

    //--------------------------------------------------------------

		optionsMenu = new JMenu("Options");
    add(optionsMenu);

		configItem = new JMenuItem("Basic Configuration...");
		configItem.addActionListener( new ConfigActionListener() );
		optionsMenu.add( configItem );

    //optionsMenu.addSeparator();

		filterItem = new JMenuItem("Filtering...");
		filterItem.addActionListener( new FilterActionListener() );
		optionsMenu.add( filterItem );

    /*
    optionsMenu.addSeparator();
		testItem = new JMenuItem("UPnP Compliance Testing...");
		testItem.addActionListener( new TestActionListener() );
		optionsMenu.add( testItem );
    */

    //--------------------------------------------------------------


	}

  public class SaveAsActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
			System.out.println("Saving debug session (TODO - Not yet impl)");
    }
  }

  public class ConfigActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      DebugConfigDialog.getInstance().setVisible(true);
    }
  }

  public class FilterActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      DebugFilterDialog.getInstance().setVisible(true);
    }
  }

  public class TestActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
    }
  }

  public class AboutActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
    }
  }

}
