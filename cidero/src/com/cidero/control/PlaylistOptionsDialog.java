/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: PlaylistOptionsDialog.java,v $
 *
 */

package com.cidero.control;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.Vector;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;
import com.cidero.util.MrUtil;


/**
 *  Playlist options configuration dialog
 */
public class PlaylistOptionsDialog extends JDialog
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaController controller;
  AppPreferences pref;

  JPanel     savePanel = new JPanel();

  JLabel     xmlDirLabel = new JLabel("XML Playlist Dir");
  JTextField xmlDirText = new JTextField(32);
  JButton    xmlDirBrowse = new JButton("Browse...");
  JLabel    twonkyDbLabel = new JLabel("Twonky Db File");
  JButton    twonkyDbBrowse = new JButton("Browse...");
  JTextField twonkyDbText = new JTextField(32);
  JLabel    m3uDirLabel = new JLabel("M3U Playlist Dir");
  JTextField m3uDirText = new JTextField(32);
  JButton    m3uDirBrowse = new JButton("Browse...");


  JPanel buttonPanel = new JPanel();
  JButton okButton = new JButton("OK");
  JButton applyButton = new JButton("Apply");
  JButton cancelButton = new JButton("Cancel");

  String xmlDirDefault;
  String m3uDirDefault;

  /** 
   * Dialog is a singleton
   */
  static PlaylistOptionsDialog dialog = null;
  
  public static PlaylistOptionsDialog getInstance()
  {
    if( dialog == null )
      dialog = new PlaylistOptionsDialog( MediaController.getInstance() );

    return dialog;
  }

  /** 
   * Constructor
   */
  private PlaylistOptionsDialog( MediaController controller )
  {
    super( controller.getFrame(), "Playlist Options", false );

    this.controller = controller;
    pref = controller.getPreferences();

    String userHome = MrUtil.getUserHomeUnix();
    xmlDirDefault = userHome + "/.cidero/MediaController/playlists/xml";
    m3uDirDefault = userHome + "/.cidero/MediaController/playlists/m3u";

    // Initialize UI components based on preferences
    setUIFromPreferences();
    
    // Set position to right-hand side of parent debug window (more or less)
    setLocationRelativeTo( controller.getFrame() );
    Container cp = getContentPane();

    cp.setLayout( new BorderLayout() );
    
    //--------- Play Queue Save Options ---------

		GridBagConstraints 	gbc = new GridBagConstraints();
    GridBagLayout saveLayout = new GridBagLayout();

    savePanel.setLayout( saveLayout );
    
		savePanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Play Queue Save Options"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    // Make small button borders and add button listeners 

    xmlDirBrowse.addActionListener( new BrowseDirActionListener(xmlDirText) );
    m3uDirBrowse.addActionListener( new BrowseDirActionListener(m3uDirText) );
    twonkyDbBrowse.addActionListener( new BrowseFileActionListener(twonkyDbText) );

    /*
     *  XML save directory fields
     */
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 0.0;
    gbc.weighty = 1.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    saveLayout.setConstraints( xmlDirLabel, gbc );
    savePanel.add( xmlDirLabel );

    gbc.gridx = 1;
    gbc.gridy = 0;
    gbc.weightx = 1.0;
    gbc.fill = GridBagConstraints.BOTH;
    gbc.anchor = GridBagConstraints.WEST;
    saveLayout.setConstraints( xmlDirText, gbc );
    savePanel.add( xmlDirText );

    gbc.gridx = 2;
    gbc.gridy = 0;
    gbc.weightx = 0.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    gbc.insets = new Insets( 0, 4, 0, 4 );
    saveLayout.setConstraints( xmlDirBrowse, gbc );
    savePanel.add( xmlDirBrowse );

    /*
     * M3U file fields
     */
    gbc.gridx = 0;
    gbc.gridy = 1;
    gbc.weightx = 0.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    gbc.insets = new Insets( 0, 0, 0, 0 );
    saveLayout.setConstraints( m3uDirLabel, gbc );
    savePanel.add( m3uDirLabel );

    gbc.gridx = 1;
    gbc.gridy = 1;
    gbc.weightx = 1.0;
    gbc.fill = GridBagConstraints.BOTH;
    gbc.anchor = GridBagConstraints.WEST;
    saveLayout.setConstraints( m3uDirText, gbc );
    savePanel.add( m3uDirText );

    gbc.gridx = 2;
    gbc.gridy = 1;
    gbc.weightx = 0.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    gbc.insets = new Insets( 0, 4, 0, 4 );
    saveLayout.setConstraints( m3uDirBrowse, gbc );
    savePanel.add( m3uDirBrowse );

    /*
     * Twonky Db fields
     */
    gbc.gridx = 0;
    gbc.gridy = 2;
    gbc.weightx = 0.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    gbc.insets = new Insets( 0, 0, 0, 0 );
    saveLayout.setConstraints( twonkyDbLabel, gbc );
    savePanel.add( twonkyDbLabel );

    gbc.gridx = 1;
    gbc.gridy = 2;
    gbc.weightx = 1.0;
    gbc.fill = GridBagConstraints.BOTH;
    gbc.anchor = GridBagConstraints.WEST;
    saveLayout.setConstraints( twonkyDbText, gbc );
    savePanel.add( twonkyDbText );

    gbc.gridx = 2;
    gbc.gridy = 2;
    gbc.weightx = 0.0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.anchor = GridBagConstraints.WEST;
    gbc.insets = new Insets( 0, 4, 0, 4 );
    saveLayout.setConstraints( twonkyDbBrowse, gbc );
    savePanel.add( twonkyDbBrowse );


    cp.add( savePanel, BorderLayout.CENTER );


    //--------------------------------------------------------------
    // 'OK', 'Apply', 'Cancel' button panel at bottom of dialog
    //--------------------------------------------------------------

    okButton.addActionListener( new OkActionListener() );
    applyButton.addActionListener( new ApplyActionListener() );
    cancelButton.addActionListener( new CancelActionListener() );

    buttonPanel.setLayout( new FlowLayout() );
    buttonPanel.add( okButton );
    buttonPanel.add( applyButton );
    buttonPanel.add( cancelButton );
    cp.add( buttonPanel, BorderLayout.SOUTH );
    
    pack();
    setVisible( true );
  }

  /*
  public class BrowseFileFilter extends FileFilter
  {
    public String getDescription()
    {
      return "All Files";
    }

    public boolean accept(File file)
    {
      if (file.isDirectory()) {
        return true;
      }
      return true;
    }
  }
  */

  class BrowseDirActionListener implements ActionListener
  {
    JTextField textField;

    public BrowseDirActionListener( JTextField textField )
    {
      this.textField = textField;
    }

    public void actionPerformed( ActionEvent e )
    {
      JFileChooser fileChooser = new JFileChooser( textField.getText() );
      fileChooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );
      fileChooser.setFileHidingEnabled( false );

      int retVal = fileChooser.showSaveDialog( savePanel );
      if( retVal == JFileChooser.APPROVE_OPTION )
      {
        File file = fileChooser.getSelectedFile();
        try 
        {
          textField.setText( 
                            file.getCanonicalPath().replaceAll("\\\\","/") );
        }
        catch( IOException ex )
        {
          logger.warning("Error getting full path for selected file" + ex );
        }
      }
    }
  }

  class BrowseFileActionListener implements ActionListener
  {
    JTextField textField;

    public BrowseFileActionListener( JTextField textField )
    {
      this.textField = textField;
    }

    public void actionPerformed( ActionEvent e )
    {
      String browseStartFile = textField.getText().trim();
      if( browseStartFile.length() == 0 )
        browseStartFile = System.getProperty("user.home");

      JFileChooser fileChooser = new JFileChooser( browseStartFile );
      fileChooser.setFileHidingEnabled( false );
      //BrowseFileFilter browseFileFilter = new BrowseFileFilter();
      //fileChooser.setFileFilter( browseFileFilter );

      int retVal = fileChooser.showSaveDialog( savePanel );
      if( retVal == JFileChooser.APPROVE_OPTION )
      {
        File file = fileChooser.getSelectedFile();
        try 
        {
          textField.setText( 
                            file.getCanonicalPath().replaceAll("\\\\","/") );
        }
        catch( IOException ex )
        {
          logger.warning("Error getting full path for selected file" + ex );
        }
      }
    }
  }



  /**
   *  Button action listeners
   */
  public class OkActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();  // in-memory copy
      setVisible( false );
    }
  }

  public class ApplyActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();
    }
  }

  public class CancelActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      // Restore settings from preferences
      setUIFromPreferences();
      setVisible( false );
    }
  }
  
  public void setUIFromPreferences()
  {
    String xmlDir = pref.get( "xmlSaveDirectory", "" );
    if( xmlDir.length() == 0 )
      xmlDirText.setText( xmlDirDefault );
    else
      xmlDirText.setText( xmlDir );

    String m3uDir = pref.get( "m3uSaveDirectory", "" );
    if( m3uDir.length() == 0 )
      m3uDirText.setText( m3uDirDefault );
    else
      m3uDirText.setText( m3uDir );

    String twonkyDbFile = pref.get( "twonkyDbFile", "" );
    twonkyDbText.setText( twonkyDbFile );
  }

  public void updatePreferences()
  {
    String xmlDir = xmlDirText.getText().trim();
    if( xmlDir.equals( xmlDirDefault ) )
      pref.put("xmlSaveDirectory", "" );
    else
      pref.put("xmlSaveDirectory", xmlDir );

    String m3uDir = m3uDirText.getText().trim();
    if( m3uDir.equals( m3uDirDefault ) )
      pref.put("m3uSaveDirectory", "" );
    else
      pref.put("m3uSaveDirectory", m3uDir );

    pref.put("twonkyDbFile", twonkyDbText.getText().trim() );

  }
  
}

  
