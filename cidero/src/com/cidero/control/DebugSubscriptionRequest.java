/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: DebugSubscriptionRequest.java,v $
 *
 */

package com.cidero.control;

import java.util.ArrayList;
import java.util.logging.Logger;
import java.net.URL;
import java.net.MalformedURLException;

import org.cybergarage.upnp.Device;
import org.cybergarage.upnp.Service;
import org.cybergarage.upnp.event.*;
import com.cidero.swing.text.XMLStyledDocument;

/**
 *  Debug container for subscription request messages. There are a 
 *  number of types of search requests. Examples below:
 *
 *  1.  Initial subscription request
 *
 *      SUBSCRIBE <eventURL> HTTP/1.1
 *      Host: <publisherHost>:<publisherPort>
 *      Callback: <deliveryURL>
 *      NT: upnp:event
 *      Timeout: Second-<requestedDuration>    (-1 for infinite)
 *
 *      Device responds with:
 *
 *      HTTP/1.1 200 OK
 *      Date: <Date>
 *      Server: OS/version UPnP/1.0 product/version
 *      SID: uuid:<subscriptionId>
 *      Timeout: Second-<actual granted subscription duration>
 *  
 *  2.  Subscription renewal. Control point needs to issue one of these
 *      periodically, in advance of the subscription timeout.
 *      Note that no 'Callback' or 'NT' header is allowed in this message
 *
 *      SUBSCRIBE <eventURL> HTTP/1.1
 *      Host: <publisherHost>:<publisherPort>
 *      SID: uuid:<subscriptionId>
 *      Timeout: Second-<requestedDuration>    (-1 for infinite)
 *
 *  3.  Subscription cancellation:
 *
 *      UNSUBSCRIBE <eventURL> HTTP/1.1
 *      Host: <publisherHost>:<publisherPort>
 *      SID: uuid:<subscriptionId>
 *
 */      
class DebugSubscriptionRequest extends DebugObj
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  SubscriptionRequest  subReq;         // Raw request (HTTP)
  String               friendlyName;   // target device's friendlyName
  String               serviceType;    // target device's service type
  
  /**
   * Constructor
   */
  public DebugSubscriptionRequest( Service service, 
                                   SubscriptionRequest subReq )
  {
    setStatus( DebugObj.STATUS_OK );
    this.subReq = subReq;
    // skip over 1st 29 chars (urn:schemas-upnp-org:service:)
    friendlyName = service.getRootDevice().getFriendlyName();
    serviceType = service.getServiceType().substring(29);
  }
    
  /**
   * Single line string represention. Used in top pane of debug window
   */
  public String toSingleLineString()
  {
    return getTimeString() + " SubscribeRequest: " +
      friendlyName + ":" + serviceType;
  }
  
  /**
   * Full (may be multiple lines) string represention. Used in bottom
   * pane of debug window
   */
  public String toString()
  {
    return subReq.toString();
  }

  /** 
   *  Append a text representation of the object to an XML-capable
   *  StyledDocument class.
   *
   * @param doc          XMLStyledDocument instance (normally displayed
   *                     in JTextPane)
   * @param autoFormat   Enable XML auto-formatting. Setting this to
   *                     false disables all the special XML-sensitive logic
   */ 
  public void append( XMLStyledDocument doc, boolean autoFormat )
  {
    doc.appendString("Subscription Request:\n\n" + subReq.toString() );
  }

  static boolean enabled = true;

  public static void setEnabled( boolean flag )
  {
    enabled = flag;
  }
  public static boolean getEnabled()
  {
    return enabled;
  }

  public boolean isDisplayable()
  {
    return true;
  }

}

 

