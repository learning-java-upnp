/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: AudioItemModel.java,v $
 *
 */

package com.cidero.control;

import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

import com.cidero.upnp.CDSAudioBroadcast;
import com.cidero.upnp.CDSAudioItem;
import com.cidero.upnp.CDSMusicTrack;
import com.cidero.upnp.CDSObject;

/**
 * Audio track model for track table in AudioItem tabbed window
 *
 */
public class AudioItemModel extends AbstractTableModel
{
  private final static Logger logger = 
       Logger.getLogger("com.cidero.control");

  private String[] columnNames =
  { "Artist", "Title", "Time", "Genre", "Trk#" };

  ArrayList trackList;

  public AudioItemModel()
  {
    trackList = new ArrayList();
  }
  
  /* Clear model (remove all track info references) */
  public void clear()
  {
    trackList.clear();
  }
  
  public void add( CDSAudioItem track )
  {
    trackList.add( track );
  }

  public int getColumnCount()
  {
    //    logger.fine("AudioItemModel: getColumnCount: " + 
    //                       columnNames.length);
    return columnNames.length;
  }

  public int getRowCount()
  {
    //    logger.fine("AudioItemModel: getRowCount");
    return trackList.size();
  }

  public String getColumnName( int col )
  {
    return columnNames[col];
  }

  public Object getValueAt( int row, int col )
  {
    CDSAudioItem obj = 
      (CDSAudioItem)trackList.get( row );

    if( obj instanceof CDSMusicTrack )
    {
      CDSMusicTrack track = (CDSMusicTrack)obj;

      switch( col )
      {
        case 0:  // Artist

          // Some CDS implementations use the 'Creator' field instead of
          // the 'Artist' field in the CDSMusicTrack object.
          if( track.getArtist() == null )
            return track.getCreator();
          else
            return track.getArtist();
        case 1:  // Track
          return track.getTitle();
        case 2:  // Time
          return track.getDurationNoMillisec();
        case 3:  // Genre
          return track.getGenre();
        case 4:  // orig track #

          return new Integer( track.getOriginalTrackNum() );

          /*
          if( track.getOriginalTrackNum() <= 0 )
            return "-";
          else
            return Integer.toString(track.getOriginalTrackNum());
          */

        default:
          logger.fine("No obj found for row, col " + row + " " + col );
          return null;
      }
    }
    else if( (obj instanceof CDSAudioBroadcast) ||
             (obj instanceof CDSAudioItem) )
    {
      //CDSAudioBroadcast stream = (CDSAudioBroadcast)obj;
      CDSAudioItem stream = (CDSAudioItem)obj;

      switch( col )
      {
        case 0:  // Artist
          return "";
        case 1:  // Track
          return stream.getTitle();
        case 2:  // Time
          return "   ---  ";
          //return new Integer( trackInfo.getPlayingTime() );
        case 3:  // Genre
          return stream.getGenre();
        case 4:  // orig track #
          return new Integer(0);
          //return "-";

        default:
          logger.fine("No obj found for row, col " + row + " " + col );
          return null;
      }
    }
    else
    {
      logger.fine("Unknown audio item type " );
      return null;
    }
  }

  public CDSObject getObjectAtRow( int row )
  {
    return (CDSObject)trackList.get( row );
  }

  /** 
   * Convenience function that doesn't require 'source' arg
   */ 
  public void fireTableChanged()
  {
    fireTableChanged( new TableModelEvent( this ) );
  }
  
  /*
   * Don't need to implement this method unless your table's
   * data can change.
   */
  //public void setValueAt(Object value, int row, int col) {

  //    logger.fine("Setting value at " + row + "," + col
  //                       + " to " + value
  //                       + " (an instance of "
  //                       + value.getClass() + ")");

  //    data[row][col] = value;
  //    fireTableCellUpdated( row, col );

  //  }

  
}
