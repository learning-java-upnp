/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ImageListSortableThumb.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

/**
 *  Image thumbnail object for use with JLists.  Adds separator at left
 *  hand side of thumbnail JLabel that lets user know where the 'insertion'
 *  point is when shifting images around in a list view (like the PowerPoint
 *  slide sorter interface)
 */
public class ImageListSortableThumb extends JPanel
{
  JSeparator sep;
  JLabel     label;

  public ImageListSortableThumb()
  {
    //setLayout( new BorderLayout() );
    setLayout( new BoxLayout( this, BoxLayout.X_AXIS ) );
    //    setLayout( new FlowLayout(  FlowLayout.CENTER ) );

    //setBackground( Color.green );
    
    sep = new JSeparator( JSeparator.VERTICAL );
    sep.setPreferredSize( new Dimension( 4, 80 ) );
    sep.setBackground( Color.blue );
    sep.setForeground( Color.blue );
    //    add( sep, BorderLayout.WEST );
    add( sep );

    //add( Box.createGlue() );

    label = new JLabel();
    label.setOpaque( true );  // labels not opaque by default
    label.setVerticalTextPosition( JLabel.BOTTOM );
    label.setHorizontalTextPosition( JLabel.CENTER );
    //setBackground( Color.orange );
    add( label );

    add( Box.createGlue() );
    
    setBorder( emptyBorder );
    
  }

  public Dimension getPreferredSize()
  {
    revalidate();
    //    return super.getPreferredSize();
    
    //System.out.println("in getPreferredSize() ");
    Dimension labelDim = super.getPreferredSize();

    return new Dimension( (int)labelDim.getWidth() + 32,
                          (int)labelDim.getHeight() + 16 );
  }
  
  public void setLabelEnabled( boolean enabled )
  {
    label.setEnabled( enabled );
  }
  public void setLabelFont( Font font )
  {
    label.setFont( font );
  }
  public void setLabelBackground( Color bg ) 
  {
    label.setBackground( bg );
  }
  public void setLabelForeground( Color bg ) 
  {
    label.setForeground( bg );
  }
  public void setSeparatorBackground( Color bg )
  {
   sep.setForeground( bg );
   sep.setBackground( bg );
  }
  public void setLabelText( String text )
  {
    label.setText( text );
  }
  public void setLabelIcon( ImageIcon icon )
  {
    label.setIcon( icon );
  }

  static Border emptyBorder = BorderFactory.createEmptyBorder(4,4,4,4);
  static Border lineBorder = BorderFactory.createLineBorder( Color.blue, 4 );
  
  public void setSelected( boolean selected )
  {
    if( selected )
    {
      label.setBorder( lineBorder );
    }
    else
    {
      label.setBorder( emptyBorder );
    }
  }

  public void setLabelBorder( Border border )
  {
    label.setBorder( border );
  }
}

