/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: ColorOptionsDialog.java,v $
 *
 */

package com.cidero.control;

import java.util.logging.Logger;
import java.util.ArrayList;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import com.cidero.util.AppPreferences;

/**
 *  Browse options configuration dialog
 */
public class ColorOptionsDialog extends JDialog
             implements ChangeListener
{
  private final static Logger logger = Logger.getLogger("com.cidero.control");

  MediaController controller;
  AppPreferences appPref;

  JColorChooser chooser = new JColorChooser();
  JDialog       chooserDialog;

  JPanel    rendererPanel = new JPanel();
  JPanel    browserPanel = new JPanel();
  GridBagConstraints 	gbc = new GridBagConstraints();

  JPanel  buttonPanel = new JPanel();
  JButton okButton = new JButton("OK");
  JButton applyButton = new JButton("Apply");
  JButton cancelButton = new JButton("Cancel");

  ArrayList colorPrefList = new ArrayList();

  class ColorPref
  {
    public String  name;
    public JButton button;

    public ColorPref( String name, JButton button )
    {
      this.name = name;
      this.button = button;
    }
    public String getName() {
      return name;
    }
    public JButton getButton() {
      return button;
    }
  }
  
  /** 
   * Dialog is a singleton
   */
  static ColorOptionsDialog dialog = null;
  
  public static ColorOptionsDialog getInstance()
  {
    if( dialog == null )
      dialog = new ColorOptionsDialog( MediaController.getInstance() );

    return dialog;
  }

  /** 
   * Constructor
   */
  private ColorOptionsDialog( MediaController controller )
  {
    super( controller.getFrame(), "Color Options", false );

    this.controller = controller;
    appPref = controller.getPreferences();

    chooser.getSelectionModel().addChangeListener( this );

    // Set position to right-hand side of parent debug window (more or less)
    setLocationRelativeTo( controller.getFrame() );
    Container cp = getContentPane();
    cp.setLayout( new BoxLayout( cp, BoxLayout.Y_AXIS ) );

    
    GridBagLayout  browserLayout = new GridBagLayout();
    browserPanel.setLayout( browserLayout );
    
		browserPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Browser Window Colors"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    addLabelCentered( browserPanel, 1, 0, "Foreground" );
    addLabelCentered( browserPanel, 2, 0, "Background" );

    addLabelWest( browserPanel, 0, 1, "Picture Tab" );
    addFgBgButtonPair( browserPanel,
                       1, 1, "ImageItemPanel.foreground",
                       2, 1, "ImageItemPanel.background" );
    cp.add( browserPanel );


    GridBagLayout rendererLayout = new GridBagLayout();
    rendererPanel.setLayout( rendererLayout );
    
		rendererPanel.setBorder( new CompoundBorder( 
      BorderFactory.createTitledBorder("Renderer Window Colors"),
      BorderFactory.createEmptyBorder(10,10,10,10)) );

    addLabelCentered( rendererPanel, 1, 0, "Foreground" );
    addLabelCentered( rendererPanel, 2, 0, "Background" );

    addLabelWest( rendererPanel, 0, 1, "Picture View" );
    addFgBgButtonPair( rendererPanel,
                       1, 1, "ImageQueuePanel.foreground",
                       2, 1, "ImageQueuePanel.background" );
    cp.add( rendererPanel );
                                       

    //--------------------------------------------------------------
    // 'OK', 'Apply', 'Cancel' button panel at bottom of dialog
    //--------------------------------------------------------------

    okButton.addActionListener( new OkActionListener() );
    applyButton.addActionListener( new ApplyActionListener() );
    cancelButton.addActionListener( new CancelActionListener() );

    buttonPanel.setLayout( new FlowLayout() );
    buttonPanel.add( okButton );
    buttonPanel.add( applyButton );
    buttonPanel.add( cancelButton );
    cp.add( buttonPanel );
    
    pack();
    setVisible( true );
  }


  public void addLabelWest( JPanel panel, int x, int y, String label )
  {
    setCommonGridBagConstraints();
    
    JLabel jLabel = new JLabel( label );
    gbc.gridx = x;
    gbc.gridy = y;
    gbc.anchor = GridBagConstraints.CENTER;
    GridBagLayout gbl = (GridBagLayout)panel.getLayout();
    gbl.setConstraints( jLabel, gbc );
    panel.add( jLabel );
  }

  public void addLabelCentered( JPanel panel, int x, int y, String label )
  {
    setCommonGridBagConstraints();

    JLabel jLabel = new JLabel( label );
    gbc.gridx = x;
    gbc.gridy = y;
    gbc.anchor = GridBagConstraints.CENTER;
    GridBagLayout gbl = (GridBagLayout)panel.getLayout();
    gbl.setConstraints( jLabel, gbc );
    panel.add( jLabel );
  }

  public void addFgBgButtonPair( JPanel panel,
                                 int x1, int y1, String fgColorPrefName,
                                 int x2, int y2, String bgColorPrefName )
  {
    setCommonGridBagConstraints();

    gbc.anchor = GridBagConstraints.CENTER;
    gbc.fill = GridBagConstraints.BOTH;

    JButton fgButton = new JButton();
    fgButton.setBackground( appPref.getColor( fgColorPrefName, Color.black ) );
    fgButton.addActionListener( new ColorButtonListener( fgButton ) );
    gbc.gridx = x1;
    gbc.gridy = y1;
    GridBagLayout gbl = (GridBagLayout)panel.getLayout();
    gbl.setConstraints( fgButton, gbc );
    panel.add( fgButton );
    colorPrefList.add( new ColorPref( fgColorPrefName, fgButton ) );

    JButton bgButton = new JButton();
    bgButton.setBackground( appPref.getColor( bgColorPrefName, Color.white ) );
    bgButton.addActionListener( new ColorButtonListener( bgButton ) );
    gbc.gridx = x2;
    gbc.gridy = y2;
    gbl.setConstraints( bgButton, gbc );
    panel.add( bgButton );
    colorPrefList.add( new ColorPref( bgColorPrefName, bgButton ) );
  }
  
  public void setCommonGridBagConstraints()
  {
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;
    gbc.insets = new Insets( 4, 4, 4, 4 );
    gbc.fill = GridBagConstraints.HORIZONTAL;
  }
  
  JButton selectedButton;
  Color   savedColor;

  public class ColorButtonListener implements ActionListener
  {
    JButton button;
    
    public ColorButtonListener( JButton button ) {
      this.button = button;
    }
                  
    public void actionPerformed( ActionEvent e )
    {
      if( chooserDialog == null )
      {
        //dialog = JColorChooser.createDialog( MyColorChooser.this,
        chooserDialog = JColorChooser.createDialog( null,
                                             "Color Chooser",
                                             false,
                                             chooser,
                                             null,
                                             new ColorChooserCancelListener());
      }

      selectedButton = button;
      savedColor = button.getBackground();

      chooser.setColor( button.getBackground() );
      chooserDialog.setVisible( true );
    }
  }

  public class ColorChooserCancelListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      System.out.println("ColorChooserCancelListener: color = " + 
                         chooser.getColor().toString() );

      // Restore original color of selected button
      if( selectedButton != null )
        selectedButton.setBackground( savedColor );
    }
  }
  
  public void stateChanged(ChangeEvent e)
  {
    System.out.println("stateChanged: color = " + 
                       chooser.getColor().toString() );

    if( selectedButton != null )
      selectedButton.setBackground( chooser.getColor() );
  }

  /**
   *  Button action listeners for buttons at bottom of the Color Options
   *  window (not the chooser dialog)
   */
  public class OkActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();  // update in-memory copy of preferences
      setVisible( false );
    }
  }

  public class ApplyActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      updatePreferences();
    }
  }

  public class CancelActionListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      setVisible( false );
    }
  }
  
  public void updatePreferences()
  {
    for( int n = 0 ; n < colorPrefList.size() ; n++ )
    {
      ColorPref colorPref = (ColorPref)colorPrefList.get(n);
      appPref.putColor( colorPref.name,
                        colorPref.button.getBackground() );
    }
    appPref.fireAppPreferencesChanged();
  }
}

  
