package com.cidero.renderer;

import org.cybergarage.upnp.IDevice;

import com.cidero.bridge.MediaRendererException;
import com.cidero.upnp.AVTransport;
import com.cidero.upnp.ConnectionManager;
import com.cidero.upnp.RenderingControl;

public interface IMediaRenderer extends IDevice {

	// Get the device-specific instances of the UPnP services
	RenderingControl getRenderingControl();

	AVTransport getAVTransport();

	ConnectionManager getConnectionManager();

	// Set of actions needed by playback session (need for version of
	// of these actions that doesn't go via UPnP pathway)
	void avTransportSetTransportURI(String uri) throws MediaRendererException;

	void avTransportPlay(String speed) throws MediaRendererException;

	void avTransportPause() throws MediaRendererException;

	void avTransportStop() throws MediaRendererException;

}
