/*
 *  Copyright (C) 2004 Cidero, Inc.
 *
 *  Permission is hereby granted to any person obtaining a copy of 
 *  this software to use, copy, modify, merge, publish, and distribute
 *  the software for any non-commercial purpose, subject to the
 *  following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY IN CONNECTION WITH THE SOFTWARE.
 * 
 *  File: $RCSfile: AbstractMediaRendererBridge.java,v $
 *
 */

package com.cidero.renderer;

import java.util.logging.Logger;

import org.cybergarage.upnp.Device;
import org.cybergarage.upnp.device.InvalidDescriptionException;

import com.cidero.util.AppPreferences;
import com.cidero.util.MrUtil;

/**
 * Base class for UPnP bridges for non-UPnP-compliant media renderer devices
 * (most commonly music playback devices that support HTTP-GET)
 */
public abstract class AbstractMediaRenderer extends Device
// implements NotifyListener, EventListener
		implements IMediaRenderer {
	
	private static Logger logger = Logger.getLogger("com.cidero.bridge");

	protected static AppPreferences pref;

	/**
	 * Constructor
	 * 
	 * @param description
	 *            file name
	 * 
	 * @exception InvalidDescriptionException
	 *                if there is an error parsing the UPnP XML device
	 *                description
	 */
	public AbstractMediaRenderer(String descriptionFileName,
			String friendlyName) throws InvalidDescriptionException {
		//
		// Invoke constructor of UPNP device superclass. This parses the XML
		// device and service descriptions, and builds in-memory representations
		//
		super(MrUtil.getResourceAsFile(descriptionFileName));

		setFriendlyName(friendlyName);

		//
		// CLink assigns a random UUID derived from the current system time.
		// We want the UUID for a given device to be repeatable from run-to-run
		// so control points will recognize it as the same device if it is
		// shut down and restarted. TODO: Still need to support multiple
		// instances of this proxy somehow (temporarily hardcoded)
		//
		String uuid = friendlyName + "_UUID";
		logger.fine(" Setting UUID to: " + uuid);
		setUUID(uuid);
		updateUDN();

		setNMPRMode(true);
		setWirelessMode(true);

		loadPreferences();

		logger.fine("Leaving AbstractMediaRenderer base class constructor ");
	}

	/**
	 * Load Bridge preferences. Preference information comes from 2 sources -
	 * the default set of preferences that come with the program, and the
	 * (optional) user-specific set stored in the user's home directory. The
	 * default set is located in the Java classpath under the 'properties'
	 * subdirectory. The user-specific set is stored in the user's home
	 * directory, under the '.cidero' subdirectory
	 */
	public static void loadPreferences() {
		// Load shared & user-specific preferences for this application
		pref = new AppPreferences(".cidero");

		if (!pref.load("Bridge", "AbstractMediaRendererBridge")) {
			logger.severe("Missing preferences file - exiting");
			System.exit(-1);
		}
	}

}
