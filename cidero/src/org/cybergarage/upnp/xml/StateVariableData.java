/******************************************************************
*
*	CyberUPnP for Java
*
*	Copyright (C) Satoshi Konno 2002-2003
*
*	File:StateVariableData.java
*
*	Revision;
*
*	02/05/03
*		- first revision.
*	01/06/04
*		- Added setQueryListener() and getQueryListener().
*
******************************************************************/

package org.cybergarage.upnp.xml;

import org.cybergarage.upnp.control.*;

public class StateVariableData extends NodeData
{
	public StateVariableData() 
	{
	}

	private String value = "";

	/**
   * Set state variable value.  If the value has changed, set the dirty
   * bit (for us with some of the eventing logic).
   */
	public void setValue(String value)
  {
    if( value == null )
      value = "";
    
    if ( ! value.equals( this.value ) )
    {
      // System.out.println("$$$$$$$$$$  Setting dirty bit " + value + " != " + this.value);
      dirty = true;
    }
    
		this.value = value;
	}

	/**
   * Get state variable value.
   * bit.
   */
	public String getValue()
  {
		return value;
	}

	private boolean dirty = true;

  /**
   * Return dirty status (value was modified) of state variable
   */
  public boolean isDirty()
  {
    return dirty;
  }

  /**
   * Clear dirty status. 
   */
  public void clearDirty()
  {
    dirty = false;
  }
  public void setDirty()
  {
    dirty = true;
  }
  

	////////////////////////////////////////////////
	// QueryListener
	////////////////////////////////////////////////

	private QueryListener queryListener = null;

	public QueryListener getQueryListener() {
		return queryListener;
	}

	public void setQueryListener(QueryListener queryListener) {
		this.queryListener = queryListener;
	}
	
	////////////////////////////////////////////////
	// QueryResponse
	////////////////////////////////////////////////

	private QueryResponse queryRes = null;

	public QueryResponse getQueryResponse() 
	{
		return queryRes;
	}

	public void setQueryResponse(QueryResponse res) 
	{
		queryRes = res;
	}

}

