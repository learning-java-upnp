/******************************************************************
*
*	CyberUPnP for Java
*
*	Copyright (C) Satoshi Konno 2002-2003
*
*	File: ServiceData.java
*
*	Revision;
*
*	03/28/03
*		- first revision.
*	01/06/04
*		- Moved setQueryListener() and getQueryListener() to StateVariableData class.
*
******************************************************************/

package org.cybergarage.upnp.xml;

import java.util.logging.Logger;

import org.cybergarage.util.*;
import org.cybergarage.xml.*;

import org.cybergarage.upnp.event.*;

public class ServiceData extends NodeData
{
  private static Logger logger = Logger.getLogger("org.cybergarage.xml");

  public final static int SUBSCRIPTION_NOMINAL_TIMEOUT = 180;

	public ServiceData() 
	{
	}

	////////////////////////////////////////////////
	// descriptionURL
	////////////////////////////////////////////////

	private String descriptionURL = "";

	public String getDescriptionURL() {
		return descriptionURL;
	}

	public void setDescriptionURL(String descriptionURL) {
		this.descriptionURL = descriptionURL;
	}

	////////////////////////////////////////////////
	// controlActionListenerList
	////////////////////////////////////////////////

	private ListenerList controlActionListenerList = new ListenerList();

	public ListenerList getControlActionListenerList() {
		return controlActionListenerList;
	}

	////////////////////////////////////////////////
	// scpdNode
	////////////////////////////////////////////////

	private Node scpdNode = null;

	public Node getSCPDNode() {
		return scpdNode;
	}

	public void setSCPDNode(Node node) {
		scpdNode = node;
	}

	////////////////////////////////////////////////
	// SubscriberList
	////////////////////////////////////////////////

	private SubscriberList subscriberList = new SubscriberList();
	
	public SubscriberList getSubscriberList() {
		return subscriberList;
	}

	////////////////////////////////////////////////
	// SID
	////////////////////////////////////////////////

	private String sid = "";
	
	public String getSID() {
		return sid;
	}

	public void setSID(String id) {
		sid = id;
	}

	////////////////////////////////////////////////
	// Timeout
	////////////////////////////////////////////////

	private long timeout = SUBSCRIPTION_NOMINAL_TIMEOUT;  // 180 sec

	public long getTimeout() 
	{
		return timeout;
	}

	public void setTimeout(long value) 
	{
		timeout = value;
	}

	/**
   *	Add/Remove/Perform event listener for service
   * 
   *  OJN mod to have event listeners be installed on
   *  a per-service basis for convenience. (Stock CLink supports a single,
   *  library-wide event listener)
   */

	private ListenerList eventListenerList = new ListenerList();

  public void addEventListener(EventListener listener)
	{
		eventListenerList.add(listener);
    logger.fine("Added service event listener, count = " + 
                eventListenerList.size() + 
                "SID = " + getSID() );
  }

	public void removeEventListener(EventListener listener)
	{
		eventListenerList.remove(listener);
    logger.fine("removed service event listener, count = " + 
                eventListenerList.size() );
	}		

	public void performEventListener(String uuid, long seq,
                                   String name, String value)
	{
		int listenerSize = eventListenerList.size();

    logger.fine("invoking event listener for " + listenerSize + " clients" );
    //logger.fine("serviceType " + getServiceType() +
    //                "SID = " + getSID() );

		for (int n=0; n<listenerSize; n++) {
			EventListener listener = (EventListener)eventListenerList.get(n);
			listener.eventNotifyReceived(uuid, seq, name, value);
		}
	}

	/**
   * Set/Get isDeviceInstance property. On the device side, this is set
   * to true, while on the control point side, it is set to false.  This
   * is useful at runtime so the CLink code can, for example, figure out
   * whether it should try and broadcast events if a state variable is 
   * is updated (only true on the device side)
   */

  private boolean isDeviceInstance = true;

	public void setIsDeviceInstance( boolean value ) 
	{
		isDeviceInstance = value;
	}

	public boolean getIsDeviceInstance()
	{
		return isDeviceInstance;
	}

}

