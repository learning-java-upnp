package org.cybergarage.upnp;

import org.cybergarage.upnp.ssdp.SSDPPacket;

public interface IDevice {

	void unlock();

	boolean isNMPRMode();

	boolean isWirelessMode();

	int getSSDPAnnounceCount();

	String getUUID();

	void updateUDN();

	IDevice getRootDevice();

	IDevice getParentDevice();

	String getHost();

	int getPort();

	boolean isRootDevice();

	SSDPPacket getSSDPPacket();

	String getLocation();

	int getLeaseTime();

	long getTimeStamp();

	long getElapsedTime();

	boolean isExpired();

	String getURLBase();

	String getDeviceType();

	boolean isDeviceType(String value);

	String getFriendlyName();

	String getManufacturer();

	String getManufacturerURL();

	String getModelDescription();

	String getModelName();

	String getModelNumber();

	String getModelURL();

	String getSerialNumber();

	String getUDN();

	boolean hasUDN();

	String getUPC();

	String getPresentationURL();

	DeviceList getDeviceList();

	boolean isDevice(String name);

	IDevice getDevice(String name);

	ServiceList getServiceList();

	Service getService(String name);

	Service getSubscriberService(String uuid);

	StateVariable getStateVariable(String name);

	Action getAction(String name);

	IconList getIconList();

	Icon getIcon(int n);

	String getLocationURL(String host);

	void announce(String bindAddr);

	void announce();

	void byebye(String bindAddr);

	void byebye();

	/**
	 * Respond to a search request. The type of the response depends on the
	 * request's search target field
	 *
	 * @param  ssdpPacket
	 *
	 *         Incoming SSDP search request packet. There are several types
	 *         of search requests. Here's one example:
	 *
	 *         M-SEARCH * HTTP/1.1
	 *         ST: ssdp:all
	 *         MX: 3
	 *         MAN: "ssdp:discover"
	 *         HOST: 239.255.255.250:1900
	 *
	 *         The 'ST' (search target) is set depending on the search type:
	 *
	 *         1.  Search for all Root device search request.
	 *
	 *           ST: ssdp:all
	 *
	 *         2.  Root device search request
	 *
	 *           ST: upnp:rootdevice
	 *     
	 *         3.  Device specific search request (not limited to these examples)
	 *
	 *           ST: urn:schemas-upnp-org:device:MediaServer:1
	 *      
	 *          or
	 *
	 *           ST: urn:schemas-upnp-org:device:MediaServer:1
	 *
	 *
	 */
	void deviceSearchResponse(SSDPPacket ssdpPacket);

	int getHTTPPort();

	boolean start();

	boolean stop();

	String getInterfaceAddress();

	int getInstancePerspective();

}