/******************************************************************
*
*	CyberUPnP for Java
*
*	Copyright (C) Satoshi Konno 2002
*
*	File: RenewSubscriber.java
*
*	Revision:
*
*	07/07/04
*		- first revision.
*	
******************************************************************/

package org.cybergarage.upnp.control;

import java.util.logging.Logger;

import org.cybergarage.util.*;
import org.cybergarage.upnp.*;

public class RenewSubscriber extends ThreadCore
{
  private static Logger logger =
    Logger.getLogger("org.cybergarage.upnp.control");
  
	
	////////////////////////////////////////////////
	//	Constructor
	////////////////////////////////////////////////

	public RenewSubscriber(ControlPoint ctrlp)
	{
		setControlPoint(ctrlp);
	}
	
	////////////////////////////////////////////////
	//	Member
	////////////////////////////////////////////////

	private ControlPoint ctrlPoint;

	public void setControlPoint(ControlPoint ctrlp)
	{
		ctrlPoint = ctrlp;
	}
	
	public ControlPoint getControlPoint()
	{
		return ctrlPoint;
	}

	////////////////////////////////////////////////
	//	Thread
	////////////////////////////////////////////////
	
	public void run() 
	{
		ControlPoint ctrlp = getControlPoint();
		//long renewInterval = INTERVAL * 1000;

		while (isRunnable() == true)
    {
			try {
        int waitMillis = (ctrlp.getSubscriptionPeriodSec()-60)*1000;
        if( waitMillis < 0 )
        {
          logger.warning("negative wait time in subscription renewal thread!");
          waitMillis = 120*1000;
        }
        //System.out.println("******* WAITING  " + waitMillis );
        
				Thread.sleep( waitMillis );
        
			} catch (InterruptedException e) {}

			ctrlp.renewSubscriberService();
		}
	}
}
