/******************************************************************
*
*	CyberUPnP for Java
*
*	Copyright (C) Satoshi Konno 2002
*
*	File: SSDPResponse.java
*
*	Revision;
*
*	01/14/03
*		- first revision.
*	01/23/04
*		- Oliver Newell
*		- Overided HTTPResponse::getHeader() for Intel UPnP control points.
*	03/16/04
*		- Thanks for Darrell Young
*		- Fixed to set v1.1 to the HTTP version.;
*	
******************************************************************/

package org.cybergarage.upnp.ssdp;

import org.cybergarage.http.*;

public class SSDPResponse extends HTTPResponse
{
	////////////////////////////////////////////////
	//	Constructor
	////////////////////////////////////////////////
	
	public SSDPResponse()
	{
		setVersion(HTTP.VERSION_11);
	}

	////////////////////////////////////////////////
	//	ST (SearchTarget)
	////////////////////////////////////////////////

	public void setST(String value)
	{
		setHeader(HTTP.ST, value);
	}

	public String getST()
	{
		return getHeaderValue(HTTP.ST);
	}

	////////////////////////////////////////////////
	//	Location
	////////////////////////////////////////////////

	public void setLocation(String value)
	{
		setHeader(HTTP.LOCATION, value);
	}

	public String getLocation()
	{
		return getHeaderValue(HTTP.LOCATION);
	}

	////////////////////////////////////////////////
	//	USN
	////////////////////////////////////////////////

	public void setUSN(String value)
	{
		setHeader(HTTP.USN, value);
	}

	public String getUSN()
	{
		return getHeaderValue(HTTP.USN);
	}

	////////////////////////////////////////////////
	//	CacheControl
	////////////////////////////////////////////////

	public void setLeaseTime(int len)
	{
		setHeader(HTTP.CACHE_CONTROL, "max-age=" + Integer.toString(len));
	}

	public int getLeaseTime()
	{
		String cacheCtrl = getHeaderValue(HTTP.CACHE_CONTROL);
		return SSDP.getLeaseTime(cacheCtrl);
	}

	////////////////////////////////////////////////
	//	getHeader (Override)
	////////////////////////////////////////////////
	
	public String getHeader()
	{
		StringBuffer str = new StringBuffer();
	
		str.append(getStatusLineString());
		str.append(getHeaderString());
		str.append(HTTP.CRLF); // for Intel UPnP control points.
		
		return str.toString();
	}

}
