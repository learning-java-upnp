/******************************************************************
*
*	CyberUPnP for Java
*
*	Copyright (C) Satoshi Konno 2002
*
*	File: DeviceChangeListener.java
*
*	Revision;
*
*	09/12/04
*		- Added this class to allow ControlPoint applications to 
*     be notified when the ControlPoint base class adds/removes
*     a UPnP device
*	
******************************************************************/

package org.cybergarage.upnp.device;

import org.cybergarage.upnp.Device;
import org.cybergarage.upnp.IDevice;

public interface DeviceChangeListener
{
  public void deviceAdded( Device dev );
  public void deviceRemoved( IDevice dev );
}
