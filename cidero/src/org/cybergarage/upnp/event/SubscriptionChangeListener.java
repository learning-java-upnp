/******************************************************************
*
*	CyberUPnP for Java
*
*	Copyright (C) Satoshi Konno 2002-2003
*
*	File: EventListener.java
*
*	Revision;
*
*	11/18/02
*		- first revision.
*	
******************************************************************/

package org.cybergarage.upnp.event;

import org.cybergarage.upnp.Service;

/**
 * Listener interface to allow control point applications to 'see' the
 * subscription requests/responses that are managed automatically (mostly) by
 * CLink. This is useful if the control point application includes a UPnP
 * debugging view)
 */
public interface SubscriptionChangeListener
{
  public void subscriptionRequestSent( Service service,
                                       SubscriptionRequest request );
  public void subscriptionResponseReceived( Service service,
                                            SubscriptionResponse response );
}
  
