/******************************************************************
*
* CyberSOAP for Java
*
* Copyright (C) Satoshi Konno 2002
*
* File: SOAPRequest.java
*
* Revision;
*
* 12/11/02
*   - first revision.
* 02/13/04
*   - Ralf G. R. Bergs <Ralf@Ber.gs>, Inma Marin Lopez <inma@dif.um.es>.
*   - Added XML header, <?xml version=\"1.0\"?> to setContent().
* 05/11/04
*   - Changed the XML header to <?xml version="1.0" encoding="utf-8"?> in setContent().
* 
******************************************************************/

package org.cybergarage.soap;

import java.io.*;
import java.util.logging.Logger;


import org.cybergarage.http.*;
import org.cybergarage.xml.*;
import org.cybergarage.util.*;

import com.cidero.util.UTF8Util;
import com.cidero.util.XMLUtil;

/**
 * SOAP request class
 */
public class SOAPRequest extends HTTPRequest
{
  private static Logger logger = Logger.getLogger("org.cybergarage.soap");

  private final static String SOAPACTION = "SOAPACTION";
  
  static boolean escapeNonEntityAmpersands = false;

  ////////////////////////////////////////////////
  //  Constructor
  ////////////////////////////////////////////////
  
  public SOAPRequest()
  {
    setContentType(SOAP.CONTENT_TYPE);
    //setConnection("close");  Test...
    setMethod(HTTP.POST);
  }

  public SOAPRequest(HTTPRequest httpReq)
  {
    set(httpReq);
  }

  /**
   *  Set the SOAP action name. Each SOAP HTTP packet has a 
   *  SOAPACTION header of the form:
   *
   *  SOAPACTION: "urn:schemas-upnp-org:service:version#actionName" 
   *
   */
  public void setSOAPAction(String action)
  {
    setStringHeader(SOAPACTION, action);
  }
  
  public String getSOAPAction()
  {
    return getStringHeaderValue(SOAPACTION);
  }

  /**
   * Test to see if this object matches the specified SOAP action
   *
   * @param  value   Action name, e.g.
   *                 "urn:schemas-upnp-org:service:version#actionName" 
   *
   * @return true if match, false if there is no matching SOAPACTION header
   *         or no SOAPAction header at all     
   */
  public boolean isSOAPAction(String value)
  {
    String headerValue = getHeaderValue(SOAPACTION);
    if (headerValue == null)
      return false;
    if (headerValue.equals(value) == true)
      return true;

    String soapAction = getSOAPAction();
    if (soapAction == null)
      return false;
    return soapAction.equals(value);
  }
 
  /**
   *  Post a SOAP message, and read the response. 
   *
   *  @return  SOAPReponse object.
   */
  public SOAPResponse postMessage(String host, int port)
  {
    HTTPResponse httpRes = post(host, port);
    
    SOAPResponse soapRes = new SOAPResponse(httpRes);

    byte[] content = soapRes.getContent();

    if (content.length <= 0)
    {
      logger.warning("SOAP response contentLength = " + content.length +
                     " !" );
      return soapRes;
    }

    Node rootNode = parseXMLContent( content );
      
    soapRes.setEnvelopeNode( rootNode );

    return soapRes;
  }
    
  /**
   *  Some SOAP implementations have a non-standard header, and this
   *  causes the DOMParser to throw an exception. For now, bypass the
   *  existing header ( <?xml version="1.0" encoding="utf-8" ?> )
   *  and substitute a header with the ISO-8859-1 encoding. This seems
   *  to be the best solution for use with the most popular servers
   *  (Twonkyvision/Windows Media Connect)
   *
   *  One failure example motivating this is the SimpleCenter UPnP stack,
   *  which has illegal xml encoding of 'UTF8' - should be 'UTF-8'
   *
   *  Also, check the end of the packet and trim back the byte array
   *  length so the closing '>' of the last XML element is the last byte 
   *  processed. Some UPnP stacks have non-UTF-8 characters (like a NULL)
   *  after the closing '>', and the Apache DOM parser throws an exception.
   */
  public Node parseXMLContent( byte[] content )
  {
    int length = content.length;
    int offset = 0;    

    // trim off any trailing bytes after closing '>'
    for( int n = content.length-1 ; n > offset ; n-- )
    {
      if( content[n] == '>' )
        break;
      length--;
    }

    String xmlString;
    
    try
    {
      xmlString = new String( content, offset, length, "UTF-8" );

      logger.finer("Parsing incoming SOAP Request/Response:\n'" +
                    xmlString + "'\n");

      //
      // Patch for devices that don't properly doubly-escape the 'LastChange'
      // events (nested XML fragment). Philips Streamium SL50i is guilty of
      // this
      //

      // Do a quick check on the front part of the xml to make see if message
      // is an event message (the 'propertyset' term in the 1st 256 chars)
      int testLength = 256;
      if( testLength >= xmlString.length() )
        testLength = xmlString.length();
      String testString = xmlString.substring(0, testLength-1 );

      if( testString.indexOf("propertyset") >= 0 )
      {
        // If last change event, and XML within the LastChange element is not
        // properly escaped, fix it.
        /** @note Also, if Event fragment is not properly terminated, but
         *  has gibberish at the end (prior to the closing <LastChange>
         *  then remove the gibberish
         *  @todo This is a temporary patch for the NOXON Media Renderer
         */
        int lastChangeStart = xmlString.indexOf("<LastChange>");
        int lastChangeEnd = xmlString.indexOf("</LastChange>");
        if( (lastChangeStart > 0) && (lastChangeEnd > 0) )
        {
          String eventStr = xmlString.substring(lastChangeStart+12,
                                             lastChangeEnd).trim();

          if( eventStr.startsWith("<Event") )
          {
            logger.finer( "Fixing non-escaped LastChange Event" );
            
            // Non-escaped XML in last change value - escape it before 
            // proceeding
            String escapedEventStr = XML.escapeXMLChars( eventStr );

            String tmp = xmlString.substring(0,lastChangeStart+12) +
            escapedEventStr + xmlString.substring( lastChangeEnd );
            xmlString = tmp;

            //System.out.println("New xmlString: " + xmlString );
          }
          else
          {
            // Check for NOXON bug. Bug manifests itself as a string
            // of giberish (or older DIDL-Lite from previous message)
            // after the closing </Event> tag and before the closing
            // </LastChange> flag, i.e.   
            // ...&lt;/Event&gt;        </LastChange>
            //
            // Need to strip out the gibberish since it is sometimes
            // badly-formed XML
            int eventEnd = xmlString.indexOf("/Event&gt;");
            if( (eventEnd > 0) && ((lastChangeEnd - eventEnd) > 10) ) 
            {
              logger.finer("Fixing possibly non-terminated Event XML fragment (NOXON)");
              
              String tmp = xmlString.substring(0,eventEnd) + "/Event&gt;" +
                           xmlString.substring( lastChangeEnd );
              xmlString = tmp;
            }
          }
        }

      }

      /*
      if( UTF8Util.checkForMultiByteChars( xmlString ) )
        logger.info("Parsing multi-byte char SOAP XML: \n[" + xmlString + "]\n" );
      */
    }
    catch (Exception e)
    {
      logger.warning("Unsupported encoding" + e );
      return null;
    }
    
    // Some SOAP messages have non-escaped '&' characters, which fault
    // out the Apache parser. If detected in past SOAP packets, fix'em
    // up prior to first XML parse
    if( escapeNonEntityAmpersands )
      xmlString = XMLUtil.escapeNonEntityAmpersands( xmlString );


    /* Experimentation
    // Check for XML header string
    String xmlHeader = new String( content, 0, 80, "UTF-8" );
    int index = xmlHeader.indexOf("?>");
    if( index >= 24 )
    {
      // Yup it's present - skip over it  (OLD LOGIC)
      //offset = index+2;  
      //length -= offset;

      // Yup it's present - use it
      xmlString = new String( content, offset, length, "UTF-8" );
    }
    else
    {
      // Not present - use ISO encoding header
      xmlString = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" +
                   new String( content, offset, length );
    }
    */

    byte[] xmlByteArray;
    ByteArrayInputStream xmlStream;
    Parser xmlParser;
    Node rootNode;

    try
    {
      xmlByteArray = xmlString.getBytes("UTF-8");
      xmlStream = new ByteArrayInputStream(xmlByteArray);
      xmlParser = SOAP.getXMLParser();
      rootNode = xmlParser.parse( xmlStream );
      return rootNode;
    }
    catch (Exception e)
    {
      logger.warning("XML parser exception parsing SOAP content with len " + 
                     content.length + e );
      logger.warning("Content: [" + new String(content) + "]" );

      // Some SOAP messages have non-escaped '&' characters, which fault
      // out the Apache parser - try to patch it unless already done 
      // based on flag
      if( escapeNonEntityAmpersands )
        return null;

      xmlString = XMLUtil.escapeNonEntityAmpersands( xmlString );

      try
      {
        xmlByteArray = xmlString.getBytes("UTF-8");
        xmlStream = new ByteArrayInputStream(xmlByteArray);
        xmlParser = SOAP.getXMLParser();
        rootNode = xmlParser.parse( xmlStream );

        // the above patch worked, so set flag so that it's always done
        // up front from now on.
        escapeNonEntityAmpersands = true;
      }
      catch (Exception e2)
      {
        logger.warning("attempted non-entity '&' patch failed " + e2 );
        return null;
      }

      return rootNode;
    }

  }

  ////////////////////////////////////////////////
  //  Node
  ////////////////////////////////////////////////

  private Node rootNode;
  
  private void setRootNode(Node node)
  {
    rootNode = node;
  }
  
  /**
   *  Get root node of the SOAP request. If the content has not yet
   *  been parsed, do so.
   */
  private synchronized Node getRootNode()
  {
    if (rootNode != null)
      return rootNode;
      
    return parseXMLContent( getContent() );
    
  }
  
  ////////////////////////////////////////////////
  //  XML
  ////////////////////////////////////////////////

  public void setEnvelopeNode(Node node)
  {
    setRootNode(node);
  }
  
  public Node getEnvelopeNode()
  {
    return getRootNode();
  }
    
  public Node getBodyNode()
  {
    Node envNode = getEnvelopeNode();
    if (envNode == null)
      return null;
    if (envNode.hasNodes() == false)
      return null;
    return envNode.getNode(0);
  }

  ////////////////////////////////////////////////
  //  XML Contents
  ////////////////////////////////////////////////
  
  public void setContent(Node node)
  {
    String conStr = "";
    conStr += SOAP.VERSION_HEADER;
    conStr += "\n";

    // Create the XML text from the XML node heirarchy, escaping the XML
    // as necessary.
    conStr += node.toString(); 

    setContent(conStr);
  }

  ////////////////////////////////////////////////
  //  print
  ////////////////////////////////////////////////
  
  public void print()
  {
    System.out.println(toString());
    if (hasContent() == true)
      return;
    Node rootElem = getRootNode();
    if (rootElem == null)
      return;
    System.out.println(rootElem.toString());
  }
}
