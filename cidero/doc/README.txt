
1. Introduction

This file contains basic information on installing and running the Cidero
UPnP A/V applications.


2. Installation

a)  System Requirements 

    Release 1.4 or greater of the Java runtime environment is required to 
    support the Cidero software applications.  Release 1.5 is the preferred
    version in terms of performance, features, and timely patches!


b)  Download the desired software packages from www.cidero.com.  Currently,
    a single package is available - the bundled MediaController and
    RadioServer. The software bundle is contained in a single zip 
    file containing all the Cidero-specific software, as well as the 
    Cybergarage UPnP (slightly modified) and Apache XML parser libraries.
    (In previous versions these were in a separate zip file)

c)  Extract the zip file into a directory of your choice.
 
    NOTE TO WINDOWS USERS:  There is a problem with installing the software
    in a directory path containing spaces (i.e. "C:\Program Files").
    This will be fixed, but for now the install path 
    can't contain any directory names with spaces in them. Sorry 'bout that!

d)  From the main installation directory, execute the batch file (Windows)
    or shell script (Unix/MaxOSX) for one of the applications.

    Note: Under Windows, if clicking on the batch file opens up a window,
    then it quickly exits with an error before you can see any error 
    messages, the best approach to figure out what is happening is to 
    open up a command shell and execute the batch file from the command-line 
    (the window will remain visible after the error) 


3. Un-Installation

Removing all traces of the Cidero software from your system is a 
simple two-step process:

a)  Remove the installation directory

b)  Remove the user preferences directory. The Cidero software stores 
    user preferences in the $USER_HOME/.cidero directory (following 
    UNIX conventions).  Under windows, this normally ends up in 
    C:\Documents and Settings\<userName>\.cidero.  Note that the 
    '.cidero' folder may not be visible until you enable display of
    hidden files for the C:\Documents and Settings\<userName>



