
1. Introduction

This file contains basic information on installing and running the Cidero
UPnP A/V applications.


NOTE:  Multiple versions of the Cidero software can peacefully co-exist
       on a system - just be sure to unzip each version in its own directory!


2. Installation

a)  System Requirements 

    Release 1.4 or greater of the Java runtime environment is required to 
    support the Cidero software applications.  Release 1.5 is the preferred
    version in terms of performance, features, and timely patches!


b)  Download the desired software packages from www.cidero.com.  Currently,
    a single package is available - the bundled MediaController and
    RadioStation server. The software is partitioned into two zip files:

     - cidero-0.9-jdk1.5.zip  (or alternatively, cidero-0.9-jdk1.4.zip)

       Zip file containing all the Cidero-specific software. For the 0.8
       release, this zip archive includes a slightly modified version of 
       the Cybergarage UPnP library (need to resync with the author's 
       latest version when things stabilize on both ends)

     - cidero-externalcontrib-1.0.zip

       This zip file bundles the necessary .jar files for the publicly 
       available support packages used by the Cidero software, as a 
       convenience. Included are the .jar files needed for the Apache 
       Xerces XML parser and the junit testing framework. The files are
       listed below:

			    xercesImpl.jar
			    xml-apis.jar
			    junit.jar

       If you already have recent versions of these jars on your system,
       you may not need to download this zip file (but you will need to 
       either copy them to the <installDir>/lib directory or modify the 
       CLASSPATH in the startup scripts so they are found at runtime):


c)  Extract the zip files into a a directory of your choice. Both zip 
    files should be unzipped to the same parent directory. When completed,
    the <installDir>/lib directory should contain files from both zip
    archives, and look like:

      ApacheLicense-2.0.txt
      xercesImpl.jar
      xml-apis.jar
      junit.jar
      cidero-common.jar
 
    NOTE TO WINDOWS USERS:  There is a problem with installing the software
    in a directory path containing spaces (i.e. "C:\Program Files").
    This will be fixed in the next release, but for now the install path 
    can't contain any directory names with spaces in them. Sorry 'bout that!

d)  In the main installation directory, execute the batch file (Windows)
    or shell script (Unix) for one of the applications.  Note that the
    UNIX shell script needs to be hand-edited for the moment to set 
    the Java CLASSPATH properly. The Windows batch file now determines
    it automatically (in theory!).

    Note: Under Windows, if clicking on the batch file opens up a window,
    then it quickly exits with an error before you can see any error 
    messages, the best approach to figure out what is happening is to 
    open up a command shell and execute the batch file from the command-line 
    (the window will remain visible after the error) 


3. Un-Installation

Removing all traces of the Cidero software from your system is a 
simple two-step process:

a)  Remove the installation directory

b)  Remove the user preferences directory. The Cidero software stores 
    user preferences in the $USER_HOME/.cidero directory (following 
    UNIX conventions).  Under windows, this normally ends up in 
    C:\Documents and Settings\<userName>\.cidero.  Note that the 
    '.cidero' folder may not be visible until you enable display of
    hidden files for the C:\Documents and Settings\<userName>



