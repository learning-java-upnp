
1. Introduction

This file contains basic information on installing and running the Cidero
UPnP A/V applications.


2. Installation

a)  System Requirements 

    Release 1.5 or greater of the Java runtime environment is required to 
    support the Cidero software applications as of release 1.2.3 of the Cidero
    software (Java 1.4 is no longer supported). See the information on the 
    downloads page for Java 1.5 installation information for Windows, Linux,
    and Mac OSX.

b)  Download the desired software packages from www.cidero.com.  Currently,
    a single package is available - the bundled media controller,
    radio server, and UPnP bridge. The software bundle is contained in a
    single zip file containing all the Cidero-specific software, as well as
    the Cybergarage UPnP (slightly modified) and Apache XML parser libraries.

c)  Extract the zip file into a directory of your choice. In version 1.3,
    directory names with spaces are now supported (was broken in older 
    releases). However, if you want to use the Firefox 'Launchy' extension
    to select and playback online content, you should avoid using any
    spaces in the installation directory path (this is an issue with the
    extension as far as I can tell)
 

d)  From the main installation directory, execute the batch file (Windows)
    or shell script (Unix/MaxOSX) for one of the applications.

    Windows:
      Simply clicking on the '.bat' file will bring up a command window and
      run the application inside the window. 

      Note: If clicking on the batch file opens up a window, then it quickly 
      exits with an error before you can see any error messages, the best
      approach to figure out what is happening is to open up a command shell
      and execute the batch file from the command-line (the window will 
      remain visible after the error) 

    Linux, Mac OSX, Solaris

      Run the script of choice ('MediaController.sh', 'RadioServer.sh',
			'RadioServerProxy.sh', 'Bridge.sh', 'SyncProxy.sh') in a terminal
      window.  If your path is set to include the current directory (most
      common), simply typing the full name of the script should work. If
      you get a 'not found' error, then it is likely the current directory
      is not in your path - type './MediaController.sh' in that case.


3. Un-Installation

Removing all traces of the Cidero software from your system is a 
simple two-step process:

a)  Remove the installation directory

b)  Remove the user preferences directory. The Cidero software stores 
    user preferences in the $USER_HOME/.cidero directory (following 
    UNIX conventions).  Under windows, this normally ends up in 
    C:\Documents and Settings\<userName>\.cidero.  Note that the 
    '.cidero' folder may not be visible until you enable display of
    hidden files for the C:\Documents and Settings\<userName>



