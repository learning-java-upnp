

                 CIDERO SOFTWARE BUNDLE RELEASE NOTES


------------------------------------------------------------------------

Version 1.1.1  5/5/2005 - Nero MediaHome media server patches (experimental)

Nero MediaHome conforms to the DLNA guidelines, which insert extra
information in some of the standard UPnP fields. For example, when serving
up an mpeg audio track, the UPnP 'protocolInfo' reported by the server is:

  'http-get:*:audio/mpeg:DLNA.ORG_PN=MP3;DLNA.ORG_OP=01;DLNA.ORG_CI=0'

instead of simply:

  'http-get:*:audio/mpeg:*'

like most other servers we have worked with so far. This was breaking the 
server/renderer protocol matching algorithm in the controller due to fragile 
match code. Patched for now by excluding the last portion of the field from
the match.

An icon for Nero was also added.

Please report any further Nero-related bugs you encounter!



------------------------------------------------------------------------

Version 1.1   4/30/2005 - Philips Media Manager patches, Shoutcast 
                          metadata support for Roku SoundBridge, and
                          radio server test scan capability.


Media Controller

1.  Fixed two problems that occurred when using the Philips Media Manager.
    The first was an error reading the device's icon, caused by the fact
    that the PMM UPnP implementation makes use of embedded UPnP devices, 
    and this wasn't being supported properly by the controller. The second
    bug was a parsing bug when reading the PMM's timeout setting, causing
    an erroneous, early, timeout after 60 seconds. 

    Note that the icon supplied via UPnP by PMM is really the 
    'SimpleCenter' icon from simpledevices.com, the folks who supply
    some (most?) of the PMM code to Philips.  Not very Streamium-like,
    but recognizable at least!

Radio Server

1.  Added support for Artist/SongTitle info on the Roku SoundBridge when
    using the proxy mode of the RadioServer.  The Roku 'sketch' interface
    is used to display Artist/Title info on the display when this option
    is enabled via the properties file.

    Since the sketch interface disables the Roku remote when active,
    the Artist/Title info is displayed starting 30 seconds (configurable)
    into each song, allowing a user to retake control at the song transitions.
    In addition, the sketch mode is exited every 2 min (configurable) for
    30 sec so the user can be sure to have a time period where the remote
    will work.

    If switching stations, be sure to issue a 'pause' command with the 
    remote shortly after the normal Roku display appears. Then all
    subsequent browsing can be done in a liesurely way, and the broken
    radio station connections should all clean themselves up properly.

    The server is set up to (attempt to) exit sketch mode when it 
    terminates, so there aren't too many situations where the Roku gets
    stuck with a non-responsive remote. I'm sure they'll still exist though. 
    In such a case, best bet is to telnet to the Roku and issue a 'reboot' :)

    One drawback to the server is that one has to browse down about 4 levels
    to play the desired radio station. This may be fixable, but for now
    this condition exists since the server is going and retrieving 
    radio station playlists from the station web addresses when a browse
    is invoked, and converting them to an equivalent UPnP-based list
    on-the-fly. Also, the tree allows for different bit rates to be 
    selectable for a single station. The upside to the current 
    arrangement is that if a connection breaks, the SB will move to 
    the next entry in the playlist (stay on the same station/bitrate 
    in most playlist configurations I've seen)
    
    Note that the next Roku software release will likely address this
    issue in a more elegant way! If so, this functionality will likely be 
    removed. 


2.  Added radio station database test scan capability. This allows a 
    user to run a scan of all the stations in the database, and store
    access success/fail information in a file. This makes it easier to
    modify/update a custom database to ensure that all the links are 
    valid.



------------------------------------------------------------------------

Version 1.0  4/2/2005  - Picture, video, and MaxOSX support

Media Controller

1.  Added support for pictures.  Picture slideshows may be composed
    (selected, sorted) and played back using picture thumbnails. The
    thumbnail view is automatically enabled when pictures are selected
    using the browse mechanism.

2.  Added support for videos. The interface for selecting and playing
    videos is similar to the interface for audio content (select/play
    using Artist/Title info).

3.  Added support for MaxOSX (Tested with OSX-Panther)

4.  Added option to automatically select music tracks when they are
    first browsed (convenience). (Option is under 'Browse Options...')  

5.  Added property to allow UTF-8 characters (e.g. German 'umlaut') to
    be suppressed (never send them to a renderer that doesn't support them).
    This addresses some lockup problems observed on a number of renderers.
    (see the DLink DSM-320 entry in the property file 
    'MediaController.properties' for more info)

Note: The above features were primarily tested with the Twonkyvision (2.7)
and Windows Media Connect UPnP servers, and the DLink DSM-320 and Phillips
Streamium 300i UPnP Renderers. Other combinations will hopefully work as well!


Radio Server

No modifications were made to the radio server.



------------------------------------------------------------------------

Version 0.9  1/20/2005  - Miscellaneous bug fixes and minor new features

Media Controller

1.  Fixed 'pause' bug in renderer controller.

    Some 'stuck renderer' recovery logic in the controller was falsely 
    triggering if a renderer was paused for more than 8 seconds. DOH!

2.  Increased default renderer playback startup timeout from 3 seconds 
    to 6 seconds. This was in response to feedback that the timeout may
    be too short for some network configurations. The setting is also 
    now user-configurable, and can be accessed in the 'Options->Renderer 
    Options' dialog. (Don't forget to use 'File->Save Setup' after any
    change if you want to make an option change permanent)

3.  Added renderer monitoring interval setting to Renderer Options dialog.
    Reduce this from the default of 3 seconds to increase the update rate
    of the information in the renderer control window. (3 second default
    chosen to minimize controller overhead)

4.  Added a device information dialog, accessible via the Options menu.
    Device control and/or status web pages can be opened from this dialog.

5.  Put up error dialog if no renderer is selected when 'Add To Play Queue'
    button is hit.  

6.  Changed 'Pictures' and 'Movies' tabs so 'Not currently supported' is
    displayed. (reduce user confusion)

7.  REPEAT_ALL mode now handled completely on controller side - UPnP play 
    mode command is *never* sent to the renderer device. This is temporary
    patch until controller supports 'detachable' (renderer device controls
    queue once it gets started) and 'active' (controller completely in
    charge of the queue) controller modes.  Anyway, if you noticed 
    REPEAT_ALL mode wasn't quite working right, it should work better now...

8.  Table of audio items is now the free space hog in the 'Music' tabbed
    pane, so when the window is expanded in the vertical, the table gets
    all the new free space. (button panel used to get 1/2 of it) 


Radio Server

1.  Increased size of default radio station database from 10 to 60+. 
    (Target is a default set of ~100 popular internet stations)

2.  Added UPnP 'region' element to all station XML files. This field 
    contains the country & region of the primary location of the station.

3.  Established conventions for multiple genres in the genres field of
    the XML files. See the database description section on the Web site
    for details. 

4.  Added support for the optional UPnP 'presentationURL' to the device.
    The radio server now has a status page that can be accessed via a 
    web browser. The current station list, with links to the stations' 
    web sites, is generated dynamically on-demand.


------------------------------------------------------------------------

Version 0.8  1/3/2005  - Initial Release


