#!/bin/sh
#
# Script to start up Media Controller (UNIX)
#

SCRIPT_PATH=$0
echo "ScriptPath = $SCRIPT_PATH"

# OS should be 'Linux', 'Darwin' (MacOSX), 'Solaris'
OS=`uname -s`

#
# Change this path to point to java 1.5 executable for system if 'java' not
# in path
#
if [ "$OS" = "Darwin" ]; then

  # Use hardcoded path to Java 1.5 by default so Mac users don't have to
  # switch default Java version to 1.5 (which installation of 1.5 does not do
  # automatically)
  JAVA=/System/Library/Frameworks/JavaVM.framework/Versions/1.5/Commands/java

  if [ ! -f $JAVA ]; then
    echo "Java 1.5 does not appear to be installed in the expected location"
    echo "'$JAVA'"
    echo "Using default Java version on system"
    JAVA=java
  fi

else
  JAVA=java
fi

# Change this to match full path of unzipped build if you want to execute
# script from outside this directory
INSTALL_DIR=`pwd`
echo "INSTALL_DIR = $INSTALL_DIR"

LIBDIR=$INSTALL_DIR/lib
export CLASSPATH=$LIBDIR/cidero-common.jar:$LIBDIR/xercesImpl.jar:$LIBDIR/xml-apis.jar:$INSTALL_DIR

#
# If Mac OSX (Darwin), set up application to be more Mac-like in appearance
# (use fixed menu bar at top of Mac display)
#
if [ "$OS" = "Darwin" ]; then

  $JAVA -Dapple.laf.useScreenMenuBar=true -Xdock:name="MediaController" com.cidero.control.MediaController

else

  $JAVA com.cidero.control.MediaController

fi
