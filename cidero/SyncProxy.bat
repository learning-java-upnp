::
:: Batch file to start up Radio Server in proxy mode (Windows)
::
:: This code adapted from sample at www.ericphelps.com
:: It automatically determines the install directory
:: under Windows XP,NT, & 9X, in order to set the 
:: Java CLASSPATH for the application. 
:: It assumes the user runs the batch file by clicking
:: on its icon in a windows file explorer window
::
@echo off
::First test to see if we are on NT or similar OS by seeing 
::if the ampersand is interpreted as a command separator
> script echo 1234&rem
type script | find "rem"
if not errorlevel 1 goto WIN9X

:NT
echo Running under NT
del script
::Get the current batch file's short path
for %%x in (%0) do set INSTALL_DIR=%%~dpsx
for %%x in (%INSTALL_DIR%) do set INSTALL_DIR=%%~dpsx
echo INSTALL_DIR = %INSTALL_DIR%
goto TEST

:WIN9X
echo Running under Win9X
::An assumption is made that the batch file is run by double-clicking.
::This means %0 is a short file name and path with no quotes
::Test for quotes by quoting %0
if not exist "%0" goto ERROR
:: Make a line fragment per http://www.ericphelps.com/batch/lines/frag-dbg.htm
echo e 100 "set INSTALL_DIR="> script
echo rcx>> script
echo e>> script
echo n ~temp.bat>> script
echo w>> script
echo q>>script
debug < script > nul
del script
::Change to the batch file's drive
%0\
::Change to the batch file's directory
cd %0\..
::Use the TRUENAME command to get the short path
truename | find ":" >> ~temp.bat
call ~temp.bat
del ~temp.bat
set INSTALL_DIR=%INSTALL_DIR%\
echo INSTALL_DIR = %INSTALL_DIR%
goto TEST

:TEST
::For example, to test the INSTALL_DIR to see if "My.jar" is there.
:: - if not exist %INSTALL_DIR%My.jar goto ERROR
::

::Use the path of the batch file to build the Java CLASSPATH
set JAVA=java
set LIBDIR=%INSTALL_DIR%\lib
set CLASSPATH=%LIBDIR%\cidero-common.jar;%LIBDIR%\xercesImpl.jar;%LIBDIR%\xml-apis.jar;%INSTALL_DIR%

:: On most systems, once Java is installed, it will be in the user's path.
:: If not set the following variable to the full path of the Java 
:: executable (preferably version 1.5 / J2SE 5.0)
set JAVA=java

:: Run the server

%JAVA% -classpath %CLASSPATH% com.cidero.proxy.SyncProxy

:: Debug version - edit logging properties file to suit
:: %JAVA% -Djava.util.logging.config.file=%INSTALL_DIR%\properties\logging.properties -classpath %CLASSPATH% com.cidero.proxy.SyncProxy

goto DONE

:ERROR
::Insert error-handling code here
goto DONE

:DONE



